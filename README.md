1. Configure database open file .env and update the DATABASE_URL ( on dev it will be DATABASE_URL=mysql://root:@127.0.0.1:3306/elearning)
2. run the server php bin/console server:run 
3. Create the database php bin/console doctrine:database:create
4. Create a new controller php bin/console make:controller
5. install library to send mail composer require symfony/swiftmailer-bundle