<?php

namespace App\Form;

use App\Entity\NewUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
                ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
            ->add('mail', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
            ->add('formation',ChoiceType::class,[
                'choices' => [
                    'Formation 1' => 'formation 1',
                    'Formation 2' => 'formation 2',
                    'Formation 3' => 'formation 3',
                    'Formation 4' => 'formation 4',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Formation 1' == $att) {
                        return 'Formation 1';
                    } elseif ('Formation 2' == $att) {
                        return 'Formation 2';
                    } elseif ('Formation 3' == $att) {
                        return 'Formation 3';
                    } elseif ('Formation 4' == $att) {
                        return 'Formation 4';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('class',ChoiceType::class,[
                'choices' => [
                    'Classe 1' => 'classe 1',
                    'Classe 2' => 'classe 2',
                    'Classe 3' => 'classe 3',
                    'Classe 4' => 'classe 4',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Classe 1' == $att) {
                        return 'Classe 1';
                    } elseif ('Classe 2' == $att) {
                        return 'Classe 2';
                    } elseif ('Classe 3' == $att) {
                        return 'Classe 3';
                    } elseif ('Classe 4' == $att) {
                        return 'Classe 4';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('statut',ChoiceType::class,[
                'choices' => [
                    'Étudiant' => 'étudiant',
                    'Éditeur' => 'éditeur',
                    'Formateur' => 'formateur',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Étudiant' == $att) {
                        return 'Étudiant';
                    } elseif ('Éditeur' == $att) {
                        return 'Éditeur';
                    } elseif ('Formateur' == $att) {
                        return 'Formateur';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NewUser::class,
        ]);
    }
}
