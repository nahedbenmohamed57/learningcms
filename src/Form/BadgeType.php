<?php

namespace App\Form;

use App\Entity\Badge;
use Doctrine\DBAL\Types\JsonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class BadgeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
                ])
            ->add('picture', TextType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false,
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'rows' => '1'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    '0' => '0',
                    '1' => '1',
                ], 'choice_label' => function ($att, $key) {
                    if ('0' == $att) {
                        return 'Attribuer automatiquement';
                    } elseif ('1' == $att) {
                        return 'Attribuer manuellement';
                    }
                    return($key);
                },
                'required' => true,
                'expanded' => true,
            ])
            ->add('available',CheckboxType::class,[
                'attr' => [
                    'class' => 'custom-control-input',
                ],
                'required' => false,
            ])
            ->add('period', IntegerType::class, [
                'attr' => [
                    'class' => 'per-bad',
                ],
                'required' => false,
            ])
            ->add('duty', ChoiceType::class,[
                'attr' => [
                    'class' => 'select-filter small-select',
                ],
                'required' => false,
                'choices' => [
                    'CHOISIR' => '',
                    'ACQUIS' => 'acquis',
                    'EN COURS DACQUISITION' => 'en cours',
                    'NON ACQUIS' => 'non acquis',
                ]
            ])
            ->add('val_duty', TextType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev-badg',
                ],
                'required' => false,
            ])
            ->add('val_valid', TextType::class, [
                'required' => false,
            ])
            ->add('val_score', TextType::class, [
                'required' => false,
            ])
            ->add('quiz', ChoiceType::class,[
                'attr' => [
                    'class' => 'select-filter small-select text-uppercase',
                ],
                'required' => false,
                'choices' => [
                    'CHOISIR LE QUIZ' => '',
                    'ACQUIS' => 'acquis',
                    'EN COURS DACQUISITION' => 'en cours',
                    'NON ACQUIS' => 'non acquis',
                ]
            ])
            ->add('val_quiz', IntegerType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev',
                ],
                'required' => false,
            ])
            ->add('score', IntegerType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev',
                ],
                'required' => false,
            ])
            ->add('time', IntegerType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev',
                ],
                'required' => false,
            ])
            ->add('val_time', TextType::class,[
                'required' => false,
            ])
            ->add('forum', IntegerType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev',
                ],
                'required' => false,
            ])
            ->add('note',TextType::class,[
                'required' => false,
            ])
            ->add('available',CheckboxType::class,[
                'attr' => [
                    'class' => 'custom-control-input',
                ],
                'required' => false,
            ])
            /*->add('rewards',CheckboxType::class,[
                'attr' => [
                    'class' => 'custom-control-input',
                ],
                    'required' => false,
            ])*/
            /*->add('certifs',ChoiceType::class,[
                'choices' => [
                    'Certificat 1' => 'certificat 1',
                    'Certificat 2' => 'certificat 2',
                    'Certificat 3' => 'certificat 3',
                    'Certificat 4' => 'certificat 4',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Certificat 1' == $att) {
                        return 'Certificat 1';
                    } elseif ('Certificat 2' == $att) {
                        return 'Certificat 2';
                    } elseif ('Certificat 3' == $att) {
                        return 'Certificat 3';
                    } elseif ('Certificat 4' == $att) {
                        return 'Certificat 4';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
                'required' => false,
            ])*/
            ->add('note_val', ChoiceType::class,[
                'attr' => [
                    'class' => 'select-filter small-select text-uppercase',
                ],
                'required' => false,
                'choices' => [
                    'DE MINIMUM' => '',
                    'ACQUIS' => 'acquis',
                    'EN COURS DACQUISITION' => 'en cours',
                    'NON ACQUIS' => 'non acquis',
                ]
            ])
        ;
            $builder->get('note')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    // transform the array to a string
                    return implode(' , ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return explode(' , ', $tagsAsString);
                }
            ))
            ;
        $builder->get('val_valid')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    // transform the array to a string
                    return implode(' , ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return explode(' , ', $tagsAsString);
                }
            ))
        ;
        $builder->get('val_score')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    // transform the array to a string
                    return implode(' , ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return explode(' , ', $tagsAsString);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Badge::class,
        ]);
    }
}
