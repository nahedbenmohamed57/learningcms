<?php

namespace App\Form;

use App\Entity\Formation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('titre_formation',TextType::class)
            ->add('descprition_formation',TextareaType::class,[

                'required' => false,
            ])
            ->add('sens_formation',TextType::class,[

                'required' => false,
            ])
            ->add('image_formation',TextType::class,[

                'required' => false,
            ])
            ->add('video_formation',TextType::class,[

                'required' => false,
            ])
            ->add('confignote_formation',ChoiceType::class,[
        'choices' => [
            'Manuel' => 'Manuel',
            'Automatique' => 'Automatique',
            'Les deux' => 'Les deux',

        ]
    ])

            ->add('nodetype_formation',TextType::class, [

                'attr' => [
                    'class' => "custom-control-input",
                ],

                'required' => false,
            ])
        ->add('notemax_formation',TextType::class, [

        'attr' => [
            'class' => "custom-control-input",
        ],

        'required' => false,
    ])
        ->add('category_choice',ChoiceType::class,[
            'required' => false,
            'multiple' => true,
    ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Formation::class,
        ]);
    }
}
