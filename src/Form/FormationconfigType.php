<?php

namespace App\Form;

use App\Entity\Formationconfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormationconfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language',ChoiceType::class,[
                'choices' => [
                    'DANSK' =>'DANSK',
                    'DEUTSCH' =>'DEUTSCH',
                    'ENGLISH'=>'ENGLISH',
                    'ESPAÑOL' =>'ESPAÑOL',
                    'FRANÇAIS' =>'FRANÇAIS',
                    'ITALIANO' =>'ITALIANO',
                    'NEDERLANDS' =>'NEDERLANDS',
                    'POLSKI'=>'POLSKI',

                ]
            ])
            ->add('preq_formation',TextType::class, [
                'required' => false,
            ])
            ->add('point_prequis',IntegerType::class, [
            'required' => false,
            ])
            ->add('point_note',TextType::class, [
            'required' => false,
            ])
            ->add('list_note',TextType::class,[
                'required' => false,
            ])
            ->add('hour_time',TextType::class, [
                'required' => false,
            ])
            ->add('list_temp',TextType::class,[
                'required' => false,
            ])
            ->add('enable_comment',CheckboxType::class, [
                'required' => false,
            ])
            ->add('enable_point',CheckboxType::class, [
                'required' => false,
            ])
            ->add('enable_hours',CheckboxType::class, [
                'required' => false,
            ])
            ->add('enable_hourbyday',TextType::class, [
                'required' => false,
            ])
            ->add('disable_quiz',CheckboxType::class, [
                'required' => false,
            ])
            ->add('enable_inscrit',CheckboxType::class, [
                'required' => false,
            ])
            ->add('insert_forform',TextType::class, [
                'required' => false,
            ])
            ->add('form_member',CheckboxType::class, [
                'required' => false,
            ])
            ->add('chaine_formation',CheckboxType::class, [
                'required' => false,
            ])
            ->add('enable_description',CheckboxType::class, [
                'required' => false,
            ])
            ->add('enable_autodesccomplete',CheckboxType::class, [
                'required' => false,
            ])
            ->add('enanble_invit',CheckboxType::class, [
                'required' => false,
            ])
            ->add('progress_enableinvit',TextType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Formationconfig::class,
        ]);
    }
}
