<?php

namespace App\Form;

use App\Entity\Video;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'rows' => '4'
                ],
            ])
            ->add('url', TextType::class, [
                'required' => false,
            ])
            ->add('date', DateType::class, [
                'attr' => [
                    'class' => 'form-control text-center pl-5',
                    'required' => false,
                ]
            ])
            ->add('hour', TimeType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'required' => false,
                ]
            ])
            ->add('duration', IntegerType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'min' => '0',
                    'required' => false,
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Video::class,
        ]);
    }
}
