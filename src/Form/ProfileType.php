<?php


namespace App\Form;

use App\Entity\Classe;
use App\Entity\Formation;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('username')
            ->add('lastname')
            ->add('pseudo')
            ->add('phone')
            ->add('address')
            ->add('birthday',BirthdayType::class,
                ['format' => 'ddMMyyyy',
                    'placeholder' => [
                        'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                    ],
                    'required' => false,
                ])
            ->add('zipCode')
            ->add('country')
            ->add('city')
            ->add('region')
            ->add('additionalInformation')
            ->add('password',PasswordType::class,['required' => false,'empty_data' => ''])
            ->add('pinterest', TextType::class,['required' => false])
            ->add('facebook', TextType::class,['required' => false])
            ->add('twitter', TextType::class,['required' => false])
            ->add('instagrame', TextType::class,['required' => false])
            ->add('imageFile', VichImageType::class,[
                'required' => false,
                'allow_delete' => false,
                'download_label' => '...',
                'download_uri' => false,
                'image_uri' => true
            ])
            ->add('newPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Vous n\'avez pas tapé le même mot de passe',
                'options' => ['attr' => ['class' => 'form-control']],
                'required' => false,
                'first_options'  => ['label' => 'Mot de Passe'],
                'second_options' => ['label' => 'Confirmer Mot de passe']])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}