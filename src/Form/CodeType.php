<?php

namespace App\Form;

use App\Entity\Classe;
use App\Entity\Code;
use App\Entity\Formation;
use App\Form\DataTransformer\ArrayToStringTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

class CodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'rows' => '1'
                ],
            ])
            ->add('quantity',IntegerType::class,[
                'attr' => [
                'class' => 'form-control'
                    ],
                ])
            ->add('formations', EntityType::class, [
                'class' => Formation::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'titre_formation',
                'required' => false,
            ])
            ->add('class', EntityType::class, [
                'class' => Classe::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
                'required' => false,
            ])
            ->add('statut',ChoiceType::class,[
                'choices' => [
                    'Étudiant' => 'Étudiant',
                    'Éditeur' => 'Éditeur',
                    'Formateur' => 'Formateur',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Étudiant' == $att) {
                        return 'Étudiant';
                    } elseif ('Éditeur' == $att) {
                        return 'Éditeur';
                    } elseif ('Formateur' == $att) {
                        return 'Formateur';
                    }
                    return($key);
                },
                    'multiple' => true,
                    'expanded' => true,
                    'compound' => true,
            ])
            ->add('period', TextType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
        ;
        /*$builder->get('formation')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    // transform the array to a string
                    return implode(', ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return explode(', ', $tagsAsString);
                }
            ))
        ;
        $builder->get('class')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    // transform the array to a string
                    return implode(', ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return explode(', ', $tagsAsString);
                }
            ))
        ;
        $builder->get('statut')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    // transform the array to a string
                    return implode(', ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return explode(', ', $tagsAsString);
                }
            ))
        ;*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Code::class,
        ]);
    }
}
