<?php

namespace App\Form;

use App\Entity\TypeUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('type', ChoiceType::class,[
                'choices' => [
                    'NATURE' => 'nature',
                    'ÉTUDIANT' => 'étudiant',
                    'COACH' => 'coach',
                    'FORMATEUR'   => 'formateur',
                    'ÉDITEUR' => 'éditeur',
                    'ADMINISTRATEUR' => 'administrateur',
                ]
            ])
            ->add('icon', VichImageType::class,[
                'required' => false,
                'allow_delete' => false,
                'download_label' => '...',
                'download_uri' => false,
                'image_uri' => true
            ])
            ->add('color', RadioType::class,[
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeUser::class,
        ]);
    }
}
