<?php


namespace App\Controller;


use App\Entity\Category;
use App\Entity\Configchapitre;
use App\Entity\Document;
use App\Entity\User;
use App\Entity\Classe;
use App\Entity\Userchapter;
use App\Repository\BadgeRepository;
use App\Repository\CatalogueRepository;
use App\Repository\CategoryRepository;
use App\Repository\ChapterRepository;
use App\Repository\ClasseRepository;
use App\Repository\ConfigchaiptreRepository;
use App\Repository\CoursRepository;
use App\Repository\DocumentFoldersRepository;
use App\Repository\DocumentRepository;
use App\Repository\FormationRepository;
use App\Repository\ModuleRepository;
use App\Repository\QuizRepository;
use App\Repository\UserchapterRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{

    /**
     * Upload file banner
     * @Route("/uploderforformation", name="uploderforformation-file")
     */
    public function uploderforformation(Request $request, ObjectManager $manager)
    {
        $user    = $this->getUser();
        $file    = $request->files->get('file');
        //$originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        //$newFilename = $originalFilename.'-'.uniqid().'.'.$file->guessExtension();
        $newFilename = $file->getClientOriginalName();
        $dir         = __DIR__.'/../../public/images/';
        $location    = realpath($dir).'/formations/';
        try {
            $file->move($location, $newFilename);
            //save the document
            $document = new Document();
            $document->setFile($file);

            $document->setName($file->getClientOriginalName());

            $document->setUser($user);
            $document->setcreatedAt(new \DateTime);
            $manager->persist($document);
            $manager->flush();

            return new Response(
                'success'
            );

        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
            return new Response(
                'error'
            );
        }
    }
    /**
     * @Route("/sendmailfromstudent", name="sendmailfromstudent")
     */
    public function sendmesagefromstudent(Request $request, \Swift_Mailer $mailer, UserRepository $userRepositoryser)
    {
        if ($data = $request->get('sendmessagefromdatshbord')) {
            $jsonData = $request->get('sendmessagefromdatshbord');

            $user = $this->getUser();
            $userRepositoryser = $userRepositoryser->findOneBy((['id' => $user]));
            $name = $user->getUsername();
            $cpbody = "Message réçu de la part ".$name;
            $corps=$jsonData[0]['titlename'];
            $corpsmail=$jsonData[0]['message'];
            $message = (new \Swift_Message(  $cpbody))
                ->setFrom('contact@kiwi-institute.com')
                ->setTo('contact@kiwi-institute.com')
                ->setBody(
                    $this->renderView('emails/confirmationstudent.html.twig',   ['name' => $name,'corps'=>$corps,'corpsmails'=>$corpsmail]),'text/html'
                );

            $mailer->send($message);
            return $this->redirectToRoute('dashboard_student');
        }
    }

    /**
     * @Route("configchaiptre", name="configchaiptre")
     */
    public function configchaiptre(Request $request, ConfigchaiptreRepository $configchaiptreRepository, ChapterRepository $chapterRepository, ObjectManager $manager)
    {
        if ($request->get('configchapter')) {
            $jsonData = $request->get('configchapter');
            $idchapter = $jsonData[0]['idchapter'];
            $chapter = $chapterRepository->findOneBy(['id' => $idchapter]);

            if ($chapter->getConfig()) {
                $idconfigchapter = $chapter->getConfig()->getId();

            }
            if (!empty($idconfigchapter)) {
                $configchapter = $configchaiptreRepository->findOneBy(['id' => $idconfigchapter]);

            } else {
                $configchapter = new Configchapitre();
                $manager->persist($chapter);
            }
            if (isset($jsonData[0]['picture'])) {
                $configchapter->setPicture($jsonData[0]['picture']);
            }
            if (isset($jsonData[0]['time_spent'])) {
                $chunks = explode(' ', $jsonData[0]['time_spent']);
                $fromtimespent = new \DateTime($chunks[0]);
                $configchapter->setTimeSpent($fromtimespent);
            }
            if (isset($jsonData[0]['status_completed'])) {
                $configchapter->setStatusCompleted($jsonData[0]['status_completed']);
            }
            if (isset($jsonData[0]['score_chaiptre'])) {
                $configchapter->setScore($jsonData[0]['score_chaiptre']);
            }
            if (isset($jsonData[0]['score_point'])) {
                $configchapter->setScorePoint($jsonData[0]['score_point']);
            }
            if (isset($jsonData[0]['statut_accesss'])) {
                $configchapter->setStatutAccesss($jsonData[0]['statut_accesss']);
            }
            if (isset($jsonData[0]['status_dateacces'])) {
                $stauts_datess = explode(' ', $jsonData[0]['status_dateacces']);

                list( $jour, $mois, $annee ) = explode( '/', $stauts_datess[0] );
                list( $hours, $minute ) = explode(':',$stauts_datess[1]);
                $stauts_date =new \DateTime($annee.'-'.$mois.'-'.$jour.' '.$hours.':'.$minute);
                $configchapter->setStatusDateacces($stauts_date);

            }
            if (isset($jsonData[0]['staut_subscribe'])) {

                $configchapter->setStautSubscribe($jsonData[0]['staut_subscribe']);
            }
            if (isset($jsonData[0]['staut_datesubscribe'])) {
                $date =new \DateTime();
                $daterecup=$jsonData[0]['staut_datesubscribe'].' '."days";
                $datesconvert=date_add($date, date_interval_create_from_date_string($daterecup));
                $configchapter->setStautDatesubscribe($datesconvert);
            }
            if (isset($jsonData[0]['heurechapterfin'])) {
                $chunks = explode(' ', $jsonData[0]['heurechapterfin']);
                $froms = new \DateTime($chunks[0]);
                $configchapter->setValideHours($froms);
            }
            if (isset($jsonData[0]['staut_prerequis'])) {
                $configchapter->setStautPrerequis($jsonData[0]['staut_prerequis']);
            }
            $manager->persist($configchapter);
            $chapter->setConfig($configchapter);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }

    }

    /**
     * delete list of selected badges
     * @Route("deleteBadges", name="delete_badges")
     */
    public function deleteBadges(ObjectManager $entityManager, Request $request, BadgeRepository $badgeRepository)
    {
        $badges = $request->request->get('badges');

        if (!empty($badges)) {
            foreach ($badges as $key => $badge) {
                $currentBadge = $badgeRepository->find($badge);
                $entityManager->remove($currentBadge);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }

    }
    /**
     * update the user status it can be 0 for inactive user and 1 to active user
     * @Route("changeStatus", name="update_status")
     */
    public function changeStatus(Request $request, ObjectManager $manager, UserRepository $repo) {
        $id  = $request->request->get('id');
        $status  = $request->request->get('active');
        $current_user   = $repo->findOneBy(array('id' => $id ));
        if($current_user) {
            $current_user->setActive($status);
            $manager->persist( $current_user);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * delete the selected user
     * @Route("deleteUser", name="delete_user")
     */
    public function deleteUser(Request $request, ObjectManager $manager, UserRepository $repo) {
        $id  = $request->request->get('id');
        $current_user   = $repo->findOneBy(array('id' => $id ));
        if($current_user) {
            $manager->remove( $current_user);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("activateAlluser", name="activate_all_user")
     */
    public function activateAlluser(Request $request, ObjectManager $manager, UserRepository $repo) {
        $list_users = $request->request->get('users');

        if(!empty($list_users)) {
            foreach ($list_users as $key => $user) {
                $currentUser = $repo->find($user);
                $currentUser->setActive(1);
                $manager->persist($currentUser);
                $manager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("desactivateAlluser", name="desactivate_all_user")
     */
    public function desactivateAlluser(Request $request, ObjectManager $manager, UserRepository $repo) {
        $list_users = $request->request->get('users');

        if(!empty($list_users)) {
            foreach ($list_users as $key => $user) {
                $currentUser = $repo->find($user);
                $currentUser->setActive(0);
                $manager->persist($currentUser);
                $manager->flush();
            }
            return new Response(
                'success'
            );
        }else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * delete list of selected users
     * @Route("deleteAllUser", name="delete_all_user")
     */
    public function deleteAllUser(Request $request, ObjectManager $manager, UserRepository $repo) {
        $list_users = $request->request->get('users');

        if(!empty($list_users)) {
            foreach ($list_users as $key => $user) {
                $currentUser = $repo->find($user);
                $manager->remove($currentUser);
                $manager->flush();
            }
            return new Response(
                'success'
            );
        }else {
            return new Response(
                'error'
            );
        }
    }

    /**
     *@Route("/getMedia", name="get_media")
     *@Route("/getMedia/{id}", name="get_media")
     */
    public function getMedia(DocumentRepository $media, DocumentFoldersRepository $folder, Request $request, $id=null) {
        //get all content for the curret user
        $folderId = $request->request->get('folderId');

        $userMedia = [];

        if($folderId) {
            $currentFolder     = $folder->find($folderId);
            if($currentFolder) {
                $userMedia     = $currentFolder->getDocuments();
            }
        } elseif($id) {
            $media       = $media->find($id);
            $user        = $this->getUser();
            $folders     = $folder->findBy(array('user' => $user ), array('name' => 'ASC'));
            return $this->render('admin/includes/sidebarMedia.html.twig', [
                'media' => $media, 'allfolders' => $folders]);

        }else {
            $user        = $this->getUser();
            $userMedia   = $media->findBy(array('user' => $user ));
        }

        return $this->render('admin/filesView.html.twig', [
            'userMedia' => $userMedia]);
    }


    /**
     *@Route("/getFolder/{nb}", name="get_folder")
     */
    public function getFolder(DocumentFoldersRepository $folder, $nb) {
        //get all folders for the curret user
        $user    = $this->getUser();
        $folders = $folder->findBy(array('user' => $user ));
        $arr     = array_chunk($folders, $nb);

        return $this->render('admin/foldersView.html.twig', [
            'folders' => $arr]);
    }

    /**
     * @Route("/affectFolder", name="affect_folder")
     */
    public function affectFolder(Request $request, ObjectManager $entityManager, DocumentRepository $document, DocumentFoldersRepository $folder) {

        $filesIds   = trim($request->request->get('filesIds'),",");
        $foldersIds = $request->request->get('folders');
        $files      =  explode(',', $filesIds);
        $folders    = explode(',', $foldersIds);


        foreach ($files as $key => $file) {
            $media     = $document->find($file);
            foreach ($folders as $k => $folderId) {
                $currentFolder     = $folder->find($folderId);
                $media->addDocumentFolder($currentFolder);
            }
        }
        //add document to a folder
        $entityManager->persist($media);
        $entityManager->flush();
        return new Response(
            'success'
        );
    }


    /**
     * delete the selected badge
     * @Route("deleteBadge", name="delete_badge")
     */
    public function deleteBadge(Request $request, ObjectManager $manager, BadgeRepository $badgeRepository)
    {
        $id = $request->request->get('id');
        $current_badge = $badgeRepository->findOneBy(array('id' => $id));
        if ($current_badge) {
            $manager->remove($current_badge);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * delete list of selected categories
     * @Route("deleteCategories", name="delete_categories")
     */
    public function deleteCategories(ObjectManager $entityManager, Request $request, CategoryRepository $categoryRepository)
    {
        $categories = $request->request->get('categories');

        if (!empty($categories)) {
            foreach ($categories as $key => $categorie) {
                $currentCategorie = $categoryRepository->find($categorie);
                $entityManager->remove($currentCategorie);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * delete the selected category
     * @Route("deleteCategory", name="delete_category")
     */
    public function deleteCategory(Request $request, ObjectManager $manager, CategoryRepository $categoryRepository)
    {
        $id = $request->request->get('id');
        $current_category = $categoryRepository->findOneBy(array('id' => $id));
        if ($current_category) {
            $manager->remove($current_category);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }


    /**
     * delete the selected formation
     * @Route("deleteFormation", name="delete_formation")
     */
    public function deleteFormation(Request $request, ObjectManager $manager, FormationRepository $formationRepository)
    {
        $id = $request->request->get('id');
        $current_formation = $formationRepository->findOneBy(array('id' => $id));
        if ($current_formation) {
            $manager->remove($current_formation);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * delete list of selected formations
     * @Route("deleteFormations", name="delete_formations")
     */
    public function deleteFormations(ObjectManager $entityManager, Request $request, FormationRepository $formationRepository)
    {
        $formations = $request->request->get('formations');

        if (!empty($formations)) {
            foreach ($formations as $key => $formation) {
                $currentFormation = $formationRepository->find($formation);
                $entityManager->remove($currentFormation);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }


    /**
     * duplicate the list of selected categories
     * @Route("duplicateCategories", name="duplicate_categories")
     */
    public function duplicateCategories(Request $request, ObjectManager $manager, CategoryRepository $categoryRepository)
    {
        $categories = $request->request->get('categories');

        if ($categories) {
            foreach ($categories as $key => $category) {
                $currentCategorie = $categoryRepository->find($category);
                $newCategory = clone $currentCategorie;
                $cat = $newCategory->getName();
                $catName = 'copy '.' '.$cat;
                $newCategory->setName($catName);
                $manager->persist($newCategory);
                $manager->flush();
            }
            return $this->redirectToRoute('home_formation');
        }
    }

    /**
     * duplicate the list of selected formations
     * @Route("duplicateFormations", name="duplicate_formations")
     */
    public function duplicateFormations(Request $request, ObjectManager $manager, FormationRepository $formationRepository)
    {
        $formations = $request->request->get('formations');

        if ($formations) {
            foreach ($formations as $key => $formation) {
                $currentFormation = $formationRepository->find($formation);
                $newForm = clone  $currentFormation;
                $forma = $newForm->getTitreFormation();
                $formTit = 'copy '.' '.$forma;
                $newForm->setTitreFormation($formTit);
                $manager->persist($newForm);
                $manager->flush();

            }
            return $this->redirectToRoute('home_formation');
        }
    }

    /**
     *@Route("getCatalogue/{nb}", name="get_formation")
     */
    public function getCatalogue(CatalogueRepository $catalogue, $nb) {
        //get all folders for the curret user
        $user    = $this->getUser();
        $catalogues = $catalogue->findBy(array('user' => $user ));
        $arr     = array_chunk($catalogues, $nb);

        return $this->render('formations/home.html.twig', [
            'catalogues' => $arr]);
    }


    /**
     * @Route("addCateg", name="add_categ")
     * @Route("editCateg/{id}", name="edit_categ")
     */
    public function editCateg(ObjectManager $manager, Request $request, $id = null, CategoryRepository $categoryRepository) {

        $category_name = $request->get('categ_name');
        if($id) {
            //edit existing category
            $category      = $categoryRepository->findOneBy(array('id'=>$id));
        } else {
            //add new Category
            $category      = new Category();
        }
        $category->setName($category_name);
        $manager->persist($category);
        $manager->flush();
        return new Response(
            'success'
        );
    }

    /**
     * duplicate the list of selected badges
     * @Route("duplicateBadges", name="duplicate_badges")
     */
    public function duplicateBadges(Request $request, ObjectManager $manager, BadgeRepository $badgeRepository)
    {
        $badges = $request->request->get('badges');

        if ($badges) {
            foreach ($badges as $key => $badge) {
                $currentBadge = $badgeRepository->find($badge);
                $newBadge = clone $currentBadge;
                $nam = $newBadge->getName();
                $fnam = 'copy '.' '.$nam;
                $newBadge->setName($fnam);
                $manager->persist($newBadge);
                $manager->flush();
            }
            return $this->redirectToRoute('new_formation_step6');
        }
    }

    /**
     * download the selected badges
     * @Route("downloadBadges", name="download_badges")
     */
    public function downloadBadges(ObjectManager $manager, Request $request, BadgeRepository $badgeRepository, UploaderHelper $uploaderHelper)
    {

        $badges = $request->request->get('badges');

        if ($badges) {
            foreach ($badges as $key => $badge) {
                $currentBadge = $badgeRepository->find($badge);
                // Provide a name for your file with extension
                $filename = 'Badge.txt';

                // The dinamically created content of the file
                $fileContent = $currentBadge;

                // Return a response with a specific content
                $response = new Response($fileContent);

                // Create the disposition of the file
                $disposition = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                // Set the content disposition
                $response->headers->set('Content-Disposition', $disposition);

                // Dispatch request
                return $response;
            }
            return $this->redirectToRoute('new_formation_step6');
        }
    }


    /**
     * @Route("addUsers/", name="add_list_users")
     */
    public function addUsers(FormationRepository $formationRepository, Request $request, ObjectManager $manager, ClasseRepository $classeRepository, \Swift_Mailer $mailer, UserRepository $userRepo)
    {
        $users = $request->get('users') ;
        $error = false;

        if(!empty($users)) {
            foreach ($users as $user) {
                if($user['email']) {
                    if(!$userRepo->findOneBy(['email' => $user['email']])) {
                        $newUser = new User();
                        $newUser->setCreatedAT(new \DateTime)
                            ->setUpdatedAt(new \DateTime)
                            ->setActive(0)
                            ->setUsername($user['username'])
                            ->setLastname($user['lastname'])
                            ->setEmail($user['email'])
                            ->setPassword('00000000');

                        if(isset($user['roles'])) {
                            //insert roles for this user
                            $roles = $user['roles'];
                            if(!empty( $roles)) {
                                foreach ($roles as $role) {
                                    $newUser->addRole($role);
                                }
                            }
                        }
                        if(isset($user['formations'])) {
                            foreach ($user['formations'] as $formationId) {
                                $form = $formationRepository->findOneBy(array('id' => $formationId));
                                if($form) {
                                    //affact the formation for the new user
                                    $newUser->addFormationsSup($form);
                                }
                            }
                            //insert formation for this user
                        }
                        if(isset($user['class'])) {
                            //insert class for this user
                            foreach ($user['class'] as $classId) {
                                $newClass = $classeRepository->findOneBy(array('id' => $classId));
                                if($newClass) {
                                    //affact the class for the new user
                                    $newUser->addClasse($newClass);
                                }
                            }
                        }
                        $manager->persist($newUser);
                        $manager->flush();
                        //send mail to user
                        //send confirmation mail
                        $name = $newUser->getUsername();
                        //update this link when the website is online
                        $link = 'http://lms.kiwi-institute.com/inscriptionUser?email=' . $newUser->getemail();
                        $message = (new \Swift_Message('Confirmation d\'inscription!'))
                            ->setFrom('informatique@kiwi-institute.com')
                            ->setTo($newUser->getemail())
                            ->setBody(
                                $this->renderView('emails/confirmation.html.twig',   ['name' => $name, 'link' => $link])  ,'text/html'
                            );
                        $mailer->send($message);
                    } else {
                        $error = true;
                    }
                } else {
                    $error = true;
                }
            }
        }
        if($error) {
            return new Response(
                'sucess'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("updateChapterLink", name="update_chapter_link")
     */
    public function updateChapterLink(Request $request, ChapterRepository $chapterRepository, ObjectManager $manager) {
        $id   = $request->get('id');
        $link = $request->get('link');
        $chapter = $chapterRepository->findOneBy(['id' => $id]);
        if($chapter) {
            $chapter->setExternalLink($link);
            $manager->persist($chapter);
            $manager->flush();
        }
        return new Response(
            'sucess'
        );
    }

    /**
     * @Route("updatefilepicture", name="updatefilepicture")
     */
    public function updatepictureformation(Request $request, FormationRepository $formationRepository, ObjectManager $manager) {
        $id   = $request->get('idformation');
        $pictureimage = $request->get('pictureofformation');
        $formationfind = $formationRepository->findOneBy(['id' => $id]);

        if($formationfind) {
            $formationfind->setBanner($pictureimage);
            $manager->persist($formationfind);
            $manager->flush();
        }
        return new Response(
            'sucess'
        );
    }


    /**
     * @Route("updatefilepicturecours", name="updatefilepicturecours")
     */
    public function updatefilepicturecours(Request $request, CoursRepository $coursRepository, ObjectManager $manager) {
        $id   = $request->get('idcours');
        $pictureimage = $request->get('pictureoffcourbanner');
        $coursfind = $coursRepository->findOneBy(['id' => $id]);


        if($coursfind) {
            $coursfind->setBanner($pictureimage);
            $manager->persist($coursfind);
            $manager->flush();
        }
        return new Response(
            'sucess'
        );
    }

    /**
     * @Route("/deleteImage", name="delete_image")
     */
    public function deleteImage(Request $request, ObjectManager $entityManager, DocumentRepository $document) {

        $mediaId   = $request->request->get('id');
        $media     = $document->find($mediaId);
        if($media) {

            //delete file from server
            $user = $this->getUser();
            $dir  = __DIR__.'/../../public/images/uploads/';

            $imagePath = realpath($dir).'\\'.$user->getId().'\documents\\'.$media->getName();

            if (file_exists($imagePath) )
            {
                unlink ( $imagePath );
            }
            //delete file from database
            $entityManager->remove($media);
            $entityManager->flush();

            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("hourschapterbyuser", name="hourschapterbyuser")
     */
    public function hourschapterbyuser(Request $request, ObjectManager $manager ,ChapterRepository $chapterRepository,UserchapterRepository $userchapterRepository,FormationRepository $formationRepository)
    {

        if ($data = $request->request->get('idchapter')) {
            // récupére id de chaiptre
            $idchapter = $request->get('idchapter');
            // récupére le temps passé par formation
            $gethour = $request->get('hours');
            // récupére le temps chaiptre recommandé
            $heurevalid=$request->get('tempsfinale');
            // récupére le temps validation automatique
            $heurevalid_afer=$request->get('tempsvalid');
            $id_formation=$request->get('idformation');
            $user = $this->getUser();
            $userchapterRepository=$userchapterRepository->findOneBy(array('user' => $user,'chapter'=>$idchapter));
            $chunks = explode(' ', $gethour);
            $from = new \DateTime($chunks[0]);
            $converttempsrecuper=$from->format('U');

            if (!empty($userchapterRepository)) {
                $saveruserchapters = $userchapterRepository;
            }
            else{
                $saveruserchapters=new Userchapter();
                $saveruserchapters->setUser($user);
                $chapter = $chapterRepository->findOneBy(['id' => $idchapter]);
                $foramtion=$formationRepository->findOneBy(['id'=>$id_formation]);
                $saveruserchapters->setChapter($chapter);
                $saveruserchapters->setFormation($foramtion);
            }

            if($heurevalid!=-1){
                $chunkstemp = explode(' ', $heurevalid);
                $froms = new \DateTime($chunkstemp[0]);
                $converttempsvalid=$froms->format('U');
                if ($converttempsrecuper <  $converttempsvalid ){
                    $saveruserchapters->setStatuts("In progress");
                    $saveruserchapters->setHoursPass($from);
                }
                if ($converttempsrecuper == $converttempsvalid ){
                    $saveruserchapters->setStatuts("Completed");
                    $saveruserchapters->setHoursPass($from);
                }

                if ($converttempsrecuper > $converttempsvalid ){
                    $saveruserchapters->setStatuts("Completed");
                    $saveruserchapters->setHoursPass($froms);
                }
            }
            if($heurevalid_afer!=-1){
                $tempsvalide_auto = explode(' ', $heurevalid_afer);
                $tempsvalide_convert = new \DateTime($tempsvalide_auto[0]);
                $converttempsvalid_auto=$tempsvalide_convert->format('U');

                if ($converttempsrecuper <  $converttempsvalid_auto ){

                    $saveruserchapters->setStatuts("In progress");
                    $saveruserchapters->setHoursPass($from);
                }
                if ($converttempsrecuper == $converttempsvalid_auto ){

                    $saveruserchapters->setStatuts("Completed");
                    $saveruserchapters->setHoursPass($from);
                }

                if ($converttempsrecuper > $converttempsvalid_auto ){

                    $saveruserchapters->setStatuts("Completed");
                    $saveruserchapters->setHoursPass($tempsvalide_convert);
                }
            }
            if ($heurevalid_afer==-1 && $heurevalid==-1 ) {

                $saveruserchapters->setStatuts("In progress");
                $saveruserchapters->setHoursPass($from);

            }
            $manager->persist($saveruserchapters);
            $manager->flush();
            return new Response(
                'success'
            );
        }else{
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("getJsonFormation/", name="get_json_formation")
     */
    public function getJsonFormation(FormationRepository $formationRepository, Request $request,ModuleRepository $moduleRepository, CoursRepository $coursRepository, ChapterRepository $chapterRepository, QuizRepository $quizRepository)
    {
        $formationId = $request->get('formationId') ;
        $myResult = [];
        if($formationId) {
            $formation = $formationRepository->findOneBy(['id' =>$formationId]);

            //get the list of module for the current formation
            if(!empty($formation->getModules())) {
                foreach ( $formation->getModules() as  $module) {

                    if($module->getStatus()) {
                        $md = [];
                        $md['id']    = $module->getChartId();
                        $md['name']  = $module->getName();
                        $md['pid']   = $module->getPid();
                        $md['iddb'] = $module->getId();
                        $md['type']  ="module";
                        $md['child'] =  [];
                        if(!empty($module->getCours())) {
                            foreach ($module->getCours() as $c) {
                                if($c->getStatus()) {
                                    $modulCour = [];
                                    $modulCour['id']       = $c->getChartId();
                                    $modulCour['name']     = $c->getName();
                                    $modulCour['pid']      = $c->getPid();
                                    $modulCour['iddb']      = $c->getId();
                                    $modulCour['type']     = "Cours";
                                    if(!empty($c->getChapters())) {
                                        $chapters = [];
                                        foreach ($c->getChapters() as $chap) {
                                            if($chap->getStatus()) {
                                                $modulchap = [];
                                                $modulchap['id']   =  $chap->getChartId();
                                                $modulchap['name'] = $chap->getName();
                                                $modulchap['pid']  = $chap->getPid();
                                                $modulchap['iddb'] = $chap->getId();
                                                $modulchap['type'] = "Chap";
                                                //get quiz

                                                $quizs = $quizRepository->findAll(['chapter' => $chap->getId()]);

                                                if(!empty($quizs)) {
                                                    $listquiz = [];
                                                    foreach ($quizs as $quiz) {
                                                        if($quiz->getStatus()) {
                                                            $modulQuiz= [];
                                                            $modulQuiz['id']   =  $quiz->getChartId();
                                                            $modulQuiz['name'] = $quiz->getName();
                                                            $modulQuiz['pid']  = $quiz->getPid();
                                                            $modulQuiz['iddb'] = $quiz->getId();
                                                            $modulQuiz['type'] = "quiz";
                                                            array_push($listquiz, $modulQuiz);
                                                        }
                                                    }

                                                    $modulchap['child'] = $listquiz;
                                                }
                                                array_push($chapters, $modulchap);
                                            }
                                        }
                                        $modulCour['child']    = $chapters;
                                    }
                                    array_push($md['child'], $modulCour);
                                }
                            }
                        }
                        array_push($myResult, $md);
                    }

                }
            }

            //les cours qui sont diretement sous la formation
            if(!empty($formation->getCours())) {
                foreach ($formation->getCours() as $cour) {
                    if($cour->getStatus()) {
                        $courFormation = [];
                        $courFormation['id']    =  $cour->getChartId();
                        $courFormation['name']  = $cour->getName();
                        $courFormation['pid']   = $cour->getPid();
                        $courFormation['iddb']  = $cour->getId();
                        $courFormation['type']      = "Cours";
                        $courFormation['child'] = [];

                        if(!empty($cour->getChapters())) {
                            $chapters = [];
                            foreach ($cour->getChapters() as $chap) {
                                if($chap->getStatus()) {
                                    $modulchap = [];
                                    $modulchap['id']    =  $chap->getChartId();
                                    $modulchap['name']  = $chap->getName();
                                    $modulchap['pid']   = $chap->getPid();
                                    $modulchap['iddb']  = $chap->getId();
                                    $modulchap['type']  = "Chap";
                                    $quizs = $quizRepository->findBy(array('chapter' => $chap));
                                    if(!empty($quizs)) {
                                        $listquiz = [];
                                        foreach ( $quizs as $quiz) {
                                            if($quiz->getStatus()) {
                                                $modulQuiz= [];
                                                $modulQuiz['id']   =  $quiz->getChartId();
                                                $modulQuiz['name'] = $quiz->getName();
                                                $modulQuiz['pid']  = $quiz->getPid();
                                                $modulQuiz['iddb'] = $quiz->getId();
                                                $modulQuiz['type'] = "quiz";
                                                array_push($listquiz, $modulQuiz);
                                            }
                                        }
                                        $modulchap['child'] = $listquiz;
                                    }
                                    array_push($chapters, $modulchap);
                                }

                            }
                            $courFormation['child']    = $chapters;
                        }
                        array_push($myResult, $courFormation);
                    }

                }
            }
            //les chapitre qui sont directement sou la formation
            if(!empty($formation->getChapters())) {
                foreach ($formation->getChapters() as $chap) {
                    if($chap->getStatus()) {
                        $modulchap = [];
                        $modulchap['id']    =  $chap->getChartId();
                        $modulchap['name']  = $chap->getName();
                        $modulchap['pid']   = $chap->getPid();
                        $modulchap['iddb']   = $chap->getId();
                        $modulchap['type']  ="Chap";
                        $quizs = $quizRepository->findBy(array('chapter' => $chap));
                        if(!empty($quizs)) {
                            $childQuiz = [];
                            foreach ($quizs as $quiz) {
                                if($quiz->getStatus()) {
                                    $modulQuiz= [];
                                    $modulQuiz['id']   =  $quiz->getChartId();
                                    $modulQuiz['name'] = $quiz->getName();
                                    $modulQuiz['pid']  = $quiz->getPid();
                                    $modulQuiz['iddb'] = $quiz->getId();
                                    $modulQuiz['type'] = "quiz";
                                    array_push($childQuiz, $modulQuiz);
                                }

                            }
                            $modulchap['child'] = $childQuiz;
                        }
                        array_push($myResult, $modulchap);
                    }
                }
            }

            /* if(!empty($formation->getQuizzes())) {
                 foreach ($formation->getQuizzes() as $currentQuiz) {
                     if($currentQuiz->getStatus()) {
                         $quiz = [];
                         $quiz['id']    =  $currentQuiz->getChartId();
                         $quiz['name']  = $currentQuiz->getName();
                         $quiz['pid']   = $currentQuiz->getPid();
                         $quiz['iddb']   = $currentQuiz->getId();
                         $quiz['type']  = "quiz";
                         array_push($myResult, $quiz);
                     }
                 }
             }*/
        }
        $response = new JsonResponse($myResult, 200);
        return $response;
    }

    /**
     * delete the selected classe
     * @Route("deleteClasse", name="delete_classe")
     */
    public function deleteClasse(Request $request, ObjectManager $manager, ClasseRepository $classeRepository)
    {
        $id = $request->request->get('id');
        $current_classe = $classeRepository->findOneBy(array('id' => $id));
        if ($current_classe) {
            $manager->remove($current_classe);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }
    /**
     * delete list of selected classes
     * @Route("deleteClasses", name="delete_classes")
     */
    public function deleteClasses(ObjectManager $entityManager, Request $request, ClasseRepository $classeRepository)
    {
        $classes = $request->request->get('classes');

        if (!empty($classes)) {
            foreach ($classes as $key => $classe) {
                $currentClasse = $classeRepository->find($classe);
                $entityManager->remove($currentClasse);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }

    }

    /**
     * duplicate the list of selected classes
     * @Route("duplicateClasses", name="duplicate_classes")
     */
    public function duplicateClasses(Request $request, ObjectManager $manager, ClasseRepository $classeRepository)
    {
        $classes = $request->request->get('classes');

        if ($classes) {
            foreach ($classes as $key => $classe) {
                $currentClasse = $classeRepository->find($classe);
                $newClasse = clone $currentClasse;
                $nam = $newClasse->getName();
                $fnam = 'copy '.' '.$nam;
                $newClasse->setName($fnam);
                $manager->persist($newClasse);
                $manager->flush();

            }
            return $this->redirectToRoute('home_classe');
        }
    }


}