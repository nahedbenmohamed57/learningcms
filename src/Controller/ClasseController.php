<?php

namespace App\Controller;

use App\Entity\Classe;
use App\Form\ClasseType;
use App\Repository\ClasseRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("{_locale}/classe/")
 */
class ClasseController extends AbstractController
{
    /**
     * @Route("home", name="home_classe")
     */
    public function index(Request $request, ObjectManager $manager, ClasseRepository $classeRepository)
    {
        $classes = $classeRepository->findAll();
        $classe = new Classe();

        $form = $this->createForm(ClasseType::class, $classe);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($classe);

            $manager->flush();

            return $this->redirectToRoute('home_classe');
        }

        $form->handleRequest($request);
        return $this->render('classe/home.html.twig', [
            'form' => $form->createView(),
            'toggled' => true,
            'classes' => $classes,
        ]);
    }

    /**
     * duplicate the selected classe
     * @Route("duplicateClasse/{id}", name="duplicate_classe")
     */
    public function duplicateClasse(ObjectManager $manager, ClasseRepository $classeRepository,Classe $classe)
    {
        $newClasse = clone $classe;
        $nam = $newClasse->getName();
        $fnam = 'copy'.' '.$nam;
        $newClasse->setName($fnam);

        $manager->persist($newClasse);

        $manager->flush();

        return $this->redirectToRoute('home_classe');
    }
}
