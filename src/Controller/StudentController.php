<?php


namespace App\Controller;


use App\Entity\Abonnement;
use App\Entity\Contact;
use App\Entity\Document;
use App\Entity\Formation;
use App\Entity\Preferences;
use App\Entity\StudentFolders;
use App\Entity\User;
use App\Entity\Userchapter;
use App\Form\ContactType;
use App\Form\PreferencesType;
use App\Form\ProfileType;
use App\Repository\CategoryRepository;
use App\Repository\ChapterRepository;
use App\Repository\CoursRepository;
use App\Repository\FormationRepository;
use App\Repository\ModuleRepository;
use App\Repository\PreferencesRepository;
use App\Repository\StudentFoldersRepository;
use App\Repository\StudentNotesRepository;
use App\Repository\UserchapterRepository;
use App\Repository\UserRepository;
use DateTimeZone;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Response;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * The student Module
 * @Route("{_locale}/student/")
 */
class StudentController extends AbstractController
{
    /**
     * @Route("index", name="dashboard_student")
     */
    public function dashboard(FormationRepository $formationRepository,UserRepository $userRepository ,ModuleRepository $moduleRepository,CoursRepository $coursRepository,ChapterRepository $chapterRepository,StudentNotesRepository $studentNotesRepository) {
        $user                   = $this->getUser();
        //$findformationbyuser    = $formationRepository->findBy(array('user' => $user), array('upadatedate' => 'desc'), 4);

        $findformationbyuser  = $user->getFormationsSup();

        $countnombredeformation = count($findformationbyuser);
        // $numberheure            = $user->getCounter();
        $numberheure            = '0';
        $formationId            = null;
        $modules                = [];
        $cours                  = [];
        $chapter                = [];
        if (!empty($findformationbyuser)) {
            $modules = $moduleRepository->findBy(['formation' => $formationId]);
            if (!empty($modules)) {
                foreach ($modules as $module) {
                    $module->type = "modul";
                    $module->cour = $coursRepository->findBy(['module' => $module]);
                    if (!empty($module->cour)) {
                        foreach ($module->cour as $cour) {
                            $cour->chapters = $chapterRepository->findBy(['cours' => $cour]);
                        }
                    }
                }
            }
            $chapter = $chapterRepository->findBy(['formation' => $formationId]);
            $cours   = $coursRepository->findBy(['formation' => $formationId]);
            if (!empty($cours)) {
                foreach ($cours as $cour) {
                    $cour->chapters = $chapterRepository->findBy(['cours' => $cour]);
                }
            }
        }
        $findstudentfolder = $studentNotesRepository->findBy(array('user' => $user,'status' => 1), array('updateAt' => 'desc'), 10);

        //$namesutdentfolder=$findstudentfolder->getTitle();
        return $this->render('student/dashbaord.html.twig', [
            'numberofformation'=>$countnombredeformation,
            'numhours'=>$numberheure,
            'modules' => $modules,
            'cours' => $cours,
            'chapters' => $chapter,
            'formations'=>$findformationbyuser,
            'namestudent'=>$findstudentfolder,
        ]);
    }

    /**
     * @Route("profile/{id}", name="user_profile")
     */
    public function edit_profile(ObjectManager $manager, Request $request, UserPasswordEncoderInterface $encoder, $id = null, UserRepository $userRepository) {

        $roleUser = 'user';
        if($id == null) {
            $user = $this->getUser();
            if(!$user) {
                return $this->redirectToRoute('login_form');
            }
        } else {
            $user = $userRepository->findOneBy(array('id' => $id));
            if(!$user) {
                return $this->redirectToRoute('users_list');
            }

        }
        $roles = $user->getRoles();

        if(in_array('ROLE_ADMIN', $roles)) {
            $roleUser = 'admin';
        }
        $pass = $user->getPassword();
        $user->setUpdatedAt(new \DateTime);
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);
        $gender = $user->getgender();
        if($form->isSubmitted() && $form->isValid()) {

            $oldPassword = $request->request->get('profile')['password'];
            $gender      = $request->request->get('gender');
            $user->setGender($gender);
            $user->setPassword($pass);

            if($oldPassword != "" ) {
                //modification de mot de passe
                if ($encoder->isPasswordValid($user, $oldPassword)) {
                    //if the same passord
                    $newEncodedPassword = $encoder->encodePassword($user, $user->getNewPassword());
                    $user->setPassword($newEncodedPassword);
                    $this->addFlash(
                        'success',
                        'Modification effectué avec succées!'
                    );

                } else  {
                    $this->addFlash('error', 'Ancien mot de passe incorrect !');
                }
            }

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('user_profile' );
        }

        return $this->render('user/user_profile.html.twig', [
            'formUser' => $form->createView(),'gender' =>  $gender, 'roleUser' => $roleUser
        ]);
    }



    /**
     * @Route("abonnement", name="student_abonnement")
     */
    public function abonnement() {
        //get all abonnements
        $repository = $this->getDoctrine()->getRepository(Abonnement::class);
        $abonnementsAnnuel  = $repository->findBy(['type' => 'annual']);
        $abonnementsMonthly = $repository->findBy(['type' => 'monthly']);

        return $this->render('student/abonnement.html.twig', [
            'annual' => $abonnementsAnnuel, 'monthly' => $abonnementsMonthly
        ]);
    }

    /**
     * @Route("pannier", name="student_pannier")
     */
    public function pannier() {
        return $this->render('student/pannier.html.twig');
    }

    /**
     * @Route("preferences", name="student_preferences")
     */
    public function preferences(ObjectManager $manager, Request $request, PreferencesRepository $preferencesRepository)
    {
        $pref = new Preferences();
        $form = $this->createForm(PreferencesType::class, $pref);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {


            $id = $this->getUser();

            $prefUser = $preferencesRepository->findOneBy(array('user' => $id));

            if ($request->isMethod('POST')) {

                $mailconf = $request->request->getInt('mail_conf');

                $vidconf = $request->request->getInt('vid_conf');

                if (!empty($prefUser)) {
                    //do update
                    $prefUser->setMailConfig($mailconf);

                    $prefUser->setVideoConfig($vidconf);

                    //$manager->persist($pref);

                    $manager->flush();

                    return $this->redirectToRoute('student_preferences');
                } else {
                    //do insert
                    $pref->setUser($id);


                    $pref->setMailConfig($mailconf);

                    $pref->setVideoConfig($vidconf);

                    $manager->persist($pref);

                    $manager->flush();

                    return $this->redirectToRoute('student_preferences');
                }
            }
        }
        return $this->render('student/preferences.html.twig',[
            'form' =>$form->createView()
        ]);
    }

    /**
     * @Route("products", name="student_products")
     */
    public function products() {
        return $this->render('student/products.html.twig');
    }

    /**
     * @Route("persenalData", name="student_data")
     */
    public function persenalData() {
        return $this->render('student/persenalData.html.twig');
    }

    /**
     * @Route("invoice", name="student_invoice")
     */
    public function invoice() {
        return $this->render('student/invoice.html.twig');
    }

    /**
     * @Route("formations", name="student_formations")
     */
    public function formations(FormationRepository $formationRepository, CategoryRepository $categoryRepository) {
        $currentUser = $this->getUser();
        $formations  = $currentUser->getFormationsSup();
        //$formations = $formationRepository->findAll();
        $categories = $categoryRepository->findAll();

        return $this->render('student/formations.html.twig',
            [
                'toggled' => true,
                'formations' => $formations,
                'categories' => $categories,
            ]);
    }

    /**
     * @Route("showFormations/{formationId}", name="show_formation")
     */
    public function showFormations(FormationRepository $formationRepository, ModuleRepository $moduleRepository, CoursRepository $coursRepository, ChapterRepository $chapterRepository, $formationId = null, StudentFoldersRepository $foldersRepository, StudentNotesRepository $studentNote) {
        if($formationId) {
            $formation = $formationRepository->findOneBy(['id' =>$formationId]);
            $formations = $formationRepository->findBy(['user' => $this->getUser()]);
            $modules = $moduleRepository->findBy(['formation' => $formation, 'status' => 1]);
            if(!empty($modules)) {
                foreach ( $modules as  $module) {
                    //get list cour
                    $module->cour = $coursRepository->findBy(['module' => $module, 'status' => 1]);
                    if(!empty( $module->cour)) {
                        foreach ( $module->cour as $cour) {
                            $cour->chapters = $chapterRepository->findBy(['cours' => $cour, 'status' => 1]);
                        }
                    }
                }
            }
            $chapter = $chapterRepository->findBy(['formation' => $formation, 'status' => 1]);
            $cours = $coursRepository->findBy(['formation' => $formation, 'status' => 1]);

            if(!empty($cours)) {

                foreach ( $cours as $cour) {
                    $cour->chapters = $chapterRepository->findBy(['cours' => $cour, 'status' => 1]);
                }
            }
            $user      = $this->getUser();
            $folders   = $foldersRepository->findBy(array('user' => $user ));
            $notes     =  $studentNote->findBy(array('user' => $user,'status' => 1));
            return $this->render('student/showFormations.html.twig',[
                'formations' => $formations,
                'formation'  => $formation,
                'modules'    => $modules,
                'cours'      => $cours,
                'chapters'   => $chapter,
                'folders'    => $folders,
                'notes'      => $notes

            ]);
        } else {
            dump('404: vous devez choisir une formation');
        }
    }

    /**
     *@Route("showcourses/{formationId}/{courId}", name="showcourses")
     */
    public function showModule($courId = null, $formationId = null, ChapterRepository $chapterRepository, CoursRepository $coursRepository, FormationRepository $formationRepository,ModuleRepository $moduleRepository ,UserchapterRepository $userchapterRepository, StudentFoldersRepository $foldersRepository, StudentNotesRepository $studentNote) {
        if(!empty($courId)) {
            $formation = $formationRepository->findOneBy(['id' =>$formationId]);
            $modules   = $moduleRepository->findBy(['formation' => $formation, 'status' => 1]);
            $cour      = $coursRepository->findOneBy(['id' => $courId, 'status' => 1]);
            $listChapters = $chapterRepository->findBy(['cours' => $cour, 'status' => 1]);

            if(!empty($modules)) {
                foreach ( $modules as  $module) {
                    //get list cour
                    $module->cour = $coursRepository->findBy(['module' => $module, 'status' => 1]);
                    if(!empty( $module->cour)) {
                        foreach ( $module->cour as $courM) {
                            $courM->chapters = $chapterRepository->findBy(['cours' => $cour, 'status' => 1]);
                        }
                    }
                }
            }
            $chaptere = $chapterRepository->findBy(['formation' => $formation, 'status' => 1]);
            $course = $coursRepository->findBy(['formation' => $formation, 'status' => 1]);

            if(!empty($course)) {
                foreach ( $course as $cour1) {
                    $cour1->chapters = $chapterRepository->findBy(['cours' => $course, 'status' => 1]);
                }
            }
            $user      = $this->getUser();
            $folders   = $foldersRepository->findBy(array('user' => $user ));
            $notes     =  $studentNote->findBy(array('user' => $user,'status' => 1));
            return $this->render('student/showModule.html.twig', [
                'formation' => $formation,
                'chapters' => $listChapters,
                'cour' => $cour,
                'module'=>$modules,
                'course'=>$course,
                'chapterseach'=>$chaptere,
                'folders'    => $folders,
                'notes'      => $notes

            ]);
        } else {
            dump('404');
        }

    }
    /**
     *@Route("course/{id}", name="show_course")
     */
    public function course() {
        return $this->render('student/course.html.twig');
    }

    /**
     *@Route("generatepdf", name="show_pdf")
     */

    public function indexpdf(UserRepository $userRepository)
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);
        $user = $this->getUser();
        $getuser = $userRepository->find($user);
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('student/mypdf.html.twig', [
            'title' => "Welcome to our PDF Test",
            'infouser'=>$getuser,
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mesinformations.pdf", [
            "Attachment" => true
        ]);
    }

    /**
     * @Route("Contact", name="contact_student")
     */
    public function contactStudent(ObjectManager $manager, Request $request, \Swift_Mailer $mailer) {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($contact);

            $manager->flush();

            $message = (new \Swift_Message('Bonjour !'))
                ->setFrom($contact->getMail())
                ->setTo('informatique@kiwi-institute.com')
                ->setBody(
                    $contact->getMessage());
            $mailer->send($message);
            return $this->redirectToRoute('type_user');
        }
        return $this->render('contact/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     *@Route("generatecsv", name="show_csv")
     */
    public function generatecsv(UserRepository $userRepository){
        $format = 'csv';
        $exportTo = 'php://output';
        $exporterWriter = '\Exporter\Writer\\' . ucfirst($format) . 'Writer';
        $user = $this->getUser();
        $data = $userRepository->find($user);
        $filename = "export_".date("Y_m_d_His").".csv";
        $response = $this->render('AppDefaultBundle:Default:csvfile.html.twig', array('data' => $data));

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Description', 'Submissions Export');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');

        $response->prepare();
        $response->sendHeaders();
        $response->sendContent();
    }

}
