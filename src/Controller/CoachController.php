<?php


namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("{_locale}/coach/")
 */
class CoachController extends AbstractController
{

    /**
     * @Route("home", name="dashboard_coach")
     */
    public function index(Request $request, ObjectManager $manager)
    {
        return $this->render('coach/dashboard.html.twig');
    }

    /**
     * @Route("statisticcoach", name="statistic_coach")
     */
    public function statisticcoach() {

    }
}