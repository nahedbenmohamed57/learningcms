<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Video;
use App\Repository\CommentRepository;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\VideoRepository;

class VideosettingController extends AbstractController
{
    /**
     * @Route("student/Videosetting/{videoId}", name="Videosetting")
     */
    public function index(ObjectManager $manager, Request $request, CommentRepository $commentRepository,VideoRepository $videoRepository, $videoId=null)
    {

            $comments = $commentRepository->findAll();

            $video = $videoRepository->findOneBy(['id' =>$videoId]);

            $videos = $videoRepository->findAll();
        return $this->render('Videosetting/index.html.twig', [
            'video' => $videoRepository->videoDetails($videoId),
            'comments' => $comments,
            'videos' => $videos,
        ]);
    }

    /**
     * @Route("/getComments", name="get_comments")
     */
    public function ajaxSendUsers(CommentRepository $commentRepository, Request $request)
    {
        if(!empty($request->request->get('id'))){
            $id = $request->request->get('comment');
            $messages = $commentRepository->findBy(['id' => $id]);
        }
        return $messages;
    }

    /**
     * @Route("student/newComment/{video}", name="new_comment", methods={"POST"})
     */
    public function newComment(Video $video, Request $request, ObjectManager $manager)
    {
        $user = $this->getUser();

        if (!empty(trim($request->request->get('message')))) {

            $message = $request->request->get('message');
            $comment = new Comment();
            $comment->setContent($message);
            $comment->setUser($user);
            $comment->setVideo($video);
            $comment->setCreatedAt(new \DateTime);
            $manager->persist($comment);
            $manager->flush();

            /* return new Response(
                 'success'
             );
             }else{
             return new Response(
             'error'
             );
             }*/
        }

        return new JsonResponse(array('id' => $user->getPicture(),'name' => $user->getUsername() ,'image' => $user->getPicture()));
        /*return $this->redirectToRoute('Videosetting', [
            'videoId' => $video->getId()
        ]);*/
    }
    /*public static function submitChat($messages){
        $chat = new ChatLine(array(
            'text'        => $messages,
        ));

        // The save method returns a MySQLi object
        $insertID = $chat->save()->insert_id;

        return array(
            'status'    => 1,
            'insertID'    => $insertID
        );
    }

    public static function getChats($lastID,CommentRepository $commentRepository){
        $lastID = (int)$lastID;

        $comments = $commentRepository->findBy(['id' => $lastID]);

        return array('chats' => $comments);
    }*/
}
