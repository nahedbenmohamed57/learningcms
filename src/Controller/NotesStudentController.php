<?php

namespace App\Controller;

use App\Entity\StudentFolders;
use App\Entity\StudentNotes;
use App\Entity\User;
use App\Form\ProfileType;
use App\Repository\StudentFoldersRepository;
use App\Repository\StudentNotesRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use phpDocumentor\Reflection\Location;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * The student Module
 * @Route("{_locale}/notestudent")
 */

class NotesStudentController extends AbstractController
{
    /**
     * @Route("/indexnote", name="notes_student")
     */
    public function index()
    {
        return $this->render('notes_student/index.html.twig', [
            'controller_name' => 'NotesStudentController',
        ]);
    }

    /**
     * @Route("/notes", name="student_note")
     */
    public function note(StudentFoldersRepository $studentFolder, Request $request, ObjectManager $manager, StudentNotesRepository $studentNote) {
        //get list of folder by user
        $user      = $this->getUser();
        $folders   = $studentFolder->findBy(array('user' => $user ));
        $userNotes = $studentNote->findBy(array('user' => $user,'status' => 1));
        $note = new StudentNotes();
        $note->setCreatedAt(new \DateTime)
            ->setType('text')
            ->setUpdateAt(new \DateTime)
            ->setStatus(1);
        $foldersIds = [];
        if(!empty($folders)) {
            foreach ($folders as $key => $f) {
                array_push($foldersIds, $f->getId());
            }
        }

        $form = $this->createFormBuilder($note)
            ->add('title')
            ->add('studentFolder', EntityType::class, [
                'class' => StudentFolders::class,
                'choice_label'  => 'name',
                'required' => false,
                'choice_label'  => function ($stfolders) {
                    if($stfolders->getUser() == $this->getUser())  return $stfolders->getName();
                },
                /* 'choice_value' => function ($stfolders) {
                       if($stfolders->getUser() == $this->getUser())  return $stfolders->getName();
                 },*/

            ])
            ->add('value', CKEditorType::class,['config' => ['uiColor' => '#ffffff', 'toolbar' => [ "/", [ 'Bold','Italic','Underline','Strike', 'Blockquote','Subscript','Superscript','-' ], [ 'NumberedList','BulletedList','-','-','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ], ['Table' ], '/', [ 'Styles', 'Format','Font','FontSize' ], [ 'TextColor' ] ]]])
            ->getForm();

        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()) {
            $value = $request->request->get('form')['value'];
            if($value) {
                $note->setUser($user);
                $manager->persist( $note);
                $manager->flush();
            }
            $this->addFlash(
                'success',
                'Votre note est ajoutée avec succée!'
            );
            return $this->redirectToRoute('student_note');
        }
        return $this->render('notes_student/note.html.twig', [
            'folders' => $folders, 'notes' => $userNotes, 'formNote' => $form->createView(),
        ]);
    }

    /**
     * @Route("/addFolder", name="add_folder_note")
     */
    public function addFolder(Request $request, ObjectManager $entityManager) {
        $folderName = $request->request->get('folder_name');
        if($folderName) {
            $currentUser= $this->getUser();
            if($currentUser) {
                //add a new folder for the connected user
                $folder = new StudentFolders();
                $folder->setName($folderName);
                $folder->setUser($currentUser);
                $entityManager->persist($folder);
                // actually executes the queries (i.e. the INSERT query)
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/updateFolder", name="update_folder")
     */
    public function updateFolderName(Request $request, ObjectManager $entityManager, StudentFoldersRepository $studentFolder) {

        $folderName = $request->request->get('folder_name');
        $folderId   = $request->request->get('id');
        $folder     = $studentFolder->find($folderId);
        if($folder) {
            $folder->setName($folderName);
            $entityManager->persist($folder);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/deleteFolder", name="delete_folder")
     */
    public function deleteFolder(Request $request, ObjectManager $entityManager, StudentNotesRepository $studentNotes,StudentFoldersRepository $studentFolder) {
        $folderId   = $request->request->get('id');
        $folder     = $studentFolder->find($folderId);
        $notes = $studentNotes->findBy(['studentFolder'=>$folderId]);
        foreach ($notes as $key => $note) {
            $note->setStudentFolder(null);
            $note->setStatus(0);
            $entityManager->persist($note);
            $entityManager->flush();
        }

        if ($folder){
            $entityManager->remove($folder);
            $entityManager->flush();
        }
        return new Response(
            'success'
        );

        /* if($folder) {
             $folder->removeStudentNote($note);
             $entityManager->persist($folder);
             $entityManager->flush();
             return new Response(
                 'success'
             );
         } else {
             return new Response(
                 'error'
             );
         }*/
    }

    /**
     * @Route("/duplicateFolder", name="duplicate_folder")
     */
    public function duplicateFolder(Request $request, ObjectManager $entityManager) {
        $folderName = $request->request->get('folder_name');
        if($folderName) {
            $currentUser = $this->getUser();
            if($currentUser) {
                //add a new folder for the connected user
                $folder = new StudentFolders();
                $folder->setName($folderName.' copie');
                $folder->setUser($currentUser);
                $entityManager->persist($folder);
                // actually executes the queries (i.e. the INSERT query)
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/updateNote/{id}", name="update_note")
     */
    public function updateNote(ObjectManager $objectManger, Request $request, StudentNotes $note) {

        $form = $this->createFormBuilder($note)
            ->add('title')
            ->add('value', CKEditorType::class,['config' => ['uiColor' => '#ffffff', 'toolbar' => [['Undo','Redo','Bold','Italic','Underline','Strike', 'Blockquote','Subscript','Superscript','-',  'NumberedList','BulletedList','-','-','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock', 'Table', 'Styles', 'Format','Font','FontSize', 'TextColor' ], "/" ]]])
            ->add('studentFolder', EntityType::class, [
                'class' => StudentFolders::class,
                'choice_label'  => 'name',
                'placeholder' => 'Choisir un dossier',
                'required' => false,
                'choice_label'  => function ($stfolders) {
                    if($stfolders->getUser() == $this->getUser())  return $stfolders->getName();
                }])
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            //update the date of the creation
            if(!$note->getId()) {
                $note->setCreatedAt(new \DateTime);
            }
            $note->setUpdateAt(new \DateTime);
            $objectManger->persist($note);
            $objectManger->flush();

            $this->addFlash(
                'success',
                'Votre note a été mise à jour!'
            );
            return $this->redirectToRoute('student_note');
        }
        $note->setValue(strip_tags($note->getValue()));
        return $this->render('notes_student/update_note.html.twig', [
            'formNote' => $form->createView(),
            'note' => $note
        ]);
    }

    /**
     * @Route("/deleteNote/{id}", name="delete_note")
     */
    public function deleteNote(ObjectManager $entityManager, StudentNotes $note) {
        if($note) {
            $note->setStatus(0);
            $entityManager->persist($note);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Votre note a été suprimée avec succée!'
            );
            return $this->redirectToRoute('student_note');
        }
    }

    /**
     * @Route("/showNotes/{id}", name="show_notes")
     */
    public function showNoteByFolder($id, StudentNotesRepository $studentNote, StudentFoldersRepository $foldersRepository) {
        //get all notes for the selected folder
        $userNotes = $studentNote->findBy(array('studentFolder' =>  $id,'status' => 1 ));
        $folders = $foldersRepository->findOneBy(array('id' =>  $id));
        return $this->render('notes_student/note_by_floder.html.twig', [
            'notes' => $userNotes,
            'folder' => $folders
        ]);
    }

    /**
     * @Route("/duplicateNote/{id}", name="duplicate_note")
     */
    public function duplicateNote(ObjectManager $entityManager, StudentNotesRepository $studentNote, $id, StudentNotes $note) {

        $newNote = new StudentNotes();
        if($note) {
            $newNote->setCreatedAt(new \DateTime)
                ->setValue($note->getValue())
                ->setType($note->getType())
                ->setStudentFolder($note->getStudentFolder())
                ->setTitle($note->getTitle().' copie')
                ->setStatus(1)
                ->setUser($note->getUser());
            //add a new note
            $entityManager->persist($newNote);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Votre note a été dupliquée avec succée!'
            );

        }
        return $this->redirectToRoute('student_note');
    }

    /**
     * @Route("/deleteNotes", name="delete_notes")
     */
    public function deleteNotes(ObjectManager $entityManager, Request $request, StudentNotesRepository $studentNote) {
        $notes = $request->request->get('notes');
        if(!empty($notes)) {
            foreach ($notes as $key => $note) {
                $currentNote = $studentNote->find($note);
                $currentNote->setStatus(0);
                $entityManager->persist($currentNote);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        }

    }

    /**
     * @Route("/saveAudio", name="save_audio")
     */
    public function saveaudio() {
        return $this->render('notes_student/record.html.twig');
    }

    /**
     * download the selected folder
     * @Route("/downloadFolder/{id}", name="download_folder")
     */
    public function downloadFolder(ObjectManager $manager, StudentFoldersRepository $studentFolder, UploaderHelper $uploaderHelper,StudentNotes $note, $id)
    {
        $folder = $studentFolder->find($id);

        if (!$folder) {
            $array = array('statuts' => 0,
                'message' => 'Dossier n \ existe pas'
            );
            $response = new JsonResponse($array, 200);
            return $response;
        } else {
            $notes = $folder->getStudentNotes();
            $myNotes = [];
            foreach ($notes as $key => $note) {
                $note->setValue(strip_tags($note->getValue()));
                array_push($myNotes, $note);
            }
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $dompdf = new Dompdf($pdfOptions);

            $html = $this->renderView('student/notesPdf.html.twig', [
                'title' => "Liste des notes",
                'notes'=> $myNotes,
            ]);

            $dompdf->loadHtml($html);

            $dompdf->setPaper('A4', 'portrait');

            $dompdf->render();
            $dompdf->stream("mynotes.pdf", [
                "Attachment" => true
            ]);
            return $this->redirectToRoute('student_note');
        }
    }

    /**
     * download the selected note
     * @Route("/downloadNote/{id}", name="download_note")
     */
    public function downloadNote(ObjectManager $manager, StudentNotesRepository $studentNote, UploaderHelper $uploaderHelper,StudentNotes $note, $id)
    {
        $note = $studentNote->find($id);
        $note->setValue(strip_tags($note->getValue()));
        if (!$note) {
            $array = array('statuts' => 0,
                'message' => 'Fichier n \ existe pas'
            );
            $response = new JsonResponse($array, 200);
            return $response;
        }
        // Provide a name for your file with extension
        $filename = 'Note.doc';

        // The dinamically created content of the file
        $fileContent = $note;

        // Return a response with a specific content
        $response = new Response($fileContent);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);

        // Dispatch request
        return $response;
    }
    /**
     * download notes
     * @Route("/downloadNotes/{notes}", name="download_notes")
     */
    public function downloadNotes(ObjectManager $manager, Request $request, StudentNotesRepository $studentNote, UploaderHelper $uploaderHelper, $notes = null)
    {
        if($notes) {
            $listNote = explode(',', $notes);

            $myNotes = [];
            foreach ($listNote as $key => $note) {
                $currentNote = $studentNote->find($note);
                $currentNote->setValue(strip_tags($currentNote->getValue()));
                array_push($myNotes, $currentNote);
            }
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $dompdf = new Dompdf($pdfOptions);

            $html = $this->renderView('student/notesPdf.html.twig', [
                'title' => "Liste des notes",
                'notes'=> $myNotes,
            ]);

            $dompdf->loadHtml($html);

            $dompdf->setPaper('A4', 'portrait');

            $dompdf->render();
            $dompdf->stream("mynotes.pdf", [
                "Attachment" => true
            ]);
            return $this->redirectToRoute('student_note');
        }
    }
    /**
     * function to test mails
     * @Route("/sendmailfromstudents", name="sendmailfromstudents")
     */
    public function sendmailfromstudent()
    {
        return $this->render('emails/confirmationstudent.html.twig', ['name'=>'','corps'=>'','corpsmails'=>'']);
    }

    /**
     * @Route("/duplicateListNote/{noteIds}", name="duplicate_notes")
     */
    public function duplicateListNote(ObjectManager $entityManager, StudentNotesRepository $studentNote, $noteIds = null) {
        if($noteIds) {

            $listNote = explode(',', $noteIds);
            $myNotes = [];
            foreach ($listNote as $key => $note) {
                $currentNote = $studentNote->find($note);
                $newNote = new StudentNotes();
                $newNote->setCreatedAt(new \DateTime)
                    ->setValue($currentNote->getValue())
                    ->setType($currentNote->getType())
                    ->setStudentFolder($currentNote->getStudentFolder())
                    ->setTitle($currentNote->getTitle().' copie')
                    ->setStatus(1)
                    ->setUser($currentNote->getUser());
                $entityManager->persist($newNote);
            }
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Les notes sélectionnées ont été dupliquées avec succée !'
            );
        }
        return $this->redirectToRoute('student_note');
    }

    /**
     * @Route("/addAjaxNote/", name="add-ajax-note")
     */
    public function addAjaxNote(ObjectManager $entityManager, StudentNotesRepository $studentNote, Request $request, StudentFoldersRepository $studentFolder) {
        $noteTitle = $request->request->get('title');
        $noteFolder = $request->request->get('folder');
        $noteValue = $request->request->get('content');
        $idNote    = $request->request->get('idNote');
        $user = $this->getUser();
        if($noteTitle) {

            if($idNote){
                $note = $studentNote->find(array('id' => $idNote));
            } else {
                $note = new StudentNotes();
                $note->setCreatedAt(new \DateTime)
                    ->setType('text')
                    ->setStatus(1)
                    ->setUser($user);
            }
            $note->setUpdateAt(new \DateTime)
                ->setTitle($noteTitle)
                ->setValue($noteValue);

            if(!empty($noteFolder)) {
                //update the note folder
                $folder =  $studentFolder->find($noteFolder);
                if($folder) {
                    $note->setStudentFolder($folder);
                }
            }

            $entityManager->persist($note);
            $entityManager->flush();
            return new Response(
                'success'
            );
        }

    }

    /**
     * @Route("/getAjaxNote/", name="get-ajax-note")
     */
    public function getAjaxNote(ObjectManager $entityManager, StudentNotesRepository $studentNote, Request $request, StudentFoldersRepository $studentFolder) {
        $noteId = $request->request->get('noteId');
        if($noteId) {
            $note = $studentNote->findOneBy(['id' => $noteId]);
            $folder = $note->getStudentFolder();
            $folderId = '';
            if($folder) {
                $folderId = $folder->getId();
            }
            $res = ['title' => $note->getTitle(), 'value' =>  $note->getValue(), 'folder' => $folderId];
            return new Response(
                json_encode($res)
            );
        } else {
            return new Response(
                'error'
            );
        }

    }
}
