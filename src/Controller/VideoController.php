<?php

namespace App\Controller;

use App\Entity\Video;
use App\Form\VideoType;
use App\Repository\ClasseRepository;
use App\Repository\FormationRepository;
use App\Repository\TypeUserRepository;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/{_locale}/video")
 */
class VideoController extends AbstractController
{
    /**
     * @Route("/", name="vide   o")
     */
    public function index()
    {
        return $this->render('video/index.html.twig', [
            'controller_name' => 'VideoController',
        ]);
    }
    /**
     * @Route("/live-videos", name="live_videos")
     */
    public function dashboardbooks(Request $request, VideoRepository $videoRepository)
    {
        $videos = $videoRepository->findAll();


        return $this->render('video/booksvideo.html.twig', [
            'videos' => $videos,
        ]);

    }
    /**
     * @Route("/SaveMail", name="save_mail")
     */
    public function ajaxSendMaillive(ObjectManager $manager, \Swift_Mailer $mailer, Request $request, TypeUserRepository $typeUserRepository,UserRepository $userRepository, FormationRepository $formationRepository)
    {
        $users          = $request->request->get('members');
        $usersClasse    = $request->request->get('classes');
        $usersFormation = $request->request->get('formations');
        $usersType      = $request->request->get('types');
        $form = $request->request->get('form');
        $video = new Video();
        $typeEmails = array();

        foreach ($form as $f){
            if($f['name'] == 'title')  {
                $video->setName($f['value'] );
            }
            if($f['name'] == 'description')  {
                $video->setDescription($f['value'] );
            }
            if($f['name'] == 'url')  {
                $video->setUrl($f['value'] );
            }
            if($f['name'] == 'date')  {
                $video->setDate(new \DateTime($f['value']));
            }
            if($f['name'] == 'hour')  {
                $video->setHour(new \DateTime($f['value']));
            }
            if($f['name'] == 'duration')  {
                $video->setDuration($f['value'] );
            }
        }

        $manager->persist($video);

        if(!empty($users)){
            //get users subscribed on this class
            $membersEmails = array_unique($users);
        }
        foreach ($membersEmails as $userEmail){
            $usersMails = $userRepository->findBy(['email' => $userEmail]);
            foreach ($usersMails as $user){
                $video->addUser($user);

            }
        }

        if(!empty($usersFormation)) {
            //get users subscribed on this formation
            foreach ($usersFormation as $formation){
                $usersForm = $userRepository->getEmailsByFormations($formation);
                $usersEmails = array_unique($usersForm, SORT_REGULAR);
            }
        }
        foreach ($usersEmails as $userFormation){
            $users = $userRepository->findBy(['email' => $userFormation]);
            foreach ($users as $user){
                $video->addUser($user);

            }
        }

        if(!empty($usersClasse)) {
            //get users subscribed on this class
            foreach ($usersClasse as $cl){
                $usersClass = $userRepository->getEmailsByClasses($cl);
                $classUsers = array_unique($usersClass, SORT_REGULAR);
            }
        }

        foreach ($classUsers as $userClasse){
            $usersClasses = $userRepository->findBy(['email' => $userClasse]);
            foreach ($usersClasses as $user){
                $video->addUser($user);

            }
        }

        if(!empty($usersType)){
            //get users by role
            foreach ($usersType as $userType){
                $user = $userRepository->findBy(['id' => $userType]);
                foreach ($user as $u){
                    $Email = $u->getEmail();
                    array_push($typeEmails, $Email);
                }
            }
            array_unique($typeEmails, SORT_REGULAR);
        }

        foreach ($typeEmails as $typeEmail){
            $usersTypes = $userRepository->findBy(['email' => $typeEmail]);
            foreach ($usersTypes as $user){
                $video->addUser($user);

            }
        }

        $manager->flush();

        $emails = array_merge($usersEmails, $membersEmails, $classUsers, $typeEmails);

        $title = $video->getName();
        $date = $video->getDate();
        $newDate = $date->format('d/m/Y');
        $description = $video->getDescription();
        $hour = $video->getHour();
        $newHour = $hour->format('h:i');
        $url = $video->getUrl();
        $videoId = $video->getId();
        $link = 'http://127.0.0.1:8000/student/Videosetting/' .$videoId;
        $message = (new \Swift_Message('Live : ' . $video->getName()))
            ->setFrom('mohamednheri@hotmail.com')
            ->setTo($emails)
            ->setBody(
                $this->renderView('emails/live.html.twig', ['title' => $title, 'date' => $newDate, 'hour' => $newHour, 'url' => $url, 'link' => $link,'description' => $description]), 'text/html'
            );
        $mailer->send($message);

        return new Response(
            'sucess'
        );
    }
    /**
     * @Route("/live-video", name="admin_live")
     */
    public function liveVideo(ObjectManager $manager, \Swift_Mailer $mailer, Request $request)
    {
        return $this->render('video/livevideo.html.twig',
            [

            ]
        );
    }
    /**
     * @Route("/getFormations", name="get_formations")
     */
    public function ajaxSendFormations(FormationRepository $formationRepository, UserRepository $userRepository, Request $request)
    {

        $formations = $formationRepository->findBy(['status' => '1']);

        $myFormations = [];

        foreach ($formations as $formation){
            $md['titre_formation']  = $formation->getTitreFormation();
            $md['id']  = $formation->getId();
            array_push($myFormations, $md);
        }

        /*$users = $userRepository->findBy(['active' => '1']);

        $myUsers = [];

        foreach ($users as $user) {
            $formationsupp = $user->getFormationsSup();
            foreach ($formationsupp as $s){
                $md['titre_formation'] = $s->getTitreFormation();
                $md['user'] = $s->getUser()->getEmail();
                array_push($myUsers, $md);
            }
        }*/

        $response = new JsonResponse($myFormations, 200);
        return $response;
    }
    /**
     * @Route("/getUsers", name="get_users")
     */
    public function ajaxSendUsers(UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findBy(['active' => '1']);

        $userResult = [];

        foreach ($users as $user){
            $md['username']  = $user->getUsername();
            $md['lastname']  = $user->getLastname();
            $md['email']  = $user->getEmail();
            array_push($userResult, $md);
        }
        $response = new JsonResponse($userResult, 200);
        return $response;
    }
    /**
     * @Route("/getClasses", name="get_classes")
     */
    public function ajaxSendClasses(ClasseRepository $classeRepository, Request $request)
    {
        $classes = $classeRepository->findAll();

        $myClasses = [];

        foreach ($classes as $classe){
            $md['name']  = $classe->getName();
            $md['id']  = $classe->getId();
            array_push($myClasses, $md);
        }


        $response = new JsonResponse($myClasses, 200);
        return $response;
    }
    /**
     * @Route("/getUsertype", name="get_type")
     */
    public function ajaxSendType(UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findBy(['active' => '1']);

        $myUsers = [];

        foreach ($users as $user){
            foreach($user->getRoles() as $role){
                if ($role == 'ROLE_STUDENT'){
                    $md['type'] = 'étudiant';
                }
                if ($role == 'ROLE_FORMATOR'){
                    $md['type'] = 'formateur';
                }
                if ($role == 'ROLE_EDITOR'){
                    $md['type'] = 'éditeur';
                }
            }
            $md['id'] = $user->getId();
            array_push($myUsers, $md);
        }
        $response = new JsonResponse($myUsers, 200);
        return $response;
    }
    /**
     * @Route("/updateVideo/{id}", name="update_video")
     */
    public function updateVideo(ObjectManager $objectManger, Request $request, VideoRepository $videoRepository, Video $video)
    {
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $objectManger->flush();

            $this->addFlash(
                'success',
                'Votre video a été mise à jour!'
            );
            return $this->redirectToRoute('admin_books');
        }
        return $this->render('video/update_video.html.twig', [
            'form' => $form->createView(),
            'video' => $video
        ]);

    }
    /**
     * @Route("/deleteVideo/{id}", name="delete_video")
     */
    public function deleteVideo(ObjectManager $entityManager, Request $request, $id, VideoRepository $videoRepository)
    {
        $current_video = $videoRepository->findOneBy(array('id' => $id));
        if($current_video) {
            $entityManager->remove($current_video);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Votre video a été suprimée avec succée!'
            );
            return $this->redirectToRoute('admin_books');
        }
    }
}
