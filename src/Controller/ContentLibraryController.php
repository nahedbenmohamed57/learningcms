<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Repository\ChapterRepository;
use App\Repository\CoursRepository;
use App\Repository\FormationRepository;
use App\Repository\ModuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Image;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Document;
use App\Entity\DocumentFolders;
use App\Repository\DocumentRepository;
use App\Repository\DocumentFoldersRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * @Route("{_locale}/admincontentlibrary")
 */
class ContentLibraryController extends AbstractController
{
    /**
     *@Route("/contentLibrary", name="content_library" , methods={"GET"})
     */
    public function contentLibrary(DocumentRepository $media, Request $request, DocumentFoldersRepository $folder, FormationRepository $formationRepository, ModuleRepository $moduleRepository, CoursRepository $coursRepository, ChapterRepository $chapterRepository) {
        //get all content for the curret user
        $user        = $this->getUser();
        $userMedia   = $media->findBy(array('user' => $user ));
        $search = $request->get('search');
        //get folders
        $folders   = $folder->findBy(array('user' => $user ), array('name' => 'ASC'));
        $arr = array_chunk($folders, 8);



        $formation = $formationRepository->findBy(['user' => $user]);

        $modules = $moduleRepository->findBy(['formation' => $formation]);
        if (!empty($modules)) {
            foreach ($modules as $module) {
                //get list cour
                $module->type = "modul";
                $module->cour = $coursRepository->findBy(['module' => $module]);
                if (!empty($module->cour)) {
                    foreach ($module->cour as $cour) {
                        $cour->chapters = $chapterRepository->findBy(['cours' => $cour]);
                    }
                }
            }
        }
        //get chapter with parent formation
        $chapter = $chapterRepository->findBy(['formation' => $formation]);
        //get cours with parent formation
        $cours = $coursRepository->findBy(['formation' => $formation]);
        if (!empty($cours)) {
            foreach ($cours as $cour) {
                $cour->chapters = $chapterRepository->findBy(['cours' => $cour]);
            }
        }
        //$search = $request->get('search');
        //$list = $media->search($search);

        return $this->render('admin/contentLibrary.html.twig', [
            'userMedia' => $userMedia,'folders' => $arr,'allfolders' => $folders, 'formation' => $formation,
            'modules' => $modules,
            'cours' => $cours,
            'chapters' => $chapter
        ]);
    }

    /**
     * @Route("/ajax/snippet/image/send", name="ajax_snippet_image_send")
     */
    public function ajaxSnippetImageSendAction(Request $request, ObjectManager $manager)
    {
        $user     = $this->getUser();
        $random   =  random_int(1, 200);
        $document = new Document();

        $media    = $request->files->get('file');
        $document->setFile($media);
        //concatenate the file name with a randomizer value to prevent duplicate file name registration
        $document->setName($random.''.$media->getClientOriginalName());
        $document->setUser($user);
        $document->setcreatedAt(new \DateTime);
        $document->setSize($media->getsize());

        $document->upload($random);
        $manager->persist($document);
        $manager->flush();
        $id = $document->getId();

        //infos sur le document envoyé
        //var_dump($request->files->get('file'));die;
        return new JsonResponse(array('success' => true,'id' => $id));

        $this->redirectToRoute('content_library');

    }

    /**
     * @Route("/updateFile", name="updat_file")
     */
    public function updateFile(Request $request, ObjectManager $entityManager, DocumentRepository $document, DocumentFoldersRepository $folder) {

        $mediaId   = $request->request->get('id');
        $foldersIds  = $request->request->get('foldersIds');
        $media     = $document->find($mediaId);

        if($foldersIds) {
            //add media to selected folders
            $folders    = explode(',', $foldersIds);

            $oldfolder  = $media->getDocumentFolders();
            $oldFolderIds = [];
            if($oldfolder) {
                foreach ($oldfolder as $key => $f) {
                    array_push($oldFolderIds, $f->getId());
                    if(!in_array($f->getId(), $folders)) {
                        //remove the folder for the current media
                        $currentFolder     = $folder->find($f->getId());
                        $media->removeDocumentFolder($currentFolder);
                    }
                }
            }
            foreach ($folders as $k => $folderId) {
                $currentFolder     = $folder->find($folderId);
                $media->addDocumentFolder($currentFolder);
            }
        }

        $media->setTitle($request->request->get('title'));
        $media->setAlternatifText($request->request->get('alternativText'));
        $media->setDescription($request->request->get('description'));
        $pic = $request->request->get('picture');
        $name = $request->request->get('name');
        $user = $this->getUser();
        $dir  = __DIR__.'/../../public/images/uploads/';

        $image_array_1  = explode(',', $pic);

        $image_array_2 =  explode(",", $image_array_1[1]);

        $data = base64_decode($image_array_2[0]);

        $imageName = realpath($dir). '\\' . $user->getId() . '\documents\\' .$name;


        //file_put_contents($imageName, $data);

        $media->setName($name);

        if ($media){
            //update file
            $entityManager->persist($media);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/addFolderDocument", name="add_folder_document")
     */
    public function addFolderDocument(Request $request, ObjectManager $entityManager) {
        $folderName = $request->request->get('folder_name');

        if($folderName) {
            $currentUser = $this->getUser();
            if($currentUser) {
                //add a new folder for the connected user
                $folder = new DocumentFolders();
                $folder->setName($folderName);
                $folder->setUser($currentUser);
                $entityManager->persist($folder);
                // actually executes the queries (i.e. the INSERT query)
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/updateFolderDoc", name="update_folder_doc")
     */
    public function updateFolderDoc(Request $request, ObjectManager $entityManager, DocumentFoldersRepository $Folder) {

        $folderName = $request->request->get('folder_name');
        $folderId   = $request->request->get('id');
        $folder     = $Folder->find($folderId);

        if($folder) {
            $folder->setName($folderName);
            $entityManager->persist($folder);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/deleteFolderDoc", name="delete_folder_doc")
     */
    public function deleteFolderDoc(Request $request, ObjectManager $entityManager, DocumentFoldersRepository $Folder) {
        $folderId   = $request->request->get('id');
        $folder     = $Folder->find($folderId);
        if($folder) {
            $entityManager->remove($folder);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/deleteFiles", name="delete_files")
     */
    public function deleteFiles(ObjectManager $entityManager, Request $request, DocumentRepository $document) {
        $files = $request->request->get('files');

        if(!empty($files)) {
            foreach ($files as $key => $file) {
                $currentFile = $document->find($file);
                //delete file from server
                $user = $this->getUser();
                $dir  = __DIR__.'/../../public/images/uploads/';

                $imagePath = realpath($dir).'\\'.$user->getId().'\documents\\'.$currentFile->getName();

                if (file_exists($imagePath) )
                {
                    unlink ( $imagePath );
                }
                //remove file from database
                $entityManager->remove($currentFile);
                $entityManager->flush();

            }
            return new Response(
                'success'
            );
        }

    }
    /**
     * download the selected file
     * @Route("/downloadFile/{id}", name="download_file")
     */
    public function downloadFile(ObjectManager $manager, DocumentRepository $documentRepository, UploaderHelper $uploaderHelper, $id)
    {
        $document = $documentRepository->find($id);
        if (!$document) {
            $array = array('statuts' => 0,
                'message' => 'Fichier n \ existe pas'
            );
            $response = new JsonResponse($array, 200);
            return $response;
        }
        // Provide a name for your file with extension
        $filename = 'Fichier.doc';

        // The dinamically created content of the file
        $fileContent = $document;

        // Return a response with a specific content
        $response = new Response($fileContent);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);

        // Dispatch request
        return $response;
    }
}