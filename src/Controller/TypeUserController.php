<?php

namespace App\Controller;

use App\Entity\TypeUser;
use App\Form\UserType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("{_locale}/admin/CreateUser/")
 */
class TypeUserController extends AbstractController
{
    /**
     * @Route("createTypeUser", name="new_type_user")
     */
    public function createTypeUser(ObjectManager $manager, Request $request)
    {
        $typeUser = new TypeUser();
        $form = $this->createForm(UserType::class, $typeUser);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($typeUser);

            $manager->flush();

            return $this->redirectToRoute('new_type_step2');

        }

        return $this->render('type_user/step1.html.twig',[
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("type/step2", name="new_type_step2")
     */
    public function step2()
    {
        return $this->render('type_user/step2.html.twig',['toggled' => true]);
    }
    /**
     * @Route("type/step3", name="new_type_step3")
     */
    public function step3()
    {
        return $this->render('type_user/step3.html.twig',['toggled' => true]);
    }
    /**
     * @Route("type/step4", name="new_type_step4")
     */
    public function step4()
    {
        return $this->render('type_user/step4.html.twig',['toggled' => true]);
    }
}
