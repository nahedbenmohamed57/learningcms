<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("{_locale}/statisticalstudent")
 */

class StatisticalController extends AbstractController
{
    /**
     * @Route("/statistical", name="statistical")
     */
    public function index()
    {
        return $this->render('statistical/index.html.twig', [
            'controller_name' => 'StatisticalController',
        ]);
    }
}
