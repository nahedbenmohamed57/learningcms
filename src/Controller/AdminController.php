<?php

namespace App\Controller;

use App\Entity\Video;
use App\Form\FormationType;
use App\Form\RegistrationType;
use App\Form\VideoType;
use App\Repository\deleteRepository;
use App\Repository\FormationRepository;
use App\Repository\TypeUserRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Document;
use App\Entity\DocumentFolders;
use App\Repository\DocumentRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Form\ProfileType;


/**
 * @Route("/{_locale}/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin_global_menu")
     */
    public function index()
    {
        return $this->render('admin/global_menu.html.twig');
    }

    /**
     * @Route("/dashboardAdmin", name="admin_dashboard")
     */
    public function dashboardAdmin()
    {
        return $this->render('admin/dashboard_admin.html.twig');
    }

    /**dashboardAdmin
     * @Route("/users/addRole/{id}", name="add_role")
     */
    public function addRole(User $user, ObjectManager $manager)
    {
        //add admin role
        if ($user) {
            $user->addRole("ROLE_ADMIN");
            $manager->persist($user);
            $manager->flush();
        }
        return $this->redirectToRoute('home_page');
    }
    /*public function getRoleName($role = nulle) {
        if($role == 'ROLE_USER' ) {
            return 'utilisateur';
        } else {
            return 'Administrateur';
        }
    }*/

    /**
     * @Route("/getAllUser", name="get_list_users")
     */
    public function getAllUser()
    {
        return new JsonResponse(array('data' => 'nahed'));
    }

    /**
     * @Route("/config", name="admin_config")
     */
    public function config()
    {
        return $this->render('admin/config.html.twig');
    }

    /**
     * @Route("forums", name="admin_forums")
     */
    public function dashboardforum()
    {
        return $this->render('admin/forum_admin.html.twig');
    }

    /**
     * @Route("/statistical", name="admin_statistical")
     */
    public function dashboardstatistical()
    {
        return $this->render('admin/statistical_admin.html.twig');
    }

    /**
     * @Route("/planning", name="admin_planning")
     */
    public function dashboardplanning()
    {
        return $this->render('admin/planings_admin.html.twig');
    }

    /**
     * @Route("/awards", name="admin_awards")
     */
    public function dashboardawards()
    {
        return $this->render('admin/awards_admin.html.twig');
    }

    /**
     * @Route("/messaging", name="admin_messagerie")
     */
    public function dashboardmessaging()
    {
        return $this->render('admin/messagerie.html.twig');
    }


    /**
     * @Route("/rewards", name="admin_rewards")
     */
    public function rewards()
    {
        return $this->render('admin/rewards.html.twig');
    }

    /**
     * @Route("/planing", name="admin_planing")
     */
    public function planing()
    {
        return $this->render('admin/planing.html.twig');
    }

    /**
     * @Route("/profil", name="admin_profil")
     */
    public function profil(ObjectManager $manager, Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository)
    {

        $user = $this->getUser();
        $roles = $user->getRoles();
        $roleUser = 'user';
        if (in_array('ROLE_ADMIN', $roles)) {
            $roleUser = 'admin';
        }
        $pass = $user->getPassword();
        $user->setUpdatedAt(new \DateTime);
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);
        $gender = $user->getgender();
        if ($form->isSubmitted() && $form->isValid()) {

            $oldPassword = $request->request->get('profile')['password'];
            $gender = $request->request->get('gender');
            $user->setGender($gender);
            $user->setPassword($pass);

            if ($oldPassword != "") {
                //modification de mot de passe
                if ($encoder->isPasswordValid($user, $oldPassword)) {
                    //if the same passord
                    $newEncodedPassword = $encoder->encodePassword($user, $user->getNewPassword());
                    $user->setPassword($newEncodedPassword);
                    $this->addFlash(
                        'success',
                        'Modification effectué avec succées!'
                    );

                } else {
                    $this->addFlash('error', 'Ancien mot de passe incorrect !');
                }
            }

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('admin_profil');
        }

        return $this->render('admin/profil.html.twig', [
            'formUser' => $form->createView(), 'gender' => $gender, 'roleUser' => $roleUser
        ]);

        //return $this->render('admin/profil.html.twig');
    }

    /**
     * @Route("/messagerie", name="admin_messagerie")
     */
    public function messagerie()
    {
        return $this->render('admin/messagerie.html.twig');
    }

    /**
     * @Route("/forums", name="admin_forums")
     */
    public function forums()
    {
        return $this->render('admin/forums.html.twig');
    }

    /**
     * @Route("/code", name="admin_code")
     */
    public function code()
    {
        return $this->render('admin/code.html.twig');
    }

    /**
     * function to test mails
     * @Route("/showMail", name="showMail")
     */
    public function showMail()
    {
        return $this->render('emails/confirmation.html.twig', ['name' => '', 'link' => '']);
    }

    /**
     * @Route("/testcode", name="testcode")
     */
    public function testselect2()
    {
        return $this->render('user/testselect2.html.twig');
    }

    /**
     * function to test mails
     * @Route("/showMaillampe", name="showMaillampe")
     */
    public function showMaillampe()
    {
        return $this->render('emails/confirmationlamp.html.twig', ['name' => '', 'corps' => '']);
    }

    /**
     * @Route("/sendlampe", name="sendlampe")
     */
    public function sendlampe(Request $request, \Swift_Mailer $mailer, UserRepository $userRepositoryser)
    {
        if ($data = $request->get('sendnotelamp')) {
            $jsonData = $request->get('sendnotelamp');
            $user = $this->getUser();
            $userRepositoryser = $userRepositoryser->findOneBy((['id' => $user]));
            $cpbody = "Suggestion reçu sur Kiwi institute";
            $name = $user->getUsername();
            $message = (new \Swift_Message($jsonData[0]))
                ->setFrom('informatique@kiwi-institute.com')
                ->setTo($user->getemail())
                ->setBody(
                    $this->renderView('emails/confirmationlamp.html.twig', ['name' => $name, 'corps' => $jsonData[0]]), 'text/html'
                );
            $mailer->send($message);
            return $this->redirectToRoute('admin_dashboard');
        }

    }

    /**
     * @Route("/testselect2", name="testselect2")
     */
    public function testselects2()
    {
        return $this->render('testfile.html.twig');
    }
}
