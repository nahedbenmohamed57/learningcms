<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CalendarsettingController extends AbstractController
{
    /**
     * @Route("{_locale}/student/calendarsetting", name="calendarsetting")
     */
    public function index()
    {
        return $this->render('calendarsetting/index.html.twig', [
            'controller_name' => 'CalendarsettingController',
        ]);
    }
}
