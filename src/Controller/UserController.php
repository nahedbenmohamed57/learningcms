<?php
namespace App\Controller;

use App\Entity\delete;
use App\Entity\Contact;
use App\Entity\Classe;
use App\Form\ClasseType;
use App\Form\ContactType;
use App\Repository\ClasseRepository;
use App\Repository\FormationRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/{_locale}/admin/")
 */
class UserController extends AbstractController
{
    /**
     * List all type of users and allow admin to add new users
     * @Route("typeUser", name="type_user")
     */
    public function typeUser() {

        return $this->render('user/type_user.html.twig',['toggled' => true]);
    }
    /**
     * @Route("membres", name="membres")
     * @Route("membres/{classId}", name="membres")
     */
    public function membres($classId = null, UserRepository $usersRepo, Request $request, ObjectManager $manager, ClasseRepository $classeRepository, FormationRepository $formationRepository) {

        //to optimize this fucntion get user by class and remove the test in the view
        $users     = $usersRepo->findAll();
        $listClass = $classeRepository->findAll();
        $formations = $formationRepository->findAll();
        $currentCalss = $classeRepository->findOneBy(['id' => $classId]);
        $class = new Classe();
        $form  = $this->createForm(ClasseType::class, $class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($class);

            $manager->flush();

            return $this->redirectToRoute('membres',['classId' => $classId]);
        }
        return $this->render('user/type_member.html.twig', ['users' => $users,'listClass' => $listClass, 'currentCalss' => $currentCalss,'formations' => $formations, 'form' => $form->createView()]);
    }

    /**
     * Affact user to a specific class
     * @Route("affectUserClass/", name="affect_user_class")
     */
    public function affectUserClass(Request $request, ObjectManager $manager, UserRepository $userRepo, ClasseRepository $classeRepository) {

        $userId  =  $request->get('userId');

        $classIds =  $request->get('selectedClass');
        $user  = $userRepo->findOneBy(['id' => $userId ]);

        if(!empty($classIds)) {
            foreach ($classIds as $classId) {
                $newClass = $classeRepository->findOneBy(array('id' => $classId));
                if($newClass) {
                    //affect the class for the new user
                    $user->addClasse($newClass);
                    $manager->persist($user);
                    $manager->flush();
                }
            }
        } else {
            $classes = $classeRepository->findAll();
            foreach ($classes as $class) {
                $user->removeClasse($class);
                $manager->persist($user);
                $manager->flush();
            }
        }

        return new Response(
            'success'
        );

    }

    /**
     * @Route("addMembers/", name="add_members")
     */
    public function addMembers(ClasseRepository $classeRepository, FormationRepository $formationRepository ) {

        $formations = $formationRepository->findAll();
        $classes    = $classeRepository->findAll();
        return $this->render('user/add_member.html.twig', [
            'formations' => $formations,
            'classes'    => $classes,
        ]);
    }

    /**
     * @Route("Code", name="code")
     */
    public function codes() {

        return $this->render('user/code.html.twig',['toggled' => true]);
    }
    /**
     * get list of users acoording to the given ruls if the param role is empty return list off all users
     * @Route("getUsers/{role}", name="users_list")
     */
    public function getUsers(UserRepository $repo, $role = null) {
        if($role) {
            $users = $repo->findByRole($role);
        } else {
            $users = $repo->findAll();
        }

        /*switch ($role) {
            case "1": $type = "admin";
            case "2": $type = "user";
            case "3": $type = "editor";
            case "4": $type = "coach";
            case "5" : $type = "formator";
            default: $type = "user";
        }*/
        //get list of all users

        return $this->render('user/list-user.html.twig',['users' => $users]);
    }

    /**
     * @Route("updateCounter", name="update_counter")
     */
    public function updateCounter(Request $request, ObjectManager $manager, UserRepository $repo) {
        $counter      = $request->request->get('counter');
        $current_user = $this->getUser();
        if($current_user) {
            $oldCounter = $current_user->getCounter();
            $tmstamp = strtotime($oldCounter) + strtotime($counter);
            $current_user->setCounter(date("H:m:s", $tmstamp));
            $manager->persist( $current_user);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * List all type of users and allow admin to add new users
     * @Route("invitationcode", name="admin_invitationcode")
     */
    public function typeInvitationcode() {

        return $this->render('user/invitationcode.html.twig',['toggled' => true]);
    }
    /**
     * List all type of users and allow admin to add new users
     * @Route("userclass", name="admin_userclass")
     */
    public function typeuserclass() {

        return $this->render('user/userclass.html.twig',['toggled' => true]);
    }


    /**
     * List all type of users and allow admin to add new users
     * @Route("Member", name="admin_member")
     */
    public function typeMember($classId = null, UserRepository $usersRepo, Request $request, ObjectManager $manager, ClasseRepository $classeRepository) {
        //to optimize this fucntion get user by class and remove the test in the view
        $users     = $usersRepo->findAll();
        $listClass = $classeRepository->findAll();
        $currentCalss = $classeRepository->findOneBy(['id' => $classId]);
        $class = new Classe();
        $form  = $this->createForm(ClasseType::class, $class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($class);

            $manager->flush();

            return $this->redirectToRoute('membres',['classId' => $classId]);
        }
        return $this->render('user/type_member.html.twig', ['users' => $users,'listClass' => $listClass, 'currentCalss' => $currentCalss, 'form' => $form->createView()]);
        //return $this->render('user/type_member.html.twig',['toggled' => true]);
    }
    /**
     * @Route("Contact", name="contact")
     */
    public function contact(ObjectManager $manager, Request $request, \Swift_Mailer $mailer) {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($contact);

            $manager->flush();

            $message = (new \Swift_Message('Bonjour !'))
                ->setFrom($contact->getMail())
                ->setTo('informatique@kiwi-ins  titute.com')
                ->setBody(
                    $contact->getMessage());
            $mailer->send($message);
            return $this->redirectToRoute('type_user');
        }
        return $this->render('contact/contact_admin.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Affect users to a specific classes
     * @Route("affectUsersClasses/", name="affect_users_classes")
     */
    public function affectUsersClasses(Request $request, ObjectManager $manager, UserRepository $userRepo, ClasseRepository $classeRepository) {
        $usersIds  =  $request->get('userIds');
        $uselectedClass =  $request->get('selectedClass');
        foreach ($usersIds as $userId) {
            $user  = $userRepo->findOneBy(['id' => $userId ]);
            foreach ($uselectedClass as $classId) {
                $newClass = $classeRepository->findOneBy(array('id' => $classId));
                if($newClass) {
                    //affect the class for the new user
                    $user->addClasse($newClass);
                    $manager->persist($user);
                    $manager->flush();
                }
            }
        }

        return new Response(
            'success'
        );

    }

    /**
     * Affect users to a specific formations
     * @Route("affectUsersFormations", name="affect_users_formation")
     */
    public function affectUsersFormations(Request $request, ObjectManager $manager, UserRepository $userRepo, FormationRepository $formationRepository) {

        $usersIds  =  $request->get('userIds');
        $selectedFormation =  $request->get('selectedFormation');
        foreach ($usersIds as $userId) {
            $user  = $userRepo->findOneBy(['id' => $userId ]);
            foreach ($selectedFormation as $formationId) {
                $newFormation = $formationRepository->findOneBy(array('id' => $formationId));
                if($newFormation) {
                    //affect the class for the new user
                    $user->addFormationsSup($newFormation);
                    $manager->persist($user);
                    $manager->flush();
                }
            }
        }

        return new Response(
            'success'
        );

    }

    /**
     * @Route("kitKiwi", name="kit_kiwi")
     */
    public function kitKiwi() {
        return $this->render('admin/kit_Kiwi.html.twig');
    }
}