<?php

namespace App\Controller;

use App\Entity\Messagerie;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

class MessagerieController extends AbstractController
{
    /**
     * @Route("{_locale}/messagerie", name="messagerie")
     */
    public function addmessagerie( ObjectManager $manager,Request $request,\Swift_Mailer $mailer,UserRepository $userRepositoryser)
    {
        if ($data = $request->get('sendmessagetable')) {
            $jsonData = $request->get('sendmessagetable');
            $message=new Messagerie();

            $message->setName($jsonData[0]);

            $message->setMessage(strip_tags($jsonData[1]));
            $manager->persist($message);
            $manager->flush();
            $user = $this->getUser();
            $userRepositoryser=$userRepositoryser->findOneBy((['id' => $user]));

            $cpbody = trim(preg_replace('/(&nbsp;)+|\s\K\s+/','',$jsonData[1]));

            $messages = (new \Swift_Message(  $jsonData[0]))
                ->setFrom('informatique@kiwi-institute.com')
                ->setTo($userRepositoryser->getEmail())
                ->setBody(
                    $cpbody, 'text/html') ;
            $mailer->send($messages);
            return $this->redirectToRoute('dashboard_student');
        }
    }

}
