<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfileType;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\Session;
class AuthenticationController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index() {
        $user  = $this->getUser();
       if($user) {
           $roles = $user->getRoles();
           if($user->getActive() == 0) {
               //redirect to index page confirm_inscription
              // return $this->redirectToRoute('confirm_inscription');
               return $this->redirectToRoute('login_form');
           }
           if(in_array('ROLE_ADMIN', $roles)) {
               //if user has role admin redirect him to Videosetting admin
               return $this->redirectToRoute('admin_global_menu');
           } else if(in_array('ROLE_USER', $roles)) {
               //if user has role user redirect him to Videosetting student
               return $this->redirectToRoute('dashboard_student');
           }
       } else {
           return $this->redirectToRoute('login_form');
       }

    }

    /**
     * @Route("/inscription", name="registration")
     */
    public function registration(ObjectManager $manager, Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $user = new User();
        $user->setCreatedAT(new \DateTime)
            ->setUpdatedAt(new \DateTime)
            ->setActive(0);
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            //encoder le mot de passe
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);

            $manager->flush();

            //send confirmation mail
            $name = $user->getUsername();
            //update this link when the website is online
            $link = 'http://lms.kiwi-institute.com/login?email='.$user->getemail();
            $message = (new \Swift_Message('Confirmation d\'inscription!'))
                ->setFrom('informatique@kiwi-institute.com')
                ->setTo($user->getemail())
                ->setBody(
                    $this->renderView('emails/confirmation.html.twig',   ['name' => $name, 'link' => $link])  ,'text/html'
                ) ;
            $mailer->send($message);
            return $this->redirectToRoute('home_page');
        }

        return $this->render('authentication/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="login_form")
     */
    public function connexion(UserRepository $repo, ObjectManager $manager,AuthenticationUtils $authenticationUtils) {

        if(isset($_GET['email']) && !empty($_GET['email'])) {
            //when user open this link from the email we set active to 1 and the user have the chose to connect or not
            $email  = $_GET['email'];
            $user   = $repo
                ->findOneByEmail($email);
            if($user) {
                $user->setActive(1);
                $manager->persist($user);
                $manager->flush();
            }
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        //if the user is a student redirect him to the Videosetting student
        //return $this->render('authentication/login.html.twig',['error' => $error]);
        return $this->render('authentication/login_kiwi.html.twig',['error' => $error]);
    }

    /**
     * @Route("/logout", name="logout_form")
     */
    public function logout() {}

    /**
     * @Route("/confirmation", name="confirm_inscription")
     */
    public function confirmInscription() {
        return $this->render('authentication/confirm_inscription.html.twig');
    }

    /**
     * @Route("/forgottenPassword", name="app_forgotten_password")
     */
    public function forgottenPassword(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator, ObjectManager $entityManager) {

        if ($request->isMethod('POST')) {
            $email = $request->request->get('email');
            $user = $entityManager->getRepository(User::class)->findOneByEmail($email);
            /* @var $user User */
            if ($user === null) {
                $this->addFlash('danger', 'Email Inconnu');
                return $this->redirectToRoute('app_forgotten_password');
            }
            $token = $tokenGenerator->generateToken();
            try{
                $user->setResetToken($token);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
               return $this->redirectToRoute('app_forgotten_password');
            }
            $url = $this->generateUrl('app_reset_password', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);

            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('informatique@kiwi-institute.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView('emails/reset.html.twig',   ['url' => $url])  ,'text/html'
                ) ;
                /*->setBody(
                    "Voici le token pour reseter votre mot de passe : " . $url,
                    'text/html'
                );*/
            $mailer->send($message);
            $this->addFlash('notice', 'Mail envoyé');

            return $this->redirectToRoute('app_forgotten_password');
        }

        return $this->render('authentication/forget_password.html.twig');
    }

    /**
    * @Route("/reset_password/{token}", name="app_reset_password")
    */
    public function resetPassword(Request $request, string $token, UserPasswordEncoderInterface $passwordEncoder)
    {

        if ($request->isMethod('POST')) {
            $entityManager = $this->getDoctrine()->getManager();

            $user = $entityManager->getRepository(User::class)->findOneByResetToken($token);
            /* @var $user User */

            if ($user === null) {
                $this->addFlash('danger', 'Token Inconnu');
                return $this->redirectToRoute('login_form');
            }

            $user->setResetToken(null);
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $entityManager->flush();

            $this->addFlash('notice', 'Mot de passe mis à jour');

            return $this->redirectToRoute('login_form');
        }else {

            return $this->render('authentication/reset_password.html.twig', ['token' => $token]);
        }

    }

    /**
     *@Route("/deleteUser/{id}", name="delete_user")
     */
    public function deleteUser($id, ObjectManager $manager) {
        $currentUserId = $this->getUser()->getId();
        $repository = $this->getDoctrine()->getRepository(User::class);

        $user = $repository->find($id);
        if ($currentUserId == $id)
        {
            //delete the user session
            $session = $this->get('session');
            $session = new Session();
            $session->invalidate();
            //delete all user account from database
            if($user) {
                $manager->remove($user);
                $manager->flush();
            }
            return $this->redirectToRoute('login_form');

        } else {
            //only delete user action do by admin
            if($user) {
                $manager->remove($user);
                $manager->flush();
            }
            //return the admin to hear Videosetting not exist yet
        }

    }

    /**
     * @Route("addRole/{id}", name="add_role")
     */
    public function addRole(User $user, ObjectManager $manager) {
        //add admin role
        $user->addRole("ROLE_ADMIN");
        $manager->persist($user);
        $manager->flush();

        //return $this->render('authentication/profil_users.html.twig',['user' => $user]);
    }

    /**
     * This function allow user to update thier password before conected on the plateforme
     * @Route("/inscriptionUser", name="inscription_form")
     */
    public function inscriptionUser(UserRepository $repo, Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $manager,AuthenticationUtils $authenticationUtils) {

        if(isset($_GET['email']) && !empty($_GET['email'])) {

            //when user open this link from the email we set active to 1 and the user have the chose to connect or not
            $email = $_GET['email'];
            $user  = $repo->findOneByEmail($email);
            if($user) {
                if($user->getActive()) {
                    return $this->redirectToRoute('login_form');
                } else {
                    $form = $this->createFormBuilder()
                        ->add('password', RepeatedType::class, [
                            'type' => PasswordType::class,
                            'invalid_message' => 'Vous n\'avez pas tapé le même mot de passe',
                            'options' => ['attr' => ['class' => 'form-control']],
                            'required' => true,
                            'first_options'  => ['label' => 'Mot de Passe'],
                            'second_options' => ['label' => 'Confirmer Mot de passe']])
                        ->getForm();
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid() && !empty($user)) {
                        $data = $form->getData();
                        $hash = $encoder->encodePassword($user, $data['password']);
                        $user->setPassword($hash);
                        $user->setActive(1);
                        $manager->persist($user);
                        $manager->flush();
                        return $this->redirectToRoute('home_page');
                    }
                    return $this->render('authentication/registration_user.html.twig',[
                        'form' => $form->createView(),
                    ]);
                }
                $error = $authenticationUtils->getLastAuthenticationError();
                //if the user is a student redirect him to the Videosetting student
                return $this->render('authentication/registration_user.html.twig',[
                    'error' => $error,
                ]);
            } else {
                return $this->redirectToRoute('login_form');
            }
        }

    }

    /**
     * @Route("legal_notice", name="legal_notice")
     */
    public function leganotice() {
        return $this->render('authentication/legal_notice.html.twig');
    }

    /**
     * @Route("dataProtection", name="data_protection")
     */
    public function dataProtection() {
        return $this->render('authentication/data_protection.html.twig');
    }
}