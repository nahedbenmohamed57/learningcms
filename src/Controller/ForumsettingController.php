<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("{_locale}/forumsettiengs")
 */
class ForumsettingController extends AbstractController
{
    /**
     * @Route("/forum", name="forum")
     */
    public function index()
    {
        return $this->render('forumsetting/index.html.twig', [
            'controller_name' => 'ForumsettingController',
        ]);
    }
}
