<?php


namespace App\Controller;

use App\Entity\Badge;
use App\Entity\Category;
use App\Entity\delete;
use App\Entity\Code;
use App\Entity\Configchapitre;
use App\Entity\Configcours;
use App\Entity\NewUser;
use App\Entity\Chapters;
use App\Entity\Configmodule;
use App\Entity\Cours;
use App\Entity\Module;
use App\Entity\Noteformation;
use App\Entity\Quiz;
use App\Entity\User;
use App\Entity\Userchapter;
use App\Entity\UserFormation;
use App\Entity\UserClass;
use App\Form\BadgeType;
use App\Form\CategoryType;
use App\Form\CodeType;
use App\Form\NewUserType;
use App\Form\ProfileType;
use App\Form\RegistrationType;
use App\Form\UserType;
use App\Repository\BadgeRepository;
use App\Repository\CatalogueRepository;
use App\Repository\CategoryRepository;
use App\Repository\ChapterRepository;
use App\Repository\deleteRepository;
use App\Repository\CodeRepository;
use App\Repository\ConfigchaiptreRepository;
use App\Repository\CoursRepository;
use App\Repository\FormationconfigRepository;
use App\Repository\ModuleRepository;
use App\Repository\NewUserRepository;
use App\Entity\Formation;
use App\Entity\Formationconfig;
use App\Form\FormationconfigType;
use App\Form\FormationType;
use App\Repository\DocumentRepository;
use App\Repository\FormationRepository;
use App\Repository\QuizRepository;
use App\Repository\UserFormationRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use http\Exception\BadHeaderException;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use DeepCopy\DeepCopy;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Constraints\Date;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * @Route("{_locale}/formation/")
 */
class FormationsController extends AbstractController
{
    /**
     * @Route("step1", name="new_formation_step1")
     * @Route("step1/{formationId}", name="new_formation_update")
     */
    public function step1(ObjectManager $manager, Request $request, DocumentRepository $media, CategoryRepository $categoryRepository, FormationRepository $formationRepository, $formationId = null)
    {
        $categories = $categoryRepository->findAll();

        $user = $this->getUser();
        $userMedia = $media->findBy(array('user' => $user));
        if ($formationId != "") {
            $formationRepository = $formationRepository->find($formationId);
            $formationidupdate = $formationRepository->getId();
        }
        if (!empty($formationidupdate)) {
            $formation = $formationRepository;
        } else {
            $formation = new Formation();
            $formation->setcreatedAt(new \DateTime);
        }
        $form = $this->createForm(FormationType::class, $formation);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if($request->request->get('categs')){
                foreach ($request->request->get('categs') as $categorie) {
                    $formation->addCategorie($categorie);
                }
            }
            $formation->setUpadatedate(new \DateTime);
            $formation->setUser($user);
            $manager->persist($formation);
            $manager->flush();
            return $this->redirectToRoute('new_formation_step2', ['formationId' => $formation->getId()]);

        }

        return $this->render('formations/step_1.html.twig', [
            'form' => $form->createView(),
            'toggled' => true,
            'userMedia' => $userMedia,
            'categories' => $categories,
            'formationId' => $formationId,
        ]);
    }

    /**
     * @Route("step2/{formationId}", name="new_formation_step2")
     */
    public function step2(ObjectManager $manager, Request $request, FormationRepository $formationRepository, FormationconfigRepository $formationstep2update,  CoursRepository $coursRepository, ModuleRepository $moduleRepository, ChapterRepository $chapterRepository,ConfigchaiptreRepository $configchaiptreRepository, $formationId)
    {
        $user = $this->getUser();
        $findformationbyuser=$formationRepository->findBy(array('user' => $user),array('titre_formation' => 'ASC'));
        $formationRepository = $formationRepository->find($formationId);
        $nameformation = $formationRepository->getTitreFormation();
        $config = $formationRepository->getConfigformation();
        if (!empty($config)) {
            $configFormation = $formationstep2update->findOneBy(array('id' => $config->getId()));
        } else {
            $configFormation = new Formationconfig();
        }
        $forms = $this->createForm(FormationconfigType::class, $configFormation);
        $forms->handleRequest($request);
        if ($forms->isSubmitted()) {
            $formationRepository->setConfigformation($configFormation);
            $manager->persist($configFormation);
            $manager->flush();
            return $this->redirectToRoute('new_formation_step3', ['formationId' => $formationId]);
        }
        if (!empty($findformationbyuser)) {

            $modules = $moduleRepository->findBy(['formation' => $formationId]);
            if (!empty($modules)) {
                foreach ($modules as $module) {
                    $module->type = "modul";
                    $module->cour = $coursRepository->findBy(['module' => $module]);
                    if (!empty($module->cour)) {
                        foreach ($module->cour as $cour) {
                            $cour->chapters = $chapterRepository->findBy(['cours' => $cour]);
                        }
                    }
                }
            }
            $chapter = $chapterRepository->findBy(['formation' => $formationId]);
            $cours = $coursRepository->findBy(['formation' => $formationId]);
            if (!empty($cours)) {
                foreach ($cours as $cour) {
                    $cour->chapters = $chapterRepository->findBy(['cours' => $cour]);
                }
            }
        }
        $config= $configchaiptreRepository->findAll();
        if (!empty($config)) {
            foreach ($config as $jointures => $val) {
                $config = $val->getChoiceChapter();
            }
        }
        if ($request->get('sendnodename')) {
            $jsonData = $request->get('sendnodename');

            $addnote= new Noteformation();


            if (isset($jsonData[0]['titlename'])) {

                $addnote->setTitle(($jsonData[0]['titlename']));
            }

            if (isset($jsonData[0]['comment'])) {
                $addnote->setComment(($jsonData[0]['comment']));
            }
            if (isset($jsonData[0]['note'])) {
                $addnote->setNote(($jsonData[0]['note']));
            }

            $addnote->setFormation($formationRepository);
            $manager->persist($addnote);
            $manager->flush();


        }
        return $this->render('formations/step_2.html.twig', [
            'form' => $forms->createView(),
            'toggled' => true,
            'nameformation' => $nameformation,
            'formation' => $formationRepository,
            'modules' => $modules,
            'cours' => $cours,
            'chapters' => $chapter,
            'formations'=>$findformationbyuser,
            'idformation'=>$formationId,
            'namedevoir'=>$config,
        ]);
    }
    /**
     * @Route("step3/{formationId}", name="new_formation_step3")
     */
    public function step3($formationId, Request $request, ObjectManager $manager, FormationRepository $formationRepository, DocumentRepository $media ,ModuleRepository $moduleRepository, CoursRepository $coursRepository, ChapterRepository $chapterRepository, QuizRepository $quizRepository)
    {
        $formationDetails = $formationRepository->find($formationId);
        $nameformation    = $formationDetails->getTitreFormation();
        $user             = $this->getUser();
        $userMedia        = $media->findBy(array('user' => $user));
        $devoirs          = $media->findAll();

        if ($request->get('moduleconfig_forms')) {
            $jsonData = $request->get('moduleconfig_forms');

            $jsonData = json_decode($jsonData);

            $formationDetails->setJsonData($jsonData);
            foreach (array_filter($jsonData) as $k => $node) {
                //first level
                if($node->type == 'module') {
                    //save module
                    if(isset($node->iddb)) {
                        $module = $moduleRepository->findOneBy(['id' => $node->iddb]);
                    } else {
                        $module = new Module();
                    }

                    $module->setName($node->name);
                    $module->setPid($node->pid);
                    $module->setChartId($node->id);
                    $module->setStatus('1');
                    $module->setFormation($formationDetails);
                    if(!isset($node->iddb)) {
                        $manager->persist($module);
                    }
                    $manager->flush();
                    //find module childs
                    if(isset($node->child)  && count($node->child) > 0) {
                        foreach (array_filter($node->child) as $item) {
                            if(!isset($item->type)) dd($item);
                            if($item->type == 'Cours') {
                                //save cour
                                if(isset($item->iddb)) {
                                    $cours = $coursRepository->findOneBy(['id' => $item->iddb]);
                                } else {
                                    $cours = new Cours();
                                }

                                $cours->setName($item->name);
                                $cours->setPid($item->pid);
                                $cours->setChartId($item->id);
                                $cours->setStatus('1');
                                $cours->setModule($module);
                                if(!isset($item->iddb)) {
                                    $manager->persist($cours);
                                }
                                $manager->flush();
                                if(isset($item->child) && count($item->child) > 0) {
                                    //save chapter for this cursus
                                    foreach ($item->child as $lastItem) {
                                        if($lastItem->type == 'Chap') {
                                            if(isset($lastItem->iddb)) {
                                                $chap = $chapterRepository->findOneBy(['id' => $lastItem->iddb]);
                                            } else {
                                                $chap = new Chapters();
                                            }
                                            $chap->setName($lastItem->name);
                                            $chap->setPid($lastItem->pid);
                                            $chap->setStatus('1');
                                            $chap->setChartId($lastItem->id);
                                            $chap->setCours($cours);
                                            if(!isset($lastItem->iddb)) {
                                                $manager->persist($chap);
                                            }
                                            $manager->flush();

                                            if(isset($lastItem->child) && count($lastItem->child) > 0) {
                                                //chapter has quiz
                                                foreach (array_filter($lastItem->child) as $lastItem2) {
                                                    if($lastItem2->type == 'quiz') {
                                                        if(isset($lastItem2->iddb)) {
                                                            $quiz = $quizRepository->findOneBy(['id' => $lastItem2->iddb]);
                                                        } else {
                                                            $quiz = new Quiz();
                                                        }

                                                        $quiz->setName($lastItem2->name);
                                                        $quiz->setPid($lastItem2->pid);
                                                        $quiz->setChartId($lastItem2->id);
                                                        $quiz->setStatus('1');
                                                        $quiz->setChapter($chap);
                                                        if(!isset($lastItem2->iddb)) {
                                                            $manager->persist($quiz);
                                                        }
                                                        $manager->flush();
                                                    }
                                                }
                                            }
                                        }  else if($lastItem->type == 'quiz') {
                                            if(isset($lastItem->iddb)) {
                                                $quiz = $quizRepository->findOneBy(['id' => $lastItem->iddb]);
                                            } else {
                                                $quiz = new Quiz();
                                            }
                                            $quiz->setName($lastItem->name);
                                            $quiz->setPid($lastItem->pid);
                                            $quiz->setChartId($lastItem->id);
                                            $quiz->setStatus('1');
                                            $quiz->setCours($cours);
                                            if(!isset($lastItem->iddb)) {
                                                $manager->persist($quiz);
                                            }
                                            $manager->flush();
                                        }
                                    }
                                }
                            } else if($item->type == 'Chap') {
                                //save chapter (chapter into modul direct add filed module id on the entity chapter
                            } else if($item->type == 'quiz') {
                                if(isset($item->iddb)) {
                                    $quiz = $quizRepository->findOneBy(['id' => $item->iddb]);
                                } else {
                                    $quiz = new Quiz();
                                }
                                $quiz->setName($item->name);
                                $quiz->setPid($item->pid);
                                $quiz->setChartId($item->id);
                                $quiz->setStatus('1');
                                $quiz->setModule($module);
                                if(!isset($item->iddb)) {
                                    $manager->persist($quiz);
                                }
                                $manager->flush();
                            }
                        }
                    }
                } else if($node->type == 'Cours') {

                    if(isset($node->iddb)) {
                        $cours = $coursRepository->findOneBy(['id' => $node->iddb]);
                    } else {
                        $cours = new Cours();
                    }
                    $cours->setName($node->name);
                    $cours->setPid($node->pid);
                    $cours->setChartId($node->id);
                    $cours->setStatus('1');
                    $cours->setFormation($formationDetails);
                    if(!isset($node->iddb)) {
                        $manager->persist($cours);
                    }
                    $manager->flush();
                    //find childs
                    if(isset($node->child)  && count($node->child) > 0 ) {
                        //save chapter for this curs
                        foreach ($node->child as $lastItem) {
                            if($lastItem) {
                                if($lastItem->type == 'Chap') {
                                    if(isset($lastItem->iddb)) {
                                        $chap = $chapterRepository->findOneBy(['id' => $lastItem->iddb]);
                                    } else {
                                        $chap = new Chapters();
                                    }
                                    $chap->setName($lastItem->name);
                                    $chap->setPid($lastItem->pid);
                                    $chap->setChartId($lastItem->id);
                                    $chap->setStatus('1');
                                    $chap->setCours($cours);
                                    if(!isset($lastItem->iddb)) {
                                        $manager->persist($chap);
                                    }
                                    $manager->flush();
                                } else if($lastItem->type == 'quiz') {

                                    if(isset($lastItem->iddb)) {
                                        $quiz = $quizRepository->findOneBy(['id' => $lastItem->iddb]);
                                    } else {
                                        $quiz = new Quiz();
                                    }
                                    $quiz->setName($lastItem->name);
                                    $quiz->setPid($lastItem->pid);
                                    $quiz->setChartId($lastItem->id);
                                    $quiz->setStatus('1');
                                    $quiz->setCours($cours);
                                    if(!isset($lastItem->iddb)) {
                                        $manager->persist($quiz);
                                    }
                                    $manager->flush();
                                }
                            }
                        }
                    }
                } else if($node->type == 'Chap') {

                    if(isset($item->iddb)) {
                        $chap = $chapterRepository->findOneBy(['id' => $node->iddb]);
                    } else {
                        $chap = new Chapters();;
                    }
                    $chap->setName($node->name);
                    $chap->setPid($node->pid);
                    $chap->setChartId($node->id);
                    $chap->setStatus('1');
                    $chap->setFormation($formationDetails);
                    if(!isset($node->iddb)) {
                        $manager->persist($chap);
                    }
                    $manager->flush();
                    if(isset($node->child)  && count($node->child) > 0 ) {
                        //child has quiz
                        foreach ($node->child as $quizes) {
                            if($quizes->type == 'quiz') {
                                if(isset($quizes->iddb)) {
                                    $quiz = $quizRepository->findOneBy(['id' => $quizes->iddb]);
                                } else {
                                    $quiz = new Quiz();
                                }
                                $quiz->setName($quizes->name);
                                $quiz->setPid($quizes->pid);
                                $quiz->setChartId($quizes->id);
                                $quiz->setStatus('1');
                                $quiz->setChapter($chap);
                                if(!isset($quizes->iddb)) {
                                    $manager->persist($quiz);
                                }
                                $manager->flush();
                            }
                        }
                    }
                } else if($node->type == 'quiz' ) {

                    //quis sous formation
                    if(isset($node->iddb)) {
                        $quiz = $quizRepository->findOneBy(['id' => $node->iddb]);
                    } else {
                        $quiz = new Quiz();
                    }
                    $quiz->setName($node->name);
                    $quiz->setPid($node->pid);
                    $quiz->setChartId($node->id);
                    $quiz->setStatus('1');
                    $quiz->setFormation($formationDetails);
                    //save the quiz
                    if(!isset($node->iddb)) {
                        $manager->persist($quiz);
                    }
                    $manager->flush();
                }
            }

            //return $this->redirectToRoute('new_formation_step5carnetdenote', ['formationId' => $formationId]);
        }
        if ($request->get('toDelete')) {
            $toDelete = $request->get('toDelete');
            $toDelete = json_decode($toDelete);
            foreach (array_filter($toDelete) as $k => $node) {
                if($node->type == 'module' && isset($node->iddb)) {
                    //save module
                    $module = $moduleRepository->findOneBy(['id' => $node->iddb]);
                    $module->setStatus('0');
                    //delete all child
                    if(isset($node->child)) {
                        foreach ($node->child as $firstChild) {
                            //get the module child
                            if($firstChild->type == 'Cours' && isset($firstChild->iddb)) {
                                $cour = $coursRepository->findOneBy(['id' => $firstChild->iddb]);
                                $cour->setStatus('0');
                                if(isset($firstChild->child)) {
                                    foreach ($firstChild->child as $secondChild) {
                                        if($secondChild->type =='Chap' && isset($secondChild->iddb)) {
                                            //get the chapter child
                                            $chapt = $chapterRepository->findOneBy(['id' => $secondChild->iddb]);
                                            $chapt->setStatus('0');
                                            if(isset($secondChild->child)) {
                                                foreach ($secondChild->child as $thirdChild) {
                                                    if(isset($thirdChild->iddb)) {
                                                        $quiz = $quizRepository->findOneBy(['id' => $thirdChild->iddb]);
                                                        $quiz->setStatus('0');
                                                        //get the quiz child
                                                        if(isset($thirdChild->child)) {
                                                            foreach ($thirdChild->child as $lastItem) {
                                                                if($lastItem->type == 'Chap' && isset($lastItem->iddb))  {
                                                                    $chapChild = $chapterRepository->findOneBy(['id' => $lastItem->iddb]);
                                                                    $chapChild->setStatus('0');
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            } else if($firstChild->type == 'Chap' && isset($firstChild->iddb)) {
                                //child is chapter
                                $chapt = $chapterRepository->findOneBy(['id' => $firstChild->iddb]);
                                $chapt->setStatus('0');
                                if(isset($firstChild->child)) {
                                    foreach ($firstChild->child as $secondChild) {
                                        if(isset($secondChild->iddb)) {
                                            $quiz = $quizRepository->findOneBy(['id' => $secondChild->iddb]);
                                            $quiz->setStatus('0');
                                            //get the quiz child
                                            if(isset($secondChild->child)) {
                                                foreach ($secondChild->child as $thirdChild) {
                                                    if($thirdChild->type == 'Chap' && isset($thirdChild->iddb))  {
                                                        $chapChild = $chapterRepository->findOneBy(['id' => $thirdChild->iddb]);
                                                        $chapChild->setStatus('0');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if($node->type == 'Cours' && isset($node->iddb)) {
                    $cour = $coursRepository->findOneBy(['id' => $node->iddb]);
                    $cour->setStatus('0');
                    if(isset($node->child)) {
                        foreach ($node->child as $firstChild) {
                            if($firstChild->type == 'Chap' && isset($firstChild->iddb)) {
                                $chapt = $chapterRepository->findOneBy(['id' => $firstChild->iddb]);
                                $chapt->setStatus('0');
                                if(isset($firstChild->child)) {
                                    foreach ($firstChild->child as $secondChild) {
                                        if(isset($secondChild->iddb)) {
                                            $quiz = $quizRepository->findOneBy(['id' => $secondChild->iddb]);
                                            $quiz->setStatus('0');
                                            //get the quiz child
                                            if(isset($secondChild->child)) {
                                                foreach ($secondChild->child as $thirdChild) {
                                                    if($thirdChild->type == 'Chap' && isset($thirdChild->iddb))  {
                                                        $chapChild = $chapterRepository->findOneBy(['id' => $thirdChild->iddb]);
                                                        $chapChild->setStatus('0');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else if($firstChild->type =='quiz' && isset($firstChild->iddb)) {
                                $quiz = $quizRepository->findOneBy(['id' => $firstChild->iddb]);
                                $quiz->setStatus('0');
                                //get the quiz child
                                if(isset($firstChild->child)) {
                                    foreach ($firstChild->hild as $thirdChild) {
                                        if($thirdChild->type == 'Chap' && isset($thirdChild->iddb))  {
                                            $chapChild = $chapterRepository->findOneBy(['id' => $thirdChild->iddb]);
                                            $chapChild->setStatus('0');
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if($node->type == 'Chap' && isset($node->iddb)) {
                    $chapter = $chapterRepository->findOneBy(['id' => $node->iddb]);
                    $chapter->setStatus('0');
                    if(isset($node->child)) {
                        foreach ($node->child as $secondChild) {
                            if(isset($secondChild->iddb)) {
                                $quiz = $quizRepository->findOneBy(['id' => $secondChild->iddb]);
                                $quiz->setStatus('0');
                                //get the quiz child
                                if($secondChild->child) {
                                    foreach ($secondChild->child as $thirdChild) {
                                        if($thirdChild->type == 'Chap' && isset($thirdChild->iddb))  {
                                            $chapChild = $chapterRepository->findOneBy(['id' => $thirdChild->iddb]);
                                            $chapChild->setStatus('0');
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if($node->type == 'quiz' && isset($node->iddb)) {
                    $quiz = $quizRepository->findOneBy(['id' => $node->iddb]);
                    $quiz->setStatus('0');
                    if(isset($node->child)) {
                        foreach ($node->child as $secondChild) {
                            if($secondChild->type == 'Chap' && isset($secondChild->iddb))  {
                                $chapChild = $chapterRepository->findOneBy(['id' => $secondChild->iddb]);
                                $chapChild->setStatus('0');
                            }
                        }
                    }
                }
            }
            $manager->flush();
        }
        //get all modules
        $listModules   = $moduleRepository->findAll();
        $listCours     = $coursRepository->findAll();
        $listChapters  = $chapterRepository->findAll();
        $listQuiz      = $quizRepository->findAll();

        return $this->render('formations/step_3.html.twig',
            ['toggled' => true,
                'formationId' => $formationId,
                'nameformation' => $nameformation,
                'userMedia' => $userMedia,
                'devoirs'=>$devoirs,
                'listModules' => $listModules,
                'listCours' => $listCours,
                'listhHapters' => $listChapters,
                'listQuiz' => $listQuiz,
            ]);
    }

    /**
     * @Route("step5carnetdenote/{formationId}", name="new_formation_step5carnetdenote")
     */
    public function step5(FormationRepository $formationRepository,$formationId,ConfigchaiptreRepository $configchaiptreRepository)
    {
        $formationRepository = $formationRepository->find($formationId);
        $nameformation = $formationRepository->getTitreFormation();
        $config= $configchaiptreRepository->findAll();
        if (!empty($config)) {
            foreach ($config as $jointures => $val) {
                $config = $val->getChoiceChapter();
            }
        }
        return $this->render('formations/step5carnetdenote.html.twig',
            [
                'toggled' => true,
                'formationId' => $formationId,
                'nameformation' =>$nameformation,
                'namedevoir'=>$config,
            ]);
    }
    /**
     * @Route("badgesCertifs/{formationId}", name="new_formation_step6")
     */
    public function step6(Request $request, FormationRepository $formationRepository, BadgeRepository $badgRepo, $formationId)
    {
        $badge = $formationRepository->find($formationId);
        if (!empty($formationId)) {
            $badges = $badgRepo->findAll();
            $formation = $formationRepository->findOneBy(['id' => $formationId]);
            return $this->render('formations/step_6.html.twig', [
                'badges' => $badges,
                'badge' => $badge,
                'toggled' => true,
                'formation' => $formation,
            ]);
        } else {
            return $this->createNotFoundException('The badges and certifs of this formation does not exist');
        }
    }
    /**
     * @Route("badge/{formationId}", name="new_badge_step6")
     */
    public function badge(ObjectManager $manager, Request $request, FormationRepository $formationRepository, BadgeRepository $badgRepo, DocumentRepository $media, ModuleRepository $moduleRepository, CoursRepository $coursRepository, ChapterRepository $chapterRepository, $formationId)
    {
        $id = $formationRepository->find($formationId);
        if ($id) {
            /*$badges = $badgRepo->findAll();*/
            $formation = $formationRepository->findOneBy(['id' => $formationId]);
            //list of lateset updated formations
            $formations = $formationRepository->findBy(['user' => $this->getUser()]);
            //get the list of module for the current formation
            $modules = $moduleRepository->findBy(['formation' => $formation]);
            if (!empty($modules)) {
                foreach ($modules as $module) {
                    //get list cour
                    $module->type = "modul";
                    $module->cour = $coursRepository->findBy(['module' => $module]);
                    if (!empty($module->cour)) {
                        foreach ($module->cour as $cour) {
                            $cour->chapters = $chapterRepository->findBy(['cours' => $cour]);
                        }
                    }
                }
            }
            //get chapter with parent formation
            $chapter = $chapterRepository->findBy(['formation' => $formation]);
            //get cours with parent formation
            $cours = $coursRepository->findBy(['formation' => $formation]);
            if (!empty($cours)) {
                foreach ($cours as $cour) {
                    $cour->chapters = $chapterRepository->findBy(['cours' => $cour]);
                }
            }
            $cour = $coursRepository->findOneBy(['formation' => $formation]);
            $module = $moduleRepository->findOneBy(['formation' => $formation]);
            $chapitre = $chapterRepository->findOneBy(['formation' => $formation]);
            $badge = new Badge();
            $user = $this->getUser();
            $userMedia = $media->findBy(array('user' => $user));

            $form = $this->createForm(BadgeType::class, $badge);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $badge->setCreatedAt(new \DateTime);
                $badge->setUpdatedAt(new \DateTime);

                $formation->addBadge($badge);

                $manager->persist($badge);

                $manager->flush();
                return $this->redirectToRoute('new_formation_step6', ['formationId' => $formationId]);
            }
            return $this->render('formations/step_6_badge.html.twig', [
                'form' => $form->createView(),
                'toggled' => true,
                'userMedia' => $userMedia,
                'formations' => $formations,
                'formation' => $formation,
                'modules' => $modules,
                'cours' => $cours,
                'cour' => $cour,
                'module' => $module,
                'chapitre' => $chapitre,
                'chapters' => $chapter,
                'formationId' => $formationId,
                /*'badges' => $badges,*/
            ]);
        } else {
            return $this->createNotFoundException('ID does not exist');
        }
    }

    /**
     * download the selected badge
     * @Route("downloadBadge/{id}", name="download_badge")
     */
    public function downloadBadge(ObjectManager $manager, BadgeRepository $badgeRepository, UploaderHelper $uploaderHelper,Badge $badge, $id)
    {
        $Badge = $badgeRepository->find($id);
        if (!$Badge) {
            $array = array('statuts' => 0,
                'message' => 'Fichier n \n existe pas'
            );
            $response = new JsonResponse($array, 200);
            return $response;
        }
        // Provide a name for your file with extension
        $filename = 'Badge.docx';

        // The dinamically created content of the file
        $fileContent = $badge;

        // Return a response with a specific content
        $response = new Response($fileContent);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);

        // Dispatch request
        return $response;
    }

    /**
     * duplicate the selected badge
     * @Route("duplicateBadge/{id}", name="duplicate_badge")
     */
    public function duplicateBadge(ObjectManager $manager, BadgeRepository $badgeRepository, FormationRepository $formationRepository,Badge $badge)
    {
        $newBadge = clone $badge;
        $nam = $newBadge->getName();
        $fnam = 'copy'.' '.$nam;
        $newBadge->setName($fnam);

        $manager->persist($newBadge);

        $manager->flush();

        $f = $newBadge->getFormation()->getId();

        return $this->redirectToRoute('new_formation_step6', ['formationId' => $f]);
    }

    /**
     * duplicate the selected formation
     * @Route("duplicateFormation", name="duplicate_formation")
     */
    public function duplicateFormation(Request $request, ObjectManager $manager, FormationRepository $formationRepository)
    {


        if ($data = $request->request->get('duplicateFormations')) {
            $id = $request->request->get('formationId');
            $formation = $formationRepository->findOneBy(array('id' => $id));
            if ($formation) {
                $newFormation = new Formation();
                $name = $formation->getTitreFormation();
                $fnam = 'copy' . ' ' . $name;
                $newFormation->setTitreFormation($fnam);
                if (isset($data[0])) {
                    $newFormation->setDescpritionFormation($data[0]);
                }

                $manager->persist($newFormation);

                $manager->flush();
                return $this->redirectToRoute('home_formation');

            }
        }
    }

    /**
     * @Route("step7", name="new_formation_step7")
     */
    public function step7()
    {

        return $this->render('formations/step7.html.twig', ['toggled' => true]);
    }
    /**
     * @Route("addUsersToFormation", name="add_users_to_formation")
     */
    public function addUsersToFormation(ObjectManager $manager, Request $request, FormationRepository $formationRepository,\Swift_Mailer $mailer, UserRepository $newUserRepository)
    {
        dump("addUsersToFormation()\": the $deleteRepository argument is type-hinted with the non-existent class or interface: \"App\Repository\deleteRepository\".
");
        /*$formations = $formationRepository->findAll();
        $deletes    = $deleteRepository->findAll();
        return $this->render('formations/addUsers.html.twig', [
            'toggled' => true,
            'formations' => $formations,
            'deletes'    => $deletes,
        ]);*/
    }

    /**
     * @Route("newCode", name="new_code")
     */
    public function newCode(ObjectManager $manager, Request $request, CodeRepository $codeRepository, FormationRepository $fo)
    {

        $code = new Code();
        $formation = new Formation();
        $class = new delete();


        $form = $this->createForm(CodeType::class, $code);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $formation->setCode($code);
            $class->setCode($code);
            $manager->persist($code);

            $manager->flush();

            return $this->redirectToRoute('code_gen', ['id' => $code->getId()]);
        }
        return $this->render('formations/new_code.html.twig', [
            'form' => $form->createView(),
            'toggled' => true,
        ]);
    }

    /**
     * @Route("code/{id}", name="edit_code")
     */
    public function editCode(ObjectManager $manager, Request $request, $id, CodeRepository $codeRepository)
    {

        $code = $codeRepository->findBy(array('id' => $id));

        $form = $this->createForm(CodeType::class, $code);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->flush();

            return $this->redirectToRoute('code_gen', ['id' => $id]);
        }
        return $this->render('formations/new_code.html.twig', [
            'form' => $form->createView(),
            'toggled' => true,
        ]);
    }

    /**
     * @Route("createCertification/{formationId}", name="new_inscrip_step7")
     */
    public function createCertification($formationId)
    {
        return $this->render('formations/step_6_certificat.html.twig',
            ['toggled' => true,
                'formationId'=>$formationId,
            ]);
    }

    /**
     * @Route("Step7code", name="new_code_step7")
     */
    public function Step7code()
    {
        return $this->render('formations/step7_code.html.twig', ['toggled' => true]);
    }

    /**
     * @Route("Step7users", name="new_user_step7")
     */
    public function Step7users(UserRepository $repo)
    {
        $users = $repo->findAll();
        return $this->render('formations/step7_users.html.twig', ['users' => $users]);
    }

    /**
     * @Route("codegen/{id}", name="code_gen")
     */
    public function Codegen(ObjectManager $manager, Request $request, $id, CodeRepository $codeRepository)
    {
        $code = $codeRepository->findBy(array('id' => $id));
        return $this->render('formations/step7_v4.html.twig', [
            'code' => $code,
            'toggled' => true
        ]);
    }

    /**
     * @Route("home/{fromCategory}", name="home_formation", defaults={"fromCategory": "false"})
     */
    public function Home(ObjectManager $manager, Request $request, CategoryRepository $categoryRepository, FormationRepository $formationRepository, CatalogueRepository $catalogueRepository, $fromCategory = false) {

        $categorie = $formationRepository->findAll();
        $categories   = $categoryRepository->findBy(array('id' => $categorie ), array('name' => 'ASC'));
        $arr = array_chunk($categories, 8);
        $user = $this->getUser();
        //$formationUser = $formationRepository->findOneBy(array('user' => $user));
        //$formationId = 0;
        $formation = $formationRepository->findBy(array('user' => $user));

        $formations = $formationRepository->searchByUpdatedDate();
        $categories = $categoryRepository->findAll();

        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($category);

            $manager->flush();

            return $this->redirectToRoute('home_formation');
        }
        return $this->render('formations/home.html.twig', [
            'form' => $form->createView(),
            'categories' => $categories,
            'formations' => $formations,
            'toggled' => true,
            'catalogues' => $arr,
            'fromCategory' => $fromCategory,
            'formation' => $formation,
        ]);
    }

    /**
     * duplicate the selected category
     * @Route("duplicateCategory/{id}", name="duplicate_category")
     */
    public function duplicateCategory(ObjectManager $manager, CategoryRepository $categoryRepository, $id, Category $category)
    {
        $newCategory = new Category();
        if ($category) {
            $name = $category->getName();
            $Categorynam = 'copy' . ' ' . $name;
            $newCategory->setName($Categorynam);

            $manager->persist($newCategory);

            $manager->flush();

        }
        return $this->redirectToRoute('home_formation', ['fromCategory' => 'true']);
    }

    /**
     * @Route("newForm/{formationId}", name="new_formation")
     */
    public function NewFormation(FormationRepository $formationRepository, DocumentRepository $media, ModuleRepository $moduleRepository, CoursRepository $coursRepository, ChapterRepository $chapterRepository, $formationId = null)
    {

        if($formationId) {
            $formation = $formationRepository->findOneBy(['id' =>$formationId]);
            if(empty($formation)){
                return $this->render('eroor/error.html.twig', ['statuscode'=>'404', 'statustext'=>'formation n"existe pas'
                ]);
            }
            //list of lateset updated formations
            $formations = $formationRepository->findBy(['user' => $this->getUser()]);
            //get the list of module for the current formation
            $modules = $moduleRepository->findBy(['formation' => $formation, 'status' => 1]);
            $userMedia = $media->findBy(array('user' => $this->getUser()));
            if(!empty($modules)) {
                foreach ( $modules as  $module) {
                    //get list cour
                    $module->cour = $coursRepository->findBy(['module' => $module,'status' => 1]);
                    if(!empty( $module->cour)) {
                        foreach ( $module->cour as $cour) {
                            $cour->chapters = $chapterRepository->findBy(['cours' => $cour,'status' => 1]);
                        }
                    }
                }
            }
            //get chapter with parent formation
            $chapter = $chapterRepository->findBy(['formation' => $formation,'status' => 1]);
            $cours = $coursRepository->findBy(['formation' => $formation,'status' => 1]);

            if(!empty($cours)) {
                foreach ( $cours as $cour) {
                    $cour->chapters = $chapterRepository->findBy(['cours' => $cour,'status' => 1]);
                }
            }

            return $this->render('formations/new.html.twig', [
                'toggled'    => true,
                'formations' => $formations,
                'formation'  => $formation,
                'modules'    => $modules,
                'cours'      => $cours,
                'chapters'   => $chapter,
                'userMedia' => $userMedia,
            ]);
        } else {
            return $this->render('eroor/error.html.twig', ['statuscode'=>'404', 'statustext'=>'id formation n"existe pas'
            ]);
        }
    }

    /**
     * @Route("chapterByCour/{formationId}/{courId}", name="show_chapters")
     */
    public function chapterByCour($courId = null, $formationId = null, DocumentRepository $media, ChapterRepository $chapterRepository, CoursRepository $coursRepository, FormationRepository $formationRepository)
    {
        if(!empty($courId)) {
            $userMedia = $media->findBy(array('user' => $this->getUser()));
            $formation = $formationRepository->findOneBy(['id' =>$formationId]);
            $formations=$formationRepository->findAll();
            $cour = $coursRepository->findOneBy(['id' => $courId]);
            $listChapters = $chapterRepository->findBy(['cours' => $cour]);
            return $this->render('formations/new_module.html.twig', ['toggled' => true,
                'formation' => $formation,
                'chapters' => $listChapters,
                'cour' => $cour,
                'formations'=>$formations,
                'userMedia' => $userMedia,
            ]);
        } else {
            dump('404');
        }

    }

}