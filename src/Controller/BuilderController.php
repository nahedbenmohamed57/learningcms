<?php
namespace App\Controller;

use App\Entity\Devoir;
use App\Entity\Document;
use App\Entity\BuilderContent;
use App\Repository\BuilderContentRepository;
use App\Repository\ChapterRepository;
use App\Repository\DocumentRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * The builder Module
 * @Route("/builder/")
 */
class BuilderController extends AbstractController
{
    /**
     * The home page of the builders accept as parameter th id of the chapter
     * @Route("/homePage/{idChapter}", name="build_home_page")
     */
    public function home($idChapter = 1)
    {
        return $this->render('builder/home.html.twig', ['idChapter' => $idChapter]);
    }

    /**
     * Build from empty page
     * @Route("/buildChapter/{idChapter}", name="build_chapter")
     */
    public function newPage(DocumentRepository $media, $idChapter = null, ChapterRepository $chapterRepo, BuilderContentRepository $builderRepo)
    {
        if($idChapter) {
            $user        = $this->getUser();
            $userMedia   = $media->findBy(array('user' => $user ));
            $chapter     = $chapterRepo->findOneBy(array('id' => $idChapter));

            if(!empty($chapter)) {
                $dataBuilder = $chapter->getBuilderContent();
                if(!$dataBuilder) {
                    $dataBuilder = '';
                }
                return $this->render('builder/newPage.html.twig', [
                    'userMedia'   => $userMedia,
                    'idChapter'   => $idChapter,
                    'dataBuilder' => $dataBuilder
                ]);
            } else {
                throw $this->createNotFoundException('The chapter does not exist');
            }
        } else {
            //show 404 page
            throw $this->createNotFoundException('The chapter does not exist');
        }
    }

    /**
     * Upload file
     * @Route("uploadForBuilder", name="upload-file")
     */
    public function uploadForBuilder(Request $request, ObjectManager $manager)
    {
        $user        = $this->getUser();
        $data    = $request->files->get('file');
        /* Getting file name */

        $filename = $data->getClientOriginalName();

        /* Location */
        $dir  = __DIR__.'/../../public/images/uploads/';
        $location = realpath($dir).'\\'.$user->getId().'\documents\\'.$filename;
        //$location = __DIR__.'/../../public/images/uploadBuilder/'.$filename;
        if (file_exists($location)) {
            //$location =  __DIR__.'/../../public/images/uploadBuilder/copie-1-'.$filename;
        }
        $uploadOk = 1;

        if($uploadOk == 0){
            return new Response(
                'error'
            );
        }else{
            /* Upload file */
            if(move_uploaded_file($data->getLinkTarget(), $location)) {

                //save the document
                $document = new Document();
                $document->setFile($data);

                $document->setName($data->getClientOriginalName());
                if (strpos($data->getClientOriginalName(), 'devoir') !== false) {
                    $document->setType("devoir");
                    $devoir =new Devoir();
                    $devoir->setName("devoir");
                    $manager->persist($devoir);
                    $manager->flush();
                }
                $document->setUser($user);
                $document->setcreatedAt(new \DateTime);
                $manager->persist($document);
                $manager->flush();

                return new Response(
                    'success'
                );
            }else{
                return new Response(
                    'error'
                );
            }
        }
    }
    /**
     * Save the chapter
     * @Route("saveChapter", name="save_chapter")
     */
    public function saveChapter(Request $request, ObjectManager $manager, BuilderContentRepository $builderRepo, ChapterRepository $chapterRepo)
    {
        $builderData    = $request->request->get('builderData');
        $components     = $builderData['components'];
        $htmlData       = $builderData['htmlData'];;
        $dynamicContent = $builderData['dynamicContent'];
        $generalConfig  = $builderData['generalConfig'];
        $chapterId      = $request->request->get('chapterId');

        //get the chapter entity witch have the given id
        $chapter = $chapterRepo->findOneBy(array('id' => $chapterId));
        if(!empty($chapter)) {
            $builder = $chapter->getBuilderContent();
            if(!empty($builder)){
                //find the builder by chapter id if builder exist update it else save a new builder for the given chapter
            } else {
                //if no result create a empty builder
                $builder       = new BuilderContent();
                $builder->setChapter($chapter);
            }
            if(!empty($generalConfig)) {
                $builder->setGeneralConfig($generalConfig);
            }
            $builder->setComponents($components);
            $builder->setHtmlContent($htmlData);
            $builder->setDynamicContent($dynamicContent);

            //insert the new chapter content
            $manager->persist($builder);
            $manager->flush();
            //update the chapter builder_id
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @param ObjectManager $manager
     * @Route("getChapter/{chapterId}", name="get_chapter")
     */
    public function getChapter(ObjectManager $manager, BuilderContentRepository $builder, ChapterRepository $chapterRepo, $chapterId) {
        $chapter = $chapterRepo->findOneBy(array('id' => $chapterId));
        if($chapter) {
            $data = $builder->findOneBy(array('chapter' => $chapter));
            $style="";
            $image = '';
            $video = '';
            if($data) {
                if(!empty($data->getGeneralConfig())){
                    $generalConfig =  $data->getGeneralConfig();
                    foreach ($generalConfig as $conf) {
                        if($conf['type'] == 'fondColor'){
                            $color = $conf['fondColor'];
                            $style.='background-color:'. $color.';';
                        }
                        if($conf['type'] == 'image'){
                            $image = $conf;
                        }
                        if($conf['type'] == 'video'){
                            $video = $conf;
                        }
                        if($conf['type'] == 'text'){
                            $style.='font-weight:'.$conf['fondText'].';';
                            $style.='font-family:'.$conf['policeText'].';';
                            $style.='font-size:'.$conf['sizePolice'].'px;';
                            $style.='color:'.$conf['colorText'].';';
                        }
                    }
                }
                return $this->render('builder/chapter.html.twig', [
                    'htmlContent' => $data->getHtmlContent(),
                    'globalStyle' => $style
                ]);
            } else {
                return $this->createNotFoundException('The chapter does not exist');
            }

        } else {
            return $this->createNotFoundException('The chapter does not exist');
        }

    }

    /**
     * @Route("chapterToBuild/{idChapter}", name="chapter_to_build")
     */
    public function chapterToBuild($idChapter, ChapterRepository $chapterRepo, BuilderContentRepository $builderRepo)
    {
        if($idChapter) {
            $user        = $this->getUser();
            $chapter     = $chapterRepo->findOneBy(array('id' => $idChapter));

            if(!empty($chapter)) {
                $dataBuilder = $chapter->getBuilderContent();
                if($dataBuilder) {
                    $data = [
                        'idChapter'      => $idChapter,
                        'general_config' => $dataBuilder->getGeneralConfig(),
                        'components'     => $dataBuilder->getComponents()
                    ];
                    return new Response(
                        json_encode($data)
                    );
                } else {
                    return new Response(
                        'error'
                    );
                }

            } else {
                return new Response(
                    'error'
                );
            }
        } else {
            //show 404 page
            return new Response(
                'error'
            );
        }
    }
}