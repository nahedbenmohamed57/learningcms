<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use mysql_xdevapi\Collection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }
    /**
     * @param string $role
     *
     * @return array
     */
    public function findByRole($role)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"'.$role.'"%');

        return $qb->getQuery()->getResult();
    }

    /**
     *  To be optimised
     * @param integer $formationID
     *
     * @return array
     */
    public function getEmailsByFormations($formationID)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u');
            //->where('u.getFormations_sup' != $formationRepo);

        $result = $qb->getQuery()->getResult();
        $users = [];
        foreach ($result as $res) {
            $formSup = $res->getFormationsSup();
            if(!empty($formSup)) {
                foreach ($formSup as $f) {
                    if($f->getId() == $formationID) {
                        array_push($users, $res->getEmail());
                    }
                }
            }
        }
        return $users;
    }

    /**
     * @param integer $classeID
     *
     * @return array
     */
    public function getEmailsByClasses($classeID)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u');
        //->where('u.getFormations_sup' != $formationRepo);

        $result = $qb->getQuery()->getResult();
        $usersClass = [];
        foreach ($result as $res) {
            $classes = $res->getClasse();
            if(!empty($classes)) {
                foreach ($classes as $classe) {
                    if($classe->getId() == $classeID) {
                        array_push($usersClass, $res->getEmail());
                    }
                }
            }
        }
        return $usersClass;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
