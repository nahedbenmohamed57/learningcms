<?php

namespace App\Repository;

use App\Entity\Formationconfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Configformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Configformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Configformation[]    findAll()
 * @method Configformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormationconfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Formationconfig::class);
    }

    // /**
    //  * @return Configformation[] Returns an array of Configformation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Configformation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
