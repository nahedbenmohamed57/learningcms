<?php

namespace App\Repository;

use App\Entity\Userchapter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Userchapter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Userchapter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Userchapter[]    findAll()
 * @method Userchapter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserchapterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Userchapter::class);
    }

    // /**
    //  * @return Userchapter[] Returns an array of Userchapter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Userchapter
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
