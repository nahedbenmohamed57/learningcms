<?php

namespace App\Repository;

use App\Entity\Configcours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Configcours|null find($id, $lockMode = null, $lockVersion = null)
 * @method Configcours|null findOneBy(array $criteria, array $orderBy = null)
 * @method Configcours[]    findAll()
 * @method Configcours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfigcoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configcours::class);
    }

    // /**
    //  * @return Configcours[] Returns an array of Configcours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Configcours
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
