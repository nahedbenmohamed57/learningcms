<?php

namespace App\Repository;

use App\Entity\StudentNotes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StudentNotes|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentNotes|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentNotes[]    findAll()
 * @method StudentNotes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentNotesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StudentNotes::class);
    }

    // /**
    //  * @return StudentNotes[] Returns an array of StudentNotes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StudentNotes
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
