<?php

namespace App\Repository;

use App\Entity\Noteformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Noteformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Noteformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Noteformation[]    findAll()
 * @method Noteformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NoteformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Noteformation::class);
    }

    // /**
    //  * @return Noteformation[] Returns an array of Noteformation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Noteformation
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
