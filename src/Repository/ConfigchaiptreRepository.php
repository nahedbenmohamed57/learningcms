<?php

namespace App\Repository;

use App\Entity\Configchaiptre;
use App\Entity\Configchapitre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Configchaiptre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Configchaiptre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Configchaiptre[]    findAll()
 * @method Configchaiptre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfigchaiptreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configchapitre::class);
    }

    // /**
    //  * @return Configchaiptre[] Returns an array of Configchaiptre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Configchaiptre
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
