<?php

namespace App\Repository;

use App\Entity\StudentFolders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StudentFolders|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentFolders|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentFolders[]    findAll()
 * @method StudentFolders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentFoldersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StudentFolders::class);
    }

    // /**
    //  * @return StudentFolders[] Returns an array of StudentFolders objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StudentFolders
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
