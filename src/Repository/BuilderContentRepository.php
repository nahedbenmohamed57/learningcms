<?php

namespace App\Repository;

use App\Entity\BuilderContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BuilderContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuilderContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuilderContent[]    findAll()
 * @method BuilderContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuilderContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuilderContent::class);
    }

    // /**
    //  * @return BuilderContent[] Returns an array of BuilderContent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BuilderContent
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
