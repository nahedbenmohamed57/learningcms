<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190916161753 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE chapters DROP FOREIGN KEY FK_C7214371C583534E');
        $this->addSql('DROP TABLE devoir');
        $this->addSql('DROP TABLE messagerie');
        $this->addSql('DROP INDEX IDX_C7214371C583534E ON chapters');
        $this->addSql('ALTER TABLE chapters DROP devoir_id');
        $this->addSql('ALTER TABLE cours DROP pid');
        $this->addSql('ALTER TABLE document CHANGE type type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BFF7A2C2FC');
        $this->addSql('DROP INDEX UNIQ_404021BFF7A2C2FC ON formation');
        $this->addSql('ALTER TABLE formation DROP badge_id');
        $this->addSql('ALTER TABLE module DROP pid');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE devoir (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE messagerie (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, from_to VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, message VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE chapters ADD devoir_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chapters ADD CONSTRAINT FK_C7214371C583534E FOREIGN KEY (devoir_id) REFERENCES devoir (id)');
        $this->addSql('CREATE INDEX IDX_C7214371C583534E ON chapters (devoir_id)');
        $this->addSql('ALTER TABLE cours ADD pid VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE document CHANGE type type VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE formation ADD badge_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BFF7A2C2FC FOREIGN KEY (badge_id) REFERENCES badge (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_404021BFF7A2C2FC ON formation (badge_id)');
        $this->addSql('ALTER TABLE module ADD pid VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
