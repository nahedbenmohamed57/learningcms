<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190613070309 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_profile');
        $this->addSql('ALTER TABLE user ADD pseudo VARCHAR(255) DEFAULT NULL, ADD phone VARCHAR(255) DEFAULT NULL, ADD address VARCHAR(255) DEFAULT NULL, ADD zip_code VARCHAR(100) DEFAULT NULL, ADD country VARCHAR(255) DEFAULT NULL, ADD city VARCHAR(255) DEFAULT NULL, ADD region VARCHAR(255) DEFAULT NULL, ADD additional_information VARCHAR(255) DEFAULT NULL, ADD pinterest LONGTEXT DEFAULT NULL, ADD facebook LONGTEXT DEFAULT NULL, ADD twitter LONGTEXT DEFAULT NULL, ADD instagrame LONGTEXT DEFAULT NULL, ADD picture LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_profile (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, pseudo VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, phone VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, address VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, zip_code VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci, country VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, city VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, region VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, additional_information VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, pinterest LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, facebook LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, twitter LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, instagrame LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, picture LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX UNIQ_D95AB405A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_profile ADD CONSTRAINT FK_D95AB405A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user DROP pseudo, DROP phone, DROP address, DROP zip_code, DROP country, DROP city, DROP region, DROP additional_information, DROP pinterest, DROP facebook, DROP twitter, DROP instagrame, DROP picture');
    }
}
