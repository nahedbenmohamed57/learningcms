<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190904100255 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE chaiptre');
        $this->addSql('DROP TABLE user_profile');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE chaiptre (id INT AUTO_INCREMENT NOT NULL, quiz_id INT DEFAULT NULL, cours_id INT DEFAULT NULL, formation_id INT DEFAULT NULL, config_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX UNIQ_CC835DC324DB0683 (config_id), INDEX IDX_CC835DC35200282E (formation_id), UNIQUE INDEX UNIQ_CC835DC3853CD175 (quiz_id), INDEX IDX_CC835DC37ECF78B0 (cours_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE user_profile (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, pseudo VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, phone VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, address VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, zip_code VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci, country VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, city VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, region VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, additional_information VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, pinterest LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, facebook LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, twitter LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, instagrame LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, picture LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX UNIQ_D95AB405A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE chaiptre ADD CONSTRAINT FK_CC835DC324DB0683 FOREIGN KEY (config_id) REFERENCES configchapitre (id)');
        $this->addSql('ALTER TABLE chaiptre ADD CONSTRAINT FK_CC835DC35200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE chaiptre ADD CONSTRAINT FK_CC835DC37ECF78B0 FOREIGN KEY (cours_id) REFERENCES cours (id)');
        $this->addSql('ALTER TABLE chaiptre ADD CONSTRAINT FK_CC835DC3853CD175 FOREIGN KEY (quiz_id) REFERENCES quiz (id)');
        $this->addSql('ALTER TABLE user_profile ADD CONSTRAINT FK_D95AB405A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }
}
