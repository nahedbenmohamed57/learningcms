<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190723094638 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE abonnement (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, devise VARCHAR(100) NOT NULL, type VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, size VARCHAR(255) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, alternatif_text VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_D8698A76A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_document_folders (document_id INT NOT NULL, document_folders_id INT NOT NULL, INDEX IDX_3B3A4601C33F7837 (document_id), INDEX IDX_3B3A4601DE761A76 (document_folders_id), PRIMARY KEY(document_id, document_folders_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_folders (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_5A5875D3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student_folders (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_130166F7A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student_notes (id INT AUTO_INCREMENT NOT NULL, student_folder_id INT DEFAULT NULL, user_id INT DEFAULT NULL, value LONGTEXT NOT NULL, type VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, status TINYINT(1) DEFAULT NULL, INDEX IDX_892CDF11714278C2 (student_folder_id), INDEX IDX_892CDF11A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE document_document_folders ADD CONSTRAINT FK_3B3A4601C33F7837 FOREIGN KEY (document_id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document_document_folders ADD CONSTRAINT FK_3B3A4601DE761A76 FOREIGN KEY (document_folders_id) REFERENCES document_folders (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document_folders ADD CONSTRAINT FK_5A5875D3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE student_folders ADD CONSTRAINT FK_130166F7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE student_notes ADD CONSTRAINT FK_892CDF11714278C2 FOREIGN KEY (student_folder_id) REFERENCES student_folders (id)');
        $this->addSql('ALTER TABLE student_notes ADD CONSTRAINT FK_892CDF11A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE user_profile');
        $this->addSql('ALTER TABLE user ADD pseudo VARCHAR(255) DEFAULT NULL, ADD phone VARCHAR(255) DEFAULT NULL, ADD address VARCHAR(255) DEFAULT NULL, ADD zip_code VARCHAR(100) DEFAULT NULL, ADD country VARCHAR(255) DEFAULT NULL, ADD city VARCHAR(255) DEFAULT NULL, ADD region VARCHAR(255) DEFAULT NULL, ADD additional_information VARCHAR(255) DEFAULT NULL, ADD pinterest LONGTEXT DEFAULT NULL, ADD facebook LONGTEXT DEFAULT NULL, ADD twitter LONGTEXT DEFAULT NULL, ADD instagrame LONGTEXT DEFAULT NULL, ADD picture LONGTEXT DEFAULT NULL, ADD updated_at DATETIME NOT NULL, ADD birthday DATE DEFAULT NULL, ADD gender TINYINT(1) DEFAULT NULL, ADD reset_token VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE document_document_folders DROP FOREIGN KEY FK_3B3A4601C33F7837');
        $this->addSql('ALTER TABLE document_document_folders DROP FOREIGN KEY FK_3B3A4601DE761A76');
        $this->addSql('ALTER TABLE student_notes DROP FOREIGN KEY FK_892CDF11714278C2');
        $this->addSql('CREATE TABLE user_profile (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, pseudo VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, phone VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, address VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, zip_code VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci, country VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, city VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, region VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, additional_information LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, pinterest LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, facebook LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, twitter LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, instagrame LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, picture LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX UNIQ_D95AB405A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_profile ADD CONSTRAINT FK_D95AB405A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE abonnement');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE document_document_folders');
        $this->addSql('DROP TABLE document_folders');
        $this->addSql('DROP TABLE student_folders');
        $this->addSql('DROP TABLE student_notes');
        $this->addSql('ALTER TABLE user DROP pseudo, DROP phone, DROP address, DROP zip_code, DROP country, DROP city, DROP region, DROP additional_information, DROP pinterest, DROP facebook, DROP twitter, DROP instagrame, DROP picture, DROP updated_at, DROP birthday, DROP gender, DROP reset_token');
    }
}
