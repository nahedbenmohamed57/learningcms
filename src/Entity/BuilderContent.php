<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BuilderContentRepository")
 */
class BuilderContent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $general_config = [];

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $components = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $html_content;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Chapters", inversedBy="builderContent", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $chapter;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $dynamicContent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGeneralConfig(): ?array
    {
        return $this->general_config;
    }

    public function setGeneralConfig(?array $general_config): self
    {
        $this->general_config = $general_config;

        return $this;
    }

    public function getComponents(): ?array
    {
        return $this->components;
    }

    public function setComponents($components): self
    {
        $this->components = $components;

        return $this;
    }

    public function getHtmlContent(): ?string
    {
        return $this->html_content;
    }

    public function setHtmlContent(?string $html_content): self
    {
        $this->html_content = $html_content;

        return $this;
    }

    public function getChapter(): ?Chapters
    {
        return $this->chapter;
    }

    public function setChapter(Chapters $chapter): self
    {
        $this->chapter = $chapter;

        return $this;
    }

    public function getDynamicContent(): ?string
    {
        return $this->dynamicContent;
    }

    public function setDynamicContent(?string $dynamicContent): self
    {
        $this->dynamicContent = $dynamicContent;

        return $this;
    }
}
