<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoursRepository")
 */
class Cours
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Quiz", cascade={"persist", "remove"})
     */
    private $quiz;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Module", inversedBy="cours")
     */
    private $module;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Configcours", cascade={"persist", "remove"})
     */
    private $config;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="cours")
     */
    private $formation;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chapters", mappedBy="cours")
     */
    public $chapters;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $chartId;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":"1"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $banner;

    public function __construct()
    {
        $this->chapters = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

/*    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }*/



    public function getQuiz(): ?Quiz
    {
        return $this->quiz;
    }

    public function setQuiz(?Quiz $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }

    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getConfig(): ?Configcours
    {
        return $this->config;
    }

    public function setConfig(?Configcours $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getPid(): ?string
    {
        return $this->pid;
    }

    public function setPid(?string $pid): self
    {
        $this->pid = $pid;

        return $this;
    }

    public function getChartId(): ?string
    {
        return $this->chartId;
    }

    public function setChartId(?string $chartId): self
    {
        $this->chartId = $chartId;

        return $this;
    }

    /**
     * @return Collection|Chapters[]
     */
    public function getChapters(): Collection
    {
        return $this->chapters;
    }

    public function addChapter(Chapters $chapter): self
    {
        if (!$this->chapters->contains($chapter)) {
            $this->chapters[] = $chapter;
            $chapter->setCours($this);
        }

        return $this;
    }

    public function removeChapter(Chapters $chapter): self
    {
        if ($this->chapters->contains($chapter)) {
            $this->chapters->removeElement($chapter);
            // set the owning side to null (unless already changed)
            if ($chapter->getCours() === $this) {
                $chapter->setCours(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(?string $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    
}
