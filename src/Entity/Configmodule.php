<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigmoduleRepository")
 */
class Configmodule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statut_score;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $score;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $access;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $access_input;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $access_subscribe;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $acces_days;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statut_prequis;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $choice_preq;

    /**
     * @return mixed
     */
    public function getAccesDays()
    {
        return $this->acces_days;
    }

    /**
     * @param mixed $acces_days
     */
    public function setAccesDays($acces_days): void
    {
        $this->acces_days = $acces_days;
    }



    /**
     * @return mixed
     */
    public function getChoicePreq()
    {
        return $this->choice_preq;
    }

    /**
     * @param mixed $choice_preq
     */
    public function setChoicePreq($choice_preq): void
    {
        $this->choice_preq = $choice_preq;
    }

    /**
     * @return mixed
     */
    public function getPointPreq()
    {
        return $this->point_preq;
    }

    /**
     * @param mixed $point_preq
     */
    public function setPointPreq($point_preq): void
    {
        $this->point_preq = $point_preq;
    }


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $point_preq;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icone_image;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatutScore(): ?int
    {
        return $this->statut_score;
    }

    public function setStatutScore(int $statut_score): self
    {
        $this->statut_score = $statut_score;

        return $this;
    }

    public function getScore(): ?string
    {
        return $this->score;
    }

    public function setScore(string $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getAccess(): ?int
    {
        return $this->access;
    }

    public function setAccess(int $access): self
    {
        $this->access = $access;

        return $this;
    }

    public function getAccessInput(): ?string
    {
        return $this->access_input;
    }

    public function setAccessInput(string $access_input): self
    {
        $this->access_input = $access_input;

        return $this;
    }

    public function getAccessSubscribe(): ?string
    {
        return $this->access_subscribe;
    }

    public function setAccessSubscribe(string $access_subscribe): self
    {
        $this->access_subscribe = $access_subscribe;

        return $this;
    }

    public function getIcone(): ?string
    {
        return $this->icone;
    }

    public function setIcone(string $icone): self
    {
        $this->icone = $icone;

        return $this;
    }

    public function getIconeImage(): ?string
    {
        return $this->icone_image;
    }

    public function setIconeImage(string $icone_image): self
    {
        $this->icone_image = $icone_image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatutPrequis()
    {
        return $this->statut_prequis;
    }

    /**
     * @param mixed $statut_prequis
     */
    public function setStatutPrequis($statut_prequis): void
    {
        $this->statut_prequis = $statut_prequis;
    }


}
