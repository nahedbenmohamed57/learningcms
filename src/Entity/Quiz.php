<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizRepository")
 */
class Quiz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $chartId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation")
     */
    private $formation;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cours")
     */
    private $cours;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chapters")
     */
    private $chapter;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Module")
     */
    private $module;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":"1"})
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }
    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(?Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    public function getChapter(): ?Chapters
    {
        return $this->chapter;
    }

    public function setChapter(Chapters $chapter): self
    {
        $this->chapter = $chapter;

        return $this;
    }

    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }
    public function getPid(): ?string
    {
        return $this->pid;
    }

    public function setPid(?string $pid): self
    {
        $this->pid = $pid;

        return $this;
    }

    public function getChartId(): ?string
    {
        return $this->chartId;
    }

    public function setChartId(?string $chartId): self
    {
        $this->chartId = $chartId;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }


}
