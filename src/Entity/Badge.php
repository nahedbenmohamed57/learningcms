<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BadgeRepository")
 * @Vich\Uploadable
 */
class Badge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $period;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $duty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quiz;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $score;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $time;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $forum;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $rewards;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $val_duty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $val_quiz;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $val_time;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $certifs;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $note_val;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $available;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="badge")
     */
    private $formation;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $note = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $val_valid = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $val_score = [];


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture): void
    {
        $this->picture = $picture;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getPeriod(): ?int
    {
        return $this->period;
    }

    public function setPeriod(?int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getDuty(): ?string
    {
        return $this->duty;
    }

    public function setDuty(?string $duty): self
    {
        $this->duty = $duty;

        return $this;
    }

    public function getQuiz(): ?string
    {
        return $this->quiz;
    }

    public function setQuiz(?string $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }

    public function getScore(): ?string
    {
        return $this->score;
    }

    public function setScore(?string $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(?string $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getForum(): ?string
    {
        return $this->forum;
    }

    public function setForum(?string $forum): self
    {
        $this->forum = $forum;

        return $this;
    }



    /**
     * @return mixed
     */
    public function getRewards()
    {
        return $this->rewards;
    }

    /**
     * @param mixed $rewards
     */
    public function setRewards($rewards): void
    {
        $this->rewards = $rewards;
    }



    public function getValDuty(): ?string
    {
        return $this->val_duty;
    }

    public function setValDuty(?string $val_duty): self
    {
        $this->val_duty = $val_duty;

        return $this;
    }

    public function getValQuiz(): ?string
    {
        return $this->val_quiz;
    }

    public function setValQuiz(?string $val_quiz): self
    {
        $this->val_quiz = $val_quiz;

        return $this;
    }

    public function getValTime(): ?string
    {
        return $this->val_time;
    }

    public function setValTime(?string $val_time): self
    {
        $this->val_time = $val_time;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCertifs()
    {
        return $this->certifs;
    }

    /**
     * @param mixed $certifs
     */
    public function setCertifs($certifs): void
    {
        $this->certifs = $certifs;
    }



    public function getNoteVal(): ?string
    {
        return $this->note_val;
    }

    public function setNoteVal(?string $note_val): self
    {
        $this->note_val = $note_val;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAvailable(): ?int
    {
        return $this->available;
    }

    public function setAvailable(?int $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function __toString()
    {
        return "Badge Nom: ". $this->getName() . " / Badge Description: ". $this->getDescription();

    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getNote(): ?array
    {
        return $this->note;
    }

    public function setNote(?array $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getValValid(): ?array
    {
        return $this->val_valid;
    }

    public function setValValid(?array $val_valid): self
    {
        $this->val_valid = $val_valid;

        return $this;
    }

    public function getValScore(): ?array
    {
        return $this->val_score;
    }

    public function setValScore(?array $val_score): self
    {
        $this->val_score = $val_score;

        return $this;
    }


}
