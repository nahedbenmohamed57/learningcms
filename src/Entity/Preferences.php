<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PreferencesRepository")
 */
class Preferences
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $mail_config;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $video_config;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMailConfig(): ?int
    {
        return $this->mail_config;
    }

    public function setMailConfig(?int $mail_config): self
    {
        $this->mail_config = $mail_config;

        return $this;
    }

    public function getVideoConfig(): ?int
    {
        return $this->video_config;
    }

    public function setVideoConfig(?int $video_config): self
    {
        $this->video_config = $video_config;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
