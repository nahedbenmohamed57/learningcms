<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CodeRepository")
 */
class Code
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $period;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $statut = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\formation", mappedBy="code")
     */
    public $formations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Classe", mappedBy="code")
     */
    private $class;

    public function __construct()
    {
        $this->formations = new ArrayCollection();
        $this->class = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }


    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(?string $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle();
    }


    public function getStatut(): ?array
    {
        return $this->statut;
    }

    public function setStatut(?array $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return Collection|formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setCode($this);
        }

        return $this;
    }

    public function removeFormation(formation $formation): self
    {
        if ($this->formations->contains($formation)) {
            $this->formations->removeElement($formation);
            // set the owning side to null (unless already changed)
            if ($formation->getCode() === $this) {
                $formation->setCode(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Classe[]
     */
    public function getClass(): Collection
    {
        return $this->class;
    }

    public function addClass(Classe $class): self
    {
        if (!$this->class->contains($class)) {
            $this->class[] = $class;
            $class->setCode($this);
        }

        return $this;
    }

    public function removeClass(Classe $class): self
    {
        if ($this->class->contains($class)) {
            $this->class->removeElement($class);
            // set the owning side to null (unless already changed)
            if ($class->getCode() === $this) {
                $class->setCode(null);
            }
        }

        return $this;
    }

}
