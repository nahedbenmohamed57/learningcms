<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigchapitreRepository")
 */
class Configchapitre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;


    /**
     * @ORM\Column(type="time", nullable=true)
     */
    public $time_spent;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status_completed;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $score;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $score_point;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statut_accesss;

    /**
     * @ORM\Column(type="datetime", length=255, nullable=true)
     */
    private $status_dateacces;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $staut_subscribe;

    /**
     * @ORM\Column(type="datetime", length=255, nullable=true)
     */
    private $staut_datesubscribe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $staut_prerequis;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $choice_prerequis;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $valide_hours;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $staut_chapitre;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    public $choice_chapter = [];



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimeSpent()
    {
        return $this->time_spent;
    }

    /**
     * @param mixed $time_spent
     */
    public function setTimeSpent($time_spent): void
    {
        $this->time_spent = $time_spent;
    }

    /**
     * @return mixed
     */
    public function getValideHours()
    {
        return $this->valide_hours;
    }

    /**
     * @param mixed $valide_hours
     */
    public function setValideHours($valide_hours): void
    {
        $this->valide_hours = $valide_hours;
    }


    public function getStatusCompleted(): ?string
    {
        return $this->status_completed;
    }

    public function setStatusCompleted(string $status_completed): self
    {
        $this->status_completed = $status_completed;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getScorePoint(): ?string
    {
        return $this->score_point;
    }

    public function setScorePoint(string $score_point): self
    {
        $this->score_point = $score_point;

        return $this;
    }

    public function getStatutAccesss(): ?int
    {
        return $this->statut_accesss;
    }

    public function setStatutAccesss(int $statut_accesss): self
    {
        $this->statut_accesss = $statut_accesss;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusDateacces()
    {
        return $this->status_dateacces;
    }

    /**
     * @param mixed $status_dateacces
     */
    public function setStatusDateacces($status_dateacces): void
    {
        $this->status_dateacces = $status_dateacces;
    }


    public function getStautSubscribe(): ?int
    {
        return $this->staut_subscribe;
    }

    public function setStautSubscribe(int $staut_subscribe): self
    {
        $this->staut_subscribe = $staut_subscribe;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStautDatesubscribe()
    {
        return $this->staut_datesubscribe;
    }

    /**
     * @param mixed $staut_datesubscribe
     */
    public function setStautDatesubscribe($staut_datesubscribe): void
    {
        $this->staut_datesubscribe = $staut_datesubscribe;
    }


    public function getStautPrerequis(): ?int
    {
        return $this->staut_prerequis;
    }

    public function setStautPrerequis(int $staut_prerequis): self
    {
        $this->staut_prerequis = $staut_prerequis;

        return $this;
    }

    public function getChoicePrerequis(): ?string
    {
        return $this->choice_prerequis;
    }

    public function setChoicePrerequis(string $choice_prerequis): self
    {
        $this->choice_prerequis = $choice_prerequis;

        return $this;
    }



    public function getStautChapitre(): ?int
    {
        return $this->staut_chapitre;
    }

    public function setStautChapitre(int $staut_chapitre): self
    {
        $this->staut_chapitre = $staut_chapitre;

        return $this;
    }

    public function getChoiceChapter(): ?array
    {
        return $this->choice_chapter;
    }

    public function setChoiceChapter(?array $choice_chapter): self
    {
        $this->choice_chapter = $choice_chapter;

        return $this;
    }

}
