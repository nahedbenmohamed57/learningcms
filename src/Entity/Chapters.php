<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChaptersRepository")
 */
class Chapters
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $chartId;

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param mixed $pid
     */
    public function setPid($pid): void
    {
        $this->pid = $pid;
    }



    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Quiz", cascade={"persist", "remove"})
     */
    private $quiz;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="chapters")
     */
    private $formation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Configchapitre", cascade={"persist", "remove"})
     */
    private $config;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BuilderContent", mappedBy="chapter", cascade={"persist", "remove"})
     */
    private $builderContent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cours", inversedBy="chapters")
     */
    private $cours;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $external_link;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":"1"})
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Userchapter", mappedBy="chapter")
     */
    private $userchapters;

    public function __construct()
    {
        $this->userchapters = new ArrayCollection();
    }
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

/*    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }*/

/*    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }*/



    public function getQuiz(): ?Quiz
    {
        return $this->quiz;
    }

    public function setQuiz(?Quiz $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }


    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getConfig(): ?Configchapitre
    {
        return $this->config;
    }

    public function setConfig(?Configchapitre $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getBuilderContent(): ?BuilderContent
    {
        return $this->builderContent;
    }

    public function setBuilderContent(BuilderContent $builderContent): self
    {
        $this->builderContent = $builderContent;

        // set the owning side of the relation if necessary
        if ($this !== $builderContent->getChapter()) {
            $builderContent->setChapter($this);
        }

        return $this;
    }

    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(?Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }
    public function getChartId(): ?string
    {
        return $this->chartId;
    }

    public function setChartId(?string $chartId): self
    {
        $this->chartId = $chartId;

        return $this;
    }

    public function getExternalLink(): ?string
    {
        return $this->external_link;
    }

    public function setExternalLink(?string $external_link): self
    {
        $this->external_link = $external_link;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|Userchapter[]
     */
    public function getUserchapters(): Collection
    {
        return $this->userchapters;
    }

    public function addUserchapter(Userchapter $userchapter): self
    {
        if (!$this->userchapters->contains($userchapter)) {
            $this->userchapters[] = $userchapter;
            $userchapter->setChapter($this);
        }

        return $this;
    }

    public function removeUserchapter(Userchapter $userchapter): self
    {
        if ($this->userchapters->contains($userchapter)) {
            $this->userchapters->removeElement($userchapter);
            // set the owning side to null (unless already changed)
            if ($userchapter->getChapter() === $this) {
                $userchapter->setChapter(null);
            }
        }

        return $this;
    }

}
