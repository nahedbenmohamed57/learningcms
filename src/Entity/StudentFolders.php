<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StudentFoldersRepository")
 */
class StudentFolders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="studentFolders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StudentNotes", mappedBy="studentFolder")
     */
    private $studentNotes;

    public function __construct()
    {
        $this->studentNotes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|StudentNotes[]
     */
    public function getStudentNotes(): Collection
    {
        return $this->studentNotes;
    }

    public function addStudentNote(StudentNotes $studentNote): self
    {
        if (!$this->studentNotes->contains($studentNote)) {
            $this->studentNotes[] = $studentNote;
            $studentNote->setStudentFolder($this);
        }

        return $this;
    }

    public function removeStudentNote(StudentNotes $studentNote): self
    {
        if ($this->studentNotes->contains($studentNote)) {
            $this->studentNotes->removeElement($studentNote);
            // set the owning side to null (unless already changed)
            if ($studentNote->getStudentFolder() === $this) {
                $studentNote->setStudentFolder(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return "Dossier Nom: ". $this->getName();

    }
}
