<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $titre_formation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $descprition_formation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sens_formation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $image_formation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $video_formation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confignote_formation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nodetype_formation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="formations")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Catalogue", inversedBy="formations")
     */
    private $catalogue;

    /**
     * @return mixed
     */
    public function getCatalogue()
    {
        return $this->catalogue;
    }

    /**
     * @param mixed $catalogue
     */
    public function setCatalogue($catalogue): void
    {
        $this->catalogue = $catalogue;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }
    /**
     * @return mixed
     */
    public function getNodetypeFormation()
    {
        return $this->nodetype_formation;
    }

    /**
     * @param mixed $nodetype_formation
     */
    public function setNodetypeFormation($nodetype_formation): void
    {
        $this->nodetype_formation = $nodetype_formation;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="formations")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notemax_formation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Formationconfig", cascade={"persist", "remove"})
     */
    private $configformation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Module", mappedBy="formation")
     */
    private $modules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chapters", mappedBy="formation")
     */
    private $chapters;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cours", mappedBy="formation")
     */
    private $cours;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Badge", mappedBy="formation")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $update_date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Badge", mappedBy="formation")
     */
    private $badge;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Code", inversedBy="formations")
     */
    private $code;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    public $category_choice = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Noteformation", mappedBy="formation")
     */
    private $noteformations;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $jsonData = [];

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":"1"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $banner;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Userchapter", mappedBy="formation")
     */
    private $userchapters;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $favorite;



    public function __construct()
    {
        $this->modules = new ArrayCollection();
        $this->chapters = new ArrayCollection();
        $this->cours = new ArrayCollection();
        $this->badge = new ArrayCollection();
        $this->noteformations = new ArrayCollection();
        $this->userchapters = new ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getConfigformation()
    {
        return $this->configformation;
    }

    /**
     * @param mixed $configformation
     */
    public function setConfigformation($configformation): void
    {
        $this->configformation = $configformation;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreFormation(): ?string
    {
        return $this->titre_formation;
    }

    public function setTitreFormation(string $titre_formation): self
    {
        $this->titre_formation = $titre_formation;

        return $this;
    }

    public function getDescpritionFormation(): ?string
    {
        return $this->descprition_formation;
    }

    public function setDescpritionFormation(string $descprition_formation): self
    {
        $this->descprition_formation = $descprition_formation;

        return $this;
    }

    public function getSensFormation(): ?int
    {
        return $this->sens_formation;
    }

    public function setSensFormation(int $sens_formation): self
    {
        $this->sens_formation = $sens_formation;

        return $this;
    }

    public function getImageFormation(): ?string
    {
        return $this->image_formation;
    }

    public function setImageFormation(string $image_formation): self
    {
        $this->image_formation = $image_formation;

        return $this;
    }

    public function getVideoFormation(): ?string
    {
        return $this->video_formation;
    }

    public function setVideoFormation(string $video_formation): self
    {
        $this->video_formation = $video_formation;

        return $this;
    }

    public function getConfignoteFormation(): ?string
    {
        return $this->confignote_formation;
    }

    public function setConfignoteFormation(string $confignote_formation): self
    {
        $this->confignote_formation = $confignote_formation;

        return $this;
    }



    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getNotemaxFormation(): ?string
    {
        return $this->notemax_formation;
    }

    public function setNotemaxFormation(string $notemax_formation): self
    {
        $this->notemax_formation = $notemax_formation;

        return $this;
    }

    /**
     * @return Collection|Module[]
     */
    public function getModules(): Collection
    {
        return $this->modules;
    }

    public function addModule(Module $module): self
    {
        if (!$this->modules->contains($module)) {
            $this->modules[] = $module;
            $module->setFormation($this);
        }

        return $this;
    }

    public function removeModule(Module $module): self
    {
        if ($this->modules->contains($module)) {
            $this->modules->removeElement($module);
            // set the owning side to null (unless already changed)
            if ($module->getFormation() === $this) {
                $module->setFormation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Chapters[]
     */
    public function getChapters(): Collection
    {
        return $this->chapters;
    }

    public function addChapters(Chapters $chapters): self
    {
        if (!$this->chapters->contains($chapters)) {
            $this->chapters[] = $chapters;
            $chapters->setFormation($this);
        }

        return $this;
    }

    public function removeChapters(Chapters $chapters): self
    {
        if ($this->chapters->contains($chapters)) {
            $this->chapters->removeElement($chapters);
            // set the owning side to null (unless already changed)
            if ($chapters->getFormation() === $this) {
                $chapters->setFormation(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setFormation($this);
        }
        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->contains($cour)) {
            $this->cours->removeElement($cour);
            // set the owning side to null (unless already changed)
            if ($cour->getFormation() === $this) {
                $cour->setFormation(null);
            }
        }

        return $this;
    }




    public function getCategoryId(): ?Category
    {
        return $this->category_id;
    }

    public function setCategoryId(?Category $category_id): self
    {
        $this->category_id = $category_id;

        return $this;
    }

    public function getCatalogueId(): ?Catalogue
    {
        return $this->catalogue_id;
    }

    public function setCatalogueId(?Catalogue $catalogue_id): self
    {
        $this->catalogue_id = $catalogue_id;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPid(): ?string
    {
        return $this->pid;
    }

    public function setPid(?string $pid): self
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * @param mixed $update_date
     */
    public function setUpdateDate($update_date): void
    {
        $this->update_date = $update_date;
    }



    /**
     * @return Collection|Badge[]
     */
    public function getBadge(): Collection
    {
        return $this->badge;
    }

    public function addBadge(Badge $badge): self
    {
        if (!$this->badge->contains($badge)) {
            $this->badge[] = $badge;
            $badge->setFormation($this);
        }

        return $this;
    }

    public function removeBadge(Badge $badge): self
    {
        if ($this->badge->contains($badge)) {
            $this->badge->removeElement($badge);
            // set the owning side to null (unless already changed)
            if ($badge->getFormation() === $this) {
                $badge->setFormation(null);
            }
        }

        return $this;
    }

    public function getCode(): ?Code
    {
        return $this->code;
    }

    public function setCode(?Code $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryChoice()
    {
        return $this->category_choice;
    }


    function addCategorie($categorie) {
        $this->category_choice[] = $categorie;
    }

    /**
     * @return Collection|Noteformation[]
     */
    public function getNoteformations(): Collection
    {
        return $this->noteformations;
    }

    public function addNoteformation(Noteformation $noteformation): self
    {
        if (!$this->noteformations->contains($noteformation)) {
            $this->noteformations[] = $noteformation;
            $noteformation->setFormation($this);
        }

        return $this;
    }

    public function removeNoteformation(Noteformation $noteformation): self
    {
        if ($this->noteformations->contains($noteformation)) {
            $this->noteformations->removeElement($noteformation);
            // set the owning side to null (unless already changed)
            if ($noteformation->getFormation() === $this) {
                $noteformation->setFormation(null);
            }
        }

        return $this;
    }

    public function getJsonData(): ?array
    {
        return $this->jsonData;
    }

    public function setJsonData(?array $jsonData): self
    {
        $this->jsonData = $jsonData;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(?string $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function getUserchapter(): ?Userchapter
    {
        return $this->userchapter;
    }

    public function setUserchapter(?Userchapter $userchapter): self
    {
        $this->userchapter = $userchapter;

        return $this;
    }

    /**
     * @return Collection|Userchapter[]
     */
    public function getUserchapters(): Collection
    {
        return $this->userchapters;
    }

    public function addUserchapter(Userchapter $userchapter): self
    {
        if (!$this->userchapters->contains($userchapter)) {
            $this->userchapters[] = $userchapter;
            $userchapter->setFormation($this);
        }

        return $this;
    }

    public function removeUserchapter(Userchapter $userchapter): self
    {
        if ($this->userchapters->contains($userchapter)) {
            $this->userchapters->removeElement($userchapter);
            // set the owning side to null (unless already changed)
            if ($userchapter->getFormation() === $this) {
                $userchapter->setFormation(null);
            }
        }

        return $this;
    }

    public function getFavorite(): ?string
    {
        return $this->favorite;
    }

    public function setFavorite(?string $favorite): self
    {
        $this->favorite = $favorite;

        return $this;
    }


}
