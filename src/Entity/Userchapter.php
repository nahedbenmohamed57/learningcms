<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserchapterRepository")
 */
class Userchapter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="time", nullable=true)
     */
    public $hours_pass;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userchapters")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chapters", inversedBy="userchapters")
     */
    private $chapter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $statuts;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="userchapters")
     */
    private $formation;



    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHoursPass()
    {
        return $this->hours_pass;
    }

    /**
     * @param mixed $hours_pass
     */
    public function setHoursPass($hours_pass): void
    {
        $this->hours_pass = $hours_pass;
    }


    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getChapter(): ?Chapters
    {
        return $this->chapter;
    }

    public function setChapter(?Chapters $chapter): self
    {
        $this->chapter = $chapter;

        return $this;
    }

    public function getStatuts(): ?string
    {
        return $this->statuts;
    }

    public function setStatuts(?string $statuts): self
    {
        $this->statuts = $statuts;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }





}
