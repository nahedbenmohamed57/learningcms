<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @Vich\Uploadable
 * @UniqueEntity(
 * fields = {"email"},
 * message = "L'email que vous avez indiqué est déjà utilisé !")
 */
class User implements UserInterface , \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Assert\NotBlank
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Assert\Length(min="8", minMessage="Votre mot de passe doit faire minimum 8 caractères")
     */
    protected $password;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pseudo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $region;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $additionalInformation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $pinterest;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $facebook;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $twitter;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $instagrame;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $picture;

    /**
     * @ORM\Column(type="array")
     */
    public $roles;

    /**
     * @Vich\UploadableField(mapping="users_images", fileNameProperty="picture")
     * @var File|null
     */
     public $imageFile;

     /**
      * @ORM\Column(type="datetime")
      * @var \DateTime
      */
    protected $updatedAt;

    public $newPassword;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $gender;

    /**
     * @var string le token qui servira lors de l'oubli de mot de passe
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $resetToken;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StudentFolders", mappedBy="user", orphanRemoval=true)
     */
    private $studentFolders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StudentNotes", mappedBy="user")
     */
    private $studentNotes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="user")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DocumentFolders", mappedBy="user")
     */
    private $documentFolders;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $counter;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Formation", mappedBy="user")
     */
    private $formations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Classe", inversedBy="users")
     */
    private $classe;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Formation")
     */
    private $formations_sup;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Userchapter", mappedBy="user")
     */
    private $userchapters;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Video", mappedBy="user")
     */
    private $videos;


    public function __construct()
    {
        $this->roles = ['ROLE_USER'];
        $this->studentFolders = new ArrayCollection();
        $this->studentNotes = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->documentFolders = new ArrayCollection();
        $this->formations = new ArrayCollection();
        $this->classe = new ArrayCollection();
        $this->formations_sup = new ArrayCollection();
        $this->userchapters = new ArrayCollection();
        $this->videos = new ArrayCollection();

    }
    public function getNewPassword() {
        return $this->newPassword;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

    public function getPinterest(): ?string
    {
        return $this->pinterest;
    }

    public function setPinterest(?string $pinterest): self
    {
        $this->pinterest = $pinterest;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getInstagrame(): ?string
    {
        return $this->instagrame;
    }

    public function setInstagrame(?string $instagrame): self
    {
        $this->instagrame = $instagrame;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    function addRole($role) {
        $this->roles[] = $role;
    }

    public function eraseCredentials() {}

    public function getSalt() {}

    public function getRoles() {
        if (empty($this->roles)) {
            return ['ROLE_USER'];
        }
        return $this->roles;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     * @throws \Exception
     */

    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            ) = unserialize($serialized);
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getGender(): ?bool
    {
        return $this->gender;
    }

    public function setGender(?bool $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getResetToken(): string
    {
        return $this->resetToken;
    }

    /**
     * @param string $resetToken
     */
    public function setResetToken(?string $resetToken): void
    {
        $this->resetToken = $resetToken;
    }

    /**
     * @return Collection|StudentFolders[]
     */
    public function getStudentFolders(): Collection
    {
        return $this->studentFolders;
    }

    public function addStudentFolder(StudentFolders $studentFolder): self
    {
        if (!$this->studentFolders->contains($studentFolder)) {
            $this->studentFolders[] = $studentFolder;
            $studentFolder->setUser($this);
        }

        return $this;
    }

    public function removeStudentFolder(StudentFolders $studentFolder): self
    {
        if ($this->studentFolders->contains($studentFolder)) {
            $this->studentFolders->removeElement($studentFolder);
            // set the owning side to null (unless already changed)
            if ($studentFolder->getUser() === $this) {
                $studentFolder->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StudentNotes[]
     */
    public function getStudentNotes(): Collection
    {
        return $this->studentNotes;
    }

    public function addStudentNote(StudentNotes $studentNote): self
    {
        if (!$this->studentNotes->contains($studentNote)) {
            $this->studentNotes[] = $studentNote;
            $studentNote->setUser($this);
        }

        return $this;
    }

    public function removeStudentNote(StudentNotes $studentNote): self
    {
        if ($this->studentNotes->contains($studentNote)) {
            $this->studentNotes->removeElement($studentNote);
            // set the owning side to null (unless already changed)
            if ($studentNote->getUser() === $this) {
                $studentNote->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setUser($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getUser() === $this) {
                $document->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DocumentFolders[]
     */
    public function getDocumentFolders(): Collection
    {
        return $this->documentFolders;
    }

    public function addDocumentFolder(DocumentFolders $documentFolder): self
    {
        if (!$this->documentFolders->contains($documentFolder)) {
            $this->documentFolders[] = $documentFolder;
            $documentFolder->setUser($this);
        }

        return $this;
    }

    public function removeDocumentFolder(DocumentFolders $documentFolder): self
    {
        if ($this->documentFolders->contains($documentFolder)) {
            $this->documentFolders->removeElement($documentFolder);
            // set the owning side to null (unless already changed)
            if ($documentFolder->getUser() === $this) {
                $documentFolder->setUser(null);
            }
        }

        return $this;
    }

    public function zip_code(): ?string
    {
        return $this->counter;
    }

    public function setCounter(string $counter): self
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setUser($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->contains($formation)) {
            $this->formations->removeElement($formation);
            // set the owning side to null (unless already changed)
            if ($formation->getUser() === $this) {
                $formation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Classe[]
     */
    public function getClasse(): Collection
    {
        return $this->classe;
    }

    public function addClasse(Classe $classe): self
    {
        if (!$this->classe->contains($classe)) {
            $this->classe[] = $classe;
        }

        return $this;
    }

    public function removeClasse(Classe $classe): self
    {
        if ($this->classe->contains($classe)) {
            $this->classe->removeElement($classe);
        }

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormationsSup(): Collection
    {
        return $this->formations_sup;
    }

    public function addFormationsSup(Formation $formationsSup): self
    {
        if (!$this->formations_sup->contains($formationsSup)) {
            $this->formations_sup[] = $formationsSup;
        }

        return $this;
    }

    public function removeFormationsSup(Formation $formationsSup): self
    {
        if ($this->formations_sup->contains($formationsSup)) {
            $this->formations_sup->removeElement($formationsSup);
        }

        return $this;
    }

    /**
     * @return Collection|Userchapter[]
     */
    public function getUserchapters(): Collection
    {
        return $this->userchapters;
    }

    public function addUserchapter(Userchapter $userchapter): self
    {
        if (!$this->userchapters->contains($userchapter)) {
            $this->userchapters[] = $userchapter;
            $userchapter->setUser($this);
        }

        return $this;
    }

    public function removeUserchapter(Userchapter $userchapter): self
    {
        if ($this->userchapters->contains($userchapter)) {
            $this->userchapters->removeElement($userchapter);
            // set the owning side to null (unless already changed)
            if ($userchapter->getUser() === $this) {
                $userchapter->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->addUser($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->videos->contains($video)) {
            $this->videos->removeElement($video);
            $video->removeUser($this);
        }

        return $this;
    }



}
