<?php


namespace App\Chat;


use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class Chat implements MessageComponentInterface
{
    public $clients;

    /**
     * Chat constructor.
     */
    public function __construct()
    {
        $this->clients = new \SplObjectStorage();
    }

    /**
     * @param ConnectionInterface $conn
     */
    function onOpen(ConnectionInterface $conn): void
    {
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
    }

    /**
     * @param ConnectionInterface $conn
     */
    function onClose(ConnectionInterface $conn): void
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        unset($this->connectedUsersNames[$conn->resourceId]);
        unset($this->connectedUsers[$conn->resourceId]);
       /* foreach ($this->connections as $key => $connection){
            if($connection === $conn){
                unset($this->connections[$key]);
                break;
            }
        }

        $conn->send('Bye, see u soon.' . PHP_EOL);
        $conn->close();*/
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    function onError(ConnectionInterface $conn, \Exception $e): void
    {
        $conn->send('Error ' . $e->getMessage() . PHP_EOL);
        $conn->close();
    }

    /**
     * @param ConnectionInterface $from
     * @param string $msg
     */
    function onMessage(ConnectionInterface $from, $msg): void
    {
        $messageData = json_decode(trim($msg));

        foreach ($this->clients as $client) {
                $client->send($messageData);
        }
    }
}