$('#delete-listClasses').click(function () {
    //get all selected badges
    var classes = [];
    $.each($("input[name='classe-check']:checked"), function () {
        classes.push($(this).attr('data-id'));
    });
    console.log(classes);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: '/classe/deleteClasses',
        data: {classes: classes},
        success: function (data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
$('.delete-class').click(function (e) {
    $('.delete-class').removeClass('active');
    $(this).addClass('active');
})
$('#delete-selectedClass').click(function (e) {
    e.preventDefault();
    var id = $('.delete-class.active').attr('data-id');
    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: '/classe/deleteClasse',
        success: function (data) {
            location.reload();
        }
    });
});
$('.duplicate-classes').click(function () {
    //get all selected classes
    var classes = [];
    $.each($("input[name='classe-check']:checked"), function () {
        classes.push($(this).attr('data-id'));
    });
    console.log(classes);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: '/classe/duplicateClasses',
        data: {classes: classes},
        success: function (data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
