var globalFontstep = {};
$(document).ready(function() {
    $("#file-to-upload-step1 #file").change(function(){
        //download file and save it on the library
        var fd    = new FormData();
        var files = $('#file-to-upload-step1  #file')[0].files[0];
        var typedoc=$("#videodoc").val();
        var typeextention=$("#library-modal-step1").data("type");



        var ext = $('#file-to-upload-step1  #file')[0].files[0].type.split('/')[1];

        if(ext=="pdf" && typedoc=="doc"
            || typedoc=="docx" && typedoc=="doc"
            || typedoc=="doc" && typedoc=="doc"
            || ext=="msword" && typedoc=="doc" ) {
            fd.append('file', files);
            $('#file-to-upload-step1  .value-file').val(files.name);

            uploadFile(fd);
        }


        var typevideo=  $("#typevideoorimage").val();
        if(typevideo!=""){
            if($('#file-to-upload-step1  #file')[0].files[0].type.split('/')[0]==typevideo)
            {
                fd.append('file', files);
                $('#file-to-upload-step1  .value-file').val(files.name);
                $("#nameimageforrmation").val(files.name);
                uploadFile(fd);
            }
            else{
                alert("Merci de choisir un type correct");
            }

        }


    });
    $('.dounload-btn').click(function() {
        $('#dounload').addClass("show");
        $('#gallerie').removeClass('show');
        $('#models').removeClass('show');

    })
    $('.gallerie-btn').click(function() {
        $('#dounload').removeClass('show');
        $('#gallerie').addClass("show");
        $('#models').removeClass('show');
    });
    $('.models-btn').click(function() {
        $('#dounload').removeClass('show');
        $('#gallerie').removeClass("show");
        $('#models').addClass("show");
    });
    $('#upload-fond-doc').click(function(){
        $('#library-modal-step1').attr('data-type','doc');
        $('#library-modal-step1 .image').addClass('d-none');
        $('#library-modal-step1 .video').addClass('d-none');
        $('#library-modal-step1 .doc').removeClass('d-none');
        $("#videodoc").val("doc");
        $("#typevideoorimage").val("");
    });
    $('#upload-fond-video').click(function(){
        $('#library-modal-step1').attr('data-type','video');
        $('#library-modal-step1 .image').addClass('d-none');
        $('#library-modal-step1 .video').removeClass('d-none');
        $('#library-modal-step1 .doc').addClass('d-none');
        $("#typevideoorimage").val("video");
    });
    $('#upload-global-image').click(function(){
        $('#library-modal-step1').attr('data-type','image');
        $('#library-modal-step1 .video').addClass('d-none');
        $('#library-modal-step1 .doc').addClass('d-none');
        $('#library-modal-step1.image').removeClass('d-none');
        $("#typevideoorimage").val("image");
    });
    $('#upload-global-images').click(function(){
        $('#library-modal-step1').attr('data-type','image');
        $('#library-modal-step1 .video').addClass('d-none');
        $('#library-modal-step1 .doc').addClass('d-none');
        $('#library-modal-step1.image').removeClass('d-none');
        $("#typevideoorimage").val("image");
    });
    $('#library-modal-step1 .card-img-top').click(function  (){
        $('#library-modal .card-img-top').removeClass('selected')
        $(this).addClass('selected');
    });
    $('#library-modal-step1 #save-file').click(function () {
        var dataType = $('#library-modal-step1').attr("data-type");
        var useconnect =$("#user-id").val();
        var selectedImage = $('#library-modal-step1 .value-file').val();
        var imageFromMedia = $('#library-modal-step1 .card-img-top.selected').attr('data-image-gl');
        var imageFromdoc = $('#library-modal-step1 .card-img-top.selected').attr('data-doc-gl');
        $('#library-modal-step1').modal('hide');

        if(dataType == "doc") {
            console.log("doc");
            globalFontstep.doc = {};
            if(imageFromdoc) {
                console.log(imageFromdoc);
                globalFontstep.doc.filename = imageFromdoc;
                globalFontstep.doc.type="devoir";
                $('#picture').val(globalFontstep.doc.filename);

            } else {

                globalFontstep.doc.filename = selectedImage;
                globalFontstep.doc.type="devoir";
                console.log(  globalFontstep.doc.filename);
                $('#picture').val(globalFontstep.doc.filename);
            }

        }
        if(dataType == 'image') {
            globalFontstep.image = {};
            if(imageFromMedia) {
                globalFontstep.image.filename = imageFromMedia;
                $('#imagestep1 img').attr('src','/images/uploads/'+useconnect+'/documents/'+imageFromMedia);
                $('#imagestep1 ').css("background","transparent");
                $('#formation_image_formation').val(globalFontstep.image.filename);
                $('#icone_image').val(globalFontstep.image.filename);
                $('#badge_picture').val(globalFontstep.image.filename);
                $('#imagestep1 img').addClass("img-responsive");
                $('#imagestep1 img').addClass("sizeimg");
                $('#moduleconfigitemdat .box img').addClass("img-responsive");
                $('#moduleconfigitemdat .box img').attr('src','/images/uploads/'+useconnect+'/documents/'+imageFromMedia);
                $('#moduleconfigitemdat .box img').css("background","transparent");
                $('#imageformationstep1').css("background","transparent");
                $('#imageformationstep1 img').addClass("sizeimg");
                $('#imageformationstep1 img').attr('src','/images/uploads/'+useconnect+'/documents/'+imageFromMedia);
                $(".hiddenimgappend").addClass("hidden");
                $(".head-img-formation").addClass("hidden");
                $(".hiddenimgappend").addClass("hidden");
                $(".head-img-formation").addClass("hidden");
                $("#appendpicture").removeClass("hidden");
                $('#appendpicture img').attr('src','/images/uploads/'+useconnect+'/documents/'+imageFromMedia);
                $('#appendpicture img').addClass("img-responsive");

            } else {
                globalFontstep.image.filename = selectedImage;
                $('#imagestep1 img').attr('src','/images/uploads/'+useconnect+'/documents/'+imageFromMedia);
                $('#imagestep1').css("background","transparent");
                $('#icone_image').val(globalFontstep.image.filename);
                $('#badge_picture').val(globalFontstep.image.filename);
                $('#formation_image_formation').val(globalFontstep.image.filename);
                $('#imageformationstep1').css("background","transparent");
                $('#imagestep1 img').addClass("sizeimg");
                $('#imageformationstep1 img').addClass("sizeimg");
                $('#imageformationstep1 img').attr('src','/images/uploads/'+useconnect+'/documents/'+selectedImage);
                $('#moduleconfigitemdat .box img').addClass("img-responsive");
                $('#moduleconfigitemdat .box img').attr('src','/images/uploads/'+useconnect+'/documents/'+imageFromMedia);
                $('#moduleconfigitemdat .box img').css("background","transparent");
                $('#imagestep1 img').addClass("img-responsive");
                $(".hiddenimgappend").addClass("hidden");
                $(".head-img-formation").addClass("hidden");
                $("#appendpicture").removeClass("hidden");
                $('#appendpicture img').attr('src','/images/uploads/'+useconnect+'/documents/'+selectedImage);
                $('#appendpicture img').addClass("img-responsive");
                $("#nameimageforrmation").val(globalFontstep.image.filename);
            }


        } else if(dataType == "video") {
            //save the video of font on the table globalFont
            var imageFromMedia = $('#library-modal-step1 .card-img-top.selected').attr('data-video-gl');
            globalFontstep.video = {};
            if(imageFromMedia) {
                globalFontstep.video.videoFont = imageFromMedia;
                $('#formation_video_formation').val( globalFontstep.video.videoFont);
                $("#videostep img").remove();
                $("#advideo").attr('controls', 'true');
                $("#advideo source").attr('src', '/images/uploads/'+useconnect+'/documents/'+selectedImage);
                $("#advideo").css("opacity","1");
                $("#advideo").css("height","100%");
                $('#videostep').css("background","transparent");
            } else {

                globalFontstep.video.videoFont = selectedImage;
                $('#formation_video_formation').val( globalFontstep.video.videoFont);
                $("#videostep img").remove();
                $("#advideo").attr('controls', 'true');
                $("#advideo source").attr('src', '/images/uploads/'+useconnect+'/documents/'+selectedImage);
                $("#advideo").css("opacity","1");
                $("#advideo").css("height","100%");
                $('#videostep').css("background","transparent");

            }

        }

    });
});
function uploadFile(file) {
    console.log(file);
    $.ajax({
        type: "POST",
        url: '../../builder/uploadForBuilder',
        data: file,
        contentType: false,
        processData: false,
        success: function(response){

        },
    });
}