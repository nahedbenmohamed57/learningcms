var canvas = document.getElementById('canvas');
canvas.addEventListener('circleProgressBar.afterDraw', function (e) {
    var span = document.getElementById('canvas-data');
    span.innerText = (e.detail.self.getValue() * 100).toFixed(0);
}, false);
canvas.addEventListener('circleProgressBar.afterFrameDraw', function (e) {
    var span = document.getElementById('canvas-data');
    var currentValue = (e.detail.self.getValue() * e.detail.progress * 100).toFixed(0);
    var currentSpanValue = span.innerText;
    if (currentSpanValue != currentValue) {
        span.innerText = currentValue;
    }
}, false);
var rainbowColors = ['#3f2d87', '#aa2b87', '#d22228', '#d22228', '#d22228'];
var colors = ['#336330'];
var circleProgressBar = new CircleProgressBar(canvas, {colors: colors,trackLineColor:'#5A9346'});
circleProgressBar.setValue(0.9);

/** cercel 2 **/
var canvas2 = document.getElementById('canvas2');

canvas2.addEventListener('circleProgressBar.afterDraw', function (e) {
    var span = document.getElementById('canvas-data2');
    span.innerText = (e.detail.self.getValue() * 100).toFixed(0);
}, false);
canvas2.addEventListener('circleProgressBar.afterFrameDraw', function (e) {
    var span = document.getElementById('canvas-data2');
    var currentValue = (e.detail.self.getValue() * e.detail.progress * 100).toFixed(0);
    var currentSpanValue = span.innerText;
    if (currentSpanValue != currentValue) {
        span.innerText = currentValue;
    }
}, false);;
var rainbowColors = ['#3f2d87', '#aa2b87', '#d22228', '#d22228', '#d22228'];
var colors2 = ['#0B5D69'];
var circleProgressBar2 = new CircleProgressBar(canvas2, {colors: colors2, trackLineColor:'#AEE0ED'});
circleProgressBar2.setValue(0.8);

/** cercel 3 **/
var canvas3 = document.getElementById('canvas3');

canvas3.addEventListener('circleProgressBar.afterDraw', function (e) {
    var span = document.getElementById('canvas-data3');
    span.innerText = (e.detail.self.getValue() * 100).toFixed(0);
}, false);
canvas3.addEventListener('circleProgressBar.afterFrameDraw', function (e) {
    var span = document.getElementById('canvas-data3');
    var currentValue = (e.detail.self.getValue() * e.detail.progress * 100).toFixed(0);
    var currentSpanValue = span.innerText;
    if (currentSpanValue != currentValue) {
        span.innerText = currentValue;
    }
}, false);;
var rainbowColors = ['#AEE0ED'];
var colors3 = ['#0B5D69'];
var circleProgressBar3 = new CircleProgressBar(canvas3, {colors: colors3, trackLineColor:'#AEE0ED'});
var getheureforhidden=document.getElementById('counthours').value;
console.log(getheureforhidden);
datesplit = getheureforhidden.split(':');
var heurefinale=datesplit[0]/100+" h";
console.log(heurefinale);
circleProgressBar3.setValue( heurefinale);
/** cercel 4 **/

var canvas4 = document.getElementById('canvas4');

canvas4.addEventListener('circleProgressBar.afterDraw', function (e) {
    var span = document.getElementById('canvas-data4');
    span.innerText = (e.detail.self.getValue() * 100).toFixed(0);
}, false);
canvas4.addEventListener('circleProgressBar.afterFrameDraw', function (e) {
    var span = document.getElementById('canvas-data4');
    var currentValue = (e.detail.self.getValue() * e.detail.progress * 100).toFixed(0);
    var currentSpanValue = span.innerText;
    if (currentSpanValue != currentValue) {
        span.innerText = currentValue;
    }
}, false);;
var colors5 = ['#5A9346'];
var circleProgressBar4 = new CircleProgressBar(canvas4, {colors: colors5, trackLineColor:'#336330'});
var numbertotal=document.getElementById('numberformation').value /100;
circleProgressBar4.setValue(numbertotal);
