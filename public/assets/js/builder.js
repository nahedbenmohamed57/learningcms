
/* Non-popup picker */
pickerFixed = new Picker({
    parent: document.querySelector('#main-color'),
    popup: false,
    alpha: false,
    editorFormat: 'rgb',
    color: '#fff',
    onChange: function(color) {
        //document.querySelector('.appercu-color').style.backgroundColor = color.rgbaString;
        if(document.querySelector('.cl-1')) {
            document.querySelector('.cl-1').style.backgroundColor = color.rgbaString;
            document.querySelector('.cl-2').style.backgroundColor = color.rgbaString;
            document.querySelector('.cl-3').style.backgroundColor = color.rgbaString;
            document.querySelector('.cl-4').style.backgroundColor = color.rgbaString;
            document.querySelector('.cl-5').style.backgroundColor = color.rgbaString;
        }
    },
});

pickerFixed.setColor('lime', true);
pickerFixed.hide();

var k =  1;
var numelment  = 1;
var globalFont = {'general': {},'components': []};
/*const pickr3 = new Pickr({
    el: '#color-picker-3',
    useAsButton: true,
    default: "303030",
    showAlways: true,
    components: {
        preview: true,
        opacity: true,
        hue: true,

        interaction: {
            input: true,
        }
    },

    onChange(hsva, instance) {
        //update the global fond color

        if(!$('#config-sidebar').hasClass('d-none')) {
            globalFont.general.fondColor = hsva.toRGBA().toString();
            $('#config-sidebar .appercu-color').attr('data-color', hsva.toRGBA().toString());
            $('#config-sidebar .appercu-color').css('background-color', hsva.toRGBA().toString());
            delete globalFont.general.image;
            delete  globalFont.general.video;
        } else {
            $('#fond .appercu-color').attr('data-color', hsva.toRGBA().toString());
            $('#fond .appercu-color').css('background-color', hsva.toRGBA().toString());
        }
    }
});

 */

//pickr3.hide();

$(document).ready(function() {
    var chapterId = $('#chapterId').val();
    $.ajax({
        type: "GET",
        url: '/builder/chapterToBuild/'+chapterId,
        success: function(response){
            if(response != 'error') {
                let result = JSON.parse(response);
                if(JSON.parse(result.components.length) >0) {
                    let lastId = result.components[result.components.length-1].id;
                    let idTab  = lastId.split('-');
                    numelment = parseInt(idTab[1])+1;
                    result.components.forEach(function(obj) {
                        if(obj.id){
                            globalFont.components.push(obj);
                        }
                    })
                }
                if(result.general_config) {
                    console.log(result.general_config)
                    result.general_config.forEach(function(obj) {
                        if(obj.type == 'text') {
                            globalFont.general.text = obj;
                            if(obj.colorText) {
                                $('#fondText .appercu-color-text').attr('data-color', obj.colorText)
                                $('#fondText .appercu-color-text').css('background-color', obj.colorText);
                                $('.builder').css('color', obj.colorText);
                            }
                        } else if(obj.type == 'image') {
                            globalFont.general.image = obj;
                            $('#config-sidebar #file-fond .value-file').val(obj.name);
                        } else if(obj.type == 'video') {
                            globalFont.general.video = obj;
                            $('#config-sidebar #video-font .value-file').val(obj.filename);
                        } else if(obj.type == 'fondColor') {
                            globalFont.general.fondColor = obj;
                            $('#config-sidebar .appercu-color').attr('data-color', obj.fondColor);
                            $('#config-sidebar .appercu-color').css('background-color', obj.fondColor);
                        }

                    });
                    /*globalFont.general = result.general_config ;
                    if(globalFont.general.fondColor) {
                        $('#config-sidebar .appercu-color').attr('data-color', globalFont.general.fondColor);
                        $('#config-sidebar .appercu-color').css('background-color', globalFont.general.fondColor);
                    }
                    if(globalFont.general.image) {
                        //set the image config
                        $('#config-sidebar #file-fond .value-file').val(globalFont.general.image.name);
                    }
                    if(globalFont.general.video) {
                        //set the video config
                        $('#config-sidebar #video-font .value-file').val(globalFont.general.video.filename);
                    }
                    if( globalFont.general.text) {
                        if( globalFont.general.text.colorText){
                            $('#fondText .appercu-color-text').attr('data-color',globalFont.general.text.colorText)
                            $('#fondText .appercu-color-text').css('background-color', globalFont.general.text.colorText);
                            $('.bulider-content p').css('color',globalFont.general.text.colorText);
                        }
                    }*/
                }
            }
        },
    });
    $('.show-grid').click(function() {
        let offsetEelment = $(this).offset();
        $('#et-fb-settings-column').css('top',offsetEelment.top+50+'px');
        $('#et-fb-settings-column').css('left',offsetEelment.left-170+'px');
        $('.type-grid').removeClass('update-grid')
        if($('#et-fb-settings-column').hasClass('hidden')) {
            $('#et-fb-settings-column').removeClass('hidden');
        } else {
            $('#et-fb-settings-column').addClass('hidden');
        }
    })

    $('.type-grid .et-fb-settings-options-wrap ul li').click(function() {
        var selectedRow = $('.type-grid').attr('data-row_id');

        $('.show-grid.hidden').removeClass('hidden');
        $('.first-img').addClass('hidden');

        $('.default-content').addClass('newElment');
        var cols  = $(this).attr('data-layout');

        var list  = cols.split(',');
        var customHtml ='<div class ="row connectedsortable ml-1 mr-1" id="row-'+k+'"><div class="action-col"><a href="#"><img src="/assets/images/builder/zoom.png"></a>'+
            '<a href="#" onclick="showConfigSidebarRow('+k+')" ><img src="/assets/images/builder/edit-col.png"></a><a href="#" onclick="duplicateRow('+k+')"><img src="/assets/images/builder/duplicate.png"></a>'+
            '<a href="#" class="update-row-'+k+'" onclick="updateRow('+k+')"><img src="/assets/images/builder/update-row.png"></a><a href="#"  onclick="updateVisibility('+k+')" ><img src="/assets/images/builder/detail.png"></a><a href="#"><img src="/assets/images/builder/download-sm.png"></a>'+
            '<a href="#" class="delete-row" onclick="deleteRow('+k+')"><img src="/assets/images/builder/trash-sm.png"></a></div>';

        var elemntWidh = 0;
        list.forEach(function(element) {
            var widthCol = element.split('_');
            var first = widthCol[0];
            var second = widthCol[1];
            elemntWidh = 100 /second * first;
            // construre the html elment
            customHtml += '<div class="list-group-item col-item" style="width: '+elemntWidh+'%"></div>';
        });
        customHtml +='</div>';

        if(selectedRow) {
            var listCols = $('#row-'+selectedRow +' .col-item');
            var sumWidh  = 0;
            for (var i = 0; i < listCols.length; i++) {
                listCols[i].setAttribute('style','width:'+elemntWidh+'%');
                sumWidh+=elemntWidh;
                delete list[ i ];
            }
            if(list.length > 0 && listCols.length < list.length) {

                list.forEach(function(element) {
                    var widthCol = element.split('_');
                    var first = widthCol[0];
                    var second = widthCol[1];
                    elemntWidh = 100 /second * first;
                    // construre the html elment
                    var newCol = '<div class="list-group-item col-item m-0" style="width: '+elemntWidh+'%"></div>';
                    $('#row-'+selectedRow).append(newCol);
                });
                $('.type-grid').attr('data-row_id','');
            }

        } else {
            $('.bulider-content').append(customHtml);
        }

        $('.type-grid').addClass('hidden');
        var tds = $('.bulider-content .col-item');
        tds.each(function(index, element) {
            new Sortable(element, {
                group: {
                    name: 'shared',

                },
                animation: 150,
            });
        });
        k++;
    });
    var tds = $('.bulider-content .col-item');
    tds.each(function(index, element) {
        new Sortable(element, {
            group: {
                name: 'shared',

            },
            animation: 150,
        });
    });
    var uploadImage = document.getElementById('upload-image');
    if($('#upload-image').length >0) {
        new Sortable(uploadImage, {
            group: {
                name: 'shared',
                pull: 'clone'
            },
            animation: 150,
            onEnd: function (subevt) {
                $('.custom-row').removeClass('new-row');
                customHtml = '<div class="updateElment" data-type="image" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                    '<a href="#" onclick="showConfigSidebarParams('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a>'+
                    '<a href="#" onclick="showConfigSidebar('+numelment+')""><img src="/assets/images/builder/bagette-magic.png"></a>'+
                    '<a href="#" onclick="duplicateCol('+numelment+')"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a>'+
                    '<a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                    '<a href="#" class="delete-row" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
                ;
                $('.bulider-content').find('.d-none').removeClass('d-none').addClass('new-row sort-row');
                $('.new-row').children().addClass('nod-'+numelment);
                $('.bulider-content .custom-file-upload').remove();
                $('.new-row .col-12').append(customHtml);
                //add the image on the json
                addcomponent(numelment, 'image');
                numelment++;
            }
        });
    }
    //upload a video
    var uploadVideo = document.getElementById('upload-video');
    if($('#upload-video').length >0) {
        new Sortable(uploadVideo, {
            group: {
                name: 'shared',
                pull: 'clone'
            },
            animation: 150,
            onEnd: function (subevt) {
                $('.custom-row').removeClass('new-row');
                customHtml = '<div class="updateElment" data-type="video" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                    '<a href="#" onclick="showConfigSidebarParams('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a>'+
                    '<a href="#" onclick="showConfigSidebar('+numelment+')""><img src="/assets/images/builder/bagette-magic.png"></a>'+
                    '<a href="#" onclick="duplicateCol('+numelment+')"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a>'+
                    '<a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                    '<a href="#" class="delete-row" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
                ;
                $('.bulider-content').find('.custom-row.d-none').removeClass('d-none').addClass('new-row sort-row');
                $('.new-row').children().addClass('nod-'+numelment);
                $('.bulider-content .custom-file-upload').remove();
                $('.new-row .col-12').append(customHtml);
                addcomponent(numelment, 'video');
                numelment++;
            }
        });
    }
    //add text
    var uploadText = document.getElementById('upload-text');
    if($('#upload-text').length >0) {
        new Sortable(uploadText, {
            group: {
                name: 'shared',
                pull: 'clone'
            },
            animation: 150,
            onEnd: function (subevt) {
                $('.custom-row').removeClass('new-row');
                customHtml = '<div class="updateElment" data-type="text" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                    '<a href="#" onclick="editText('+numelment+')" ><img src="/assets/images/builder/pencil-cl.png"></a>'+
                    '<a href="#" onclick="showConfigSidebar('+numelment+')""><img src="/assets/images/builder/bagette-magic.png"></a>'+
                    '<a href="#" onclick="duplicateCol('+numelment+')"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a>'+
                    '<a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                    '<a href="#" class="delete-row" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
                ;
                $('.bulider-content').find('.custom-row.d-none').removeClass('d-none').addClass('new-row sort-row');
                $('.new-row').children().addClass('nod-'+numelment);
                $('.bulider-content .custom-file-upload').remove();
                $('.new-row .col-12').append(customHtml);
                addcomponent(numelment, 'text');
                numelment++;
            }
        });
    }
    var getComposition = document.getElementById('get-composition');
    if($('#get-composition').length >0) {
        new Sortable(getComposition, {
            group: {
                name: 'shared',
            },
            animation: 150
        });
    }
    var listComposition = document.getElementById('list-composition');
    if($('#list-composition').length > 0) {
        var j = 1;
        new Sortable(listComposition, {
            group: {
                name: 'shared',
                pull: 'clone'
            },
            animation: 150,
            onStart: function( event, ui ) {
                //$('.custom-row').removeClass('new-row');
                //$(ui.item).append("<div>abc<br><br>abc</div>");
            },
            onEnd: function (subevt) {
               $('.custom-row').removeClass('new-row');
                $('.bulider-content').find('.custom-row.d-none').removeClass('d-none').addClass('new-row sort-row');;
                $('.bulider-content .comp>img').remove();
                $.each($(".new-row .col"), function(index, element) {
                    var type = $(this).attr('data-type');
                    console.log(type);
                    if(type != undefined ){
                        $(this).addClass('nod-'+numelment);
                        if(type == "text") {
                            customHtml = '<div class="updateElment" data-type="'+type+'" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                                '<a href="#" onclick="editText('+numelment+')" ><img src="/assets/images/builder/pencil-cl.png"></a><a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/bagette-magic.png"></a>'+
                                '<a href="#"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a><a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                                '<a href="#" id="col-'+index+'" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
                        } else {
                            customHtml = '<div class="updateElment" data-type="'+type+'" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                                '<a href="#" onclick="showConfigSidebarParams('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a><a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/bagette-magic.png"></a>'+
                                '<a href="#"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a><a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                                '<a href="#" id="col-'+index+'" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
                        }
                        $(this).append(customHtml);
                        addcomponent(numelment, type);
                        numelment++;
                    }
                });

                j++;
            }
        });
    }

    $('#list-media').click(function(){
        $('.all-modules').addClass('hidden');
        $('.medias, .get-media').removeClass('hidden');
    });
    $('#choose-image').click(function() {
        $('.medias').addClass('hidden');
        $('.medias .builder-item ').removeClass('active');
        $('#upload-text, .get-text').addClass('hidden');
        $('#upload-image, .get-img').removeClass('hidden');
        $('#composotion .comp-image').removeClass('d-none');
        $('#composotion .comp-text').addClass('d-none');
        $('#choose-image').addClass('active');
        $('#composotion').collapse('show');
    });
    $('#choose-video').click(function() {
        $('.medias').addClass('hidden');
        $('.medias .builder-item ').removeClass('active');
        $('#upload-image, .get-img').addClass('hidden');
        $('#upload-text, .get-text').addClass('hidden');
        $('#upload-video, .get-video').removeClass('hidden');
        $('#choose-video').addClass('active');
        $('#composotion').collapse('hide');
    });
    $('#choose-text').click(function(){
        $('.all-modules').addClass('hidden');
        $('.medias, .get-media, #upload-video').addClass('hidden');
        $('#upload-text, .get-text').removeClass('hidden');

        $('#composotion .comp-image').addClass('d-none');
        $('#composotion .comp-text').removeClass('d-none');
        $('#choose-image').addClass('active');
        $('#composotion').collapse('show');
    });
    $('.get-bloc').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        $('.get-media,.get-img, .get-text').addClass('hidden');
        //$('.get-img').remove();

        $('.all-modules').removeClass('hidden');
        $('.medias, #upload-image, #upload-text').addClass('hidden');

        $('#composotion').collapse('hide');
    });
    $('.get-media').click(function(e){
        e.stopPropagation();
        e.preventDefault();

        $('.all-modules, #upload-image, #upload-text').addClass('hidden');
        $('.medias').removeClass('hidden');
        $('.get-img, .get-video, .get-text').addClass('hidden');
        $('#composotion').collapse('hide');
    });

    //global font
    $("#globalFond").on("show.bs.collapse", function(){
        if($('#pills-palette-tab').hasClass('active')) {
            //show the picker color
            $('#main-color').removeClass('hidden');
            //pickr3.show();
            pickerFixed.show();
            //change the top position of the colorpicker
            let off = $('.effect ').offset();
            if(off.top > 150) {
                $('#main-color').css('top',(off.top-60)+'px');
            } else {
                $('#main-color').css('top','0px');
            }
        }
    });
    $('list-color-gr ')

    //bagette magic
    $("#fond").on("show.bs.collapse", function(){
        if($('#font-pills-palette-tab').hasClass('active')) {
           // pickr3.show();
            pickerFixed.show();
            $('.pcr-picker').css('top', '63.753px');
        }
    });

    $('#pills-palette-tab, #font-pills-palette-tab, #pills-gradient-tab').on('click', function(event){
       // pickr3.show();
        pickerFixed.show();
        //$('.pcr-picker').css('top', '63.753px');
    });

    $("#pills-image-tab, #pills-video-tab, #font-pills-image-tab, #font-pills-video-tab").on("click", function(){
       // pickr3.hide()
        pickerFixed.hide();
    });

    $("#globalFond, #fond").on("hide.bs.collapse", function(){
        //pickr3.hide()
        pickerFixed.hide();
    });

    $('#edit-font').click(function() {
        if($('#config-sidebar').hasClass('d-none')) {
            $('.custom-side').addClass('d-none');
            $('#config-sidebar').removeClass('d-none');
            $('#main-content').removeClass('col-10');
            $('#main-content').addClass('col-8');

        } else {
            $('#config-sidebar').addClass('d-none');
            $('#main-content').removeClass('col-8');
            $('#main-content').addClass('col-10');
            pickerFixed.hide();
        }
    })


    $('.list-sttaic-color span').click(function() {
        var color = $(this).attr('data-color');
        if(!$('#config-sidebar').hasClass('d-none')) {
            $('#globalFond .appercu-color').css('background-color', color);
            $('#globalFond .appercu-color').attr('data-color',color);
            globalFont.general.fondColor = color;
            delete globalFont.general.image;
            delete  globalFont.general.video;
        } else if($('#fond').hasClass('show')){
            $('#fond .appercu-color').css('background-color', color);
            $('#fond .appercu-color').attr('data-color',color);
        }else if($('#bordures').hasClass('show') && $(this).parent().hasClass('border-color')) {
            $('.appercu-color-border').css('background-color', color);
            $('.appercu-color-border').attr('data-color',color);
            $('.display-border').css('border-color',color);
            $('.display-border').css('border-width',$('#sizeVal').text()+'px');
        } else if($('#coverVideo').hasClass('show')){
            $('.cover-Video').css('background-color', color);
            $('.cover-Video').attr('data-color',color);
        }

    })
    $('.colorText span').click(function() {
        var color = $(this).attr('data-color');
        $('.appercu-color-text').css('background-color', color);
        $('.appercu-color-text').attr('data-color', color);
    })
    $('.first-btn').click(function() {
        $('.second-btn').removeClass('active');
        $('.first-btn').addClass('active');
        $('#second').removeClass('show');
        $('#first').addClass("show");
    })
    $('.second-btn').click(function() {
        $('.second-btn').addClass('active');
        $('.first-btn').removeClass('active');
        $('#first').removeClass('show');
        $('#second').addClass("show");
    });
    $('.effect-btn').click(function() {
        $('#magic-sidebar .btn-toggle .btn').removeClass('active');
        $('.effect-btn').addClass('active');
        $('#visibility').removeClass('show');
        $('#params').removeClass('show');
        $('#effect').addClass("show");
    })
    $('.visibility-btn').click(function() {
        $('#magic-sidebar .btn-toggle .btn').removeClass('active');
        $('.visibility-btn').addClass('active');
        $('#effect').removeClass('show');
        $('#params').removeClass('show');
        $('#visibility').addClass("show");
    });
    $('.params-btn').click(function() {
        $('#magic-sidebar .btn-toggle .btn').removeClass('active');
        $('.params-btn').addClass('active');
        $('#effect').removeClass('show');
        $('#visibility').removeClass("show");
        $('#params').addClass("show");
    });

    // list of sliders
    if($("#ex8").length > 0) {
        $("#ex8").slider();
        $("#ex8").on("slide", function(slideEvt) {
            $("#ex6SliderVal").text(slideEvt.value);
        });
    }
    if($('#sliderPosition').length > 0) {
        $("#sliderPosition").slider();
        $("#sliderPosition").on("slide", function(slideEvt) {
            $("#sliderPositionVal").text(slideEvt.value);
        });
    }
    if($('#sliderPositionfin').length > 0) {
        $("#sliderPositionfin").slider();
        $("#sliderPositionfin").on("slide", function(slideEvt) {
            $("#sliderPositionfinVal").text(slideEvt.value);
        });
    }
    if($('#size').length > 0) {
        $("#size").slider();
        $("#size").on("slide", function(slideEvt) {
            $("#sizeVal").text(slideEvt.value);
            $('.display-border').css('border-width',slideEvt.value+'px');
        });
    }
    if($('#teinte').length > 0) {
        $("#teinte").slider();
        $("#teinte").on("slide", function(slideEvt) {
            $("#teinteVal").text(slideEvt.value);
        });
    }
    if($('#saturation').length > 0) {
        $("#saturation").slider();
        $("#saturation").on("slide", function(slideEvt) {
            $("#saturationVal").text(slideEvt.value);
        });
    }
    if($('#lum').length > 0) {
        $("#lum").slider();
        $("#lum").on("slide", function(slideEvt) {
            $("#lumVal").text(slideEvt.value);
        });
    }
    if($('#control').length > 0) {
        $("#control").slider();
        $("#control").on("slide", function(slideEvt) {
            $("#controlval").text(slideEvt.value);
        });
    }
    if($('#invers').length > 0) {
        $("#invers").slider();
        $("#invers").on("slide", function(slideEvt) {
            $("#inversVal").text(slideEvt.value);
        });
    }
    if($('#sepia').length > 0) {
        $("#sepia").slider();
        $("#sepia").on("slide", function(slideEvt) {
            $("#sepiaVal").text(slideEvt.value);
        });
    }
    if($('#opacity').length > 0) {
        $("#opacity").slider();
        $("#opacity").on("slide", function(slideEvt) {
            $("#opacityVal").text(slideEvt.value);
        });
    }
    if($('#resum').length > 0) {
        $("#resum").slider();
        $("#resum").on("slide", function(slideEvt) {
            $("#resumVAl").text(slideEvt.value);
        });
    }
    if($("#sizeIcon").length > 0) {
        $("#sizeIcon").slider();
        $("#sizeIcon").on("slide", function(slideEvt) {
            $("#sizeIconVal").text(slideEvt.value);
        });
    }
    if($("#fondIcon").length > 0) {
        $("#fondIcon").slider();
        $("#fondIcon").on("slide", function(slideEvt) {
            $("#fondIconVal").text(slideEvt.value);
        });
    }
    if($('#sizePolice').length > 0) {
        $("#sizePolice").slider();
        $("#sizePolice").on("slide", function(slideEvt) {
            $("#sizePoliceVAl").text(slideEvt.value);
        });
    }
    $('.list-btn .nav-item a').click(function() {
        $('.pcr-app').removeClass('gradient');
    });
    $('#upload-fond-video, #upload-video-module').click(function(){
        $('#library-modal').attr('data-type','video');
        $('#library-modal .image').addClass('d-none');
        $('#library-modal .video').removeClass('d-none');
    });
    $('#upload-global-image').click(function(){
        $('#library-modal').attr('data-type','image');
        $('#library-modal .video').addClass('d-none');
        $('#library-modal .image').removeClass('d-none');
    });

    $('.choose-img a').click(function(){
        var elementId = $('#magic-sidebar').attr('data-selected-elment');
        var type = $('#elment-'+elementId).attr('data-type');
        $('#library-modal').attr('data-type',type);
        if(type == 'video') {
            $('#library-modal .video').removeClass('d-none');
            $('#library-modal .image').addClass('d-none');
        } else {
            $('#library-modal .video').addClass('d-none');
            $('#library-modal .image').removeClass('d-none');
        }
    });

    $("#file-to-upload #file").change(function(){
        //download file and save it on the library
        var fd    = new FormData();
        var files = $('#file-to-upload  #file')[0].files[0];
        fd.append('file', files);
        $('#file-to-upload  .value-file').val(files.name);
        uploadFile(fd);
    });
    $('#library-modal .card-img-top').click(function  (){
        $('#library-modal .card-img-top').removeClass('selected')
        $(this).addClass('selected')
    });
    $('#library-modal #save-file').click(function () {

        let dataType      = $('#library-modal').attr("data-type");
        let selectedImage = $('#library-modal .value-file').val();
        if(dataType == 'image' || dataType == 'image-module') {
            var nameFromMedia = $('#library-modal .card-img-top.selected').attr('data-image-gl');
        } else if(dataType == 'video') {
            var nameFromMedia = $('#library-modal .card-img-top.selected').attr('data-video-gl');
        }

        $('#library-modal').modal('hide');

        //save the image for the selected elment
        let selectedElement = $('#selected-elment').attr('data-selected-elment');

        if(selectedElement) {
            if(nameFromMedia) {
                var filename = nameFromMedia;
            } else {
               var filename = selectedImage;
            }

            globalFont.components.forEach(function(obj) {
                if(obj.id == "elment-"+selectedElement) {
                    //update the component file
                    obj.filename = filename;
                }
            });

            var userId = $('#user-id').val();

            if(dataType == 'image' || dataType == 'image-module') {
                $('#elment-'+selectedElement).parent().find('.default-image').attr('src','/images/uploads/'+userId+'/documents/'+filename);
                $('#elment-'+selectedElement).parent().find('.default-image').css('width','100%');
            } else if(dataType == 'video') {
                $('.param-video .value-file').val(filename);
            }

           // globalFont.components.push(component);
        } else {
            //il we choose image for global font
            if(dataType == 'image') {
                var image  = {};
                image.name = nameFromMedia;
                if( globalFont.general.video ) {
                    delete globalFont.general.video;
                }
                globalFont.general.image = image;
                $('#file-fond .value-file').val(nameFromMedia);

            } else if(dataType == "video") {
                //save the video of font on the table globalFont
                var video = {};
                video.filename = nameFromMedia;
                if(globalFont.general.image ) {
                    delete globalFont.general.image
                }
                globalFont.general.video = video;
                $('#video-font .value-file').val( nameFromMedia);
            }
            if(globalFont.general.fondColor) {
                delete globalFont.general.fondColor;
            }
        }
    })

    $('.discart-change, .btn-discart').click(function() {
        $('.custom-side').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
       // $('.pcr-app').removeClass('visible');
    })
    $('#video-type').on('change',function () {
        if($('#video-type').val() == 0) {
            //Video URL
            $('.upload-resource').addClass('d-none');
            $('.custom-link').removeClass('d-none')
        } else {
            //custom resource
            $('.upload-resource').removeClass('d-none');
            $('.custom-link').addClass('d-none')
        }
    })
    $('#save-module-config').click(function() {
        //pickr3.hide();
        pickerFixed.hide();
        let selectedElement = $('#selected-elment').attr('data-selected-elment');
        if( globalFont.components) {
            globalFont.components.forEach(function(obj) {
                if(obj.id == 'elment-'+selectedElement) {
                    //update image config
                    var type = $('#'+obj.id).attr('data-type');
                    if(type == "image") {
                        let urlImage = $('#url-image').val();
                        let openLink = $("#image-open-link").val();
                        obj.linkImage = urlImage;
                        obj.openLink = openLink;
                        obj.type = 'image';
                    } else if(type == "video") {
                        //set the video attribute
                        if($('#video-type').val() == 0) {
                            obj.videoVal = $('#video-link').val();
                        } else {
                            obj.videoVal = "";
                        }
                        obj.type = 'video';
                    }
                    //set elment fond
                    let fondComponent = $('#fond .appercu-color').attr('data-color');
                    console.log(fondComponent);
                    if(fondComponent != undefined) {
                        obj.fondColor = fondComponent;
                        $('.nod-'+selectedElement).css('background-color', fondComponent);
                    }
                }
            });
        }
        let top      = $('#espacement .m-top').val();
        let buttom   = $('#espacement .m-top').val();
        let left     = $('#espacement .m-left').val();
        let right    = $('#espacement .m-right').val();
        let p_top    = $('#espacement .p-top').val();
        let p_buttom = $('#espacement .p-top').val();
        let p_left   = $('#espacement .p-left').val();
        let p_right  = $('#espacement .p-right').val();
        //border raduis value
        let borderTop          = $('.bordure .left-top').val();
        let borderLeftBottom   = $('.bordure .left-bottom').val();
        let borderRightTop     = $('.bordure .right-top').val();
        let borderRightButtom  = $('.bordure .right-buttom').val();
        let border             = $('.display-border').attr('style');

        //style of the row
        if(selectedElement.indexOf("row") == 0) {

            $('#'+selectedElement+' .col-item').css('background-color', $('#fond .appercu-color').attr('data-color'));
            //update margin  espacements of row
            $('#'+selectedElement+' .col-item').css('margin',top+'px '+right+'px '+buttom+'px '+left+'px')
            //update the padding espacement of the row
            $('#'+selectedElement+' .col-item').css('padding',p_top+'px '+p_right+'px '+p_buttom+'px '+p_left+'px');
            //update the bloc border

            $('#'+selectedElement).attr('style',border);
            //get the border raduis value
            $('#'+selectedElement).css('border-radius',borderTop+'px '+borderRightTop+'px '+borderRightButtom+'px '+borderLeftBottom+'px');

        } else {
            //update the style of the selected bloc
            if($('#fond .appercu-color').attr('data-color')){
                $('.nod-'+selectedElement).css('background-color', $('#fond .appercu-color').attr('data-color'));
            }
            console.log(top);
            console.log(p_top);
            $('.nod-'+selectedElement).css('margin',top+'px '+right+'px '+buttom+'px '+left+'px');
            $('.nod-'+selectedElement).css('padding',p_top+'px '+p_right+'px '+p_buttom+'px '+p_left+'px');
            $('.nod-'+selectedElement).attr('style',border);
            $('.nod-'+selectedElement).css('border-radius',borderTop+'px '+borderRightTop+'px '+borderRightButtom+'px '+borderLeftBottom+'px');
        }




        $('.custom-side').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
        //$('.pcr-app').removeClass('visible');
    });
    $('#discart-change').click(function() {
        $('.custom-side').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
        $('.custom-side input').val('');
        $('.custom-side select').val(0);
    })
    $('#appercu-Builder-btn').click(function() {
        console.log(globalFont);
        createContent();
    })
    $('#create-page-btn').click(function() {
        $('.default-content').removeClass('d-none');
        //$('.builder').find('#main-content').removeClass('col-12').addClass('col-10');
       // $('.builder').find('.left-build').removeClass('d-none');
    });
    $('.border1').click(function () {
        if($('.display-border').hasClass('bordered')) {
            $('.display-border').removeClass('bordered');
            $('.display-border').css('border','none');
        } else {
            $('.display-border').addClass('bordered');
            $('.display-border').css('border','1px solid');
            updateBorderColor()
        }

    })
    $('.border2').click(function () {
        if($('.display-border').hasClass('bordered')) {
            $('.display-border').removeClass('bordered');
            $('.display-border').css('border','none');
        } else {
            $('.display-border').addClass('bordered');
            $('.display-border').css('border','1px dashed');
            $('.display-border').css('border-top','1px solid');
            updateBorderColor();
        }

    })
    $('.border3').click(function () {
        if($('.display-border').hasClass('bordered')) {
            $('.display-border').removeClass('bordered');
            $('.display-border').css('border','none');
        } else {
            $('.display-border').addClass('bordered');
            $('.display-border').css('border','1px dashed');
            $('.display-border').css('border-right','1px solid');
            updateBorderColor();
        }

    })
    $('.border4').click(function () {
        if($('.display-border').hasClass('bordered')) {
            $('.display-border').removeClass('bordered');
            $('.display-border').css('border','none');
        } else {
            $('.display-border').addClass('bordered');
            $('.display-border').css('border','1px dashed');
            $('.display-border').css('border-bottom','1px solid');
            updateBorderColor();
        }

    })
    $('.border5').click(function () {
        if($('.display-border').hasClass('bordered')) {
            $('.display-border').removeClass('bordered');
            $('.display-border').css('border','none');
        } else {
            $('.display-border').addClass('bordered');
            $('.display-border').css('border','1px dashed');
            $('.display-border').css('border-left','1px solid');
            updateBorderColor();
        }
    })

    $('#save-global-config').click(function() {

        //get the data of image and video
        //param global fond image
       /* if($('.appercu-color').attr('data-color') && globalFont.general.fondColor ) {
            globalFont.general.fondColor =   $('.appercu-color').attr('data-color');
            delete globalFont.general.image;
            delete  globalFont.general.video;
        } else {
            globalFont.general.fondColor =  '';
        }*/
        if( globalFont.general.image != undefined) {
            globalFont.general.image.paralax          = $('input[name=paralax]:checked').val();
            globalFont.general.image.sizeGlobalImg    = $("#size-global-img").val();
            globalFont.general.image.positionGlobaImg = $("#position-global-img").val();
            globalFont.general.image.repetGlobalImg   = $("#repet-global-img").val();
            globalFont.general.image.fondGlobalImage  =  $("#fondu-global-image").val();
        }
        //get the video data
        if( globalFont.general.video != undefined) {
            globalFont.general.video.widthVideoFond = $('#width-video-fond').val();
            globalFont.general.video.heightVideoFond = $('#height-video-fond').val();
            globalFont.general.video.pauseRead  = $('input[name=pause-read]:checked').val();
            globalFont.general.video.pauseVue =  $('input[name=pause-vue]:checked').val();
        }
        var text = {};
        text.policeText = $('#police-text').val();
        text.fondText   = $('#fond-text').val();
        text.sizePolice = $('#sizePoliceVAl').text();
        text.colorText  = '';
        if($('.appercu-color-text').attr('data-color')) {
            text.colorText  = $('.appercu-color-text').attr('data-color');
            $('.builder').css('color', text.colorText);
        }

        globalFont.general.text = text;
        //remove the sidebar
        $('.custom-side').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
        //pickr3.hide();
        pickerFixed.hide();
       // $('.pcr-app').removeClass('visible');
        console.log(  globalFont.general);
    })
    $('.show-model').click(function () {
        $('#choose-page').modal('hide');
    })

    $('.dounload-btn').click(function() {
        $('#dounload').addClass("show");
        $('#gallerie').removeClass('show');
        $('#models').removeClass('show');
        $('#library-modal .btn-toggle .btn').removeClass('active');
        $('.dounload-btn').addClass('active');
    })
    $('.gallerie-btn').click(function() {
        $('#dounload').removeClass('show');
        $('#gallerie').addClass("show");
        $('#models').removeClass('show');
        $('#library-modal .btn-toggle .btn').removeClass('active');
        $('.gallerie-btn').addClass('active');
    });
    $('.models-btn').click(function() {
        $('#dounload').removeClass('show');
        $('#gallerie').removeClass("show");
        $('#models').addClass("show");
        $('#library-modal .btn-toggle .btn').removeClass('active');
        $('.models-btn').addClass('active');
    });

    if($(".select2").length > 0) {
        $(".select2").select2({
            minimumResultsForSearch: -1
        });
    }
    
    $('#update-text').click(function (e) {
        e.preventDefault();
        let data  = CKEDITOR.instances.editor.getData();
        let activeElemnt = $('.text-content').attr('data-active-el');
        $('#elment-'+activeElemnt).parent().find('.content').html(data);
        //update the value of the object data
        globalFont.components.forEach(function(obj) {
            if(obj.id == 'elment-'+activeElemnt) {
                obj.value = data;
            }
        });

        $('#edit-text').modal().hide();
        $('.modal-backdrop').remove();
    });
    //save the chapter content
    $('#save-chapter').click(function () {
        createContent();
        let dynamicContent = $('.bulider-content').html();
        $('#appercu-Builder .updateElment').parent().attr('id', $('#appercu-Builder .updateElment').attr('id'));
        $('#appercu-Builder').find('.updateElment').addClass('d-none');
        let chapterHtml = $('#appercu-Builder').html();
        $('.builder').find('.action-col').remove();
        $('.builder').addClass('chapter');
        //save chapter on the database
        $('.builder').html(chapterHtml);
        var dataToSend = {};
        let general = [];
        console.log(globalFont.general.fondColor);
        if(globalFont.general.fondColor) {
            let fond = {};
            fond.type = 'fondColor';
            fond.fondColor = globalFont.general.fondColor
            general.push(fond);
        }
        if(globalFont.general.text) {
            globalFont.general.text.type = "text";
            general.push(globalFont.general.text)
        }
        if(globalFont.general.image) {
            globalFont.general.image.type= "image";
            general.push(globalFont.general.image)
        }
        if(globalFont.general.video) {
            globalFont.general.video.type= "video";
            general.push(globalFont.general.video)
        }
        dataToSend.generalConfig   = general;
        dataToSend.components      = globalFont.components;
        dataToSend.htmlData        = chapterHtml;
        dataToSend.dynamicContent  = dynamicContent;
        //Global page fond
        console.log(dataToSend);
        $.ajax({
            type: "POST",
            url: '../saveChapter',
            //data: { generalConfiguration: globalFont.general, components: globalFont.components, htmlData:chapterHtml,dynamicContent: dynamicContent, chapterId: parseInt(chapterId)},
            data : { builderData : dataToSend, chapterId : parseInt(chapterId) },
            success: function(response){
                if(response == 'success') {
                   // window.location.href = "/builder/getChapter/"+parseInt(chapterId);
                }
            },
        });
    })
});

/** functions **/

function deleteCol(key) {

    if( $('.nod-'+key).children() != undefined) {
        $('.nod-'+key).children().remove();
    } else {
        $('.nod-'+key).html('');
        $('.nod-'+key).parent().parent().remove();
    }
    $('.nod-'+key).remove();
    var rows = $('.sort-row .col');
    rows.each(function(index, element) {
       if($(this).children().length == 0) {
           //$(this).remove();
          // $(this).parent().parent().parent().remove();
       }
    })
    //$('#elment-'+key).parent().html('');
    globalFont.components.forEach(function(obj, index) {
        if(obj.id == 'elment-'+key) {
            delete globalFont.components[index];
            numelment--;
        }
    });
    //console.log(globalFont.components);
}
function duplicateCol(key) {
    var toDuplicate = $('#elment-'+key).parent().html();
    $('#elment-'+key).parent().after('<div class="col-12 colned">'+toDuplicate+'</div>');
    $('.colned .updateElment').remove();

    customHtml = '<div class="updateElment" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
        '<a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a>'+
        '<a href="#" onclick="showConfigSidebar('+numelment+')""><img src="/assets/images/builder/bagette-magic.png"></a>'+
        '<a href="#" onclick="duplicateCol('+numelment+')"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a>'+
        '<a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
        '<a href="#" class="delete-row" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
    ;
    $('.colned').append(customHtml);
    $('.cloned').removeClass('cloned');
    numelment++
}

function deleteRow(index) {
    globalFont.components.forEach(function(obj, key) {
        if(obj.id != undefined) {
           if($('#row-'+index+' #'+obj.id).length >0) {
               //remove elment from array componment
               delete globalFont.components[key];
               numelment--;
           }
        }
    });
    $('#row-'+index).remove();
    var hasContent = $('.bulider-content').html();
    if($.trim($(".bulider-content").html())=='') {
        $('.default-content').removeClass('newElment');
        $('.show-grid').addClass('hidden');
        $('.first-img').removeClass('hidden');
    }
}
function duplicateRow(index) {
    var itemToDuplicate =  $('#row-'+index).html();
    $('.bulider-content #row-'+index).after('<div class ="row connectedsortable ml-1 mr-1" id="row-'+(parseInt(index)+k-1)+'">'+itemToDuplicate+'</div>');
}
function updateRow(index) {

    if( $('.type-grid').hasClass('hidden')) {
        let offsetEelment = $('.update-row-'+index).offset();
        $('#et-fb-settings-column').css('top',offsetEelment.top+20+'px');
        $('#et-fb-settings-column').css('left',offsetEelment.left+35+'px');
        $('.type-grid').removeClass('hidden');
        $('.type-grid').addClass('update-grid');
    } else {
        $('.type-grid').addClass('hidden');
        $('.type-grid').removeClass('update-grid');
    }
    $('.type-grid').attr('data-row_id',index);
}
function updateVisibility(index) {

    //if($('#magic-sidebar').hasClass('d-none')) {
    $('.custom-side').addClass('d-none');
    $('#magic-sidebar').removeClass('d-none');
    $('#main-content').removeClass('col-10');
    $('#main-content').addClass('col-8');
    //$('.pcr-app').removeClass('visible');
   // pickr3.hide()
    pickerFixed.hide();
    $('.params-btn').removeClass('active');
    $('.effect-btn').removeClass('active');
    $('.visibility-btn').addClass('active');

    $('#params').removeClass('show');
    $('#effect').removeClass('show');
    $('#visibility').addClass('show');
}

function showConfigSidebar(elementId) {

    let nodeStyle = $('#node-'+elementId).attr('style');
    if(nodeStyle != undefined) {
        $('.display-border').attr('style',nodeStyle);
    }

    $('#selected-elment, #magic-sidebar').attr('data-selected-elment',elementId);
    $('.custom-side').addClass('d-none');
    $('#magic-sidebar').removeClass('d-none');
    $('#main-content').removeClass('col-10');
    $('#main-content').addClass('col-8');
    //$('.pcr-app').removeClass('visible');
    $('.effect-btn').addClass('active');
    $('.params-btn').removeClass('active');
    $('.visibility-btn').removeClass('active');
    $('#effect').addClass('show');
    $('#visibility').removeClass('show');
    $('#params').removeClass('show');
   // pickr3.hide()
    pickerFixed.hide();

}
function showConfigSidebarRow(rowId) {
    let rowStyle = $('#row-'+rowId).attr('style');
    let colItemStyle = $('#row-'+rowId+' .col-item').attr('style');
    if(rowStyle != undefined) {
       $('.display-border').attr('style',rowStyle);
     } else {
       $('.display-border').attr('style','');
     }

    $('#selected-elment, #magic-sidebar').attr('data-selected-elment', 'row-'+rowId);
    $('.custom-side').addClass('d-none');
    $('#magic-sidebar').removeClass('d-none');
    $('#main-content').removeClass('col-10');
    $('#main-content').addClass('col-8');
    //$('.pcr-app').removeClass('visible');
    $('.effect-btn').addClass('active');
    $('.params-btn').removeClass('active');
    $('.visibility-btn').removeClass('active');
    $('#effect').addClass('show');
    $('#visibility').removeClass('show');
    $('#params').removeClass('show');
    //pickr3.hide();
    pickerFixed.hide();
}
function showConfigSidebarParams(elementId) {
    var type = $('#elment-'+elementId).attr('data-type');
    //update image information
    $('.param-module').addClass('d-none');
    if(type == 'image' || type == 'image-module') {
        $('.param-image').removeClass('d-none');
    }
    if(type == 'video') {
        $('.param-video').removeClass('d-none');
    }
    if(globalFont.components) {
        globalFont.components.forEach(function(obj) {
            if(obj.id == 'elment-'+elementId) {
                $('#url-image').val(obj.linkImage);
                $("#image-open-link").val(obj.openLink);
            } else {
                $('#url-image').val('');
                $("#image-open-link").val('');
            }
        });
    }
    $('#selected-elment, #magic-sidebar').attr('data-selected-elment',elementId);
    $('.custom-side').addClass('d-none');
    $('#magic-sidebar').removeClass('d-none');
    $('#main-content').removeClass('col-10');
    $('#main-content').addClass('col-8');
    //$('.pcr-app').removeClass('visible');

    $('.params-btn').addClass('active');
    $('.effect-btn').removeClass('active');
    $('.visibility-btn').removeClass('active');

    $('#effect').removeClass('show');
    $('#visibility').removeClass('show');
    $('#params').addClass('show');
    //pickr3.hide();
    pickerFixed.hide();
}
/** this function save document on a specific folder **/
function uploadFile(file) {
    $.ajax({
        type: "POST",
        url: '../uploadForBuilder',
        data: file,
        contentType: false,
        processData: false,
        success: function(response){

        },
    });
}
/** push the new component on the global table **/
function addcomponent (numElment, type) {
    var component = {};
    component.id   = "elment-"+numelment;
    component.type = type;
    console.log(component);
    globalFont.components.push(component);
}

function editText(numElment) {
    var typeText = $('#elment-'+numElment).parent().attr('data-text-type');
    var textContent = $('#elment-'+numElment).parent().find('.content').html();

    $('.text-content').attr('data-active-el', numElment);
    $('.text-content').attr('data-type-active-el',typeText);

    if (CKEDITOR.instances.editor) CKEDITOR.instances.editor.destroy();
    CKEDITOR.replace('editor', {
        skin: 'moono',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode:CKEDITOR.ENTER_P,
        toolbar: [{ name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
            { name: 'basicstyles', groups: [ 'basicstyles' ], items: [ 'Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor' ] },
            { name: 'scripts', items: [ 'Subscript', 'Superscript' ] },
            { name: 'justify', groups: [ 'blocks', 'align' ], items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
            { name: 'paragraph', groups: [ 'list', 'indent' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'] },
            { name: 'links', items: [ 'Link', 'Unlink' ] },
            { name: 'spell', items: [ 'jQuerySpellChecker' ] },
        ],
        toolbarLocation: 'bottom',
        uiColor: '#aa2b87',
    });
    CKEDITOR.instances.editor.setData(textContent);
    $('#edit-text').modal().show();
}
function createContent() {
    //update the style of the body
    //update the color fond of the page
    if(globalFont.general.fondColor) {
        $('#appercu-Builder .global-content').css('background-color', globalFont.general.fondColor);
        $('#appercu-Builder .global-content').css('background-image','inherit');
        //remove the video
        $('.page-font').html('');
    }
    //put a background image
    var userId = $('#user-id').val();
    if(globalFont.general.image) {
        $('#appercu-Builder .global-content').css('background-image','url(../../images/uploads/'+userId+'/documents/'+globalFont.general.image.name+')');
        $('#appercu-Builder .global-content').css('background-color','inherit');
        //remove the video
        $('.page-font').html('');
    }

    if(globalFont.components) {
        //we have module inserted
        globalFont.components.forEach(function(obj) {
            //get the type of the module
            var type = $('#'+obj.id).attr('data-type');
            if(type == "image") {
                if(obj.linkImage) {
                    //get the image and add link on this image
                    var srcImage = $('#'+obj.id).parent().find('.default-image').attr('src');
                    var newImage = $('#'+obj.id).parent().find('.default-image.new').attr('src');
                    if(newImage == undefined) {
                        $('#'+obj.id).parent().find('.default-image').remove();
                        var target = "";
                        if(obj.openLink == 1) {
                            target="_blank";
                        }
                        if(srcImage  != undefined) {
                            var newHtml =  '<a href="'+obj.linkImage+'" target="'+target+'"><img class="default-image new" src="'+srcImage+'" width="100%;"></a>';
                            $('#'+obj.id).before(newHtml);
                        }
                    }
                }
            } else if(type == "video") {
                //put a components vedio
                $('#'+obj.id).parent().find('.default-video').remove();
                var video, source;
                video = document.createElement('video');
                $(video).attr('id', 'video-fond-modul');
                //$(video).attr('muted', '');
                $(video).attr('width', '100%');
                $(video).attr('height', '100%');
                //$(video).attr('poster', 'http://video-js.zencoder.com/oceans-clip.jpg');
                $(video).attr('controls', 'true');

                source = document.createElement('source');
                //$(source).attr('type', 'video/mp4');
                if(obj.videoVal) {
                    $(source).attr('src', obj.videoVal);
                } else {
                    $(source).attr('src', '/images/uploads/' + userId + '/documents/' + obj.filename);
                }

                $('#' + obj.id).parent().find('.video-component').remove();
                $('#' + obj.id).parent().append('<div class="video-component"> </div>');
                $('#' + obj.id).parent().find('.video-component').html(video);
                $(video).append(source);
            }
        });
    }
    $('.default-content').addClass('d-none');
    var builderContent = $('.bulider-content').html();
    var myvideo = $('#appercu-Builder .video-fond').html();
    $('#appercu-Builder .global-content').html(builderContent);
    if(globalFont.general.video) {
        //put the baground video
        $('#appercu-Builder .global-content').css('background-image','inherit');
        $('#appercu-Builder .global-content').css('background-color','inherit');
        var obj, source;
        obj = document.createElement('video');
        $(obj).attr('id', 'video-fond-chap');
        $(obj).attr('muted', '');
        if(globalFont.general.video.widthVideoFond)  {
            $(obj).attr('width', globalFont.general.video.widthVideoFond+'px');
        } else {
            $(obj).attr('width', '100%');
        }
        if(globalFont.general.video.widthVideoFond)  {
            $(obj).attr('width', globalFont.general.video.heightVideoFond+'px');
        } else {
            $(obj).attr('height', '100%');
        }
        //$(obj).attr('poster', 'http://video-js.zencoder.com/oceans-clip.jpg');
        $(obj).attr('loop', 'true');
        $(obj).attr('autoplay', 'true');

        source = document.createElement('source');
        $(source).attr('type', 'video/mp4');
        $(source).attr('src', '/images/uploads/'+userId+'/documents/'+globalFont.general.video.filename);

        $('#appercu-Builder .page-font').html(obj);
        $(obj).append(source);
        //update video data
        if(globalFont.general.video.widthVideoFond)  {
            $('#video-fond-chap').attr('width', globalFont.general.video.widthVideoFond+'px');
        }
        if(globalFont.general.video.widthVideoFond)  {
            $('#video-fond-chap').attr('height', globalFont.general.video.heightVideoFond+'px');
        }
    }

    $('#appercu-Builder').find('.action-col').addClass('d-none');
    $('#appercu-Builder').find('.updateElment').addClass('d-none');
}

function updateBorderColor() {
    let color = '#ccc';
    if($('.appercu-color-border').attr('data-color')) {
        color = $('.appercu-color-border').attr('data-color');
    }
    $('.display-border').css('border-color',color);
    $('.display-border').css('border-width',$('#sizeVal').text()+'px');
}
