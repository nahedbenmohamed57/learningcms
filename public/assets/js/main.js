var k =  1;
var numelment = 1;
var globalFontstep = {};
var pathnamesutdent = window.location.pathname;
var tabModCours = [];
var tabModChap = [];
var tabModChapters = [];
var tabStatus = [];
var tabFormations = [];
var tabClasses = [];
var paramsFormations = [];

$(document).ready(function() {

    /*    $(".select2-container").click(function () {
            if (pathnamesutdent == "/profile") {
                console.log("ttt");
                $(".select2-results").addClass("noowflew");
            }
        });*/
    if(pathnamesutdent!="/student/notes"){
    }

    $(".emoji span").on("click", function(){
        $(".emoji span").removeClass("active");
        var icone= $(this).data("id-simle");
        $(this).addClass("active");
        $("#icon").val(icone);

    });
    var clicks = 0;
    $("#addrow").click(function () {
        clicks++;
        var $ct = $('#clone-box');
        $('#textBox').val(clicks);
        var i = $("#textBox").val();
        i = parseInt(i) + 1;

        $ct.append('<div class="blocinput"> <label for="indice1">Indice n° ' + i + ' :</label><input type="text" class="form-control" name="" id="demo_' + i + '" value="" /> </div> ')
    });
    var applyFiles = function() {
        if (this.files.length <= 0) {

            $('.guide').show();
        } else {
            $('.choosen').empty();
            $('.guide').hide();

            for (var i = 0; i < this.files.length; ++i) {
                $('.choosen').append($('<li>').html(this.files[i].name));
            }
        }
    }

    $('input[type="file"]').each(function() {
        applyFiles.call(this);
    }).change(function() {
        applyFiles.call(this);
    });
    listCourq = document.getElementById('sortable1');
    listCourq1 = document.getElementById('sortable2');
    if($('#sortable1').length > 0) {
        new Sortable(listCourq, {
            group: 'shared',
            animation: 150
        });
        new Sortable(listCourq1, {
            group: 'shared',
            animation: 150
        });
    }

    if($('#datetpicket').length > 0) {
        $('#datetpicket').datetimepicker({
            language: 'fr',
            pickTime: false
        });
    }
    if($('#datetpickets').length > 0) {
        $('#datetpickets').datetimepicker({
            language: 'fr',
            pickTime: false
        });
    }
    if($('#datetpicketmodule').length > 0) {
        $('#datetpicketmodule').datetimepicker({
            language: 'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    }

    if($('#dateacces').length > 0) {
        $('#dateacces').datepicker();
    }
    if($('#dataTable').length > 0) {
        $('#dataTable').DataTable({
            paging: false,
            scrollY: 500,
        });
    }

    $('.dataTables_filter label').before('<input id="search-btn" type="checkbox"><label for="search-btn"></label>');
    $('.dataTables_filter label input').attr("id","search-bar");
    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });

    $('.btn-toggle .btn').click(function() {
        $(this).parent().find('.btn').removeClass('active');
        $(this).addClass('active');
    });

    var img = $('.vich-image a').attr('href');

    if (img) {
        $('.img-profile').attr('src', img);
    }
    // Change the type of input to password or text
    $('.show-pass').click(function() {
        var temp = document.getElementById("typepass");
        showPassword(temp, '.show-pass');
    })
    //confirm password inscription
    $('#registration_password_first').click(function() {
        var temp = document.getElementById("registration_password_first");
        showPassword(temp, '.show-pass-1');
    })
    $('#registration_password_second').click(function() {
        var temp = document.getElementById("registration_password_second");
        showPassword(temp, '.show-pass-2');
    })


    //confirm password compleate inscription
    $('#form_password_first').click(function() {
        var temp = document.getElementById("form_password_first");
        showPassword(temp, '.show-pass-1');
    })
    $('#form_password_second').click(function() {
        var temp = document.getElementById("form_password_second");
        showPassword(temp, '.show-pass-2');
    })

    $('.show-1 .show-password').click(function() {
        var temp = document.getElementById("profile_password");
        showPassword(temp, '.show-1 .show-password');
    })

    $('.show-2').click(function() {
        var temp = document.getElementById("profile_newPassword_first");
        showPassword(temp, '.show-2');
    })
    $('.show-3').click(function() {
        var temp = document.getElementById("profile_newPassword_second");
        showPassword(temp, '.show-3');
    })
    $('#checkAll').change(function() {
        // this will contain a reference to the checkbox
        if (this.checked) {
            // the checkbox is now checked
            $('.check-element').prop("checked", true);
        } else {
            // the checkbox is now no longer checked
            $('.check-element').prop("checked", false);
        }
    });

    $('.sexRadio').click(function() {
        if (this.previous) {
            this.checked = false;
            $(this).removeClass('check');
        }
        $(this).addClass('check');
        this.previous = this.checked;
    });

    $('.is_checked').click(function() {
        console.log('dark');
    })
    $('.is_unchecked').click(function() {
        console.log('is_unchecked');
    })
    var activeMod = localStorage.getItem('mod');
    console.log(activeMod);
    if (activeMod == "dark") {
        $(this).addClass('checkdark')
        $('body').addClass('dark-mod')
        $('body').find(".toggle_switch .checkbox  svg").attr("class", "is_unchecked");
        $('body').find(".toggle_switch .checkbox svg:first").attr("class", "is_checked");
    } else {
        $('body').find(".toggle_switch .checkbox  svg").attr("class", "is_checked");
        $('body').find(".toggle_switch .checkbox svg:first").attr("class", "is_unchecked");
    }
    $('.switch_3').change(function() {

        if ($(this).hasClass('checkdark')) {
            $(this).removeClass('checkdark')
            $('body').removeClass('dark-mod');
            localStorage.setItem('mod', '');
            $('body').find(".toggle_switch .checkbox  svg").attr("class", "is_checked");
            $('body').find(".toggle_switch .checkbox svg:first").attr("class", "is_unchecked");
        } else {
            $(this).addClass('checkdark')
            $('body').addClass('dark-mod')
            localStorage.setItem('mod', 'dark');
            $('body').find(".toggle_switch .checkbox  svg").attr("class", "is_unchecked");
            $('body').find(".toggle_switch .checkbox svg:first").attr("class", "is_checked");
        }
    })
    //close sidebar
    $("#sidebarToggle, #sidebarToggleTop").on("click", function(o) {
        $("body").toggleClass("sidebar-toggled"), $(".sidebar").toggleClass("toggled"), $(".sidebar").hasClass("toggled") && $(".sidebar .collapse").collapse("hide")
    }), $(window).resize(function() {
        $(window).width() < 768 && $(".sidebar .collapse").collapse("hide")
    }), $("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function(o) {
        if (768 < $(window).width()) {
            var e = o.originalEvent,
                l = e.wheelDelta || -e.detail;
            this.scrollTop += 30 * (l < 0 ? 1 : -1), o.preventDefault()
        }
    }), $(document).on("scroll", function() {
        100 < $(this).scrollTop() ? $(".scroll-to-top").fadeIn() : $(".scroll-to-top").fadeOut()
    }), $(document).on("click", "a.scroll-to-top", function(o) {
        var e = $(this);
        $("html, body").stop().animate({
            scrollTop: $(e.attr("href")).offset().top
        }, 1e3, "easeInOutExpo"), o.preventDefault()
    })
    //disable inscription whenuser dont accepte conditions
    $('#accept-terms').change(function() {
        if (this.checked) {
            $('.btn-user').removeAttr('disabled');
        } else {
            $('.btn-user').attr('disabled', 'disabled');
        }
    });
    $('.user-profile .dropdown-menu .custom-radio, .block-content select').click(function(e) {
        //e.stopPropagation();
    });
    $('.monthly-btn').click(function() {
        console.log('click')
        $('#annual').removeClass('show');
        $('#monthly').addClass("show");
    })
    $('.annual-btn').click(function() {
        $('#monthly').removeClass('show');
        $('#annual').addClass("show");
    });

    $('.first-btn').click(function() {
        console.log('click')
        $('#second').removeClass('show');
        $('#first').addClass("show");
    })
    $('.second-btn').click(function() {
        $('#first').removeClass('show');
        $('#second').addClass("show");
    });

    /** delete user confirmation modal **/
    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
    $('#save-folder').click(function() {
        var folder_name = $('#add-folder #folder_name').val();
        if (folder_name) {
            var path = $(this).attr("data-path");
            $.ajax({
                type: "POST",
                url: path,
                data: { folder_name: folder_name },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page and dismiss modal
                        $('#add-folder').modal('hide');
                        location.reload();
                    }
                }
            });
        }
    })
    $('#update-name img').click(function() {
        var id = $(this).parent().attr('data-id');
        var name = $(this).parent().attr('data-name');
        $('#update-folder #folder_name').val(name);
        $('#idfolder').val(id);

    })
    $('#update-folderName').click(function() {
        //get the new name and the folder id
        var folder_name = $('#update-folder #folder_name').val();
        var id = $('#update-folder #idfolder').val();
        if (folder_name) {
            var path = $(this).attr("data-path");
            $.ajax({
                type: "POST",
                url: path,
                data: { folder_name: folder_name, id: id },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page and dismiss modal
                        $('#update-folder').modal('hide');
                        location.reload();
                    }
                }
            });
        }
    })
    $('#delete-folder img').click(function(e) {
        var id = $(this).parent().attr('data-id');
        var path = $(this).parent().attr("data-path");
        console.log(id);
        e.preventDefault();
        if (id) {
            $.ajax({
                type: "POST",
                url: path,
                data: { id: id },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page
                        location.reload();
                    }
                }
            });
        }
    })
    //duplicate the selectedd folder
    $('#duplicate-folder img').click(function() {
        var folder_name = $(this).parent().attr('data-name')

        if (folder_name) {
            var path = $(this).parent().attr("data-path");
            $.ajax({
                type: "POST",
                url: path,
                data: { folder_name: folder_name },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page
                        location.reload();
                    }
                }
            });
        }
    })
    $('.heart').on('click', function(e) {
        if ($(this).hasClass('far fa-heart')){
            $(this).removeClass('far');
            $(this).addClass('fas fa-heart');
        }

        if($(this).hasClass('fas fa-heart')){
            $(this).removeClass('fas fa-heart');
            $(this).addClass('far fa-heart');
        }
    })

    $('#delete-all-note').click(function() {
        //get all selected notes
        var notes = [];
        $.each($("input[name='note-check']:checked"), function() {
            notes.push($(this).val());
        });
        var path = $(this).attr("data-path");
        $.ajax({
            type: "POST",
            url: path,
            data: { notes: notes },
            success: function(data) {
                if (data == "success") {
                    location.reload();
                }
            }
        });
    })
    $('#delete-badges').click(function () {
        //get all selected badges
        var badges = [];
        $.each($("input[name='badge-check']:checked"), function () {
            badges.push($(this).attr('data-id'));
        });
        console.log(badges);
        var path = $(this).attr("data-path");
        console.log(path);
        $.ajax({
            type: "POST",
            url: window.location.origin+'/deleteBadges',
            data: {badges: badges},
            success: function (data) {
                if (data == "success") {
                    location.reload();
                }
            }
        });
    });
    $('#tab-formations').click(function() {
        $('.table-forma').removeClass('hidden');
        $('.list-forma').addClass('hidden');
        var activeImage = $(this).find('img').attr('data-activeimage');
        var nonActive = $('#list-formations img').attr('data-nonactiveimage');
        $(this).find('img').attr('src', activeImage);
        $('#list-formations img').attr('src', nonActive);

    });

    $('#list-formations').click(function() {
        $('.table-forma').addClass('hidden');
        $('.list-forma').removeClass('hidden');
        var activeImage = $(this).find('img').attr('data-activeimage');
        var nonActive = $('#tab-formations img').attr('data-nonactiveimage');
        $(this).find('img').attr('src', activeImage);
        $('#tab-formations img').attr('src', nonActive);
    });
    $('#table-formations').click(function() {
        $('.tab-formations').removeClass('hidden');
        $('.list-formations').addClass('hidden');
        var activeImage = $(this).find('img').attr('data-activeimage');
        var nonActive = $('#liste img').attr('data-nonactiveimage');
        $(this).find('img').attr('src', activeImage);
        $('#liste img').attr('src', nonActive);

    });

    $('#liste-formations').click(function() {
        $('.tab-formations').addClass('hidden');
        $('.list-formations').removeClass('hidden');
        var activeImage = $(this).find('img').attr('data-activeimage');
        var nonActive = $('#tabFor img').attr('data-nonactiveimage');
        $(this).find('img').attr('src', activeImage);
        $('#tabFor img').attr('src', nonActive);
    });

    //remove folder for others user for the current user on the page note we can find a solution by dev but this is a similar solution
    $('#form_studentFolder > option').each(function() {
        if (!$(this).text()) {
            $(this).remove();
        }
    });

    var showPassword = function($temp, $class) {
        if ($temp.type === "password") {
            $temp.type = "text";
            $($class).find('.fa-eye').addClass('fa-eye-slash').removeClass('fa-eye');
        } else {
            $temp.type = "password";
            $($class).find('.fa-eye-slash').addClass('fa-eye').removeClass('fa-eye-slash');
        }
    }

    $('#click-lamp').click(function() {
        $('#lamp-modal .default-content, #lamp-modal .default-content, #lamp-modal .default-title').addClass('hidden');
        $('#lamp-modal .success-title, #lamp-modal .new-content').removeClass('hidden');
    })

    //step 3 of create formation
    $('#add-config').click(function(){

        var nbModule  = $('.nb-module').val();
        //$('#list-module').html("");
        let lastModule = $('#lastmoduleId').val();
        for(var i = 1; i<= nbModule; i++) {
            let counterId = i;
            if(lastModule) {
                let mytab = lastModule.split('-');
                counterId = parseInt(mytab[mytab.length - 1])+i;
            }

            // var module_popup = $("#moduleconfig").clone();

            // $("#moduleconfig").after($(module_popup).attr('id', "moduleconfigitemdat-"+i));
            $('#list-module').append('<div class="blocgloab"><div class="blocimage">' +
                '<button class="nobutton cloner" ><img src="/assets/images/icons/formation/15.png"/></button>'+
                '<button class="nobutton loop"><img src="/assets/images/icons/formation/16.png"/></button>' +
                '<button class="nobutton delete" ><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#moduleconfigitemdat" class="nobutton"><img src="/assets/images/icons/formation/19.png"/></button><button class="nobutton edit"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hovermodule'+counterId+'" data-name="module '+i+'" data-type="module_'+i+'" data-id="itemdat-'+counterId+'" class="item "><h6>Module '+i+'</h6> <input type="number" value="0"/> </a>      </div></div>')
        }
        //find checked old module
        $.each($("#module-div input[name='get-modul']:checked"), function() {
            if($(this).attr('id')) {
                let id = $(this).attr('data-id');
                let nameModule = $(this).attr('data-name')
                $('#list-module').append('<div class="blocgloab"><div class="blocimage">' +
                    '<button class="nobutton cloner" ><img src="/assets/images/icons/formation/15.png"/></button>'+
                    '<button class="nobutton loop"><img src="/assets/images/icons/formation/16.png"/></button>' +
                    '<button class="nobutton delete" ><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#moduleconfigitemdat" class="nobutton"><img src="/assets/images/icons/formation/19.png"/></button><button class="nobutton edit"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hovermodule-new-'+id+'" data-name="'+nameModule+'" data-type="module_'+lastModule+id+'" data-db-id="'+id+'" data-id="itemdat-'+lastModule+id+'" class="item old-module"><h6>'+nameModule+'</h6> <input type="number" value="0"/> </a>      </div></div>')
            }
        });


        var nbCour    = $('.nb-cour').val();
        //$('#list-cour').html("");
        let lastCour = $('#lastcourId').val();
        for(var i = 1; i<= nbCour; i++) {
            let counterId = i;
            if(lastCour) {
                let mytab = lastCour.split('-');
                counterId = parseInt(mytab[mytab.length - 1])+i;
            }
            $('#list-cour').append('<div class="blocgloab"><div class="blocimage">' +
                '<button class="nobutton cloner1" ><img src="/assets/images/icons/formation/15.png"/></button>' +
                '<button class="nobutton loop2"><img src="/assets/images/icons/formation/16.png"/></button>' +
                '<button class="nobutton delete1"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#courspopup"class="nobutton"> <img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit1"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hovercour'+counterId+'" data-type="cours_'+i+'" data-id="itemcour-'+counterId+'" class=" item"><h6>Cours '+i+'</h6> <input type="number" value="0"/> </a>      </div></div>')
        }

        $.each($("#cour-div input[name='get-cour']:checked"), function() {
            if($(this).attr('id')) {
                let id = $(this).attr('data-id');
                let nameCour = $(this).attr('data-name')
                $('#list-cour').append('<div class="blocgloab"><div class="blocimage">' +
                    '<button class="nobutton cloner1" ><img src="/assets/images/icons/formation/15.png"/></button>' +
                    '<button class="nobutton loop2"><img src="/assets/images/icons/formation/16.png"/></button>' +
                    '<button class="nobutton delete1"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#courspopup"class="nobutton"> <img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit1"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hovercour'+i+'" data-type="cours_'+i+'" data-db-id="'+id+'" data-id="itemcour-old'+id+'" class=" item"><h6>'+nameCour+'</h6> <input type="number" value="0"/> </a>      </div></div>')
            }
        });
        var nbChapter = $('.nb-chapter').val();
        //$('#list-chap').html("")
        let lastChapter = $('#lastchapterId').val();
        for(var i = 1; i<= nbChapter; i++) {
            let counterId = i;
            if(lastChapter) {
                let mytab = lastChapter.split('-');
                counterId = parseInt(mytab[mytab.length - 1])+i;
            }
            console.log(counterId)
            $('#list-chap').append('<div class="blocgloab"><div class="blocimage">' +
                '<button class="nobutton cloner2" ><img src="/assets/images/icons/formation/15.png"/></button>' +
                '<button class="nobutton loop2"><img src="/assets/images/icons/formation/16.png"/></button>' +
                '<button class="nobutton delete2"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#chaiptreconfig" class="nobutton popupchapt"><img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit2"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hoverchapter'+counterId+'" data-type="chaiptre_'+i+'" data-id="itemshap-'+counterId+'" class=" item" data-name="Chap '+i+'"><h6>Chap '+i+' </h6><input type="number" value="0"/> </a>      </div></div>')
        }

        $.each($("#chapter-div input[name='get-chapters']:checked"), function() {
            if($(this).attr('id')) {
                let i = 0;
                let id = $(this).attr('data-id');
                let nameChapter = $(this).attr('data-name')
                $('#list-chap').append('<div class="blocgloab"><div class="blocimage">' +
                    '<button class="nobutton cloner2" ><img src="/assets/images/icons/formation/15.png"/></button>' +
                    '<button class="nobutton loop2"><img src="/assets/images/icons/formation/16.png"/></button>' +
                    '<button class="nobutton delete2"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#chaiptreconfig" class="nobutton popupchapt"><img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit2"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hoverchapter'+i+'" data-db-id="'+id+'" data-type="chaiptre_'+i+'" data-id="itemshap-old'+id+'" class=" item" data-name="'+nameChapter+'"><h6>'+nameChapter+' </h6><input type="number" value="0"/> </a>      </div></div>')
                i++;
            }
        });
        var nbQuiz    = $('.nb-quiz').val();
        //$('#list-quiz').html("");
        let lastQizId = $('#lastQizId').val();
        for(var i = 1; i<= nbQuiz; i++) {
            let counterId = i;
            if(lastQizId) {
                let mytab = lastQizId.split('-');
                counterId = parseInt(mytab[mytab.length - 1])+i;
            }
            $('#list-quiz').append('<div class="blocgloab"><div class="blocimage">' +
                '<button class="nobutton cloner3" ><img src="/assets/images/icons/formation/15.png"/></button>' +
                '<button class="nobutton loop2"><img src="/assets/images/icons/formation/16.png"/></button>' +
                '<button class="nobutton delete3"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#quizs" class="nobutton"><img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit3"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hoverquiz'+counterId+'" data-id="itemsqiz-'+counterId+'" data-type="itemsqiz-'+counterId+'" class=" item"><h6>Quiz '+counterId+' </h6><input type="number" value="0"/> </a>      </div></div>')

        }

        $.each($("#quiz-div input[name='get-quiz']:checked"), function() {
            if($(this).attr('id')) {
                let i =1;
                let id = $(this).attr('data-id');
                let nameQuiz = $(this).attr('data-name');

                $('#list-quiz').append('<div class="blocgloab"><div class="blocimage">' +
                    '<button class="nobutton cloner3" ><img src="/assets/images/icons/formation/15.png"/></button>' +
                    '<img src="/assets/images/icons/formation/16.png"/></button>' +
                    '<button class="nobutton delete3"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#quizs" class="nobutton"><img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit3"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hoverquiz'+i+'" data-db-id="'+id+'" data-id="itemsqiz-'+id+'" data-type="itemsqiz-'+i+'" class=" item"><h6>'+nameQuiz+' </h6><input type="number" value="0"/> </a>      </div></div>')
                i++;
            }
        });
        //reset numbers
        $('.number-input input').val('0');
    })


//Update user status

    $('.activate-user').change(function() {
        var id     = $(this).attr('data-id');
        if (this.checked) {
            // activate user
            var active = 1;
        } else {
            // desactivate user
            var active = 0;
        }

        $.ajax({
            type: "POST",
            data: { id: id, active: active },
            url: window.location.origin+'/changeStatus',
            success: function(data) {
            }
        });
    });
    $('.modal-delete-user').click(function(e) {
        e.preventDefault();
        $('.td-actions .modal-delete-user').removeClass('active-user');
        $(this).addClass('active-user');
    })
    $('.delete-user').click(function(e) {
        e.preventDefault();
        var id = $('.td-actions .active-user').attr('data-id');
        $.ajax({
            type: "POST",
            data: { id: id},
            url: window.location.origin+'/deleteUser',
            success: function(data) {
                location.reload();
            }
        });
    })

    $('.activate_all_user').change(function() {
        var users = [];
        $.each($("input[name='user_check']:checked"), function() {
            users.push($(this).attr('data-id'));
        });
        if (this.checked) {
            // activate users
            $.ajax({
                type: "POST",
                url: window.location.origin+"/activateAlluser",
                data: { users: users },
                success: function(data) {
                    if (data == "success") {
                        //location.reload();
                    }
                }
            });
            $.each($("input[name='user_check']:checked"), function() {
                var id = $(this).attr('data-id');
                $( ".activate-user" ).each(function( index) {
                    if($(this).data('id') == id){
                        $(this).attr('checked','checked');
                    }
                });
            });
        } else {
            // desactivate users
            $.ajax({
                type: "POST",
                url: window.location.origin+"/desactivateAlluser",
                data: { users: users },
                success: function(data) {
                    if (data == "success") {
                        //location.reload();
                    }
                }
            });
            $.each($("input[name='user_check']:checked"), function() {
                var id = $(this).attr('data-id');
                $( ".activate-user " ).each(function( index ) {
                    if($(this).data('id') == id){
                        $(this).removeAttr('checked');
                    }
                });
            });
        }
    });

    $('#delete-selected-user').click(function() {
        //get all selected notes
        var users = [];
        $.each($("input[name='user_check']:checked"), function() {
            users.push($(this).attr('data-id'));
        });
        var path = $(this).attr("data-path");
        $.ajax({
            type: "POST",
            url: window.location.origin+"/deleteAllUser",
            data: { users: users },
            success: function(data) {
                if (data == "success") {
                    location.reload();
                }
            }
        });
    });
    if($('.carousel').length > 0) {
        $('.carousel').carousel({
            wrap: false
        })
    }

    $('.delete-image').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var path = $(this).attr("data-path");
        console.log( id);
        if (id) {
            $.ajax({
                type: "POST",
                url: path,
                data: { id: id },
                success: function(data) {
                    if (data == "success") {
                        $.ajax({
                            type: "POST",
                            url: window.location.origin+'/getMedia',
                            success: function(data) {
                                $('#file-view').html(data);
                            }
                        });
                    }
                }
            });
        }
    })


    $('.copie-link').click(function(e) {
        e.preventDefault();
    });

    $('.update-fileName').click(function(e) {
        e.preventDefault();
        var mediaId = $(this).attr('data-id');
        $("#wrapper").removeClass("toggled");

        if ($('#file-view #media-' + mediaId).hasClass('active')) {
            $('#file-view #media-' + mediaId).removeClass('active');
            $("#wrapper").addClass("toggled");
        } else {
            $('#file-view .active').removeClass('active');
            $('#file-view #media-' + mediaId).addClass('active');
        }

        if($("#wrapper").hasClass('toggled')) {
            $.ajax({
                type: "POST",
                url: window.location.origin+'/getFolder/8',
                success: function(data) {
                    $('#carousel-folder').html(data);
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: window.location.origin+'/getMedia/'+mediaId,
                data: { folderId: '', },
                success: function(data) {
                    $('#sidebar-wrapper').html(data);
                }
            });
            //update the folders displayed data
            $.ajax({
                type: "POST",
                url: window.location.origin+'/getFolder/6',
                success: function(data) {
                    $('#carousel-folder').html(data);
                }
            });
        }
    });


    $(".menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


    $('#add-folder-doc').click(function() {
        var folder_name = $('#add-folder #folder_name').val();
        if (folder_name) {
            var path = $(this).attr("data-path");
            $.ajax({
                type: "POST",
                url: path,
                data: { folder_name: folder_name },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page and dismiss modal
                        $('#add-folder').modal('hide');
                        location.reload();
                    }
                }
            });
        }
    })

    $('#update-folderDoc').click(function() {
        //get the new name and the folder id
        var folder_name = $('#update-folder #folder_name').val();
        var id          = $('#update-folder #idfolder').val();
        if (folder_name) {
            var path = $(this).attr("data-path");
            $.ajax({
                type: "POST",
                url: path,
                data: { folder_name: folder_name, id: id },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page and dismiss modal
                        $('#update-folder').modal('hide');
                        location.reload();
                    }
                }
            });
        }
    });
    $('#delete-folder-doc img').click(function() {
        var id = $(this).parent().attr('data-id');
        var path = $(this).parent().attr("data-path");
        if (id) {
            $.ajax({
                type: "POST",
                url: path,
                data: { id: id },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page
                        location.reload();
                    }
                }
            });
        }
    });
    $('#carousel-folder .full-card').hover(
        function(){ $(this).addClass('hover') },
        function(){ $(this).removeClass('hover') }
    );
    $('#carousel-folder .full-card').click(function(e) {
        e.preventDefault();
        var folderId = $(this).attr('data-id');
        //add class active to the current folder
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            folderId = '';
        } else {
            $('#carousel-folder .full-card').removeClass('active');
            $(this).addClass('active');
        }
        $.ajax({
            type: "POST",
            url: window.location.origin+'/getMedia',
            data: { folderId: folderId },
            success: function(data) {
                $('#file-view').html(data);
            }
        });
    })

    $('#file-view .main').hover(
        function(){ $(this).addClass('hover') },
        function(){ $(this).removeClass('hover') }
    );

    $('#delete-all-file').click(function() {
        //get all selected notes
        var files = [];
        $.each($("input[name='file-check']:checked"), function() {
            files.push($(this).val());
        });
        var path = $(this).attr("data-path");
        $.ajax({
            type: "POST",
            url: path,
            data: { files: files },
            success: function(data) {
                if (data == "success") {
                    location.reload();
                }
            }
        });
    })

    $('.a-declarative').hover(
        function(){ $(this).addClass('hover') },
        function(){ $(this).removeClass('hover') }
    );


    if($(".select2").length > 0) {
        $(".select2").select2({
            minimumResultsForSearch: -1
        });
    }


    if($('select.form-select').length > 0) {
        $('select.form-select').select2({
            minimumResultsForSearch: -1
        });
    }
    if($(".select-color").length > 0) {
        $(".select-color").select2({
            minimumResultsForSearch: -1
        });
    }

    if($('.select_2').length > 0) {
        $("#profile_birthday_day, #profile_birthday_month, #profile_birthday_year").select2({
            minimumResultsForSearch: -1
        })
    }
    $('#formationconfig_enable_hours').change(function() {
        // this will contain a reference to the checkbox
        if (this.checked) {
            // the checkbox is now checked
            $('#sub-delay').removeClass('hidden');
        } else {
            // the checkbox is now no longer checked
            $('#sub-delay').addClass('hidden')
        }
    });

    $('#edit-title span').click(function() {
        $('#modal-title').prop("disabled", false);
        $('#modal-title').removeClass('disabled')
    })
    $('#edit-text span').click(function() {
        $('#modal-text').prop("disabled", false);
        $('#modal-text').removeClass('disabled')
    })
    $('#edit-btn span').click(function() {
        $('#modal-btn').prop("disabled", false);
        $('#modal-btn').removeClass('disabled')
    })

    /*$('.logout').click(function (e) {
        var result = $('#timer').text();
        e.preventDefault();
        var counter = result;
        console.log(counter);
        $.ajax({
            type: "POST",
            url: "/admin/updateCounter",
            data: {counter: counter, expired: false},
            success: function (data) {
                //location.href = 'http://learningcms.test:8000/login';
                console.log(data);
            }
        });
    });*/

    /** module user **/
    $('#filter-user').change(function() {
        // this will contain a reference to the checkbox
        if (this.checked) {
            // the checkbox is now checked
        } else {
            // the checkbox is now no longer checked
        }
    });

    //StartTimers();
    document.onmousemove = function(e) {
        ResetTimers();
    }

    $('.show-grid').click(function() {
        $('.type-grid').removeClass('update-grid');
        if($('.type-grid').hasClass('hidden')) {
            $('.type-grid').removeClass('hidden');
        } else {
            $('.type-grid').addClass('hidden');
        }
    })

    $('.type-grid .et-fb-settings-options-wrap ul li').click(function() {
        var selectedRow = $('.type-grid').attr('data-row_id');

        $('.show-grid.hidden').removeClass('hidden');
        $('.first-img').addClass('hidden');

        $('.default-content').addClass('newElment');
        var cols  = $(this).attr('data-layout');

        var list  = cols.split(',');
        var customHtml ='<div class ="row connectedsortable ml-1 mr-1" id="row-'+k+'"><div class="action-col"><a href="#"><img src="/assets/images/builder/zoom.png"></a>'+
            '<a href="#" onclick="showConfigSidebar('+k+')" ><img src="/assets/images/builder/edit-col.png"></a><a href="#" onclick="duplicateRow('+k+')"><img src="/assets/images/builder/duplicate.png"></a>'+
            '<a href="#" onclick="updateRow('+k+')"><img src="/assets/images/builder/update-row.png"></a><a href="#"  onclick="updateVisibility('+k+')" ><img src="/assets/images/builder/detail.png"></a><a href="#"><img src="/assets/images/builder/download-sm.png"></a>'+
            '<a href="#" class="delete-row" onclick="deleteRow('+k+')"><img src="/assets/images/builder/trash-sm.png"></a></div>';

        var elemntWidh = 0;
        list.forEach(function(element) {
            var widthCol = element.split('_');
            var first = widthCol[0];
            var second = widthCol[1];
            elemntWidh = 100 /second * first;
            // construre the html elment
            customHtml += '<div class="list-group-item col-item m-0" style="width: '+elemntWidh+'%"></div>';
        });
        customHtml +='</div>';

        if(selectedRow) {
            var listCols = $('#row-'+selectedRow +' .col-item');
            var sumWidh  = 0;
            for (var i = 0; i < listCols.length; i++) {
                listCols[i].setAttribute('style','width:'+elemntWidh+'%');
                sumWidh+=elemntWidh;
                delete list[ i ];
            }
            if(list.length > 0 && listCols.length < list.length) {

                list.forEach(function(element) {
                    var widthCol = element.split('_');
                    var first = widthCol[0];
                    var second = widthCol[1];
                    elemntWidh = 100 /second * first;
                    // construre the html elment
                    var newCol = '<div class="list-group-item col-item m-0" style="width: '+elemntWidh+'%"></div>';
                    $('#row-'+selectedRow).append(newCol);
                });
                $('.type-grid').attr('data-row_id','');
            }

        } else {
            $('.bulider-content').append(customHtml);
        }

        $('.type-grid').addClass('hidden');
        var tds = $('.bulider-content .col-item');
        tds.each(function(index, element) {
            new Sortable(element, {
                group: {
                    name: 'shared',

                },
                animation: 150,
            });
        });
        k++;
    });
    var uploadImage = document.getElementById('upload-image');
    if($('#upload-image').length >0) {
        new Sortable(uploadImage, {
            group: {
                name: 'shared',
                pull: 'clone'
            },
            animation: 150,
            onEnd: function (subevt) {
                $('.custom-row').removeClass('new-row');
                customHtml = '<div class="updateElment" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                    '<a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a>'+
                    '<a href="#" onclick="showConfigSidebar('+numelment+')""><img src="/assets/images/builder/bagette-magic.png"></a>'+
                    '<a href="#" onclick="duplicateCol('+numelment+')"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a>'+
                    '<a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                    '<a href="#" class="delete-row" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
                ;
                $('.bulider-content').find('.d-none').removeClass('d-none').addClass('new-row');
                $('.bulider-content .custom-file-upload').remove();
                $('.new-row .col-12').append(customHtml);
                numelment++;
            }
        });
    }
    var getComposition = document.getElementById('get-composition');
    if($('#get-composition').length >0) {
        new Sortable(getComposition, {
            group: {
                name: 'shared',
            },
            animation: 150
        });
    }
    var listComposition = document.getElementById('list-composition');
    if($('#list-composition').length > 0) {
        var j = 1;
        new Sortable(listComposition, {
            group: {
                name: 'shared',
                pull: 'clone'
            },
            animation: 150,
            onStart: function( event, ui ) {
                console.log('start');
                //$(ui.item).append("<div>abc<br><br>abc</div>");
            },
            onEnd: function (subevt) {
                $('.custom-row').removeClass('new-row');
                $('.bulider-content').find('.d-none').removeClass('d-none').addClass('new-row');;
                $('.bulider-content .comp>img').remove();
                $.each($(".new-row .col"), function(index, element) {
                    customHtml = '<div class="updateElment" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                        '<a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a><a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/bagette-magic.png"></a>'+
                        '<a href="#"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a><a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                        '<a href="#" id="col-'+index+'" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
                    ;$(this).append(customHtml);
                    numelment++;
                });
                j++;
            }
        });
    }

    $('.no-collapsable').on('click', function (e) {
        e.stopPropagation();
    });

    $('#list-media').click(function(){
        $('.all-modules').addClass('hidden');
        $('.medias, .get-media').removeClass('hidden');

    });
    $('#choose-image').click(function() {
        $('.medias').addClass('hidden');
        $('#upload-image, .get-img').removeClass('hidden');
        $('#composotion').collapse('show');
    });
    $('.get-bloc').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        $('.get-media,.get-img').addClass('hidden');
        //$('.get-img').remove();

        $('.all-modules').removeClass('hidden');
        $('.medias, #upload-image').addClass('hidden');

        $('#composotion').collapse('hide');
    });
    $('.get-media').click(function(e){
        e.stopPropagation();
        e.preventDefault();

        $('.all-modules,#upload-image').addClass('hidden');
        $('.medias').removeClass('hidden');
        $('.get-img').addClass('hidden');
        $('#composotion').collapse('hide');
    });
    $('#edit-font').click(function() {
        if($('#config-sidebar').hasClass('d-none')) {
            $('.custom-side').addClass('d-none');
            $('#config-sidebar').removeClass('d-none');
            $('#main-content').removeClass('col-10');
            $('#main-content').addClass('col-8');
            $('.pcr-app').addClass('visible');
            const pickr3 = new Pickr({
                el: '#color-picker-3',
                useAsButton: true,
                default: "303030",
                showAlways: true,
                components: {
                    preview: true,
                    opacity: true,
                    hue: true,

                    interaction: {
                        input: true,
                    }
                },

                onChange(hsva, instance) {
                    $('.appercu-color').css('background-color', hsva.toRGBA().toString());
                }
            });
            $('.pcr-picker').css('top', '63.753px');
        } else {

            $('#config-sidebar').addClass('d-none');
            $('#main-content').removeClass('col-8');
            $('#main-content').addClass('col-10');
            $('.pcr-app').removeClass('visible');
        }
    })


    $('.list-sttaic-color span').click(function() {
        var color = $(this).attr('data-color');
        $('.appercu-color').css('background-color', color);
        $('.appercu-color').attr('data-color',color);
        //get the seleced row
        /*var idElment = $('#magic-sidebar #selected-elment').attr('data-selected-elment');
        $('#row-'+idElment+' .col-item').css('background-color', color);*/
    })

    /** end document ready **/
});
function deleteCol(key) {
    $('#elment-'+key).parent().html('');
}
function duplicateCol(key) {
    var toDuplicate = $('#elment-'+key).parent().html();
    $('#elment-'+key).parent().after('<div class="col-12 colned">'+toDuplicate+'</div>');
    $('.colned .updateElment').remove();

    customHtml = '<div class="updateElment" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
        '<a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a>'+
        '<a href="#" onclick="showConfigSidebar('+numelment+')""><img src="/assets/images/builder/bagette-magic.png"></a>'+
        '<a href="#" onclick="duplicateCol('+numelment+')"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a>'+
        '<a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
        '<a href="#" class="delete-row" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
    ;
    $('.colned').append(customHtml);
    $('.cloned').removeClass('cloned');
    numelment++
}
window.onbeforeunload = function(event)
{
    //save counter on local storage
    //localStorage.setItem('counter', userTime);
    //var userTimer = localStorage.getItem('counter');

};

/** set varaible to use to update user counter **/
//this varaible it can be updatted by admin
var timoutNow = 30000; // Timeout in 15 mins.
var timeoutTimer;

var timer;

var sec  = 0;
var min  = 0;
var hour = 0;
var userTime = 0;
timer = setInterval(timeHandler, 1000);

/** functions**/

function timeHandler() {
    sec++;
    if (sec == 60) {
        sec = 0;
        min++;
    }

    if (min == 60) {
        min = 0;
        hour++;
    }

    displayTime();
}

function displayTime() {

    let time = document.getElementById('timer');
    let currentMin = min;
    let currentHour = hour;

    if (sec < 10) sec = `0${sec}`;
    if (min < 10) currentMin = `0${min}`;
    if (hour < 10) currentHour = `0${hour}`;

    //time.innerHTML = `${currentHour}:${currentMin}:${sec}`
}

// Start timers.
/*function StartTimers() {
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
}*/
// Reset timers.
function ResetTimers() {
    clearTimeout(timeoutTimer);
    // StartTimers();
}
// save the timer on local storage to use it on all the framework

// Save time counter.
/*function IdleTimeout() {
    var result = $('#timer').text();
    var counter = result;
    $.ajax({
        type: "POST",
        url: "/admin/updateCounter",
        data: { counter: counter },
        success: function(data) {
            console.log(data);
        }
    });
}*/
function deleteRow(index) {
    console.log(index);
    $('#row-'+index).remove();
    var hasContent = $('.bulider-content').html();

    if($.trim($(".bulider-content").html())=='') {
        $('.default-content').removeClass('newElment');
        $('.show-grid').addClass('hidden');
        $('.first-img').removeClass('hidden');
    }
}
function duplicateRow(index) {
    var itemToDuplicate =  $('#row-'+index).html();
    $('.bulider-content #row-'+index).after('<div class ="row connectedsortable ml-1 mr-1" id="row-'+(parseInt(index)+k-1)+'">'+itemToDuplicate+'</div>');
}
function updateRow(index) {
    if( $('.type-grid').hasClass('hidden')) {
        $('.type-grid').removeClass('hidden');
        $('.type-grid').addClass('update-grid');
    } else {
        $('.type-grid').addClass('hidden');
        $('.type-grid').removeClass('update-grid');
    }
    $('.type-grid').attr('data-row_id',index);
}
function updateVisibility(index) {

    //if($('#magic-sidebar').hasClass('d-none')) {
    $('.custom-side').addClass('d-none');
    $('#magic-sidebar').removeClass('d-none');
    $('#main-content').removeClass('col-10');
    $('#main-content').addClass('col-8');
    $('.pcr-app').removeClass('visible');
    $('.effect-btn').addClass('active');
    $('.visibility-btn').removeClass('active');
    $('#effect').addClass('show');
    $('#visibility').removeClass('show');

    $('.effect-btn').removeClass('active');
    $('.visibility-btn').addClass('active');
    $('#effect').removeClass('show');
    $('#visibility').addClass('show');
    /*} else {
        $('.custom-side').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
    }*/

}

function showConfigSidebar(rowId) {
    console.log(rowId);
    $('#selected-elment').attr('data-selected-elment',rowId);

    //if($('#magic-sidebar').hasClass('d-none')) {
    $('.custom-side').addClass('d-none');
    $('#magic-sidebar').removeClass('d-none');
    $('#main-content').removeClass('col-10');
    $('#main-content').addClass('col-8');
    $('.pcr-app').removeClass('visible');
    $('.effect-btn').addClass('active');
    $('.visibility-btn').removeClass('active');
    $('#effect').addClass('show');
    $('#visibility').removeClass('show');

    /*} else {
        $('.custom-side').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
    }*/

}
/** this function allow user to create a multipel checkbox listing **/
function checkboxDropdown(el='', text="Ajouter au(x) dossier(s)") {
    var $el = $(el)

    function updateStatus(label, result, folders) {
        if(!result.length) {
            //label.html(text);
        }
    };

    $el.each(function(i, element) {
        var $list = $(this).find('.dropdown-list'),
            $label = $(this).find('.dropdown-label'),
            $inputs = $(this).find('.check'),
            defaultChecked = $(this).find('input[type=checkbox]:checked'),
            result = [];
        folders = [];
        htmlResult = [];
        htmlResult2 = [];

        updateStatus($label, result);
        if(defaultChecked.length) {
            defaultChecked.each(function () {
                if($(this).parent().parent().hasClass('catalogue-list')) {
                    htmlResult.push('<span>'+$(this).next().text()+'</span>');

                } else {
                    htmlResult2.push('<span>'+$(this).next().text()+'</span>');
                }
                result.push($(this).next().text());
                htmlResult.push('<span>'+$(this).next().text()+'</span>');



                if(el != ".dropdown-select") {
                    $label.html(result.join(", "));
                }else {
                    if($(this).parent().parent().hasClass('catalogue-list')){
                        console.log(htmlResult);
                        $('.catalogue .list-selection').html(htmlResult.join(" "));


                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));

                    }

                }

                folders.push($(this).next().attr('data-id'));
            });
        }

        $label.on('click', ()=> {
            $(this).toggleClass('open');
        });

        $inputs.on('change', function() {
            var checked = $(this).is(':checked');
            var checkedText = $(this).next().text();
            var checkedID= $(this).next().attr('data-id');
            if(checked) {
                result.push(checkedText);
                if($(this).parent().parent().hasClass('catalogue-list')) {
                    htmlResult.push('<span>'+checkedText+'</span>');
                }else {
                    htmlResult2.push('<span>'+checkedText+'</span>');
                }


                if(el != ".dropdown-select") {
                    $label.html(result.join(", "));
                }else {
                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        $('.catalogue .list-selection').html(htmlResult.join(" "));
                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));
                    }
                }

                folders.push(checkedID);
            }else{
                let index = result.indexOf(checkedText);
                if (index >= 0) {
                    result.splice(index, 1);
                    folders.splice(index, 1);

                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        htmlResult.splice(index, 1);
                    }else {
                        htmlResult2.splice(index, 1);
                    }
                }
                if(el != ".dropdown-select") {
                    $label.html(result.join(", "));
                } else {
                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        $('.catalogue .list-selection').html(htmlResult.join(" "));
                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));
                    }

                }
            }
            updateStatus($label, result, folders);
            $('#dropdown-folder, #choose-folder,#getdevoirname').attr('data-ids', folders);
        });
    });
};

/*function filePreview(input) {
    console.log("tt");
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        console.log(reader);
        reader.onload = function(e) {
            $('.vich-image  img, .img-profile').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

 */
$("#profile_imageFile_file").change(function() {
    filePreview(this);

});
var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
        var output = document.getElementById('output');
        output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
};

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

$(window).resize(function(e) {
    /*if ($(window).width() <= 768) {
       $.ajax({
            type: "POST",
            url: '/admin/getFolder/4',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
       $('.navbar-nav').addClass('toggled');
    } else if ($(window).width() <= 920) {
       $.ajax({
            type: "POST",
            url: '/admin/getFolder/6',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
    } else {
        $.ajax({
            type: "POST",
            url: '/admin/getFolder/8',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
    } */
});
if ($(window).width() <= 920) {
    $('.navbar-nav').addClass('toggled');
    if($('#contentLibrary').length > 0) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/getFolder/4',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
    }

}
if ($(window).width() <= 1250) {
    if($('#contentLibrary').length > 0) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/getFolder/6',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
    }
}

checkboxDropdown('.custom-dropdown');

/** end multicheckbox **/


checkboxDropdown('.dropdown-select', 'Sélection');
checkboxDropdowns('#formdrown', 'Sélection');


/** end functions **/
if($(".select2").length > 0) {
    $(".select2").select2({
        minimumResultsForSearch: -1
    });
}

if($('.select_2').length > 0) {
    $("#profile_birthday_day, #profile_birthday_month, #profile_birthday_year").select2({
        minimumResultsForSearch: -1
    })
}
$('#delay').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
        $('#sub-delay').removeClass('hidden');
    } else {
        // the checkbox is now no longer checked
        $('#sub-delay').addClass('hidden')
    }
});

$('#edit-title span').click(function() {
    $('#modal-title').prop("disabled", false);
    $('#modal-title').removeClass('disabled')
})
$('#edit-text span').click(function() {
    $('#modal-text').prop("disabled", false);
    $('#modal-text').removeClass('disabled')
})
$('#edit-btn span').click(function() {
    $('#modal-btn').prop("disabled", false);
    $('#modal-btn').removeClass('disabled')
})
//if($('#put-file').length > 0) {

//}

$('.first-btn').click(function() {
    $('#second').removeClass('show');
    $('#first').addClass("show");
})
$('.second-btn').click(function() {
    $('#first').removeClass('show');
    $('#second').addClass("show");
});

/** module user **/
$('#filter-user').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
    } else {
        // the checkbox is now no longer checked
    }
});
/** end module user **/
function search(key, type) {
    var settings = {
        /**
         *Mandatory
         *Text field ID where you want to implement autocomplete.
         */
        inputId: type,
        /**
         *optional
         *No. of length when keyup event fire.
         */
        inputLength: 1,

        /**
         *optional
         *Json Field name which you want to put in text field when selected.
         */
        inputValue: 'name',

        /**
         *optional
         *Fields name from json data which you want to show in table rest fields will hide.
         */
        fields: 'name',

        /**
         *optional
         *If you have json data in your page no need to pass ajax url.
         *just pass associative json in data
         */
        data:  [{'name': "nahed"},{'name': "mohamed"},{'name': "lois"},{'name': "jaafar"}]
    }

    /**
     *1st Param : settings
     *2nd Param : onSelectCallback, it is fired when select on autosuggestion with selected data
     */
    AjaxLiveSearch.init(settings, function (data) {
        $('.my-result-'+type).append('<span class="col-6">'+data.name+'<a href=""> <img src="/assets/images/icons/close.png"></a></span>');

        $('.search').val('');
    });
}

$('.effect-btn').click(function() {
    $('#visibility').removeClass('show');
    $('#effect').addClass("show");
})
$('.visibility-btn').click(function() {
    $('#effect').removeClass('show');
    $('#visibility').addClass("show");
});
$('#typestep').on("change", function(e) {
    if($("#typestep").length > 0){
        var s = $("#typestep").select2('val');
        console.log(s);
        if (s == "Devoir") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Devoir");
            $('#dev-3').removeClass('hidden');
            $('#dev-5').removeClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Valider") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Valider");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').removeClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Quiz") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Quiz");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').removeClass('hidden');
            $('#quiz-5').removeClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
        }
        if (s == "Score") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Score");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').removeClass('hidden');
            $('#score-8').removeClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Temps") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Temps");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').removeClass('hidden');
            $('#temps--4').removeClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Forum") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Forum");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').removeClass('hidden')
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Note") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Note");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').removeClass('hidden');
            $('#note--4').removeClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Récompenses") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Récompenses");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').removeClass('hidden');
            $('#recomp--4').removeClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
    }
});
$('#badge-devoir').on("change", function(e) {
    var d = $("#badge-devoir").select2('val');
    console.log(d);
    if (d == "Appréciation") {
        $('#dev-5').removeClass('hidden');
        $('#devoir-val').addClass('hidden');
        $('#dev-color').addClass('hidden');
        $('#devoir-otr').addClass('hidden');
    } else if (d == "Chiffré") {
        $('#devoir-val').removeClass('hidden');
        $('#dev-5').addClass('hidden');
        $('#dev-color').addClass('hidden');
        $('#devoir-otr').addClass('hidden');
    } else if (d == "Couleur") {
        $('#dev-color').removeClass('hidden');
        $('#dev-5').addClass('hidden');
        $('#devoir-val').addClass('hidden');
        $('#devoir-otr').addClass('hidden');
    } else if(d=="Autre"){
        $('#devoir-otr').removeClass('hidden');
        $('#dev-5').addClass('hidden');
        $('#devoir-val').addClass('hidden');
        $('#dev-color').addClass('hidden');
    }
});
$('#badge-quiz').on("change", function(e) {
    var d= $("#badge-quiz").select2('val');
    if (d=="Certificats"){
        $('#quiz-dev').removeClass('hidden')
        $('#quiz-5').addClass('hidden');
    }else {
        $('#quiz-dev').addClass('hidden');
        $('#quiz-5').removeClass('hidden');
    }
});
$(".open-button").on("click", function() {
    $(this).closest('.collapse-group').find('.collapse').collapse('show');
});

$(".close-button").on("click", function() {
    $(this).closest('.collapse-group').find('.collapse').collapse('hide');
});
if($('#color-picker-2').length > 0) {
    const pickr2 = new Pickr({
        el: '#color-picker-2',
        useAsButton: true,
        default: "303030",
        showAlways: true,
        components: {
            preview: true,
            opacity: true,
            hue: true,

            interaction: {
                input: true,
            }
        },

        onChange(hsva, instance) {
            $('.appercu-color').css('background-color', hsva.toRGBA().toString());
        }
    });
    $('.pcr-picker').css('top', '63.753px');
    $('.pcr-app').removeClass('visible');
}
$('#show-font').click(function() {
    if($('.pcr-app').hasClass('visible')) {
        $('.pcr-app').removeClass('visible');
    } else {
        $('.pcr-app').addClass('visible');
    }
})
if($("#ex8").length > 0) {
    $("#ex8").slider();
    $("#ex8").on("slide", function(slideEvt) {
        $("#ex6SliderVal").text(slideEvt.value);
    });
}
if($('#sliderPosition').length > 0) {
    $("#sliderPosition").slider();
    $("#sliderPosition").on("slide", function(slideEvt) {
        $("#sliderPositionVal").text(slideEvt.value);
    });
}
if($('#sliderPositionfin').length > 0) {
    $("#sliderPositionfin").slider();
    $("#sliderPositionfin").on("slide", function(slideEvt) {
        $("#sliderPositionfinVal").text(slideEvt.value);
    });
}
if($('#size').length > 0) {
    $("#size").slider();
    $("#size").on("slide", function(slideEvt) {
        $("#sizeVal").text(slideEvt.value);
    });
}
if($('#teinte').length > 0) {
    $("#teinte").slider();
    $("#teinte").on("slide", function(slideEvt) {
        $("#teinteVal").text(slideEvt.value);
    });
}
if($('#saturation').length > 0) {
    $("#saturation").slider();
    $("#saturation").on("slide", function(slideEvt) {
        $("#saturationVal").text(slideEvt.value);
    });
}
if($('#lum').length > 0) {
    $("#lum").slider();
    $("#lum").on("slide", function(slideEvt) {
        $("#lumVal").text(slideEvt.value);
    });
}
if($('#control').length > 0) {
    $("#control").slider();
    $("#control").on("slide", function(slideEvt) {
        $("#controlval").text(slideEvt.value);
    });
}
if($('#invers').length > 0) {
    $("#invers").slider();
    $("#invers").on("slide", function(slideEvt) {
        $("#inversVal").text(slideEvt.value);
    });
}
if($('#sepia').length > 0) {
    $("#sepia").slider();
    $("#sepia").on("slide", function(slideEvt) {
        $("#sepiaVal").text(slideEvt.value);
    });
}
if($('#opacity').length > 0) {
    $("#opacity").slider();
    $("#opacity").on("slide", function(slideEvt) {
        $("#opacityVal").text(slideEvt.value);
    });
}
if($('#resum').length > 0) {
    $("#resum").slider();
    $("#resum").on("slide", function(slideEvt) {
        $("#resumVAl").text(slideEvt.value);
    });
}

$('.list-btn .nav-item a').click(function() {
    $('.pcr-app').removeClass('gradient');

    if($(this).attr('href') !="#palette-tab") {
        $('.pcr-app').removeClass('visible');
    } else {
        $('.pcr-app').addClass('visible');
    }
});
$('#badge-quiz').on("change", function(e) {
    var d= $("#badge-quiz").select2('val');
    console.log(d);
    if (d=="Certificats"){
        $('#quiz-dev').removeClass('hidden')
        $('#quiz-5').addClass('hidden');
    }else {
        $('#quiz-dev').addClass('hidden');
        $('#quiz-5').removeClass('hidden');
    }
    if(d=="Autre"){
        $('#devoir-otr').removeClass('hidden');
        $('#dev-5').addClass('hidden');
        $('#devoir-val').addClass('hidden');
        $('#dev-color').addClass('hidden');
    }
});
$(".open-button").on("click", function() {
    $(this).closest('.collapse-group').find('.collapse').collapse('show');
});

if($('#form_snippet_image').length) {
    Dropzone.autoDiscover = false;
    //je récupère l'action où sera traité l'upload en PHP
    var _actionToDropZone = $("#form_snippet_image2").attr('action');

    //je définis ma zone de drop grâce à l'ID de ma div citée plus haut.
    var myDropzone = new Dropzone("#form_snippet_image", { url: _actionToDropZone, success:function(result, responseStatus){
            var id  = responseStatus.id;
            var ids = $('#affect-file-folder #media-ids').val();
            $('#affect-file-folder #media-ids').val(ids+','+id);
        } });
    myDropzone.on("addedfile", function(file) {
        //we can add modale to indicate theat we have uploded a new file
        //alert('nouveau fichier reçu');
        //$('#affect-file-folder').modal();
    });
    myDropzone.on("complete", function() {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/getMedia',
            success: function(data) {
                $('#file-view').html(data);
            }
        });
    });
}
$('#add-file').click(function(e) {
    e.preventDefault();
    if($('.cutsom-dropzone').hasClass('hidden')) {
        $('.cutsom-dropzone').removeClass('hidden');
    } else {
        $('.cutsom-dropzone').addClass('hidden');
    }
})

$('#affect-filetofolder').click(function(e) {
    e.preventDefault();
    var folders  = $('#dropdown-folder').attr('data-ids');
    var filesIds = $('#media-ids').val();
    if(filesIds && folders) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/affectFolder',
            data: { folders: folders, filesIds:filesIds },
            success: function(data) {
                if (data == "success") {
                    $('#affect-file-folder').modal('hide');
                }
            }
        });
    } else {
        $('#affect-file-folder').modal('hide');
    }
});
$('.forma-btn').click(function() {
    console.log('click')
    $('#categ').removeClass('show');
    $('#forma').addClass("show");
    $('#new-forma').removeClass("hidden");
    $('#new-catag').addClass("hidden");
})
$('.categ-btn').click(function() {
    $('#forma').removeClass('show');
    $('#categ').addClass("show");
    $('#new-forma').addClass("hidden");
    $('#new-catag').removeClass("hidden");
});
$('#tab-categ').click(function() {
    $('.tab-categ').removeClass('hidden');
    $('.tab-formations').addClass('hidden');
    var activeImage = $(this).find('img').attr('data-activeimage');
    var nonActive = $('#tab-formations img').attr('data-nonactiveimage');
    $(this).find('img').attr('src', nonActive);
    $('#tab-formations img').attr('src', activeImage);
});
$('#tab-formations').click(function() {
    $('.tab-formations').removeClass('hidden');
    $('.tab-categ').addClass('hidden');
    var activeImage = $(this).find('img').attr('data-activeimage');
    var nonActive = $('#tab-formations img').attr('data-nonactiveimage');
    $(this).find('img').attr('src', activeImage);
    $('#tab-categ img').attr('src', nonActive);
});
$('.delete-badge').click(function (e) {
    $('.delete-badge').removeClass('active');
    $(this).addClass('active');
})
$('#delete-bad').click(function (e) {
    e.preventDefault();
    var id = $('.delete-badge.active').attr('data-id');
    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: window.location.origin+'/deleteBadge',
        success: function (data) {
            location.reload();
        }
    });
});
var img = $('.vich-image-badge a').attr('href');

if (img) {
    $('.img-badge').attr('src', img);
}
function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $("#showimagepicture").attr('src', e.target.result);
            $('.vich-image-badge  img, .img-badge').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#badge_picture_file").change(function() {
    filePreview(this);
});
$('#typeFC').on("change", function(e) {

    var d= $("#typeFC").select2('val');

    if (d==1){
        console.log('valueoption'  +d);
        $('.formationaffect').removeClass('hidden');
        $('.classeaffecttoformation').addClass('hidden');
    }else  {
        console.log('valueoption'  +d);
        $('.formationaffect').addClass('hidden');
        $('.classeaffecttoformation').removeClass('hidden');
    }
});

$("#getvalforinput").bind("change ", function() {
    var s=$("#getvalforinput input[type='radio']:checked").val();
    $(".attributeval").val(s);
})
$("#getsensbase").bind("change ", function() {
    var s=$("#getsensbase input[type='radio']:checked").val();
    $(".savesensjs").val(s);
})

$("#choose-folder").bind("change ", function() {
    var getids=$("#choose-folder").attr("data-ids");
    $("#formationconfig_preqformation").val(getids);

})


function checkboxDropdowns(el='', text="Ajouter au(x) dossier(s)") {
    var $el = $(el)

    function updateStatus(label, result, folders) {
        if(!result.length) {
            //label.html(text);
        }
    };

    $el.each(function(i, element) {
        var $list = $(this).find('.dropdown-forum'),
            $label = $(this).find('.dropdown-labelforum '),
            $inputs = $(this).find('.check'),
            defaultChecked = $(this).find('input[type=checkbox]:checked'),
            result = [];
        folders = [];
        htmlResult = [];
        htmlResult2 = [];

        updateStatus($label, result);
        if(defaultChecked.length) {
            defaultChecked.each(function () {
                if($(this).parent().parent().hasClass('catalogue-list')) {
                    //htmlResult.push('<span>'+$(this).next().text()+'</span>');

                } else {
                    htmlResult2.push('<span>'+$(this).next().text()+'</span>');
                }
                result.push($(this).next().text());
                htmlResult.push('<span>'+$(this).next().text()+'</span>');


                if(el != ".dropdown-selectforum") {
                    $label.html(result.join(", "));
                }else {
                    if($(this).parent().parent().hasClass('catalogue-list')){
                        $('.catalogue .list-selection').html(htmlResult.join(" "));


                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));

                    }

                }

                folders.push($(this).next().attr('data-id'));
            });
        }

        $label.on('click', ()=> {
            $(this).toggleClass('open');
        });

        $inputs.on('change', function() {
            var checked = $(this).is(':checked');
            var checkedText = $(this).next().text();
            var checkedID= $(this).next().attr('data-id');
            if(checked) {
                result.push(checkedText);
                if($(this).parent().parent().hasClass('catalogue-list')) {
                    htmlResult.push('<span>'+checkedText+'</span>');
                }else {
                    htmlResult2.push('<span>'+checkedText+'</span>');
                }


                if(el != ".dropdown-selectforum") {
                    $label.html(result.join(", "));
                }else {
                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        $('.catalogue .list-selection').html(htmlResult.join(" "));
                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));
                    }
                }

                folders.push(checkedID);
            }else{
                let index = result.indexOf(checkedText);
                if (index >= 0) {
                    result.splice(index, 1);
                    folders.splice(index, 1);

                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        htmlResult.splice(index, 1);
                    }else {
                        htmlResult2.splice(index, 1);
                    }
                }
                if(el != ".dropdown-selectforum") {
                    $label.html(result.join(", "));
                } else {
                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        $('.catalogue .list-selection').html(htmlResult.join(" "));
                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));
                    }

                }
            }
            updateStatus($label, result, folders);
            $('#formdrown').attr('data-ids', folders);
        });
    });
};


$("#formdrown").bind("change ", function() {

    var getidsnew=$("#formdrown").attr("data-ids");

    $("#formationconfig_insertjoinform").val(getidsnew);

})

$('#checkAllf').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
        $('.check-element-f').prop("checked", true);
    } else {
        // the checkbox is now no longer checked
        $('.check-element-f').prop("checked", false);
    }
});
$('#delete-categories').click(function () {
    //get all selected categories
    var categories = [];
    $.each($("input[name='categorie-check']:checked"), function () {
        categories.push($(this).attr('data-id'));
    });
    console.log(categories);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: window.location.origin+'//deleteCategories',
        data: {categories: categories},
        success: function (data) {
            if (data == "success") {
                window.location.href = '/fr/formation/home/true';
            }
        }
    });
});
$('#tab-categories').click(function() {
    $('.tab-categories').removeClass('hidden');
    $('.tab-categ').addClass('hidden');
    $('.list-categories').addClass('hidden');
    var activeImage = $(this).find('img').attr('data-activeimage');
    var nonActive = $('#list-categories img').attr('data-nonactiveimage');
    $(this).find('img').attr('src', activeImage);
    $('#list-categories img').attr('src', nonActive);

});

$('#list-categories').click(function() {
    $('.tab-categ').removeClass('hidden');
    $('.tab-categories').addClass('hidden');
    var activeImage = $(this).find('img').attr('data-activeimage');
    var nonActive = $('#tab-categories img').attr('data-nonactiveimage');
    $(this).find('img').attr('src', activeImage);
    $('#tab-categories img').attr('src', nonActive);
});
$('.delete-cat').click(function (e) {
    $('.delete-cat').removeClass('active');
    $(this).addClass('active');
})
$('.del-categ').click(function (e) {
    e.preventDefault();
    var id = $('.delete-cat.active').attr('data-id');
    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: window.location.origin+'/deleteCategory',
        success: function (data) {
            if(data == 'success'){
                window.location.href = '/fr/formation/home/true';
            }
        }
    });
});
$('.delete-forma').click(function (e) {
    $('.delete-forma').removeClass('active');
    $(this).addClass('active');
})
$('.del-forma').click(function (e) {
    e.preventDefault();
    var id = $('.delete-forma.active').attr('data-id');
    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: window.location.origin+'/deleteFormation',
        success: function (data) {
            if(data == 'success'){
                window.location.href = '/fr/formation/home/true';
            }
        }
    });
});
var formations = [];
$('#delete-formas').click(function () {
    //get all selected formations

    $.each($("input[name='formation-check']:checked"), function () {
        formations.push($(this).attr('data-id'));
    });
    console.log(formations);
    var path = $(this).attr("data-path");
    $.ajax({
        type: "POST",
        url: window.location.origin+'/deleteFormations',
        data: {formations: formations},
        success: function (data) {
            if (data == "success") {
                window.location.href = '/fr/formation/home/false';
            }
        }
    });
});

$("#getdevoirname").bind("change ", function() {
    var namedevoircopyin=$("#getdevoirname").attr("data-ids");
    $("choice-chaiptre").val(namedevoircopyin);
})


$(".getcategory").bind("change ", function() {
    var getidsnew=$(".getcategory").attr("data-ids");
    $("#formation_category_choice").val(getidsnew);
})
$("#choose-folder").bind("change ", function() {

    var getidsnew=$("#choose-folder").attr("data-ids");
    var editcoursnew=$("#choose-folder").attr("data-ids");
    $("#choice_preq").val(getidsnew);
    $("#select_choiceprerequis").val(editcoursnew);
})

/*$('#categorys').on("change", function(e) {
    e.preventDefault();
    var formationId = $(this).attr('data-id');
    //add class active to the current folder

    $.ajax({
        type: "POST",
        url: '/formation/getFormation',
        data: { formationId: formationId },
        success: function(data) {
            $('#tab-forma').html(data);
        }
    });
});*/

$('.duplicate-categories').click(function () {
    //get all selected categories
    var categories = [];
    $.each($("input[name='categorie-check']:checked"), function () {
        categories.push($(this).attr('data-id'));
    });
    console.log(categories);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: window.location.origin+'/duplicateCategories',
        data: {categories: categories},
        success: function (data) {
            if (data == "success") {
                window.location.href = '/fr/formation/home/true';
            }
        }
    });
});
$('.duplicate-formations').click(function (e) {
    //get all selected formations
    var formations = [];
    $.each($("input[name='formation-check']:checked"), function () {
        formations.push($(this).attr('data-id'));
    });
    console.log(formations);
    var path = $(this).attr("data-path");
    console.log(path);
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: window.location.origin+'/duplicateFormations',
        data: {formations: formations},
        success: function (data) {
            if (data == "success") {
                window.location.href = '/fr/formation/home/true';
            }
        }
    });
});
$('#carousel-formation').hover(
    function(){ $(this).addClass('hover') },
    function(){ $(this).removeClass('hover') }
);
$('#carousel-formation').click(function(e) {
    e.preventDefault();
    var folderId = $(this).attr('data-id');
    //add class active to the current folder
    if($(this).hasClass('active')) {
        $(this).removeClass('active');
        catalogueId = '';
    } else {
        $('#carousel-formation .full-card').removeClass('active');
        $(this).addClass('active');
    }
    $.ajax({
        type: "POST",
        url: window.location.origin+'/getCatalogue',
        data: { folderId: folderId },
        success: function(data) {

        }
    });
})
//add new category
$('#add-category').click( function (e) {
    var name = $('#category_name').val();
    if(name) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/addCateg',
            data: { categ_name: name },
            success: function(data) {
                window.location.href = '/fr/formation/home/true';
            }
        });
    }
});
//update existing category
$('#update-category').click( function (e) {
    var name = $('#name-categ').val();
    var id   = $('#getCategID').val();
    if(id) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/editCateg/'+id,
            data: { categ_name: name },
            success: function(data) {
                window.location.href = '/fr/formation/home/true';
            }
        });
    }
});

$('.edit-categ').click(function () {
    var s=$(this).attr('data-id');
    $("#getCategID").val(s);
    $("#name-categ").val($(this).attr('data-name'));
});

$('.duplicate-badges').click(function () {
    //get all selected badges
    var badges = [];
    $.each($("input[name='badge-check']:checked"), function () {
        badges.push($(this).attr('data-id'));
    });
    console.log(badges);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: window.location.origin+'/duplicateBadges',
        data: {badges: badges},
        success: function (data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
$('#checkAllC').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
        $('.check-element').prop("checked", true);
    } else {
        // the checkbox is now no longer checked
        $('.check-element').prop("checked", false);
    }
});
$('.download-badges').click(function () {
    //get all selected badges
    var badges = [];
    $.each($("input[name='badge-check']:checked"), function () {
        badges.push($(this).attr('data-id'));
    });
    console.log(badges);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: window.location.origin+'/downloadBadges',
        data: {badges: badges},
        success: function (data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
var sendmessagetable=[];
$('.sendmessage').click(function () {
    var sujettformmessagerie=$("#messagesujet").val();
    var editor =     CKEDITOR.instances['subjectmessage'].getData();
    sendmessagetable.push(sujettformmessagerie);
    sendmessagetable.push(editor);



    $.ajax({
        type: "POST",
        url: '../messagerie',
        data: { sendmessagetable: sendmessagetable},
        success: function (response) {

        },
    });

});
if(pathnamesutdent=="/student/notes"){

    if($('#subjectmessage').length > 0) {
        CKEDITOR.replace('subjectmessage', {
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P,
            toolbar: [{name: 'styles', items: ['Format', 'Font', 'FontSize']},
                {
                    name: 'basicstyles',
                    groups: ['basicstyles'],
                    items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
                },

                {name: 'scripts', items: ['Subscript', 'Superscript']},
                {
                    name: 'justify',
                    groups: ['blocks', 'align'],
                    items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                },
                {
                    name: 'paragraph',
                    groups: ['list', 'indent'],
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent']
                },
                {name: 'links', items: ['Link', 'Unlink']},
                {name: 'spell', items: ['jQuerySpellChecker']},
            ],
            toolbarLocation: 'top',

        });
    }
}

$("#badge-formation").on("change", function(e) {
    if($("#badge-formation").length > 0) {
        var formation = $("#badge-formation").select2('val');
        console.log(formation);
        $("#badge_val_time").val(formation);
    }
});
var statut = "";
var jsonStatuts="";
/*$("#statut .check").click(function () {
    if ($(this).is(':checked')) {
        statut=$(this).attr("data-id-statut");
        tabStatus.push(statut);
        console.log(tabStatus);
        $("#profile_roles").val(tabStatus);
    }
    if (!$(this).is(':checked')) {
        statut=$(this).attr("data-id-statut");
        var index = tabStatus.indexOf($(this).attr("data-id-statut"));
        tabStatus.splice(index, 1);
        $("#profile_roles").val(tabStatus);
    }

    jsonStatuts  = JSON.stringify(tabStatus);
    console.log(jsonStatuts);
});
var formationname = "";
var jsonFormation="";
/*$("[id^=formation-search] .check").click(function () {
    let numUser = $(this).attr('data-num_user');
    if ($(this).is(':checked')) {

        var b = {};
         b['formationname_' + numUser] = $(this).attr("data-id-formation");


        tabFormations.push( b['formationname_' + numUser]);
        //$('#formation-select').val(tabFormations);
        $('#listFormation_'+numUser).val(b['formationname_' + numUser]);
    }
    if (!$(this).is(':checked')) {
        formationname=$(this).attr("data-id-formation");
        var index = tabFormations.indexOf($(this).attr("data-id-formation"));
        tabFormations.splice(index, 1);
        $('#formation-select').val(tabFormations);
        $('#listFormation_'+numUser).val(tabFormations);
    }

    jsonFormation  = JSON.stringify(tabFormations);
    console.log(jsonFormation);
});*/
if ($(window).width() <= 920) {
    $('.navbar-nav').addClass('toggled');
    if($('#carousel-formation').length > 0) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/getCatalogue/1',
            success: function (data) {
                $('#carousel-formation').html(data);
            }
        });
    }
}
var nameformation="";
var namecompare="";
var nameoformation={};
var jsonF=[];
$(".modal-dup").click(function () {
    nameformation=$(this).attr("data-name");
    var id = $(this).attr("data-id");
    $('#popupF').attr("data-id", id);
    $("#categ-val").val(nameformation);
    nameoformation = {
        "id":nameformation
    }
    namecompare=$(this).attr("data-name");
    $("#popupF").val(nameformation);
});

$(document).on('click', "#dupliquer-formation",function(e){
    $( "#dupForm .check" ).each(function(){
        var description =0;
        var picture=0;

        if($(this).attr('type') == 'checkbox') {
            if ($(this).is(':checked')) {
                $(this).addClass("checked");
                if ($(this).hasClass("checked")) {
                    paramsFormations.push($(this).attr("data-description") , $(this).attr("data-picture"));
                    //paramsFormations.push({picture : $(this).attr("data-picture")});
                }
            }
        }
    });
    var idF = $('#popupF').attr("data-id");
    $('#copy-form').attr("data-id", idF);

    var idP = $('#popupF').attr("data-id");
    if(idF === idP){
        var formationId = idF;
        paramsFormations.push(formationId);
        console.log(formationId);
    }
    console.log(paramsFormations);
    if(typeof url_to_send_to !== "undefined") {
        $.ajax({
            type: "POST",
            url: url_to_send_to,
            data: { duplicateFormations: paramsFormations, formationId: formationId},
            success: function (response) {
                location.reload();
            },
        });
    }
});
var arrcategorys = new Array();
$('.findcategor > input[type=hidden]').each(function(){
    arrcategorys.push($(this).val());
})
function findValueInArray(value,arr){
    var result = "Doesn't exist";
    $('.box-arrow-formation').hide();
    for(var i=0; i<arr.length; i++){
        var name = arr[i];
        if(name == value){
            result = 'Exist';
            $('.box-arrow-formation').show();
            break;
        }
    }

    return result;
}
$('#categorys').on("change", function(e) {
    if($("#categorys").length > 0) {
        var d = $("#categorys").select2('val');
        /*   var d = $("#categorys").select2('val');
           var className= jQuery.inArray(d,arrcategorys);
           findValueInArray(d, arrcategorys);*/

        console.log(d);

        const divs = document.querySelectorAll('.col-2-certif');
        divs.forEach(div => {
            div.classList.remove('hidden');
        });
        var arrcategorysd = new Array();
        arrcategorysd.push(d);
        console.log(arrcategorysd);
        document.querySelectorAll('.col-2-certif').forEach(div => {
            const className= arrcategorysd.includes(div.dataset['type']) ? "show" : "hidden";
            div.classList.add(className);
            console.log(div.dataset['type']);

        });



        /*   if (className = true) {
               $('.box-arrow-formation').show();
           } else {
               $('.box-arrow-formation').hide();
           }*/
    }
});
const ext = ["jpg", "gif", "png","jpeg"];
const extvideo=["mp4","ogv"];
const extpdf=["pdf"];
const extdoc=["doc"];

function ddmmyyyy(s){
    var part=s.split(/\//),
        date=new Date()
    date.setYear(part[2])
    date.setMonth(part[1]-1)
    date.setDate(part[0])
    return date
}
function ddmmyyyClassSortasdent(a,b){
    var A=ddmmyyyy(a.getAttribute('data-date')),
        B=ddmmyyyy(b.getAttribute('data-date'))
    return A>B? 1 : -1
}
function ddmmyyyClassSort(a,b){
    var A=ddmmyyyy(a.getAttribute('data-date')),
        B=ddmmyyyy(b.getAttribute('data-date'))
    return A<B? 1 : -1
}
$.fn.sortChildren=function(cmp){
    var self=this,
        children=$.makeArray(this.children()).sort(cmp)
    $.each(children, function(i, child){
        self.append(child)
    })
    return self
}

$('#getfilterdate').on("change", function(e) {
    if($("#s2id_autogen3").length > 0){
        var namefilter = $("#s2id_autogen3").select2('val');
        $('#dateavant').addClass("hidden");
        if(namefilter=="Plus récent"){
            $('#file-view').sortChildren(ddmmyyyClassSort)
        }
        if(namefilter=="plus ancien"){
            $('#file-view').sortChildren(ddmmyyyClassSortasdent)
        }
        if(namefilter=="plus ancien"){
            $('#file-view').sortChildren(ddmmyyyClassSortasdent)
        }
    }

})


$('#getfilterpicture').on("change", function(e) {
    const divs = document.querySelectorAll('.getattrextension');
    divs.forEach(div => {
        div.classList.remove('hidden');
    });
    if($("#s2id_autogen1").length > 0){
        var namefilter = $("#s2id_autogen1").select2('val');

        if(namefilter=="Image"){
            document.querySelectorAll('.getattrextension').forEach(div => {
                const className= ext.includes(div.dataset['extension']) ? "show" : "hidden";
                div.classList.add(className);

            });

        }
        if(namefilter=="Vidéo"){
            document.querySelectorAll('.getattrextension').forEach(div => {
                const className= extvideo.includes(div.dataset['extension']) ? "show" : "hidden";
                div.classList.add(className);
            });

        }
        if(namefilter=="PDF") {
            document.querySelectorAll('.getattrextension').forEach(div => {
                const className = extpdf.includes(div.dataset['extension']) ? "show" : "hidden";
                div.classList.add(className);
            });
        }
        if(namefilter=="Document"){
            document.querySelectorAll('.getattrextension').forEach(div => {
                const className= extdoc.includes(div.dataset['extension']) ? "show" : "hidden";
                div.classList.add(className);
            });
        }

    }

});
$('#listformationstep2').on("change", function(e) {
    var nameformationchoi = $("#listformationstep2").select2('val');
    $("#formationconfig_list_temp").val(nameformationchoi);

});
$('#listformationstep2devoir').on("change", function(e) {
    var nameformationchoi = $("#listformationstep2devoir").select2('val');
    $("#formationconfig_list_note").val(nameformationchoi);

});
$(".notprequis").on("change", function(e) {
    var nameformationchoi = $("#choose-folder").attr("data-ids");
    var getfinalyname="";
    $(this).find('input').each(function() {
        if ($(this).attr('type') == 'checkbox') {
            if ($(this).is(':checked')) {
                getfinalyname = $(this).attr("data-type");
                if(getfinalyname=="Note"){
                    $(this).addClass("checked");
                    $(".note").removeClass("hidden");
                }
                if(getfinalyname=="Score"){
                    $(".score").removeClass("hidden");
                }
                if(getfinalyname=="Temps"){
                    //console.log("tt");
                    $(".bloctemps").removeClass("hidden");
                }
                if(getfinalyname=="Valide"){
                    $(".blocformationgeneral").removeClass("hidden");
                }
            }
            if(!$(this).is(':checked')) {
                getfinalyname = $(this).attr("data-type");
                if(getfinalyname=="Note"){
                    $(this).removeClass("checked");
                    $(".note").addClass("hidden");
                }
                if(getfinalyname=="Score"){
                    $(".score").addClass("hidden");
                }
                if(getfinalyname=="Temps"){

                    $(".bloctemps").addClass("hidden");
                }
                if(getfinalyname=="Valide"){
                    console.log("cc3");
                    $(".blocformationgeneral").addClass("hidden");
                }
            }
        }
    });


});

var sendnodename=[];
var nameofnotes={};
$('#save-folder-note').click(function () {
    var titlename=$("#modal-title").val();
    var comment =     $(" #modal-text").val();
    var note=$("#modal-btn").val();


    nameofnotes = {
        "titlename": titlename,
        "comment": comment,
        "note": note,

    }
    console.log(nameofnotes);
    sendnodename.push(nameofnotes);

    console.log(url_to_send_to);
    if(typeof url_to_send_to !== "undefined") {
        $.ajax({
            type: "POST",
            url: url_to_send_to,
            data: {sendnodename: sendnodename},
            success: function (response) {

            },
        });
    }
});
(function( $ ) {

    $.fn.checked = function(value) {

        if(value === true || value === false) {
            // Set the value of the checkbox
            $(this).each(function(){ this.checked = value; });

        } else if(value === undefined || value === 'toggle') {

            // Toggle the checkbox
            $(this).each(function(){ this.checked = !this.checked; });
        }

    };

})( jQuery );
var arrtocatalogue=[];
$(".selectedall").click(function() {
    $(this).find('input').each(function(){
        arrtocatalogue=[];
        var nameselect=  $(this).attr('data-name');
        arrtocatalogue.push(nameselect);
        if($(this).attr('data-id-select') == 1){
            if($(this).is(':checked')){
                $('.custom-control-input').checked(true);
            }
            if(!$(this).is(':checked')) {
                $('.custom-control-input').checked(false);
            }
        }
        /*     var filtered = arrtocatalogue.filter(function (el) {
                 return el != null;
             });

             $.each(filtered, function(index, value){

                 $(' .catalogue .list-selection ').append('<span>' + value + '<span>');
             });*/
    });


});
$(".catalogue > .list-selection span").click(function() {
    var s=$(this).remove();
});

$('#save-user').click(function(e) {
    e.preventDefault();
    let nbUser = $('#form_user').attr('data-nb-user');
    let data = $('#form_user').serializeArray();
    console.log(data);
    let users = [];

    for (let k = 1; k <= nbUser; k++) {
        let user = {};
        arrayFormation = [];
        arrayClass     = [];
        arrayRole      = [];
        $.each($("#formation-search_"+k+" > option:selected"), function() {
            arrayFormation.push($(this).val());
        });
       /* $("input:checkbox[name=listFormation_"+k+"]:checked").each(function(){
            arrayFormation.push($(this).val());
        });*/
        user.formations = arrayFormation;
        $.each($("#class-search_"+k+" > option:selected"), function() {
            arrayClass.push($(this).val());
        });
        /*$("input:checkbox[name=class_"+k+"]:checked").each(function(){
            arrayClass.push($(this).val());
        });*/
        user.class = arrayClass;
        user.roles = [];
        $.each($("#statut_"+k+" select > option:selected"), function() {
            user.roles.push($(this).attr('data-id'));
        });

        /* $("input:checkbox[name=roles_"+k+"]:checked").each(function(){
             arrayRole.push($(this).val());
         });*/
        //user.roles = arrayRole;
        for (var i = 0; i < data.length; i++) {
            if (data[i].name == "lastname_" + k ) {
                user.lastname = data[i].value;
            }
            if (data[i].name == "username_" + k) {
                user.username = data[i].value;
            }
            if (data[i].name == "email_" + k ) {
                user.email = data[i].value;
            }
        }
        users.push(user);
    }
    console.log(window.location.origin)
    $.ajax({
        type: "POST",
        url: window.location.origin+'/addUsers/',
        data : { users : users},
        success: function(response){
          window.location.href = window.location.origin+'/fr/admin/membres';
            if(response == 'success') {

                // window.location.href = "/builder/getChapter/"+parseInt(chapterId);
            }
        },
    });
    console.log(users);
});
$('#addNewUser').click(function(e) {
    e.preventDefault();
    let nbUser = $('#form_user').attr('data-nb-user');

    let newNumber = parseInt(nbUser)+1;

    $('.list-name:last-child').after('<div class="form-group pt-0 pb-2 m-0 pr-1 user-gr list-name">'+$('.list-name:last-child').html()+'</div>');
    $('.list-name:last-child .check-element').attr('id','userCheck_'+newNumber);
    $('.list-name:last-child .custom-control-label').attr('for','userCheck_'+newNumber);

    $('.list-name:last-child input.form-control').attr('id','profile_lastname_'+newNumber);
    $('.list-name:last-child input.form-control').attr('name','lastname_'+newNumber);

    $('.list-userName:last-child').after('<div class="row list-userName">'+$('.list-userName:last-child').html()+'</div>');
    $('.list-userName:last-child input').attr('id', 'profile_username'+newNumber);
    $('.list-userName:last-child input').attr('name', 'username_'+newNumber);

    $('.list-email:last-child').after('<div class="row list-email">'+$('.list-email:last-child').html()+'</div>');
    $('.list-email:last-child input').attr('id', 'profile_email_'+newNumber);
    $('.list-email:last-child input').attr('name', 'email_'+newNumber);

    $('.list-formations:last-child').after('<div class="row list-formations">'+$('.list-formations:last-child').html()+'</div>');
    $('.list-formations:last-child .right-check ').attr('id','formation-search_'+newNumber);
    $.each($(".list-formations:last-child .right-check .custom-checkbox"), function(index, el) {
        $(this).find('input').attr('id','profile_formations_sup_'+index+''+newNumber);
        $(this).find('label').attr('for','profile_formations_sup_'+index+''+newNumber);
        $(this).find('input').attr('name','listFormation_'+index+''+newNumber);
    });

    $('.list-class:last-child').after('<div class="row list-class">'+$('.list-class:last-child').html()+'</div>');
    $('.list-class:last-child .right-check .custom-checkbox input').attr('id','profile_class_'+newNumber);
    $('.list-class:last-child .right-check .custom-checkbox input').attr('name','class_'+newNumber);

    $('.list-roles:last-child').after('<div class="row list-roles">'+$('.list-roles:last-child').html()+'</div>');
    //$('.list-roles:last-child .right-check .custom-checkbox input').attr('id','role_'+newNumber);
    // $('.list-roles:last-child .right-check .custom-checkbox input').attr('name','roles_'+newNumber);
    $('#form_user').attr('data-nb-user', newNumber);
    checkboxDropdown('.dropdown-select', 'Sélection');
})

$('.module-btn').click(function() {
    $('#step3formation .btn').removeClass('active');
    $(this).addClass('active')
    $('.typeelment').removeClass('show');
    $('#module-div').addClass("show");
});
$('.cour-btn').click(function() {
    $('#step3formation .btn').removeClass('active');
    $(this).addClass('active')
    $('.typeelment').removeClass('show');
    $('#cour-div').addClass("show");
});
$('.chapter-btn').click(function() {
    $('#step3formation .btn').removeClass('active');
    $(this).addClass('active')
    $('.typeelment').removeClass('show');
    $('#chapter-div').addClass("show");
});
$('.quiz-btn').click(function() {
    $('#step3formation .btn').removeClass('active');
    $(this).addClass('active')
    $('.typeelment').removeClass('show');
    $('#quiz-div').addClass("show");
});

// accordion step2

var arrNumbername= new Array();
$('.validation input[type=hidden]').each(function(){
    arrNumbername.push($(this).val());
})
/*$('#formationglobal a').click(function() {
    var nameformationselected=$(this).attr("data-id-formation");
    var n = arrNumbername.includes(nameformationselected);
if(n==true){
    document.querySelectorAll('.eachaccordion').forEach(div => {
        const className= nameformationselected.includes(div.dataset['id-formation']) ? "show" : "hidden";
        div.classList.add(className);

    });

}
});*/

$('.affect-user').click(function(e) {
    e.preventDefault();
    $('#affect-class').modal('show');
    let id = $(this).attr('data-id');
    let Userclasses = $(this).attr('data-userClasses');
    $('#affect-class #userclasses').val(Userclasses);
    $('#affect-class #user-id').val(id);
    let tabUserclasses = Userclasses.split(', ')
    $.each($("#affect-class input[id^='profile_class_']"), function() {
        let val = $(this).val();
        if( tabUserclasses.includes(val)) {
            $(this).prop("checked", true);
        } else  {
            $(this).prop("checked", false);
        }
    });

});
$('#affect-user-class').click(function () {
    let userId    = $('#affect-class #user-id').val();
    let classId   = $('#list-class').val();
    var selectedClass = [];
    $.each($("#affect-class input[id^='profile_class_']:checked"), function() {
        selectedClass.push($(this).val());
    });
    if(userId && selectedClass) {
        let path = $(this).attr('data-path');
        $.ajax({
            type: "POST",
            url: path,
            data: { userId: userId, selectedClass: selectedClass },
            success: function(data) {
                $('#affect-class').modal('hide');
                //location.reload();
            }
        });
    }
});
$('#affect-checked-user').click(function () {
    //select affect user to class and formation
    let selectedFormation = [];
    let listClass = [];
    let checkedUsers = [];
    $.each($("#formationS #select-multi-formation > option:selected"), function() {
        selectedFormation.push(parseInt($(this).val()));
    });
    console.log(selectedFormation);
    $.each($("#classe #select-multi-class > option:selected"), function() {
        listClass.push(parseInt($(this).val()));
    });

    $.each($(".list-name input[id^='userCheck_']:checked"), function() {
        checkedUsers.push($(this).val());
        $.each($("#formation-search_"+$(this).val()+"> option"), function() {

            if(selectedFormation.includes(parseInt($(this).val()))) {
                $(this).prop("selected", true);
            } else {
                $(this).prop("selected", false);
            }
        });
        $.each($("#class-search_"+$(this).val()+" > option"), function() {

            if(listClass.includes(parseInt($(this).val()))) {
                $(this).prop("selected", true);
            } else {
                $(this).prop("selected", false);
            }
        });
    });

})
var arraycategorys = new Array();
$('.findcategories > input[type=hidden]').each(function(){
    arraycategorys.push($(this).val());
})
$('#categoryslist').on("change", function(e) {
    if($("#categoryslist").length > 0) {
        var d = $("#categoryslist").select2('val');

        console.log(d);

        const divs = document.querySelectorAll('.listeeFormation');
        divs.forEach(div => {
            div.classList.remove('hidden');
        });
        var arrcategorysd = new Array();
        arrcategorysd.push(d);
        console.log(arrcategorysd);
        document.querySelectorAll('.listeeFormation').forEach(div => {
            const className= arrcategorysd.includes(div.dataset['type']) ? "show" : "hidden";
            div.classList.add(className);
            console.log(div.dataset['type']);
        });
    }
});
var observe;
if (window.attachEvent) {
    observe = function(element, event, handler) {
        element.attachEvent('on' + event, handler);
    };
} else {
    observe = function(element, event, handler) {
        element.addEventListener(event, handler, false);
    };
}
var firstHeight = 0;
var storeH = 0;
var storeH2 = 0;
function inittexterta() {
    var text = document.getElementById('textarealampe');

    function resizetexterta() {
        text.style.height = 'auto';
        if (storeH != text.scrollHeight) {
            text.style.height = text.scrollHeight + 'px';
            storeH = text.scrollHeight;
            if (storeH2 != storeH) {
                console.log("SENT->" + storeH);
                storeH2 = storeH;
            }
        }
    }
    /* 0-timeout to get the already changed text */
    function delayedResizetext() {
        window.setTimeout(resizetexterta, 0);
    }
    observe(text, 'change', resizetexterta);
    observe(text, 'cut', delayedResizetext);
    observe(text, 'paste', delayedResizetext);
    observe(text, 'drop', delayedResizetext);
    observe(text, 'keydown', delayedResizetext);

    text.focus();
    text.select();
    resizetexterta();
}


function resize() {
    text.style.height = 'auto';
    if (storeH != text.scrollHeight) {
        text.style.height = text.scrollHeight + 'px';
        storeH = text.scrollHeight;
        if (storeH2 != storeH) {
            console.log("SENT->" + storeH);
            storeH2 = storeH;
        }
    }
}
/* 0-timeout to get the already changed text */
function delayedResize() {
    window.setTimeout(resize, 0);
}


if($("#textarealampe").length > 0) {
    inittexterta();
}
$(document).on('click', "#lamp-modal .close",function(e){
    $(".default-content").removeClass("hidden");
    $(".new-content").addClass("hidden");
    $("#textarealampe").val("");
});
$(document).ready(function () {
    $('.formation-content').parent().parent().css('height','100%');
    if($('#categoryslist').length > 0) {
        $("#statusFilter").select2({
            minimumResultsForSearch: -1,
            placeholder: "FILTRES"
        });
        $('#categoryslist').select2({
            "language": "fr",
            minimumResultsForSearch: -1,
            placeholder: "Catégories",
        });
    }
    if($('#filter-category-formation').length > 0) {
        $('#filter-category-formation').select2({
            closeOnSelect : false,
            placeholder : "Catégories",
            minimumResultsForSearch: -1
        });
    }
    if($('#select2folder').length > 0) {
        $('#select2folder').select2({
            closeOnSelect : true,
            minimumResultsForSearch: -1
        });
    }
    if($('#select2note').length > 0) {
        $('#select2note').select2({
            closeOnSelect : true,
            minimumResultsForSearch: -1
        });
    }

});
var sendnotelamp=[];
$(document).on('click', "#click-lamp",function(e){
    var textlampe=$("#textarealampe").val();
    sendnotelamp.push(textlampe);
    $.ajax({
        type: "POST",
        url: '../sendlampe',
        data: { sendnotelamp: sendnotelamp},
        success: function (response) {
        },
    });

});

$('.download-notes').click(function () {
    //get all selected notes
    var notes = [];
    var path = $(this).attr("data-path")+'/';
    $.each($("input[name='note-check']:checked"), function () {
        notes.push($(this).attr('data-id'));
        path = path+''+$(this).attr('data-id')+',';
    });
    $(this).attr("href",path.replace(/,\s*$/, ""));
});

$('#duplicate-notes').click(function () {
    //get all selected notes
    var notes = [];
    var path = $(this).attr("data-path")+'/';
    $.each($("input[name='note-check']:checked"), function () {
        notes.push($(this).attr('data-id'));
        path = path+''+$(this).attr('data-id')+',';
    });
    $(this).attr("href",path.replace(/,\s*$/, ""));
    console.log(path.replace(/,\s*$/, ""));
    console.log(notes);
});
$('#print-doc').click(function (e) {
    e.preventDefault();

    var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    mywindow.document.write('<html><head><title>Note</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>Note</h1>');
    mywindow.document.write($('.div-impression').html());
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
    mywindow.close();

})

$('.add-chapter-link').click(function (e) {
    e.preventDefault();
    $('#new-chapter-link').modal('show');
    $('#new-chapter-link #chapter-id').val($(this).attr('data-id'));
    $('#new-chapter-link #link-chapter').val($(this).attr('data-link'));
});
$('#save-link-chapter').click(function (e) {
    e.preventDefault();
    let id = $('#new-chapter-link #chapter-id').val();
    let link = $('#link-chapter').val();
    if(id && link) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/updateChapterLink',
            data: {id: id, link: link},
            success: function (data) {
                $('#new-chapter-link').modal('hide');
                location.reload();
            }
        });
    }
})
$(document).on('click', ".uploadefilestudent",function(e){
    let idformation = $('#imageformationid').val();
    let pictureofformation = $('#imageformationbanner').val();
    if(idformation && pictureofformation) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/updatefilepicture',
            data: {idformation: idformation, pictureofformation: pictureofformation},
            success: function (data) {
                console.log(window.location.origin+'/fr/formation/updatefilepicture');
            }
        });
    }
})
$(document).on('click', ".uploadefilebannercour",function(e){
    let idcours = $('#courid').val();
    let pictureoffcourbanner = $('#imagecoursbanner ').val();
    if(idcours && pictureoffcourbanner) {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/updatefilepicturecours',
            data: {idcours: idcours, pictureoffcourbanner: pictureoffcourbanner},
            success: function (data) {
                console.log(window.location.origin+'/fr/formation/updatefilepicture');
            }
        });
    }
})
$('.chapter-content').click(function () {
    let link = $(this).attr('data-link');
    let idchapter=$(this).attr("data-id");
    let heureforbase=$(this).attr("data-hours");
    let heurefinalevalidet=$(this).attr("data-hoursfianly");
    let heurefinalevalidebyformateur=$(this).attr("data-hours-validate");
    let idcoursremovehidden=$(this).attr("data-cours-ids");
    let idmodule=$(this).attr("data-id-module");
    let idformationrecuper=$(this).attr("data-id-formation");
     let statutsbuttoncompled=$(this).attr("data-active-button");
     console.log(statutsbuttoncompled);
     if (statutsbuttoncompled==1){
$("#clinbutton").removeClass("hidden");
     }

    $('#formationstudentformation1').append("<div class='cacheinutformation'>" + idformationrecuper + "</div> ");
    $('#showformationstudents').append("<div class='cacheinutformation'>" + idformationrecuper + "</div> ");
     //$(".d-sm-flex").append("<span class='customnamechaiptreheader'>"+'| ' +name+ "</span>");
    $('#formationstudentformation1').html('<iframe src="'+link+'" class="chapter-iframe"></iframe>');
    $('#formationstudentformation1').css('padding','0');
    $('#formationstudentformation1').parent().css('margin','-1.5rem 0rem 0 -3rem');
    $('#formationstudentformation1').append("<div class='cachetimeinput' id='timesrecuper'>"+heureforbase+ "</div>");
    $('#formationstudentformation1').append("<div class='cacheinut'>" + idchapter + "</div> ");
    $('#formationstudentformation1').append("<div id='tempsterminate'>" + heurefinalevalidet + "</div> ");
    $('#formationstudentformation1').append("<div id='tempsterminatebyforma'>" + heurefinalevalidebyformateur + "</div> ");
    $('#showformationstudents').html('<iframe src="'+link+'" class="chapter-iframe"></iframe>');
    $('#showformationstudents').css('padding','0');
    $('#showformationstudents').parent().css('margin','-1.5rem 0rem 0 -3rem');
    $('#showformationstudents').append("<div class='cachetimeinput' id='timesrecuper'>"+heureforbase+ "</div>");
    $('#showformationstudents').append("<div class='cacheinut'>" + idchapter + "</div> ");
    $('#showformationstudents').append("<div id='tempsterminate'>" + heurefinalevalidet + "</div> ");
    $('#showformationstudents').append("<div id='tempsterminatebyforma'>" + heurefinalevalidebyformateur + "</div> ");
    $('body').css('overflow','hidden');
    $('.pop-up-note').removeClass('hidden');
    $('.pop-up-status').removeClass('hidden');
    $("[ data-id-module="+idmodule+"]").removeClass('hidden');
    $("[ data-id-cours="+idcoursremovehidden+"]").removeClass('hidden');
    $(window).on('beforeunload', function(){
        var tempsfinale=$("#tempsterminate").text();
        var divContent = $("#timesrecuper").text();
        var idchapter=$('.cacheinut').text();
        var tempvalidebyformateur=$('#tempsterminatebyforma').text();
        var idformation_recuper=$('.cacheinutformation').text();
        $.ajax({
            type: "POST",
            url:  window.location.origin+'/hourschapterbyuser',
            data: { idchapter: idchapter,hours:divContent,tempsfinale:tempsfinale,tempsvalid:tempvalidebyformateur,idformation:idformation_recuper },
            success: function (response) {

            },
        });
    })
    window.scrollTo(0, 0);
    $(function() {
        var timer, idle = false;
        $(window).on({
            blur: pause,
            focus:somethingHappened
        })
        $(document).on({
            mousemove: somethingHappened,
            click: somethingHappened,
            touchstart: somethingHappened
        })

        var start = new Date,
            bad = 0


        somethingHappened()

        adjust($("#timesrecuper").text())

        setInterval(function() {
            $("#timesrecuper").text(formatMilli(new Date - start - bad))
            if (idle) bad += 1000
        }, 1000)


        function pause(){
            idle=true
            var tempsfinale=$("#tempsterminate").text();
            var divContent = $("#timesrecuper").text();
            var idchapter=$('.cacheinut').text();
            var tempvalidebyformateur=$('#tempsterminatebyforma').text();
            var idformation_recuper=$('.cacheinutformation').text();
            $.ajax({
                type: "POST",
                url:  window.location.origin+'/hourschapterbyuser',
                data: { idchapter: idchapter,hours:divContent,tempsfinale:tempsfinale,tempsvalid:tempvalidebyformateur,idformation:idformation_recuper },
                success: function (response) {

                },
            });
        }
        function somethingHappened() {
            idle = false
            clearTimeout(timer)
            timer = setTimeout(function() {
                idle = true
                var tempsfinale=$("#tempsterminate").text();
                var divContent = $("#timesrecuper").text();
                var idchapter=$('.cacheinut').text();
                var tempvalidebyformateur=$('#tempsterminatebyforma').text();
                var idformation_recuper=$('.cacheinutformation').text();
                $.ajax({
                    type: "POST",
                    url:  window.location.origin+'/hourschapterbyuser',
                    data: { idchapter: idchapter,hours:divContent,tempsfinale:tempsfinale,tempsvalid:tempvalidebyformateur,idformation:idformation_recuper },
                    success: function (response) {

                    },
                });
            }, 300000)
        }
        function adjust(time) {
            const t = time.split(/:/).map(Number).reverse()
            if (t[0]) start.setSeconds(start.getSeconds() - t[0]);
            if (t[1]) start.setMinutes(start.getMinutes() - t[1]);
            if (t[2]) start.setHours(start.getHours() - t[2]);
        }
    })
    function formatMilli(milli) {
        aDate = new Date;
        aDate.setHours(0);
        aDate.setMinutes(0);
        aDate.setSeconds(0);
        aDate.setMilliseconds(milli);
        return aDate.toLocaleTimeString("en-gb", {
            hour: "numeric",
            minute: "2-digit",
            second: "2-digit"
        })
    }
})
$(document).ready(function() {
    if($(".select2customjsspecifclass").length > 0) {
        $(".select2customjsspecifclass").select2({
            dropdownCssClass: "beforeslectfrancais",

        });
    }
    if( $(".js-select2").length > 0) {
        $(".js-select2").select2({
            closeOnSelect : false,
            allowClear: true,
            tags: true,
            dropdownCssClass: "beforecheckbox"
        });
    }

    if($(".select2customjs").length > 0) {
        $(".select2customjs").select2({ minimumResultsForSearch: -1});
    }
    if($(".select-filter").length > 0) {
        $(".select-filter").select2({ minimumResultsForSearch: Infinity,

        });
    }
});
$(document).on('click', "#userDropdown,.dropdown-menu ",function(e) {

    $(".dropdown-menu").toggleClass("show");

});
$(document).on('click', "#langue .select2-container ",function(e) {
    console.log("cc");
    $(".dropdown-menu").toggleClass("shows");

});

$('#delete-all-notes').click(function() {
    //get all selected notes
    var notes = [];
    $.each($("input[name='note-checks']:checked"), function() {
        notes.push($(this).val());
    });
    var path = $(this).attr("data-path");
    $.ajax({
        type: "POST",
        url: path,
        data: { notes: notes },
        success: function(data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
})
$('#reset-note').click(function () {
    $("#update-form").trigger("reset");
})

$('#affect-list-user').click(function () {
    let selectedFormation = [];
    let listClass = [];
    let checkedUsers = [];
    $.each($("#formationS #select-formation > option:selected"), function() {
        selectedFormation.push($(this).val());
    });
    $.each($("#classe #select-class > option:selected"), function() {
        listClass.push(parseInt($(this).val()));
    });

    $.each($(".list-user input[id^='user-id-']:checked"), function() {
        checkedUsers.push($(this).val());
    });
    if(checkedUsers.length > 0) {
        if(listClass.length > 0) {
            let pathClass = $('#path-class').val();
            $.ajax({
                type: "POST",
                url: pathClass,
                data: { userIds: checkedUsers, selectedClass: listClass},
                success: function(data) {
                    if (data == "success") {
                        //location.reload();
                    }
                }
            });
        }

        if(selectedFormation.length > 0) {
            let pathFormation = $('#path-formation').val();
           $.ajax({
                type: "POST",
                url: pathFormation,
                data: { userIds: checkedUsers, selectedFormation: selectedFormation },
                success: function(data) {
                    if (data == "success") {

                    }
                }
            });
        }
    }
    location.reload();
})
$('#del-file').click(function (e) {
    e.preventDefault();
    var id = $('.delete-image').attr('data-id');
    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: window.location.origin+'/deleteImage',
        success: function (data) {
            location.reload();
        }
    });
});
/*
$("#search-bar-lib").on('keyup', function(e) { // everytime keyup event
    e.preventDefault();
    var input = $(this).val();// We take the input value
    var $search = $('#search-bar-lib');

    $.ajax({
        type: "GET",
        url: '/admin/contentLibrary',
        data: $search.serialize(),
        cache: false,
        success: function(response) {

            $('.search-media').replaceWith(response);
            console.log(response);
        },
        error: function(response) {
            console.log(response);
        }
    });
});*/
function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search-bar-lib");
    filter = input.value.toUpperCase();
    console.log(filter);
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("div")[0];
        txtValue = a.textContent || a.innerHTML;
        console.log(txtValue);
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
$('.btnpop-bib').click(function () {
    $('#affect-file-folder').modal();
});
$(document).ready(function() {
    $(".js-select2").select2({
        closeOnSelect : false,
        allowClear: true,
        tags: true,
        dropdownCssClass: "beforecheckbox"

    });

    if($(".select2customjs").length > 0) {
        $(".select2customjs").select2({ minimumResultsForSearch: -1});
    }

    if($(".select-filter").length > 0) {
        $(".select-filter").select2({ minimumResultsForSearch: Infinity});
    }
});
const nav = document.querySelector('#nav');
const menu = document.querySelector('#menu');
const menuToggle = document.querySelector('.nav__toggle');
let isMenuOpen = false;
function searchNote() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("search-bar-note");
    filter = input.value.toUpperCase();
    table = document.getElementById("note-tab");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            console.log(td)
        }
    }
}
$(document).ready(function() {
    $("#uploaderbaformation #file").change(function () {
        var fd = new FormData();
        var files = $('#uploaderbaformation  #file')[0].files[0];
        $('#uploaderbaformation .value-file').val(files.name);
        fd.append('file', files);
        var dataType = $('#uploaderbaformation').attr("data-type");
        if(dataType == 'image') {
            if($('#imageformationbanner').length > 0) {
                $('#imageformationbanner').val(files.name);
                $('.image-formformation img').attr('src', '/images/formations/' + files.name);
            }
            if($('#imagecoursbanner').length > 0) {
                $('#imagecoursbanner ').val(files.name);
                console.log("cc");
                $('.formation-image-forms img').attr('src','/images/formations/' + files.name);
            }
        }
        uploaderfilebanner(fd);

    });
});
function uploaderfilebanner(file) {

    $.ajax({
        type: "POST",
        url: window.location.origin+'/uploderforformation',
        data: file,
        contentType: false,
        processData: false,
        success: function(response){

        },
    });
}

if(menuToggle!= null) {
    $('.visiblemobile').parent().addClass('mod-mobile')
    // TOGGLE MENU ACTIVE STATE
    menuToggle.addEventListener('click', e => {
        e.preventDefault();
        isMenuOpen = !isMenuOpen;

        // toggle a11y attributes and active class
        menuToggle.setAttribute('aria-expanded', String(isMenuOpen));
        menu.hidden = !isMenuOpen;
        nav.classList.toggle('nav--open');
        if(isMenuOpen) {
            $('.visiblemobile.nav--open').parent().css('height','100vh');
        } else {
            $('.visiblemobile').parent().css('height','initial');
        }
    })

    /*nav.addEventListener('keydown', e => {
        if (!isMenuOpen || e.ctrlKey || e.metaKey || e.altKey) {
            return;
        }
        const menuLinks = menu.querySelectorAll('.nav__link');
        if (e.keyCode === 9) {
            if (e.shiftKey) {
                if (document.activeElement === menuLinks[0]) {
                    menuToggle.focus();
                    e.preventDefault();
                }
            } else if (document.activeElement === menuToggle) {
                menuLinks[0].focus();
                e.preventDefault();
            }
        }
    });*/
}

var sendmessagedasht=[];
var nameofdatshbor={};
$(document).on('click', "#sendmessagedatsh",function(e){
    var messagename=$("#sendsubject").val();
    var textmessage=$("#messagemail").val();

    if(messagename!="" && textmessage!=""){
        $(".visibiltysutdent").addClass("noshowss");
        $(".visibiltyafter").removeClass("hidden");
        $(".visibiltysutdent").delay(4000).fadeIn("slow").show(0);
        $(".visibiltyafter").delay(4000).fadeOut().hide(0);

        $("#sendsubject").val("");
        $("#messagemail").val("");

        nameofdatshbor = {
            "titlename": messagename,
            "message": textmessage,

        }
        sendmessagedasht.push(nameofdatshbor);
        $.ajax({
            type: "POST",
            url:  window.location.origin+'/sendmailfromstudent',
            data: { sendmessagefromdatshbord: sendmessagedasht},
            success: function (response) {

            },
        });

    }

});

$('#nav-sel').click(function() {
    $(this).parent().toggleClass('activeremove');
    //$('#contentsed').toggle();
});
$('.btn-toogel-div').click(function() {

    if($(this).hasClass('open-btn')) {
        $(this).removeClass('open-btn');
        $('.pop-up-status .separed-element').fadeOut(700);
    } else {
        $(this).addClass('open-btn') ;
        $('.pop-up-status .separed-element').fadeIn(700);
    }

});

$('.add-note-chap .separed-element').click(function () {

    if (CKEDITOR.instances.editor) CKEDITOR.instances.editor.destroy();
    CKEDITOR.replace('editor', {
        skin: 'moono',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode:CKEDITOR.ENTER_P,
        toolbar: [{ name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
            { name: 'basicstyles', groups: [ 'basicstyles' ], items: [ 'Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor' ] },
            { name: 'scripts', items: [ 'Subscript', 'Superscript' ] },
            { name: 'justify', groups: [ 'blocks', 'align' ], items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
            { name: 'paragraph', groups: [ 'list', 'indent' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'] },
            { name: 'links', items: [ 'Link', 'Unlink' ] },
            { name: 'spell', items: [ 'jQuerySpellChecker' ] },
        ],
        toolbarLocation: 'bottom',
        uiColor: '#ffffff ',
    });
    // CKEDITOR.instances.editor.setData(textContent);
    $('#add-note').modal('show');
})
$('#add-note-chapter').click(function () {
    let title    = $('#form_title').val();
    let folder   = $('#select2folder').val();
    console.log(folder);
    let content  = CKEDITOR.instances.editor.getData();
    let idNote = $('#idnote').val();
    console.log(idNote);
    if(title.length > 0) {
        //save the note
        var path = $(this).attr("data-path");
        $.ajax({
            type: "POST",
            url: path,
            data: { title: title, folder:parseInt(folder),content:content, idNote: idNote },
            success: function(data) {
                $('#add-note').modal('hide');
            }
        });
    }
})
$('#addNewNote').click(function () {
    $('.get-note').removeClass('d-none');
    $('#idnote').val('');
    $('#form_title').val('');
    CKEDITOR.instances.editor.setData('');
    $("#select2folder option").prop("selected", false).change();
    $("#select2note option").prop("selected", false).change();
    $('#add-note #add-note-chapter').removeClass('d-none');
});
$('#select2note').change(function() {
    let selectedNote = $(this).val();
    if(selectedNote) {
        //get the selected note
        $('#idnote').val(selectedNote);
        var path = $(this).attr("data-path");
        $.ajax({
            type: "POST",
            url: path,
            data: { noteId: selectedNote},
            success: function(data) {
                let res = JSON.parse(data);
                $('#form_title').val(res.title);
                CKEDITOR.instances.editor.setData(res.value);
                if(res.folder) {
                    $("#select2folder option[value="+res.folder+"]").prop("selected", "selected").change()
                }
                $('.get-note').removeClass('d-none');
            }
        });
    }
})

$('#uploaderbaformation #save-file').click(function () {

    $('#uploaderbaformation').modal('hide');
});
$('#uploaderbaformation').click(function () {
    $('#uploaderbaformation').attr('data-type', 'image');

});
$(".chapterpopup").click(function () {
    let idchapter = $(this).attr("data-ids");
    let namechapter = $(this).attr("data-name");
    let data_hours=$(this).attr("data-hours");
    let data_minute=$(this).attr("data-minute");
    $("#valueheureconfigchaiptre").val(data_hours);
    $("#valueminconfigchaiptre").val(data_minute);
    $(" #modal-title").empty();
    $(" #modal-title").append("PARAMÈTRES " + namechapter);
    $("#idchapter").val(idchapter);

});
$("#valueheureconfigchaiptre,#valueminconfigchaiptre").bind("click keydown keyup change ", function () {
    if($("#valueheureconfigchaiptre").val()==""){
        var getheurechaiptre = $("#valueheureconfigchaiptre").val("00");
    }
    else {
        var getheurechaiptre = $("#valueheureconfigchaiptre").val();
    }
    if($("#valueminconfigchaiptre").val()==""){
        var getminchaiptre = $("#valueminconfigchaiptre").val("00");
    }
    else {
        var getminchaiptre = $("#valueminconfigchaiptre").val();
    }
    $("#time_spent").val(getheurechaiptre + ":" + getminchaiptre + ":00");

});
$("#getheurefinchaiptre,#getminfinchaiptre").bind("click keydown keyup change ", function () {

    if($("#getheurefinchaiptre").val()==""){
        var getheurefinchaiptre = $("#getheurefinchaiptre").val("00");
    }
    else {
        var getheurefinchaiptre = $("#valueheureconfigchaiptre").val();
    }
    if($("#getminfinchaiptre").val()==""){
        var getminfinchaiptre = $("#getminfinchaiptre").val("00");
    }
    else {
        var getminfinchaiptre = $("#getminfinchaiptre").val();
    }
    $("#heurechapterfin").val(getheurefinchaiptre + ":" + getminfinchaiptre + ":00");
});
var sendconfigchapter=[];
var nameofconfigchapter={};
$(document).on('click', ".btnchapterpopup", function (e) {
    var getidmodulefrompup = $("#idchapter").val();

    var status_completed = 0;
    var score_chaiptre = 0;
    var statut_accesss = 0;
    var staut_subscribe = 0;
    var staut_prerequis = 0;
    if ($('#status_completed').is(':checked')) {
        status_completed = 1;
    }
    if ($('#score_chaiptre').is(':checked')) {
        score_chaiptre = 1;
    }
    if ($('#statut_accesss').is(':checked')) {
        statut_accesss = 1;
    }
    if ($('#staut_subscribe').is(':checked')) {
        staut_subscribe = 1;
    }
    if ($('#staut_prerequis').is(':checked')) {
        staut_prerequis = 1;
    }
    var picture = $("#picture").val();
    if($("#time_spent").val()!=""){
        var time_spent = $("#time_spent").val();
        var gethours_poup=$("#valueheureconfigchaiptre").val();
        var getminute_poup=$("#valueminconfigchaiptre").val()
        $("[ data-ids="+getidmodulefrompup+"]").attr("data-hours",gethours_poup);
        $("[ data-ids="+getidmodulefrompup+"]").attr("data-minute",getminute_poup);

    }

    var score_point = $("#score_point").val();
    if($("#status_dateacces").val()!="") {
        var status_dateacces = $("#status_dateacces").val();
    }
    if($("#staut_datesubscribe").val()!="") {
        var staut_datesubscribe = $("#staut_datesubscribe").val();
    }
    var choice_chaiptre = $("#choice-chaiptre").val();
    if($("#heurechapterfin").val()!=""){
        var heurechapterfin = $("#heurechapterfin").val();
    }
    nameofconfigchapter = {
        "idchapter": getidmodulefrompup,
        "picture": picture,
        "time_spent": time_spent,
        "status_completed": status_completed,
        "score_chaiptre": score_chaiptre,
        "score_point": score_point,
        "statut_accesss": statut_accesss,
        "status_dateacces": status_dateacces,
        "staut_subscribe": staut_subscribe,
        "staut_datesubscribe": staut_datesubscribe,
        "staut_prerequis": staut_prerequis,
        "choice_chaiptre": choice_chaiptre,
        "status_completed": status_completed,
        "score_chaiptre": score_chaiptre,
        "statut_accesss": statut_accesss,
        "heurechapterfin": heurechapterfin,
    }
    sendconfigchapter.push(nameofconfigchapter);
    $("#btnenvoyemodulechaiptre").modal('hide');
    $.ajax({
        type: "POST",
        url:  window.location.origin+'/configchaiptre',
        data: { configchapter: sendconfigchapter},
        success: function (response) {
            location.reload();
        },
    });
  $("#valueheureconfigchaiptre").val("");
    $("#valueminconfigchaiptre").val("");
    $("#score_point").val("");
    $("#datetpickets").val("");
    $("#staut_datesubscribe").val("");
    $("#getheurefinchaiptre").val("");
    $("#getminfinchaiptre").val("");


});


if($('#canvas4').length > 0) {
    $ (".chapter-content" ).click(function() {

        var canvas4 = document.getElementById('canvas4');

        canvas4.addEventListener('circleProgressBar.afterDraw', function (e) {
            var span = document.getElementById('canvas-data4');
            span.innerText = (e.detail.self.getValue() * 100).toFixed(0);
        }, false);
        canvas4.addEventListener('circleProgressBar.afterFrameDraw', function (e) {
            var span = document.getElementById('canvas-data4');

            var currentValue = (e.detail.self.getValue() * e.detail.progress * 100).toFixed(0);
            var currentSpanValue = span.innerText;
            if (currentSpanValue != currentValue) {
                span.innerText = currentValue;
            }
        }, false);
        var colors = ['#ffffff'];
        var circleProgressBar4 = new CircleProgressBar(canvas4, {colors: colors, trackLineColor: '#99b197'});
        var pourecentageprogress=$(this).attr("data-result");

        circleProgressBar4.setValue(pourecentageprogress);
    });

}

function myFunctionserach() {
    var input, filter, divparent, children, a, i, txtValue;
    input = document.getElementById("search-bar");
    filter = input.value.toUpperCase();

    divparent = document.getElementById("formationsearchparent");
    children = divparent.getElementsByTagName("li");
    for (i = 0; i < children.length; i++) {
        a = children[i].getElementsByTagName("div")[0];
        txtValue = a.textContent || a.innerHTML;

        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            children[i].style.display = "";
        } else {
            children[i].style.display = "none";
        }
    }
}

$(document).on('keyup', '#valueheureconfigchaiptre,#valueminconfigchaiptre,#getminfinchaiptre,#getheurefinchaiptre ', function () {
    var _this = $(this);
    var min = parseInt(_this.attr('min')) ;
    var max = parseInt(_this.attr('max')) ;
    var val = parseInt(_this.val()) || (min - 1);
    if(val < min)
        _this.val( min );
    if(val > max)
        _this.val( max );
});

$('#getprogressbar.Disabled').click(function() { return false; });
$('.body-progress .Disabled').click(function() { return false; });

if( $(".selectlanguahechoice").length > 0) {
   /* $(".selectlanguahechoice").suploderforformationelect2({
        "language": {
            "noResults": function(){
                return "Resultat non disponible";
            }
        },
    });*/
}
if($('#categ').length > 0) {
    $(document).on('change', "#categ", function (e) {
        var categorielists =  $('#categoryslist').val();
        console.log(categorielists);
        if (categorielists == "tout") {
            $(".listeeFormation").removeClass("hidden");
        }
    });
    $('#categoryslist').append($('<option>', {
        value: 'tout',
        text: 'Tout'
    }));
}
if($('#statusFilter').length > 0) {
    $(document).on('change', "#stat", function (e) {
        var filter_staut =  $('#statusFilter').val();
        const divs = document.querySelectorAll('.listeeFormation');
        divs.forEach(div => {
            div.classList.remove('hidden');
        });
        var arrcategorysd = new Array();
        arrcategorysd.push(filter_staut);
        document.querySelectorAll('.listeeFormation').forEach(div => {
            const className= arrcategorysd.includes(div.dataset['result']) ? "show" : "hidden";
            div.classList.add(className);
        });
    });
}

/** classes **/
$('#delete-listClasses').click(function () {
    //get all selected badges
    var classes = [];
    $.each($("input[name='classe-check']:checked"), function () {
        classes.push($(this).attr('data-id'));
    });
    console.log(classes);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: window.location.origin+'/deleteClasses',
        data: {classes: classes},
        success: function (data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
$('.delete-class').click(function (e) {
    $('.delete-class').removeClass('active');
    $(this).addClass('active');
})
$('#delete-selectedClass').click(function (e) {
    e.preventDefault();
    var id = $('.delete-class.active').attr('data-id');
    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: window.location.origin+'/deleteClasse',
        success: function (data) {
            location.reload();
        }
    });
});
$('.duplicate-classes').click(function () {
    //get all selected classes
    var classes = [];
    $.each($("input[name='classe-check']:checked"), function () {
        classes.push($(this).attr('data-id'));
    });
    console.log(classes);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: window.location.origin+'/duplicateClasses',
        data: {classes: classes},
        success: function (data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
