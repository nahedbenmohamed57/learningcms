/*var to_send ={};
*/
var editForm = function () {
    this.nodeId = null;
};

editForm.prototype.init = function (obj) {
    var that = this;
    this.obj = obj;
    this.editForm = document.getElementById("editForm");
    this.nameInput = document.getElementById("name");
    this.titleInput = document.getElementById("title");
    this.cancelButton = document.getElementById("cancel");
    this.saveButton = document.getElementById("save");

    this.cancelButton.addEventListener("click", function () {
        that.hide();
    });

    this.saveButton.addEventListener("click", function () {
        var node = chart.get(that.nodeId);
        node.name = that.nameInput.value;
        chart.updateNode(node);
        that.hide();
        $( "div[data-id^='"+node.id+"'] h6" ).text( node.name);
        if(moduleconfig_forms) {
            moduleconfig_forms.forEach(function(obj) {
                if(obj.id == node.id) {
                    obj.name = node.name;
                    var namechaiptres=node.name;
                    $("#pictureselect").val(namechaiptres);
                    $("#modal-title").empty();
                    $("#modal-title").append("PARAMÈTRES  " +''+node.name );
                } else {
                    if(obj.child) {
                        //cour
                        obj.child.forEach(function(objChild) {
                            if(objChild.id == node.id) {
                                var namechaiptres=node.name;
                                $("#pictureselect").val(namechaiptres);
                                objChild.name = node.name;
                                $("#courspopup #modal-title").empty();
                                $("#courspopup #modal-title").append("PARAMÈTRES  " +''+node.name );

                            } else if (objChild.child) {
                                //niveau 3
                                objChild.child.forEach(function(finalChild) {
                                    if(finalChild.id == node.id) {
                                        finalChild.name = node.name;
                                        var namechaiptres = node.name;
                                        $("#pictureselect").val(namechaiptres);
                                        $("#chaiptreconfig #modal-title").empty();
                                        $("#chaiptreconfig #modal-title").append("PARAMÈTRES  " +''+node.name );
                                    } else if(finalChild.child) {
                                        //change quiz name
                                        finalChild.child.forEach(function(finalChild2) {
                                            if(finalChild2.id == node.id) {
                                                finalChild2.name = node.name;
                                            }
                                        })
                                    }

                                });

                            }
                        })
                    }
                }
            });
        }
    });
};

editForm.prototype.show = function (nodeId) {
    this.nodeId = nodeId;
    if(this.nodeId != 1) {
        var currentNodeOffset = document.querySelector('[node-id="' + nodeId + '"]').getBoundingClientRect();
        this.editForm.style.top = currentNodeOffset.top + "px";
        //var left = document.body.offsetWidth / 2 - 150;
        this.editForm.style.display = "block";
        this.editForm.style.left = currentNodeOffset.left + "px";
        this.editForm.style.width = currentNodeOffset.width + "px";
        this.editForm.style.height = currentNodeOffset.height + "px";
        var node = chart.get(nodeId);
        this.nameInput.value = node.name;
    }
};

editForm.prototype.hide = function (showldUpdateTheNode) {
    this.editForm.style.display = "none";
};

/*var chart = new OrgChart(document.getElementById("tree"), {
    mouseScrool: OrgChart.action.none,
    nodeMenu:{
        edit: {text:"Edit"},
        add: {text:"Add"},
        remove: {text:"Remove"}
    },
    editUI: new editForm(),
    nodeBinding: {
        field_0: "name",
        field_1: "title"
    },
    nodes: [
        { id: 1, name: "Amber McKenzie", title: "CEO" },
        { id: 2, pid: 1, name: "Ava Field", title: "IT Manager" },
        { id: 3, pid: 1, name: "Peter Stevens", title: "HR Manager" }
    ]
});

 */


var module = {};
var data = [];
var nameofmodule={};
var moduleconfig_forms =  [];
var deletedElments = [];
var chart = new OrgChart(document.getElementById("tree"), {
    template: "ula",
    enableSearch: false,
    mouseScrool: OrgChart.action.none,
    align: OrgChart.ORIENTATION,
    enableDragDrop: true,
    //nodeMouseClick: OrgChart.action.none,
    editUI: new editForm(),
    nodeBinding: {
        field_0: "name",
        edit: 'id',
        remove: 'id',
        pdf:'id'
    },
    tags: {
        "assistant": {
            template: "ula"
        }
    },
    toolbar:{
        zoom:true
    },
    menu: {
        pdf: { text: "Export PDF" }
    }

});
function functioncharteformation() {

    var currentNode = '';
    var clientXY = [0,0];
    var nameformation_recup= document.getElementById('getnameformation').value;
    OrgChart.templates.ula.size = [150, 55];
    OrgChart.templates.ula.pointer = '<g data-pointer="pointer"></g>';
    OrgChart.templates.ula.node = '<rect x="0" y="0" height="55" width="150" fill="#039BE5" stroke-width="1" stroke="#aeaeae" rx="5" ry="5"></rect>';
    OrgChart.templates.ula.field_0 = '<text width="150" text-overflow="ellipsis" class="field_0"  style="font-size: 15px;" fill="#ffffff" x="75" y="35" text-anchor="middle">{val}</text>';
    OrgChart.templates.ula.remove = '<g data-remove-id="{val}" onclick="removeCurrentNode(this)" transform="matrix(1,0,0,1,5,3)"><rect x="0" y="0" width="16" height="16" fill="#039BE5"></rect>' + OrgChart.icon.remove(15,15, '#fff') + '</g>';
    OrgChart.templates.ula.edit = '<g data-edit-id="{val}" transform="matrix(1,0,0,1,130,5)"><rect x="0" y="0" width="16" height="16" fill="#039BE5"></rect>' + OrgChart.icon.edit(16,16, '#fff') + '</g>';
    OrgChart.templates.ula.pdf = '<g data-pdf-id="{val}" transform="matrix(1,0,0,1,5,3)"><rect x="0" y="0" width="20" height="20" fill="#039BE5"></rect>' +OrgChart.icon.pdf(24,24, '#fff') +  '</g>';

    chart.on('redraw', function(){
        var editBtns = document.querySelectorAll('[data-edit-id]');

        var ExpodrtBtn = document.querySelectorAll('[ control-node-menu-id]');
        for(var i = 0; i < ExpodrtBtn.length; i++){
            ExpodrtBtn[i].style.display = "none";
        }
        for(var i = 0; i < editBtns.length; i++){
            editBtns[i].addEventListener('click',  function(){
                chart.editUI.show(this.getAttribute("data-edit-id"));
                document.querySelector('#editForm').setAttribute('class','')
            });
        }
        var removeBtns = document.querySelectorAll('[data-remove-id]');

        for(let i = 0; i < removeBtns.length; i++){
            removeBtns[i].addEventListener('click',  function(){

            });
        }

        var butonpopup = document.querySelectorAll('[data-pdf-id]');

        for(var i = 0; i < butonpopup.length; i++){
            butonpopup[i].addEventListener('click',  function(){
                document.querySelector('#editForm').setAttribute('class','d-none')
                var substrid=$(this).attr('data-pdf-id');

                substrid = substrid.replace(/[0-9]/g, '');
                // $("#moduleconfig"+$(this).attr('data-pdf-id')).modal('show');

                if(substrid=="itemdat-"){
                    var iddatapdf=$(this).attr('data-pdf-id');

                    $("#modal-title").empty();
                    var nameModuless=document.querySelector('svg [node-id='+iddatapdf+']').
                    querySelector('text').textContent;
                    $("#modal-title").append("PARAMÈTRES  " +''+nameModuless );
                    $("#moduleconfigitemdat").modal('show');
                    $("#valueofdat").val($(this).attr('data-pdf-id'));
                    $("#chaiptreconfig").modal('hide');
                    $("#courspopup").modal('hide');
                    $("#quizs").modal('hide');
                    $("#pictureselect").val(nameModuless);

                }
                if(substrid=="itemcour-"){
                    $("#courspopup #modal-title").empty();
                    var iddatapdf=$(this).attr('data-pdf-id');
                    var namecourssvg=document.querySelector('svg [node-id='+iddatapdf+']').
                    querySelector('text').textContent;
                    $("#pictureselect").val(namecourssvg);
                    $("#courspopup #modal-title").append("PARAMÈTRES  " +''+namecourssvg );
                    $("#courspopup").modal('show');
                    $("#valueofdat").val($(this).attr('data-pdf-id'));
                    $("#moduleconfigitemdat").modal('hide');
                    $("#chaiptreconfig").modal('hide');
                    $("#quizs").modal('hide');
                }
                if(substrid=="itemshap-"){
                    $("#chaiptreconfig #modal-title").empty();
                    var iddatapdf=$(this).attr('data-pdf-id');
                    var namechaptersvg=document.querySelector('svg [node-id='+iddatapdf+']').
                    querySelector('text').textContent;
                    $("#chaiptreconfig #modal-title").append("PARAMÈTRES  " +''+namechaptersvg );
                    $("#chaiptreconfig").modal('show');
                    $("#moduleconfigitemdat").modal('hide');
                    $("#valueofdat").val($(this).attr('data-pdf-id'));
                    $("#courspopup").modal('hide');
                    $("#quizs").modal('hide');
                    $("#pictureselect").val(namechaptersvg);
                }
                if(substrid=="itemsqiz-"){
                    $("#chaiptreconfig").modal('hide');
                    $("#moduleconfigitemdat").modal('hide');
                    $("#valueofdat").val($(this).attr('data-pdf-id'));
                    $("#courspopup").modal('hide');
                    $("#quizs").modal('show');
                }
            });


        }
        var nodeElements = document.querySelectorAll('[node-id]');
        var idsElements = [];
        var status = true;


        for(var i = 0; i < nodeElements.length; i++){
            var nodeElement = nodeElements[i];
            nodeElement.ondrop = function(ev) {
                ev.preventDefault();
                var id = ev.dataTransfer.getData("id");

                var dbid = ev.dataTransfer.getData("db-id");

                idsElements.push(id);
                var itemss = document.querySelector('[data-id="' + id + '"]');
                var name = itemss.innerText;


                //var module_popup = $("#moduleconfig").clone();
                //$("#moduleconfig").after($(module_popup).attr('id', "moduleconfig"+id+"01"));

                var nodeElement = ev.target;
                while(!nodeElement.hasAttribute('node-id')){
                    nodeElement = nodeElement.parentNode;
                }
                var pid = nodeElement.getAttribute('node-id');
                module.data = data;
                var namodule = name;

                var str_name = ev.dataTransfer.getData("id");
                var str_type = str_name.replace(/[0-9]/g, '');

                if(str_type.includes("itemdat-")){
                    str_type="module";
                    let lastId = $('#list-module .blocgloab:last-child .item').attr('data-id');
                    if(lastId != undefined) {
                        let mytab = lastId.split('-');
                        let getLastChapter = parseInt(mytab[mytab.length - 1])+1;
                        itemss.setAttribute("data-id", "itemdat-"+getLastChapter);
                    }
                }
                if(str_type.includes("itemcour-")){
                    str_type="Cours";
                    let lastId = $('#list-cour .blocgloab:last-child .item').attr('data-id');
                    if(lastId != undefined) {
                        let mytab = lastId.split('-');
                        let getLastChapter = parseInt(mytab[mytab.length - 1])+1;
                        itemss.setAttribute("data-id", "itemcour-"+getLastChapter);
                    }
                }
                if(str_type.includes("itemshap-")){
                    str_type="Chap";
                    let lastId = $('#list-chap .blocgloab:last-child .item').attr('data-id');
                    if(lastId != undefined) {
                        let mytab = lastId.split('-');
                        let getLastChapter = parseInt(mytab[mytab.length - 1])+1;
                        itemss.setAttribute("data-id", "itemshap-"+getLastChapter);
                    }
                }
                if(str_type.includes("itemsqiz-")){
                    str_type="quiz";
                    let lastId = $('#list-quiz .blocgloab:last-child .item').attr('data-id');
                    if(lastId != undefined) {
                        let mytab = lastId.split('-');
                        let getLastChapter = parseInt(mytab[mytab.length - 1])+1;
                        itemss.setAttribute("data-id", "itemsqiz-"+getLastChapter);
                    }
                    namechaiptressvg=itemss.getAttribute("data-name");
                    document.getElementById('pictureselect').value=namechaiptressvg;
                }

                nameofmodule = {
                    "id": id,
                    "name": namodule,
                    "pid": pid,
                    "type":str_type,
                    "child" : []
                }
                status = true;

                if (
                    (nameofmodule['type'] == "module" && nameofmodule['pid'] != 1) ||
                    (nameofmodule['type'] == "Cours" && nameofmodule['pid'].includes("shap")) ||
                    (nameofmodule['type'] == "Chap" && nameofmodule['pid'].includes("shap")) ||
                    (nameofmodule['type'] == "Cours" && nameofmodule['pid'].includes("cour")) ||
                    (nameofmodule['type'] == "Cours" && nameofmodule['pid'].includes("itemsqiz")) ||
                    (nameofmodule['type'] == "module" && nameofmodule['pid'].includes("itemsqiz"))
                ) {
                   // alert("Vous ne repectez pas l'ordre de l'hiérarchi");
                    $("#alert-formation").modal('show');
                    status = false;
                }
                if(status) {
                    itemss.querySelector("input").value++;
                    var x = document.getElementsByClassName('blocgloab');
                    itemss.querySelector("input").parentElement.parentElement.classList.add("classadd");
                    module.data.push(nameofmodule);
                    var bottoms = []
                    if(nameofmodule.pid == 1) {
                        //niveau 1
                        moduleconfig_forms.push(nameofmodule);
                    } else {

                        moduleconfig_forms.forEach(function(obj) {
                            if(obj.id == nameofmodule.pid) {
                                if(! obj.child) {
                                    obj.child = [];
                                }
                                //add module
                                obj.child.push(nameofmodule);
                            } else if(obj.child) {
                                obj.child.forEach(function(objChild) {
                                    if(objChild.id  == nameofmodule.pid ) {
                                        if(! objChild.child) {
                                            objChild.child = [];
                                        }
                                        objChild.child.push(nameofmodule);
                                    } else if(objChild.child) {
                                        objChild.child.forEach(function(objChild2) {
                                            if(! objChild2.child) {
                                                objChild2.child = [];
                                            }
                                            objChild2.child.push(nameofmodule);
                                        });
                                    }
                                });
                            }
                        });
                    }
                    chart.addNode({
                        id: id,
                        pid: pid,
                        name: name
                    }, true);
                }
            }

            nodeElement.ondragover = function(ev) {
                ev.preventDefault();
            }
        }
    });
    if(moduleconfig_forms.length == 0) {
        chart.load([
            {id: 1, name: nameformation_recup},
        ]);
    }

    var items = document.querySelectorAll('.item');
    for(var i = 0; i < items.length; i++){
        var item = items[i];
        item.draggable = true;
        item.ondragstart = function(ev) {
            ev.dataTransfer.setData("id", ev.target.getAttribute('data-id'));
        }
    }
}

removeCurrentNode = function(node) {
    let currentId = node.getAttribute('data-remove-id');
    chart.removeNode(currentId);
    $('#editForm').addClass('d-none');
    moduleconfig_forms.forEach(function(obj,index) {
        console.log(obj.id)
        console.log(currentId)
        if (obj.id == currentId) {
            console.log('module')
            //if the selected elment is a parent delete all child
            chart.removeNode(obj.id);
            if(obj.child) {
                obj.child.forEach(function(objChild,index2) {
                    chart.removeNode(objChild.id);
                    if(objChild.child) {
                        objChild.child.forEach(function(objChild2,index3) {
                            chart.removeNode(objChild2.id);
                            if(objChild2.child) {
                                objChild2.child.forEach(function(objChild3, indexquiz) {
                                    chart.removeNode(objChild3.id);
                                });
                            }
                        });
                    }
                });
            }
            delete moduleconfig_forms[index];
            deletedElments.push(obj);
        } else if(obj.child) {
            obj.child.forEach(function(subChild,index2) {
                //child directemet sour module un cour ou un quiz
                if(subChild.id == currentId) {
                    chart.removeNode(subChild.id);
                    //delete child of this node
                    if(subChild.child) {
                        subChild.child.forEach(function(subChild2,index3) {
                            if(subChild2.pid == subChild.id ) {
                                chart.removeNode(subChild2.id);
                                if(subChild2.child) {
                                    subChild2.child.forEach(function(subChild3, indexquiz) {
                                        if(subChild3.pid == subChild2.id ) {
                                            console.log(subChild3.type)
                                            chart.removeNode(subChild3.id);
                                        }
                                    });
                                }
                            }
                        });
                    }
                    deletedElments.push(subChild);
                    delete subChild[index2];
                    delete obj.child[index2];
                } else if(subChild.child) {
                    //sous module sous cour chaptiotre ou quis
                    subChild.child.forEach(function(objChild2,index3) {
                        if(objChild2.id == currentId) {
                            chart.removeNode(objChild2.id);
                            if(objChild2.child) {
                                objChild2.child.forEach(function(objChild3, indexquiz) {
                                    chart.removeNode(objChild3.id);
                                });
                            }
                            deletedElments.push(objChild2);
                            delete objChild2[index3];
                            delete subChild.child[index3];
                        } else if(objChild2.child) {
                            objChild2.child.forEach(function(objChild3, indexquiz) {
                                if(objChild3.pid == currentId) {
                                    console.log('quiz')
                                    chart.removeNode(objChild3.id);
                                    deletedElments.push(objChild3);
                                    delete objChild3[indexquiz];
                                    delete objChild2.child[indexquiz];
                                }
                            });
                        }
                    });
                }
            });
        }
    });
    console.log('elment to delete')
    console.log(deletedElments);
    console.log(moduleconfig_forms)
}
window.onload = function() {
    if (document.getElementById("add-config")) {
        document.getElementById("add-config").onclick = function fun() {
            functioncharteformation();
        }
    }
}

$( document ).on( "click", ".delete, .delete1 ,.delete2", function() {


    $(this).parent('div').parent('div').remove();


});
$(document).on('click', '.edit, .edit1 , .edit2', function() {
    if($(this).closest('.blocimage').next('.item').hasClass('allowEdit')) {
        var inputElement = $(this).closest('.blocimage').next('.item').find('.org-input');
        var inputElementText = $(this).closest('.blocimage').next('.item').find('.org-input').val();
        var h6Element = $('<h6>'+inputElementText+'</h6>');
        inputElement.replaceWith(h6Element);
        $(this).closest('.blocimage').next('.item').removeClass('allowEdit');

        var nodeIdText = $(this).closest('.blocimage').next('.item').attr('id');
        var dataId    = $(this).closest('.blocimage').next('.item').attr('data-id').replace(/[0-9]/g, '');
        var nodeId = dataId+nodeIdText.match(/\d+/)[0]
        var node   = chart.get(nodeId);
        node.name  = inputElementText;
        chart.updateNode(node)
    } else {
        $(this).closest('.blocimage').next('.item').addClass('allowEdit');
        var h6ElementText = $(this).closest('.blocimage').next('.item').find('h6').text();
        var h6Element     = $(this).closest('.blocimage').next('.item').find('h6');
        var inputElement  = $('<input class="org-input" type="text" value="'+h6ElementText+'"/>');
        h6Element.replaceWith(inputElement);
    }
    //$(this).closest('.blocimage').next('.item').prop('contenteditable', true).focus();
});
var comptei=100;
var compteurtextclone=1;
$(document).on('click', '.cloner', function() {

    var $clone = $(this).closest('#collapseModule .blocgloab').clone();
    var getid = $(this).closest('#collapseModule .blocimage').next('.item').attr("id");
    var num = $(this).closest('#collapseModule .blocimage').next('.item').attr("data-id");

    getid=getid+comptei;
    num = num+comptei;

    $clone.insertAfter('#collapseModule .blocgloab:last');
    var textedit=$('#collapseModule .item:last').text();
    //var totaltext=textedit+' -'+" copy"+compteurtextclone;
    var totaltext= 'Copie '+textedit;
    // $('#collapseModule .item:last').empty();
    $('#collapseModule .item:last').attr('data-id', num);
    $('#collapseModule .item:last').attr('id', getid);
    $('#collapseModule .item:last').text(totaltext);
    $('#collapseModule .item:last').append('<input type="number" value="0">');
    comptei++;
    compteurtextclone++;
    functioncharteformation();
});
$(document).on('click', '.cloner1', function() {

    var $clone = $(this).closest('#list-cour .blocgloab').clone();
    var getid = $(this).closest('#list-cour .blocimage').next('.item').attr("id");
    var num = $(this).closest('#list-cour .blocimage').next('.item').attr("data-id");

    getid=getid+comptei;
    num = num+comptei;
    $clone.insertAfter('#list-cour .blocgloab:last');
    var textedit=$('#list-cour .item:last').text();
    var totaltext=textedit+' -'+" copy"+compteurtextclone;
    // $('#collapseModule .item:last').empty();
    $('#list-cour .item:last').attr('data-id', num);
    $('#list-cour .item:last').attr('id', getid);
    $('#list-cour .item:last').text(totaltext);
    $('#list-cour .item:last').append('<input type="number" value="0">');
    comptei++;
    compteurtextclone++;
    functioncharteformation();
});
$(document).on('click', '.cloner2', function() {

    var $clone = $(this).closest('#list-chap .blocgloab').clone();
    var getid = $(this).closest('#list-chap .blocimage').next('.item').attr("id");
    var num = $(this).closest('#list-chap .blocimage').next('.item').attr("data-id");

    getid=getid+comptei;
    num = num+comptei;

    $clone.insertAfter('#list-chap .blocgloab:last');
    var textedit=$('#list-chap .item:last').text();
    var totaltext=textedit+' -'+" copy"+compteurtextclone;
    // $('#collapseModule .item:last').empty();
    $('#list-chap .item:last').attr('data-id', num);
    $('#list-chap .item:last').attr('id', getid);
    $('#list-chap .item:last').text(totaltext);
    $('#list-chap .item:last').append('<input type="number" value="0">');
    comptei++;
    compteurtextclone++;
    functioncharteformation();
});
$(document).on('click', '.cloner3', function() {

    var $clone = $(this).closest('#list-quiz .blocgloab').clone();
    var getid = $(this).closest('#list-quiz .blocimage').next('.item').attr("id");
    var num = $(this).closest('#list-quiz .blocimage').next('.item').attr("data-id");

    getid=getid+comptei;
    num = num+comptei;
    $clone.insertAfter('#list-quiz .blocgloab:last');
    var textedit=$('#list-quiz .item:last').text();
    var totaltext=textedit+' -'+" copy"+compteurtextclone;
    // $('#collapseModule .item:last').empty();
    $('#list-quiz .item:last').attr('data-id', num);
    $('#list-quiz .item:last').attr('id', getid);
    $('#list-quiz .item:last').text(totaltext);
    $('#list-quiz .item:last').append('<input type="number" value="0">');
    comptei++;
    compteurtextclone++;
    functioncharteformation();
});

$(document).on('click', "#btnenvoyemodule",function(e){
    $( "div[id^='moduleconfigitemdat']" ).each(function(){
        var getidmodulefrompup=$("#valueofdat").val();
        var statut_score = 0;
        var access=0;
        var access_subscribe=0;
        var icone;
        var statut_prequis;
        $(this).find('input').each(function(){
            if($(this).attr('type') == 'checkbox'){
              $(this).prop('checked', false);
                if($(this).is(':checked')){
                    statut_score = 1;
                    access       = 1;
                    access_subscribe = 1;
                    statut_prequis = 1;
                }
            }
        });
        var score = $('#score').val();
        var access_input=$("#access_input").val();
        var point_preq=$("#point_preq").val();
        var icone_image=$("#icone_image").val();
        var icones= $("#icon").val();
        var choice_preq=$("#choice_preq").val();
        moduleconfig_forms.forEach(function(obj) {
            if(obj.id == getidmodulefrompup) {
                //update the component file
                var config = {};
                config.score            = score;
                config.statut_score     = statut_score;
                config.access           = access;
                config.access_input     = access_input;
                config.access_subscribe = access_subscribe;
                config.statut_prequis   = statut_prequis;
                config.point_req        = point_preq;
                config.icone_image      = icone_image;
                config.icones           = icones;
                config.choice_preq      = choice_preq;
                obj.config = config;
            }
        });
        $("#btnenvoyemodule").modal('hide');

    });
});


// Cours

$(document).on('click', "#btnenvoyemodulecours",function(e){

    $( "div[id^='courspopup']" ).each(function(){
        var getidmodulefrompup=$("#valueofdat").val();
        var score_cours = 0;
        var statut_access=0;
        var accesssubscribe=0;
        var icone;
        var statuts_prerequis=0;
        $(this).find('input').each(function(){
            if($(this).attr('type') == 'checkbox'){
                if($(this).is(':checked')){
                    score_cours = 1;
                    statut_access=1;
                    accesssubscribe=1;
                    statuts_prerequis=1;
                }
            }
        });
        var score_point=$("#score_point").val();
        var date_access=$("#date_access").val();
        var acces_days=$("#acces_days").val();
        var day_acces=$("#day_acces").val();
        var numberpoints_prerequis=$("#numberpoints_prerequis").val();
        var select_choiceprerequis=$("#select_choiceprerequis").val();
        moduleconfig_forms.forEach(function(obj) {
            var config = {};
            config.score_cours = score_cours;
            config.statut_access=	statut_access;
            config.score_point=score_point;
            config.date_access=date_access;
            config.day_acces=day_acces;
            config.statuts_prerequis=statuts_prerequis;
            config.accesssubscribe=accesssubscribe;
            config.numberpoint_prerequis=numberpoints_prerequis;
            config.select_choiceprerequis=select_choiceprerequis;

            if(obj.id == getidmodulefrompup) {
                //update the component file
                obj.config = config;
            } else if(obj.child) {
                obj.child.forEach(function(objChild) {
                    if(objChild.id = getidmodulefrompup) {
                        objChild.config = config;
                    }
                });
            }
        });
        $("#btnenvoyemodulecours").modal('hide');


    });
});
//chaiptre

$(document).on('click', "#btnenvoyemodulechaiptre",function(e){
    var getidmodulefrompup=$("#valueofdat").val();
    $( "div[id^='chaiptreconfig']" ).each(function(){
        var status_completed=0;
        var score_chaiptre=0;
        var statut_accesss=0;
        var staut_subscribe=0;
        var staut_prerequis=0;
        $(this).find('input').each(function(){
            if($(this).attr('type') == 'checkbox'){
                if($(this).is(':checked')){
                    status_completed=1;
                    score_chaiptre=1;
                    statut_accesss=1;
                    staut_subscribe=1;
                    staut_prerequis=1;
                }
            }
        });
        var picture=$("#picture").val();
        var time_spent=$("#time_spent").val();
        var score_point=$("#score_point").val();
        var status_dateacces=$("#status_dateacces").val();
        var staut_datesubscribe=$("#staut_datesubscribe").val();
        var choice_chaiptre= $("choice-chaiptre").val();
        moduleconfig_forms.forEach(function(obj) {
            var config = {};
            config.picture=picture;
            config.time_spent=time_spent;
            config.status_completed=status_completed;
            config.score_chaiptre=score_chaiptre;
            config.score_point=score_point;
            config.statut_accesss=statut_accesss;
            config.status_dateacces=status_dateacces;
            config.staut_subscribe=staut_subscribe;
            config.staut_datesubscribe=staut_datesubscribe;
            config.staut_prerequis=staut_prerequis;
            obj.choice_chaiptre=choice_chaiptre;
            if(obj.id == getidmodulefrompup) {
                //update the component file
                obj.config = config;
            } else if (obj.child) {
                obj.child.forEach(function(objChild) {
                    if(objChild.id == getidmodulefrompup) {
                        objChild.config = config;
                    } else if(objChild.child) {
                        objChild.child.forEach(function(objChildL2) {
                            if(objChildL2.id == getidmodulefrompup) {
                                objChildL2.config = config;
                            }
                        });
                    }
                });
            }
        });
        $("#btnenvoyemodulechaiptre").modal('hide');
    });
});

$(document).on('click',".monthly-btn",function (e) {
    e.preventDefault();

});


var iddevoirchekd=[];
$(document).on('click', '#getdevoirname', function() {
    iddevoirchekd=[]
    $(this).find('input').each(function() {
        if ($(this).attr('type') == 'checkbox') {
            if ($(this).is(':checked')) {
                var getfinalyname= $(this).attr("id");
                console.log(getfinalyname);
                iddevoirchekd.push(getfinalyname);
            }
        }
    });
    $('#getdevoirname').attr('data-ids', iddevoirchekd);

});
$(document).on('click', "#getdevoirname",function(e){

    let dataIds = $("#getdevoirname").attr("data-ids");
$("#choice-chaiptre").val(dataIds);

})
$(document).on('click', '.popupchapt', function() {
    var namechaiptres=$(this).closest('.blocimage').next('.item').attr("data-name");
    $("#pictureselect").val(namechaiptres);

});
var numberdevoir=1;
$('#save-file').click(function () {
    var namedevoir = $("#pictureselect").val();

    var result = namedevoir.split(' ').join('');
    var devoirattacher="devoir"+numberdevoir+''+result;

    $.ajax({
        type: 'POST',
        url: url_to_send_to,
        data: {
            'id': devoirattacher
        },
        success: function (data) {
            var devoirselect = $('.list-devoir');
            devoirselect.append('<div class="dropdown-list text-uppercase catalogue-list "><div class="checkbox custom-control custom-checkbox small"> <input type="checkbox" name="dropdown-group" class="check custom-control-input" id='+ devoirattacher +'> <label for='+ devoirattacher +' class="custom-control-label" data-id='+ devoirattacher +'>  '+ devoirattacher+' </label></div> </div>');

        },
    })
    numberdevoir++;
});

$(document).on('click', "#submitFormation",function(e){
    e.preventDefault();
    var element = $(this);
    // Envoie de json
    var Formationid=$("#refidfromation").val();
    console.log(moduleconfig_forms)
    if(typeof url_to_send_to !== "undefined") {
       $.ajax({
            type: "POST",
            url: url_to_send_to,
            data: { moduleconfig_forms: JSON.stringify(moduleconfig_forms), toDelete: JSON.stringify(deletedElments)},
            success: function (response) {
              window.location.href = "/fr/formation/step5carnetdenote/"+parseInt(Formationid);
            },
        })
    }
});

var formationId = document.getElementById('formationId').value;
$.ajax({
    type: "GET",
    url:  window.location.origin+'/getJsonFormation',
    data: { formationId: formationId},
    success: function (response) {
        moduleconfig_forms = response;
        var listNodes = [];
        //modules
        var FirstParent = {};
        FirstParent.id = 1;
        FirstParent.name = document.getElementById('getnameformation').value;
        listNodes.push(FirstParent);
        response.forEach(function(obj) {
            let mytab = obj.id.split('-');
            let counterId = parseInt(mytab[mytab.length - 1]);

            if(obj.type == "module") {
                var olValue =  document.getElementById('lastmoduleId').value
                if(olValue) {
                    let newVal = olValue.split('-');
                    let oldval = parseInt(newVal[newVal.length - 1]);
                    if(counterId > oldval) {
                        document.getElementById('lastmoduleId').value= obj.id;
                    }
                } else {
                    document.getElementById('lastmoduleId').value= obj.id;
                }
            }

            if(obj.type == "Cours") {
                var olValue =  document.getElementById('lastcourId').value
                if(olValue) {
                    let newVal = olValue.split('-');
                    let oldval = parseInt(newVal[newVal.length - 1]);
                    if(counterId > oldval) {
                        document.getElementById('lastcourId').value= obj.id;
                    }
                } else {
                    document.getElementById('lastcourId').value= obj.id;
                }
            }
            if(obj.type == "Chap") {
                var olValue =  document.getElementById('lastchapterId').value
                if(olValue) {
                    let newVal = olValue.split('-');
                    let oldval = parseInt(newVal[newVal.length - 1]);
                    if(counterId > oldval) {
                        document.getElementById('lastchapterId').value= obj.id;
                    }
                } else {
                    document.getElementById('lastchapterId').value= obj.id;
                }
            }
            if(obj.type == "quiz") {
                var olValue =  document.getElementById(lastQizId).value
                if(olValue) {
                    let newVal = olValue.split('-');
                    let oldval = parseInt(newVal[newVal.length - 1]);
                    if(counterId > oldval) {
                        document.getElementById('lastQizId').value= obj.id;
                    }
                } else {
                    document.getElementById('lastQizId').value= obj.id;
                }
            }

            if(obj.child) {
                //cours
                obj.child.forEach(function(objChild, k) {
                    //chapters

                    let mytab = objChild.id.split('-');
                    let counterId = parseInt(mytab[mytab.length - 1]);
                    if(objChild.type == "Cours") {
                        //get the max value
                        var olValue =  document.getElementById('lastcourId').value
                        if(olValue) {
                            let newVal = olValue.split('-');
                            let oldval = parseInt(newVal[newVal.length - 1]);
                            if(counterId > oldval) {
                                document.getElementById('lastcourId').value= objChild.id;
                            }
                        } else {
                            document.getElementById('lastcourId').value= objChild.id;
                        }
                    }
                    if(objChild.type == "Chap") {
                        var olValue =  document.getElementById('lastchapterId').value

                        if(olValue) {
                            let newVal = olValue.split('-');
                            let oldval = parseInt(newVal[mytab.length - 1]);
                            if(counterId > oldval) {
                                document.getElementById('lastchapterId').value= objChild.id;
                            }
                        } else {
                            document.getElementById('lastchapterId').value= objChild.id;
                        }
                    }
                    if(objChild.type == "quiz") {
                        var olValue =  document.getElementById('lastQizId').value
                        if(olValue) {
                            let newVal = olValue.split('-');
                            let oldval = parseInt(newVal[newVal.length - 1]);
                            if(counterId > oldval) {
                                document.getElementById('lastQizId').value = objChild.id;
                            }
                        } else {
                            document.getElementById('lastQizId').value = objChild.id;
                        }
                    }

                    if(objChild.child) {
                        objChild.child.forEach(function(objChildL2,i) {
                            listNodes.push(objChildL2);
                            let mytab = objChildL2.id.split('-');
                            let counterId = parseInt(mytab[mytab.length - 1]);
                            if(objChildL2.type == "Chap") {
                                var olValue =  document.getElementById('lastchapterId').value
                                if(olValue) {
                                    let newVal = olValue.split('-');
                                    let oldval = parseInt(newVal[newVal.length - 1]);
                                    if(counterId > oldval) {
                                        document.getElementById('lastchapterId').value= objChildL2.id;
                                    }
                                } else {
                                    document.getElementById('lastchapterId').value= objChildL2.id;
                                }

                                if(objChildL2.child) {
                                    objChildL2.child.forEach(function(objChildL3,j) {
                                        if(objChildL3.pid == objChildL2.id) {
                                            listNodes.push(objChildL3);
                                        }
                                    });
                                   // delete objChildL2.child;
                                }
                            }
                            if(objChildL2.type == "quiz") {
                                //code to optimised
                                var olValue =  document.getElementById('lastQizId').value
                                if(olValue) {
                                    let newVal = olValue.split('-');
                                    let oldval = parseInt(newVal[newVal.length - 1]);
                                    if(counterId > oldval) {
                                        document.getElementById('lastQizId').value= objChildL2.id;
                                    }
                                } else {
                                    document.getElementById('lastQizId').value= objChildL2.id;
                                }
                            }
                        });
                        //delete objChild.child;
                    }
                    listNodes.push(objChild);
                });
               //delete obj.child;
                listNodes.push(obj);
            } else {
                listNodes.push(obj);
            }
        });
      //remove duplicated node
        var clean = listNodes.filter((listNodes, index, self) =>
            index === self.findIndex((t) => (t.id === listNodes.id)))
        functioncharteformation();
        chart.load(clean);
        console.log(moduleconfig_forms);
    },
});