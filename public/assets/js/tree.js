/*var uploadImage = document.getElementById('list-module');
if($('#list-module').length >0) {
    new Sortable(uploadImage, {
        group: {
            name: 'shared',
            pull: 'clone'
        },
        animation: 150,
        onEnd: function (subevt) {

        }
    });
}
*/
var datascource = {
    'name': 'Lao Lao',
    'children': [
        { 'name': 'Bo Miao', 'id': '1',
            'children': [{ 'name': 'Li Xin', 'id': '2' }]
        },
        { 'name': 'Su Miao', 'id': '3',
            'children': [
                { 'name': 'Tie Hua', 'id': '4' },
                { 'name': 'Hei Hei', 'id': '5',
                    'children': [
                        { 'name': 'Pang Pang', 'id': '6' },
                        { 'name': 'Dan Dan', 'id': '7' },
                        { 'name': 'Dan Dan dd', 'id': '8' }
                    ]
                }
            ]
        },
        { 'name': 'Hong Miao', 'id': '8' }
    ]
};

var oc = $('#chart-container').orgchart({
    'data' : datascource,
    'draggable': true,
    'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
        if($draggedNode.find('.content').text().indexOf('manager') > -1 && $dropZone.find('.content').text().indexOf('engineer') > -1) {
            return false;
        }
        return true;
    }
});

oc.$chart.on('nodedrop.orgchart', function(event, extraParams) {
    console.log('draggedNode:' + extraParams.draggedNode.children('.title').text()
        + ', dragZone:' + extraParams.dragZone.children('.title').text()
        + ', dropZone:' + extraParams.dropZone.children('.title').text()
    );
});

//*** module***//
var datascourceModule = {
    'name': 'Lao Lao',
    'children': [
        { 'name': 'module 1', 'id': 'mod-1'},
        { 'name': 'module 2', 'id': 'mod-8' },
        { 'name': 'module 3', 'id': 'mod-9' }
    ]
};

var module = $('#list-module').orgchart({
    'data' : datascourceModule,
    'draggable': true,
    'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
        if($draggedNode.find('.content').text().indexOf('manager') > -1 && $dropZone.find('.content').text().indexOf('engineer') > -1) {
            return false;
        }
        return true;
    }
});
module.$chart.on('nodedrop.orgchart', function(event, extraParams) {
    console.log('draggedNode:' + extraParams.draggedNode.children('.title').text()

        + ', dragZone:' + extraParams.dragZone.children('.title').text()
        + ', dropZone:' + extraParams.dropZone.children('.title').text()
    );
});