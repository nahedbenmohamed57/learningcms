var tab;
$.ajax({
    type: "GET",
    url: '/fr/video/getFormations',
    success: function (response) {
        tab = response;

        console.log(tab);
    }
});

function searchFormations(key, type) {
    var valF = $('#formations').attr('data-value', key);
    var settings = {
        /**
         *Mandatory
         *Text field ID where you want to implement autocomplete.
         */
        inputId: type,
        /**
         *optional
         *No. of length when keyup event fire.
         */
        inputLength: 1,

        /**
         *optional
         *Json Field name which you want to put in text field when selected.
         */
        inputValue: 'titre_formation',

        /**
         *optional
         *Fields name from json data which you want to show in table rest fields will hide.
         */
        fields: 'titre_formation',

        /**
         *optional
         *If you have json data in your page no need to pass ajax url.
         *just pass associative json in data
         */
        data:  tab
    }

    /**
     *1st Param : settings
     *2nd Param : onSelectCallback, it is fired when select on autosuggestion with selected data
     */
    AjaxLiveSearch.init(settings, function (data) {
        console.log(data);
        var res1 = valF;
        console.log(res1);
        var res2 = data.titre_formation;
        console.log(res2);
            if(res2.match(res1)){
                $('.my-result-'+type).append('<span class="col-12 forms" id="forms" data-id='+data.id+'>'+data.titre_formation+'<span onclick="myDel();" class="pl-2" id='+data.id+'><i class="fas fa-times"></i></span></span>');
                $('.search-live').val('');
            } else {

            }
    });
}
let tabMembers;
$.ajax({
    type: "GET",
    url: '/fr/video/getUsers',
    success: function (response) {
        tabMembers = response;
        console.log(tabMembers);
    }
});
function searchStudents(key, type) {
    var settings = {
        /**
         *Mandatory
         *Text field ID where you want to implement autocomplete.
         */
        inputId: type,
        /**
         *optional
         *No. of length when keyup event fire.
         */
        inputLength: 1,

        /**
         *optional
         *Json Field name which you want to put in text field when selected.
         */
        inputValue: 'username',

        /**
         *optional
         *Fields name from json data which you want to show in table rest fields will hide.
         */
        fields: 'username',

        /**
         *optional
         *If you have json data in your page no need to pass ajax url.
         *just pass associative json in data
         */
        data:  tabMembers
    }

    /**
     *1st Param : settings
     *2nd Param : onSelectCallback, it is fired when select on autosuggestion with selected data
     */
    AjaxLiveSearch.init(settings, function (data) {
        $('.my-result-'+type).append('<span class="col-12 members" id="members" data-mail='+data.email+'>'+data.username+'<span onclick="myDelM();" class="pl-2" id='+data.id+'><i class="fas fa-times"></i></span></span>');

        $('.search-live').val('');

    });
}
let tabClass;
$.ajax({
    type: "GET",
    url: '/fr/video/getClasses',
    success: function (response) {
        tabClass = response;
        console.log(tabClass);
    }
});
function searchClasses(key, type) {
    var settings = {
        /**
         *Mandatory
         *Text field ID where you want to implement autocomplete.
         */
        inputId: type,
        /**
         *optional
         *No. of length when keyup event fire.
         */
        inputLength: 1,

        /**
         *optional
         *Json Field name which you want to put in text field when selected.
         */
        inputValue: 'name',

        /**
         *optional
         *Fields name from json data which you want to show in table rest fields will hide.
         */
        fields: 'name',

        /**
         *optional
         *If you have json data in your page no need to pass ajax url.
         *just pass associative json in data
         */
        data:  tabClass
    }

    /**
     *1st Param : settings
     *2nd Param : onSelectCallback, it is fired when select on autosuggestion with selected data
     */
    AjaxLiveSearch.init(settings, function (data) {
        $('.my-result-'+type).append('<span class="col-12 classname" id="classname" data-id='+data.id+'>'+data.name+'<span onclick="myDelC();" class="pl-2" id='+data.id+'><i class="fas fa-times"></i></span></span>');

        $('.search-live').val('');
    });
}
let tabType;
$.ajax({
    type: "GET",
    url: '/fr/video/getUsertype',
    success: function (response) {
        tabType = response;
        console.log(tabType);
    }
});
function searchType(key, type) {
    var settings = {
        /**
         *Mandatory
         *Text field ID where you want to implement autocomplete.
         */
        inputId: type,
        /**
         *optional
         *No. of length when keyup event fire.
         */
        inputLength: 1,

        /**
         *optional
         *Json Field name which you want to put in text field when selected.
         */
        inputValue: 'type',

        /**
         *optional
         *Fields name from json data which you want to show in table rest fields will hide.
         */
        fields: 'type',

        /**
         *optional
         *If you have json data in your page no need to pass ajax url.
         *just pass associative json in data
         */
        data:  tabType
    }

    /**
     *1st Param : settings
     *2nd Param : onSelectCallback, it is fired when select on autosuggestion with selected data
     */
    AjaxLiveSearch.init(settings, function (data) {
        $('.my-result-'+type).append('<span class="col-12 type" id="type" data-id='+data.id+'>'+data.type+'<span onclick="myDelT();" class="pl-2" id='+data.id+'><i class="fas fa-times"></i></span></span>');

        $('.search-live').val('');
    });
}
var forms = [];
var members = [];
var classes = [];
var users = [];
$('#livevid').submit(function(e) {

    var form = $(this);

    var tabUs = form.serializeArray();

    console.log(tabUs);
    e.preventDefault();
    $.each($("#liveFormations span[id='forms']"), function () {
        forms.push($(this).attr('data-id'));
    });
    $.each($("#liveMember span[id='members']"), function () {
        members.push($(this).attr('data-mail'));
    });
    $.each($("#liveClass span[id='classname']"), function () {
        classes.push($(this).attr('data-id'));
    });
    $.each($("#liveType span[id='type']"), function () {
        users.push($(this).attr('data-id'));
    });

    $.ajax({
        type: "POST",
        url: '/SaveMail',
        data : { formations : forms, members : members, classes: classes, types : users, form : tabUs},
        success: function(response){
            location.reload();
        },
    });
});
function myDel() {
    $("#liveFormations .forms").click(function () {
        $(this).remove();
    });
}
function myDelM(){
    $("#liveMember .members").click(function () {
        $(this).remove();
    });
}
function myDelC(){
    $("#liveClass .classname").click(function () {
        $(this).remove();
    });
}
function myDelT(){
    $("#liveType .type").click(function () {
        $(this).remove();
    });
}
$('#edit-live').click(function(e) {

    var form = $(this);

    var tabUs = form.serializeArray();

    console.log(tabUs);
    e.preventDefault();
    $.each($("#liveFormations span[id='forms']"), function () {
        forms.push($(this).attr('data-id'));
    });
    $.each($("#liveMember span[id='members']"), function () {
        members.push($(this).attr('data-mail'));
    });
    $.each($("#liveClass span[id='classname']"), function () {
        classes.push($(this).attr('data-id'));
    });
    $.each($("#liveType span[id='type']"), function () {
        users.push($(this).attr('data-id'));
    });

    $.ajax({
        type: "POST",
        url: '/SaveMail',
        data : { formations : forms, members : members, classes: classes, types : users, form : tabUs},
        success: function(response){
            location.reload();
        },
    });
});

//Chat
var socket = new WebSocket("ws://127.0.0.1:8080");

socket.onopen = function () {

};

socket.onclose = function (e) {
    if (e.wasClean) {
        console.log('Connection closed.');
    } else {
        console.log('Connection killed:(');
    }
    console.log(e.code + e.reason);
};

socket.onmessage = function (e) {
    console.log(e);
  /*  var list = document.getElementById('list');
    var message = document.createElement("div");
    var node = document.createTextNode(e.data);
    message.appendChild(node);
    message.classList.add('card');
    list.appendChild(message);*/
    var attrimage= $(".rounded-circle-comment").attr("src");
    var attrname=$("#infoUser").val();
    var dt = new Date();
    var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    var attrrcomment="à ajouté un commentaire ";
    var idVideo = $('#vidId').val();
    var message = $('#message-box').val();
    $.ajax({
        url: window.location.origin + '/student/newComment/' + parseInt(idVideo), // on donne l'URL du fichier de traitement
        type: "POST", // la requête est de type POST
        data: {message: message},
        success: function (response) {
            console.log(response);
            var image = "/images/users/" + response.image;
            console.log(response.name)
            $(".list-unstyled").append("<li  class='media pt-4'><img class='img-profile rounded-circle-comment mr-3' src="+image+"><div class='oooo'><h5><b>"+response.name+"</b> <small class='text-muted'>"+attrrcomment+"<small>"+time+"</small></small></h5><p>"+ e.data +"</p></div></li>");
        }
    });
};

socket.onerror = function (error) {
    console.log(error.message);
};

var button = document.getElementById('send');
var textarea = document.getElementById('message-box');

function sendText() {
    var text = textarea.value;
    var idVideo = $('#vidId').val();
    var message = $('#message-box').val();
    if (text.length > 0) {
        socket.send(JSON.stringify(text));
        textarea.value = '';
        console.log('test');
       /* $.ajax({
            url :  window.location.origin+ '/student/newComment/'+parseInt(idVideo), // on donne l'URL du fichier de traitement
            type : "POST", // la requête est de type POST
            data: { message: message},
            success: function(response) {
                console.log(response);
                $(".list-unstyled li:last-child img").attr('src','');
            }
        });*/
        return true;
    }

    return false;
}

button.onclick = sendText;

textarea.onkeypress = function (ev) {
    if (ev.charCode === 13) {
        sendText();
    }
};
//
